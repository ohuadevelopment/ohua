/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package org.json;

import java.io.Serializable;

public interface JSONData<T> extends Cloneable, Serializable
{
  public T deepCopy();
}
