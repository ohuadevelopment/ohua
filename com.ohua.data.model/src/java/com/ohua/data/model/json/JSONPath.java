/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.data.model.json;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ohua.engine.exceptions.Assertion;

public class JSONPath
{
  class PathReference
  {
    JSONObject _item = null;
    JSONArray _array = null;
  }
  
  private static final Pattern _vectorPattern = Pattern.compile("\\[(\\d)\\]$");
  
  private LinkedList<PathReference> _parents = new LinkedList<PathReference>();
  
  private String _path = null;
  private String _leaf = null;
  private String[] _pathElements = null;
  
  public JSONPath(String path)
  {
    _path = path;
    _path = preparePath(_path);
  }
  
  private static String preparePath(String path)
  {
    String preparedPath = path;
    if(path.startsWith("/"))
    {
      preparedPath = path.substring(1);
    }
    
    if(path.endsWith("/"))
    {
      preparedPath = path.substring(0, path.length());
    }
    
    return preparedPath;
  }
  
  public void resolvePath(JSONObject resolutionRoot)
  {
    _pathElements = _path.split("/");
    _leaf = _pathElements[_pathElements.length - 1];
    
    PathReference ref = new PathReference();
    ref._item = resolutionRoot;
    _parents.add(ref);
    
    traverse(resolutionRoot, 0);
  }
  
  private void traverse(JSONObject current, int currentIndex)
  {
    // stopping condition
    if(_pathElements.length <= currentIndex + 1)
    {
      // the leaf is not a JSONObject or JSONArray anymore so we do not want to traverse it
      return;
    }
    
    String pathElement = _pathElements[currentIndex];
    int vectorIndex = pathElement.indexOf("[");
    String key = pathElement;
    if(vectorIndex > 0)
    {
      key = pathElement.substring(0, vectorIndex);
    }
    
    JSONArray array = current.optJSONArray(key);
    if(array != null)
    {
      traverseArray(currentIndex, pathElement, array);
    }
    else
    {
      traverseItem(current, currentIndex, pathElement);
    }
  }
  
  private void traverseItem(JSONObject current, int currentIndex, String pathElement)
  {
    PathReference ref = new PathReference();
    
    try
    {
      ref._item = current.getJSONObject(pathElement);
    }
    catch(JSONException e)
    {
      System.out.println("Children are: ");
      Iterator<String> it = current.sortedKeys();
      while(it.hasNext())
      {
        System.out.println(it.next());
      }
      
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    _parents.add(ref);
    traverse(ref._item, ++currentIndex);
  }
  
  private void traverseArray(int currentIndex, String pathElement, JSONArray array)
  {
    Matcher m = _vectorPattern.matcher(pathElement);
    if(m.find())
    {
      Assertion.invariant(m.groupCount() == 1);
      String match = m.group(1);
      Integer index = Integer.valueOf(match);
      PathReference ref = new PathReference();
      try
      {
        ref._array = array;
        ref._item = array.getJSONObject(index);
        _parents.add(ref);
        
        traverse(ref._item, ++currentIndex);
      }
      catch(JSONException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    else
    {
      // traverse all children
      for(int index = 0; index < array.length(); index++)
      {
        PathReference ref = new PathReference();
        try
        {
          ref._array = array;
          ref._item = array.getJSONObject(index);
          // FIXME seems like it can not be just a list to capture the paths!
          _parents.add(ref);
          
          traverse(ref._item, ++currentIndex);
        }
        catch(JSONException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
          throw new RuntimeException(e);
        }
      }
    }
  }
  
  public String getPath()
  {
    return _path;
  }
  
  public LinkedList<PathReference> getParents()
  {
    return _parents;
  }
  
  public Object getLeaf()
  {
    int vectorIndex = _leaf.indexOf("[");
    String key = _leaf;
    if(vectorIndex > 0)
    {
      key = _leaf.substring(0, vectorIndex);
    }
    
    JSONObject current = _parents.getLast()._item;
    JSONArray array = current.optJSONArray(key);
    if(array != null)
    {
      Matcher m = _vectorPattern.matcher(_leaf);
      if(m.find())
      {
        Assertion.invariant(m.groupCount() == 1);
        String match = m.group(1);
        Integer index = Integer.valueOf(match);
        try
        {
          return array.get(index);
        }
        catch(JSONException e)
        {
          // TODO Auto-generated catch block
          e.printStackTrace();
          throw new RuntimeException(e);
        }
      }
      else
      {
        return array;
      }
    }
    else
    {
      try
      {
        return _parents.getLast()._item.get(_leaf);
      }
      catch(JSONException e)
      {
        System.err.println("Item: " + _parents.getLast()._item.toString());
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    
  }
  
  public String getLeafName()
  {
    return _leaf;
  }
  
  @Override
  public String toString()
  {
    return _path;
  }
  
  public static Set<String> getLeafs(JSONObject resolutionRoot)
  {
    Set<String> leafs = new HashSet<String>();
    
    Iterator<String> children = resolutionRoot.keys();
    while(children.hasNext())
    {
      String childKey = children.next();
      Object child = getChildNoThrow(resolutionRoot, childKey);
      Set<String> branchLeafs = getLeafs("/" + childKey, child);
      leafs.addAll(branchLeafs);
    }
    
    return leafs;
  }
  
  public static boolean elementExists(JSONObject resolutionRoot, String path)
  {
    String preparedPath = preparePath(path);
    String[] pathElements = preparedPath.split("/");
    
    for(int i = 0; i < pathElements.length; i++)
    {
      Object child = resolutionRoot.opt(pathElements[i]);
      if(child == null)
      {
        return false;
      }
      
      if(!(child instanceof JSONObject))
      {
        if(i < pathElements.length - 1)
        {
          return false;
        }
      }
      else
      {
        resolutionRoot = (JSONObject) child;
      }
    }
    
    return true;
  }
  
  private static Object getChildNoThrow(JSONObject resolutionRoot, String child)
  {
    try
    {
      return resolutionRoot.get(child);
    }
    catch(JSONException e)
    {
      e.printStackTrace();
      Assertion.invariant(false);
    }
    return null;
  }
  
  public static Set<String> getLeafs(String path, Object resolutionRoot)
  {
    // FIXME array support missing
    if(!(resolutionRoot instanceof JSONObject))
    {
      return Collections.singleton(path);
    }
    else
    {
      if(((JSONObject) resolutionRoot).length() < 1)
      {
        return Collections.singleton(path);
      }
      else
      {
        Set<String> leafs = new HashSet<String>();
        Iterator<String> children = ((JSONObject) resolutionRoot).keys();
        while(children.hasNext())
        {
          String childKey = children.next();
          Object child = getChildNoThrow((JSONObject) resolutionRoot, childKey);
          Set<String> branchLeafs = getLeafs(path + "/" + childKey, child);
          leafs.addAll(branchLeafs);
        }
        return leafs;
      }
    }
  }
  
  public void createIntermediateNodes(JSONObject resolutionRoot)
  {
    String[] pathElements = _path.split("/");
    
    // find the last resolvable element
    int i = 0;
    JSONObject current = resolutionRoot;
    for(; i < pathElements.length; i++)
    {
      Object child = current.opt(pathElements[i]);
      if(child == null)
      {
        break;
      }
      else
      {        
        resolutionRoot = (JSONObject) child;
      }
    }
    
    // append new (intermediate) children
    while(i<pathElements.length-1)
    {
      JSONObject a = new JSONObject();
      try
      {
        current.put(pathElements[i++], a);
      }
      catch(JSONException e)
      {
        Assertion.impossible(e);
      }
      current = a;
    }
    
  }
  
}
