/*
 * Copyright � Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.data.model.json;

import java.io.File;
import java.io.FileReader;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;
import org.json.XMLTokener;

import com.ohua.engine.data.model.daapi.OhuaDataAccessor;
import com.ohua.engine.exceptions.Assertion;

public class JSONDataAccessor implements OhuaDataAccessor
{
  // FIXME missing:
  // private JSONObject _root = null;
  private JSONObject _resolutionRoot = null;
  
  public void setResolutionRoot(String pathToResolutionRoot)
  {
    JSONPath jPath = new JSONPath(pathToResolutionRoot);
    jPath.resolvePath(_resolutionRoot);
    
    _resolutionRoot = (JSONObject) jPath.getLeaf();
  }
  
  public void setDataRoot(Object resolutionRoot)
  {
    Assertion.invariant(resolutionRoot instanceof JSONObject);
    _resolutionRoot = (JSONObject) resolutionRoot;
  }
  
  public Object getDataRoot()
  {
    return _resolutionRoot;
  }
  
  public List<Object> getData(String path)
  {
    Assertion.invariant(_resolutionRoot != null);
    
    JSONPath jPath = new JSONPath(path);
    jPath.resolvePath(_resolutionRoot);
    
    // FIXME needs support for multiple leafs
    return Collections.singletonList(jPath.getLeaf());
  }
  
  public void setData(String path, Object value)
  {
    JSONPath jPath = new JSONPath(path);
    
    // if it is not existing then create it
    if(!JSONPath.elementExists(_resolutionRoot, path))
    {
      jPath.createIntermediateNodes(_resolutionRoot);
    }
    
    jPath.resolvePath(_resolutionRoot);
    
    JSONObject leafParent = jPath.getParents().getLast()._item;
    if(value == null)
    {
      value = JSONObject.NULL;
    }
    try
    {
      leafParent.put(jPath.getLeafName(), value);
    }
    catch(JSONException e)
    {
//      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  public void load(File file)
  {
    String fileName = file.getName();
    int indexOf = fileName.indexOf(".");
    if(indexOf > 0)
    {
      fileName = fileName.substring(0, indexOf);
    }
    
    // we prefer .json files ...
    File jsonFile = new File(file.getParent() + "/" + fileName + ".json");
    if(jsonFile.exists())
    {
      try
      {
        _resolutionRoot = new JSONObject(new JSONTokener(new FileReader(jsonFile)));
      }
      catch(Exception e)
      {
//        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    else
    {
      // ... but can also deal easily with XML files!
      jsonFile = new File(file.getParent() + "/" + fileName + ".xml");
      if(jsonFile.exists())
      {
        try
        {
          _resolutionRoot = new JSONObject(new XMLTokener(new FileReader(jsonFile)));
        }
        catch(Exception e)
        {
//          e.printStackTrace();
          throw new RuntimeException(e);
        }
      }
      else
      {
        throw new RuntimeException("no input file for data format found.");
      }
    }
  }
  
  public void parse(String data, String format)
  {
    JSONTokener tokener = null;
    
    if(format.equals("JSON"))
    {
      tokener = new JSONTokener(data);
    }
    else if(format.equals("XML"))
    {
      tokener = new XMLTokener(data);
    }
    else
    {
      throw new RuntimeException("Unsupported conversion from '" + format + "' to JSON.");
    }
    
    try
    {
      _resolutionRoot = new JSONObject(tokener);
    }
    catch(JSONException e)
    {
//      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  public Set<String> getLeafs()
  {
    return JSONPath.getLeafs(_resolutionRoot);
  }
  
  public String dataToString(String format)
  {
    if(format.equalsIgnoreCase("JSON"))
    {
      try
      {
        return _resolutionRoot.toString(4);
      }
      catch(JSONException e)
      {
//        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    else if(format.equalsIgnoreCase("XML"))
    {
      try
      {
        return XML.toString(_resolutionRoot);
      }
      catch(JSONException e)
      {
//        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    else
    {
      throw new UnsupportedOperationException("Data format serialization from JSON to '"
                                              + format + "' not supported.");
    }
  }
  
  public boolean elementExists(String string)
  {
    return false;
  }
  
}
