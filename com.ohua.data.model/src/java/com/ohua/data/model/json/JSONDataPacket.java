/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.data.model.json;

import java.io.Serializable;

import org.json.JSONObject;

import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.InputPortEvents;

public class JSONDataPacket implements DataPacket, Serializable
{
  private JSONObject _root = new JSONObject();
  
  public JSONObject getData()
  {
    return _root;
  }

  public DataPacket deepCopy()
  {
    JSONDataPacket clone = null;
    try
    {
      clone = (JSONDataPacket) clone();
    }
    catch(CloneNotSupportedException e)
    {
      Assertion.impossible(e);
    }
    clone._root = _root.deepCopy();
    
    return clone;
  }

  public void deserialize(String data)
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  public String serialize()
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }

  public void setData(Object dataRoot)
  {
    Assertion.invariant(dataRoot instanceof JSONObject);
    _root = (JSONObject) dataRoot;
  }

  @Override
  public String toString()
  {
    if(_root != null)
    {
      return _root.toString();
    }
    else
    {
      return super.toString();
    }
  }

  @Override public InputPortEvents getEventType() {
    return InputPortEvents.DATA_PACKET_ARRIVAL;
  }
}

