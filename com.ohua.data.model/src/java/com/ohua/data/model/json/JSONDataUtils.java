/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.data.model.json;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.json.JSONObject;

import com.ohua.engine.data.model.daapi.DataUtils;

public class JSONDataUtils implements DataUtils
{
  
  public boolean equal(Object o1, Object o2)
  {
    if(o1 == null)
    {
      return o2 == null;
    }
    else if(o2 == null)
    {
      return o1 == null;
    }
    else
    {
      return o1.toString().equals(o2.toString());
    }
  }
  
  @SuppressWarnings("unchecked")
  public int compare(Object o1, Object o2)
  {
    if(o1 instanceof String)
    {
      o1 = JSONObject.stringToValue((String) o1);
    }
    if(o2 instanceof String)
    {
      o2 = JSONObject.stringToValue((String) o2);
    }

    if(o1 instanceof Number || o2 instanceof Number)
    {
      o1 = new BigDecimal(o1.toString());
      o2 = new BigDecimal(o2.toString());
    }

    if(o1 instanceof Comparable<?>)
    {
      return ((Comparable) o1).compareTo(o2);
    }
    else
    {
      throw new UnsupportedOperationException("Missing comparison capabilities. Can't compare "
                                              + o1.getClass() + " with " + o2.getClass());
    }
  }
  
  public Object add(Object before, Object delta)
  {
    if(delta == null || delta.toString().equals(""))
    {
      throw new IllegalArgumentException("'delta' must not be null or empty string.");
    }
    
    if(!(delta instanceof Number) && !(delta instanceof String))
    {
      throw new IllegalArgumentException("'delta' must be either of type String or Number.");
    }

    if(before == null)
    {
      return delta;
    }
    else
    {
      return addOperation(before, delta);
    }
  }
  
  private String addOperation(Object addend1, Object addend2)
  {
    BigDecimal beforeDec = new BigDecimal(addend1.toString());
    BigDecimal deltaDec = new BigDecimal(addend2.toString());
    return beforeDec.add(deltaDec).toString();
  }

  public Object divide(Object dividend, Object devisor)
  {
    BigDecimal beforeDec = new BigDecimal(dividend.toString());
    BigDecimal deltaDec = new BigDecimal(devisor.toString());
    
    return beforeDec.divide(deltaDec,
                            Math.max(beforeDec.scale(), deltaDec.scale()),
                            RoundingMode.HALF_UP).stripTrailingZeros().toString();
  }
  
  public Object multiply(Object multiplier, Object multiplicant)
  {
    BigDecimal beforeDec = new BigDecimal(multiplier.toString());
    BigDecimal deltaDec = new BigDecimal(multiplicant.toString());
    return beforeDec.multiply(deltaDec).toString();
  }
  
  public Object subtract(Object minuend, Object subtrahend)
  {
    BigDecimal beforeDec = new BigDecimal(minuend.toString());
    BigDecimal deltaDec = new BigDecimal(subtrahend.toString());
    return beforeDec.subtract(deltaDec).toString();
  }

}
