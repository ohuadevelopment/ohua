/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.data.model.map;

import com.ohua.engine.data.model.daapi.DataFormat;
import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.data.model.daapi.DataUtils;
import com.ohua.engine.data.model.daapi.OhuaDataAccessor;

public class MapDataFormat implements DataFormat
{
  @Override
  public DataPacket createDataPacket()
  {
    return new MapDataPacket();
  }
  
  @Override
  public OhuaDataAccessor createDataAccessor()
  {
    return new MapDataAccessor();
  }
  
  @Override
  public DataUtils getComparisonUtils()
  {
    return new MapDataUtils();
  }
}
