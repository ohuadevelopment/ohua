(defproject com.ohua.benchmarks "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]]
  :source-paths ["src" "src/clojure"]
  :java-source-paths ["src" "src/java"]
  :resource-paths ["../ohua-build/jars/ohua_0.1.jar"])
