/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.benchmarks;

import java.util.List;

import com.ohua.lang.defsfn;

/**
 * Code taken straight from:
 * http://introcs.cs.princeton.edu/java/22library/BlackScholes.java.html
 * http://introcs.cs.princeton.edu/java/22library/Gaussian.java.html
 * 
 * @author sertel
 * 
 */
public abstract class BlackScholes
{
  // return phi(x) = standard Gaussian pdf
  public static double phi(double x) {
      return Math.exp(-x*x / 2) / Math.sqrt(2 * Math.PI);
  }

  // return Phi(z) = standard Gaussian cdf using Taylor approximation
  public static double Phi(double z) {
      if (z < -8.0) return 0.0;
      if (z >  8.0) return 1.0;
      double sum = 0.0, term = z;
      for (int i = 3; sum + term != sum; i += 2) {
          sum  = sum + term;
          term = term * z * z / i;
      }
      return 0.5 + sum * phi(z);
  }


  public static class Input
  {
    @defsfn
    public Object[] generate(List<Double[]> inputData) {
      return inputData.isEmpty() ? null : inputData.remove(0);
    }
  }
  
  public static class First
  {
    @defsfn
    public Object[] calculateFirst(double S, double X, double r, double sigma, double T) {
      double d1 = (Math.log(S / X) + (r + sigma * sigma / 2) * T) / (sigma * Math.sqrt(T));
      return new Object[] { d1 };
    }
  }
  
  public static class Second
  {
    @defsfn
    public Object[] calculateSecond(double d1, double sigma, double T) {
      double d2 = d1 - sigma * Math.sqrt(T);
      return new Object[] { d2 };
    }
  }
  
  public static class Final
  {
    @defsfn
    public Object[] calculateFinal(double d1, double d2, double S, double X, double r, double T) {
      double f = S * Phi(d1) - X * Math.exp(-r * T) * Phi(d2);
      return new Object[] { f };
    }
  }
  
  public static class Store
  {
    @defsfn
    public Object[] store(List<Double> result, double f) {
      result.add(f);
      return null;
    }
  }
}
