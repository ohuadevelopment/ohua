;;;
;;; Copyright (c) Sebastian Ertel 2013-14. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.black-scholes-bench)

(ohua :import [com.ohua.benchmarks])

;;;
;;; The algorithm is taken from: http://introcs.cs.princeton.edu/java/22library/BlackScholes.java.html
;;; The data is taken from: https://github.com/rrnewton/Haskell-CnC/blob/master/examples/blackscholes_data.hs
;;;

;;; This is again just a benchmark to measure performance for data parallelism not for pipeline parallelism!

(defn black-scholes
  [data-file]
  (let [input (read-data data-file)
        output (new java.util.ArrayList)]
    (ohua
      (let [[share-price strike-price rate volatility expiration] (generate input)]
        (let [d1 (calculate-first share-price strike-price rate volatility expiration)]
        (store output
          (final-step share-price strike-price rate expiration
            (calculate-second d1 volatility expiration)
            d1)))))))
