/**
 * This class shows that type annotations for casts do not work. I filed a bug report on this.
 * 
 * @author sertel
 *
 */
public class TypeAnnotationProblems {
  
  @java.lang.annotation.Documented
  @java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
  @java.lang.annotation.Target(value = { java.lang.annotation.ElementType.TYPE_USE,
                                        java.lang.annotation.ElementType.TYPE_PARAMETER })
  public @interface Untainted {}
  
  protected static class MyState {
    public int _field = 1;
  }
  private MyState _state = null;
  
  public Object[] wrongOffset() {
    Object o = _state;
    return new Object[] { (@Untainted MyState) o };
  }
  
  public Object[] castElimination() {
    MyState o = _state;
    return new Object[] { (@Untainted MyState) o };
  }
  
  public Object[] wrongOffsetFunction() {
    return new Object[] { (@Untainted MyState) leaking() };
  }
  
  private Object leaking() {
    return _state;
  }
}
