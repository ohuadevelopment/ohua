/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.io;

import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.flowgraph.elements.packets.ISpecialMetaDataPacket;
import com.ohua.engine.online.configuration.FastTraveler;

public interface IUnboundedIOShutdownMarker extends ISpecialMetaDataPacket, IMetaDataPacket, FastTraveler
{
  // nothing yet
}
