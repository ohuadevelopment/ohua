/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.io;

import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.packets.IStreamPacket;

public class UnboundedIOShutdownMarker implements IUnboundedIOShutdownMarker
{

  /**
   * Don't copy.
   */
  @Override
  public IStreamPacket deepCopy()
  {
    return this;
  }
  
  @Override public InputPortEvents getEventType() {
    return InputPortEvents.FORCED_SHUTDOWN_PACKET_ARRIVAL;
  }
  
}
