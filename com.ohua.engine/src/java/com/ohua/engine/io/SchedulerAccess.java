/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.io;

public interface SchedulerAccess
{
  // TODO Make that none of these calls is executed in a concurrent fashion!
  public void setPoolSize(int num);
  public int getPoolSize();
}
