/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.io;

import com.ohua.engine.extension.points.IPacketHandler;
import com.ohua.engine.flowgraph.elements.operator.InputPort;

public interface IUnboundedIOShutdownHandler extends IPacketHandler
{
  public void notifyMarkerArrived(InputPort port, IUnboundedIOShutdownMarker packet);
}
