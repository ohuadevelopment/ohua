/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.io;

import java.util.concurrent.locks.ReentrantLock;

import com.ohua.engine.sections.AbstractSection;

/**
 * The idea of this class is to realize non-blocking I/O with blocking I/O calls by allowing one
 * more thread whenever an I/O section runs.
 * 
 * @author sertel
 * 
 */

// FIXME resizing the threadpool turns out to be a bad idea because it messes up the internal
// algorithm of the thread pool and it will start new threads over and over again whenever a new
// task is submitted.
// We have to come up with another solution for that. Maybe we should have a separate thread
// pool for these tasks here.
public class NonblockingIOSupport
{
  private SchedulerAccess _scheduler = null;
  
  private ReentrantLock _lock = new ReentrantLock();
  
  public NonblockingIOSupport(SchedulerAccess scheduler)
  {
    _scheduler = scheduler;
  }
  
  public void beforeExecution(AbstractSection section)
  {
    if(section.hasIOOperator())
    {
      resizePool(1);
    }
  }
  
  public void afterExecution(AbstractSection section)
  {
    // TBD What if I migrated this operator to this section?
    if(section.hasIOOperator())
    {
      resizePool(-1);
    }
  }
  
  /**
   * Sadly the above callback are not on the scheduler thread but on the thread executing the
   * section. Therefore, we need to protect access to this flag.
   * @param size
   */
  private void resizePool(int size)
  {
    _lock.lock();
    try
    {
      int new_size = _scheduler.getPoolSize() + size;
      _scheduler.setPoolSize(new_size);
    }
    finally
    {
      _lock.unlock();
    }
  }
}
