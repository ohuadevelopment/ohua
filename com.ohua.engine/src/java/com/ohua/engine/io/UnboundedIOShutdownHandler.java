package com.ohua.engine.io;

import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.AbstractOperatorRuntime;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OhuaOperator;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.operators.categories.InputIOOperator;
import com.ohua.engine.resource.management.ResourceConnection;
import com.ohua.engine.resource.management.ResourceManager;

// TODO extend this concept to handle stuck I/O operations that typically are not unbounded!
public class UnboundedIOShutdownHandler implements IUnboundedIOShutdownHandler
{
  private static class TakeDownOperator extends OhuaOperator
  {
    private OperatorCore _core = null;
    
    protected TakeDownOperator(AbstractOperatorRuntime core) {
      super(core);
      _core = core.getOp();
    }
    
    private void disableExceptions() {
   // FIXME refactorings
//      _core.executeOperator()._discardExceptions = true;
    }
  }
  
  private TakeDownOperator _operator = null;
  
  public UnboundedIOShutdownHandler(TakeDownOperator op) {
    _operator = op;
  }
  
  @Override public void addCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override public void removeCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override public void init() {
    // nothing
  }
  
  /**
   * We ask the ResourceManager for the connection and then close it.
   */
  @Override public void notifyMarkerArrived(InputPort port, IUnboundedIOShutdownMarker packet) {
    if(_operator.getUserOperator() instanceof InputIOOperator
       && ((InputIOOperator) _operator.getUserOperator()).isUnboundedInput())
    {
      _operator.disableExceptions();
      
      ResourceConnection cnn = ResourceManager.getInstance().getConnection(_operator.getId());
      try {
        ResourceManager.getInstance().finishUnboundedConnection(cnn.getConnectionID());
      }
      catch(Throwable t) {
        System.err.println("Exception caught during forced shutdown: " + t);
      }
    }
    else {
      // nothing to shutdown here. just drop the marker!
    }
  }
  
  public static TakeDownOperator aquireFeatures(AbstractOperatorRuntime core) {
    return new TakeDownOperator(core);
  }
}
