/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.io;

import com.ohua.engine.Maybe;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.InputPort.VisitorReturnType;
import com.ohua.engine.flowgraph.elements.packets.functionality.VisitorMixin;

public class UnboundedIOShutdownMarkerVisitorMixin extends
                                                  VisitorMixin<IUnboundedIOShutdownMarker, IUnboundedIOShutdownHandler>
{
  
  public UnboundedIOShutdownMarkerVisitorMixin(InputPort in)
  {
    super(in);
  }
  
  // FIXME This loop is recurring in every visitor mixing so it should probably go into the
  // base class.
  @Override
  public void notifyHandlers(IUnboundedIOShutdownMarker packet)
  {
    for(IUnboundedIOShutdownHandler handler : getAllHandlers())
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }
  
  @Override
  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.FORCED_SHUTDOWN_PACKET_ARRIVAL;
  }
  
  @Override
  public void handlePacket(IUnboundedIOShutdownMarker packet)
  {
    notifyHandlers(packet);
    
    _inputPort.setCurrentPacketToBeReturned(Maybe.empty());
    _inputPort.setVisitorReturnStatus(VisitorReturnType.DEQUEUE_NEXT_PACKET);
  }
  
}
