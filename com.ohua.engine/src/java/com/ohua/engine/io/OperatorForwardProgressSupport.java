/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.io;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import com.ohua.engine.IOperatorEventListener;
import com.ohua.engine.OperatorEvents;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.sections.AbstractSection;
import com.ohua.engine.sections.AbstractSection.SectionID;

/**
 * Whenever we perform (potentially) blocking I/O the class {@link NonblockingIOSupport} makes
 * sure all other sections can make forward progress by adding another thread to the thread
 * pool. This is not enough for the overlay network algorithms though because a blocking
 * operator will also never forward any markers. Also, we need to eventually be able to stop
 * that operator and if this operator is on a section with other operators, we need to make sure
 * that the other sections can make forward progress too. </p> Therefore, whenever an I/O
 * operator enters the user operator code, we allow the operator core code to be executed. In
 * turn, when the thread executing the user code returns then we need to make sure that another
 * thread does not concurrently execute the core code of the operator. </p> We want to make sure
 * of two things:
 * <ol>
 * <li>We never activate a section/operator (concurrently) for a notification from downstream!
 * <li>We make a best effort to not activate a section/operator (concurrently) when the operator
 * is making progress (by monitoring the incoming and outgoing arcs).
 * </ol>
 * @author sertel
 * 
 */
// FIXME see FIXMEs below
public class OperatorForwardProgressSupport
{
  
  public interface OperatorForwardProgressSchedulerAccess
  {
    void freeSection(AbstractSection section);
    
    boolean blockSection(AbstractSection section);
  }
  
  private OperatorForwardProgressSchedulerAccess _scheduler = null;
  
  private ConcurrentHashMap<SectionID, AbstractSection> _runningIOSections =
      new ConcurrentHashMap<>();
  
  private Map<SectionID, BlockedIOOperatorEventListener> _ioSections =
      new HashMap<SectionID, BlockedIOOperatorEventListener>();
  
  public OperatorForwardProgressSupport(OperatorForwardProgressSchedulerAccess scheduler)
  {
    _scheduler = scheduler;
  }
  
  public void init(List<? extends AbstractSection> sections)
  {
    for(AbstractSection section : sections)
    {
      // currently this optimization is enabled only for sections with one I/O operator on it.
      // (because we have to make sure that the arcs of this operator are thread-safe) by
      // default all inter-section arcs are thread-safe!
      if(section.hasIOOperator() && section.getOperators().size() == 1)
      {
        BlockedIOOperatorEventListener l = new BlockedIOOperatorEventListener(section);
        // FIXME enable again once this is back on the plan
//        section.getOperators().get(0).register(OperatorEvents.USER_OPERATOR_EXECUTION, l);
//        section.getOperators().get(0).register(OperatorEvents.USER_OPERATOR_RETURNED, l);
        _ioSections.put(section.getSectionID(), l);
      }
    }
  }
  
  /**
   * This is a notification when the decision was already made that the section gets executed.
   * At this point, this section is already blocked by the scheduler for parallel execution!
   * @param section
   */
  public boolean notifyOnSectionActivation(AbstractSection section)// , boolean
                                                                   // upstreamActivation)
  {
    if(_ioSections.containsKey(section.getSectionID()))
    {
      if(_runningIOSections.containsKey(section.getSectionID()))
      {
        // this is a request for scheduling an I/O section that is currently running (-> run
        // the core code only). this will submit the section for execution if and only if the
        // user section did not return yet. after this call the user section can wait on the
        // _coreRunning flag!
        return _ioSections.get(section.getSectionID()).coreRunning();
      }
      else
      {
        // this is a request for scheduling an I/O section that is not currently running (->
        // normal run)
        _runningIOSections.put(section.getSectionID(), section);
        return true;
      }
    }
    else
    {
      // a non I/O section
      return true;
    }
  }
  
  public boolean afterExecution(AbstractSection section)
  {
    // System.out.println("After execution of " + section + " the running sections count is: " +
    // _scheduler.getRunningSectionsCount());
    if(!_ioSections.containsKey(section.getSectionID()))
    {
      // This is not an I/O section so fall back to the default behavior.
      return true;
    }
    else
    {
      if(_runningIOSections.containsKey(section.getSectionID()))
      {
        if(_ioSections.get(section.getSectionID())._coreRunning)
        {
          boolean blockSection = false;
          // I'm the core!
          if(_ioSections.get(section.getSectionID())._userRunning)
          {
            // the user code is still executing. all good.
            blockSection = false;
          }
          else
          {
            // the user code finished while we were executing. -> don't free this section!
            blockSection = true;
          }
          
          // the core just finished execution, so notify the user operator to proceed execution.
          _ioSections.get(section.getSectionID()).notifySectionCompleted();
          return blockSection;
        }
        else
        {
          // a normal run
          _runningIOSections.remove(section.getSectionID());
          return true;
        }
      }
      else
      {
        // if this is an I/O section that just finished then it must be in the list of running
        // I/O sections!
        Assertion.impossible();
        return true;
      }
    }
  }
  
  private class BlockedIOOperatorEventListener implements IOperatorEventListener
  {
    private AbstractSection _section = null;
    private Map<String, Object> _sectionState = null;
    
    private volatile boolean _coreRunning = false;
    public volatile boolean _userRunning = false;
    
    private ReentrantLock _lock = new ReentrantLock();
    private Condition _condition = _lock.newCondition();
    
    BlockedIOOperatorEventListener(AbstractSection section)
    {
      _section = section;
    }
    
    public boolean coreRunning()
    {
      boolean canExecute = false;
      _lock.lock();
      try
      {
        if(_userRunning)
        {
          _coreRunning = true;
          canExecute = true;
        }
        else
        {
          Assertion.invariant(_coreRunning == false);
          _condition.signalAll();
          canExecute = false;
        }
      }
      finally
      {
        _lock.unlock();
      }
      return canExecute;
    }
    
    public void notifySectionCompleted()
    {
      _lock.lock();
      try
      {
        if(_coreRunning)
        {
          _coreRunning = false;
          _condition.signalAll();
        }
      }
      finally
      {
        _lock.unlock();
      }
    }
    
    @Override
    public void notifyOnOperatorEvent(OperatorEvents event)
    {
      switch(event)
      {
        case USER_OPERATOR_EXECUTION:
          handleUserOperatorExecution();
          break;
        case USER_OPERATOR_RETURNED:
          handleUserOperatorReturn();
          break;
        default:
          break;
      }
    }
    
    /**
     * Make sure that the scheduler allows this operators section to be executed again.
     */
    private void handleUserOperatorExecution()
    {
      // FIXME enable again once this is back on the plan
//      _sectionState = _section.getCurrentState();
//      Map<String, Object> state = new HashMap<String, Object>();
//      state.put("_active", false);
//      state.put("scheduler-quanta", _sectionState.get("scheduler-quanta"));
//      _section.applyState(state);
      
      _userRunning = true;
      _scheduler.freeSection(_section);
    }
    
    /**
     * If another thread is currently running the operator core code, then we have to wait until
     * we can release this thread again.
     * <p>
     * Scenario 1: If another task executes the core at this point then there is no other task
     * submitted but only notifications might exist! All we have to do in this case is to make
     * sure that we do not lose them!
     * </p>
     * <p>
     * Scenario 2: If the core is actually not running then there might be a task in the
     * schedulers queue that got submitted. For simplicity, we have to wait until this task
     * finished in order to get in the situation of scenario 1 again.
     * </p>
     */
    private void handleUserOperatorReturn()
    {
      // reclaim the right to run this section!
      if(!_scheduler.blockSection(_section))
      {
        // means that this section was already scheduled for execution. now we have to wait
        // there is a tricky race here: the section might have been scheduled for
        // execution but the _coreRunning flag has not been set to 'true' yet. in this case, we
        // would end up executing the section concurrently!-> return flag on the beforeExecution
        // call
      }
      else
      {
        // everything is fine because nobody was executing; just continue.
      }
      
      // wait for the core part to finish execution
      _lock.lock();
      try
      {
        // from now on we never want to be removed from the list of scheduled sections anymore
        // whenever a section is returning!
        _userRunning = false;
        
        while(_coreRunning)
          _condition.await();
      }
      catch(InterruptedException e)
      {
        Assertion.impossible(e);
      }
      finally
      {
        _lock.unlock();
      }
      // FIXME enable again once this is back on the plan
//      _section.applyState(_sectionState);
      _sectionState = null;
      
      // finally we need to reactivate this operator because it is deactivated in the concurrent
      // run, otherwise it would never finish its run.
      // FIXME enable again once this is back on the plan
//      _section.getOperators().get(0).activate();
    }
  }
}
