/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.ohua.engine.flowgraph.DataFlowComposition;

public final class ProcessRunner extends AbstractProcessRunner {
  private List<ProcessListener> _listeners = new LinkedList<ProcessListener>();
  
  public ProcessRunner(String pathToFlow) {
    super(pathToFlow);
  }
  
  public ProcessRunner() {
    super((String) null);
  }
  
  public ProcessRunner(DataFlowComposition loader) {
    super(loader);
  }
  
  public void setManager(AbstractProcessManager manager) {
    _manager = manager;
  }
  
  @Override
  public void loadRuntimeConfiguration(String pathToRuntimeConfiguration) throws IOException,
                                                                         ClassNotFoundException
  {
    super.loadRuntimeConfiguration(pathToRuntimeConfiguration);
  }
  
  public void registerListener(ProcessListener listener) {
    _listeners.add(listener);
  }
  
  // FIXME this is a leak. we should never give the process manager away. (at least not to the
  // program executing the flow.)
  public IExternalProcessManager getProcessManager() {
    if(_manager == null) {
      initializeProcessManager();
    }
    
    return _manager;
  }
  
  public void run() {
    if(_manager == null) {
      initializeProcessManager();
    }
    initialize();
    try {
      _manager.initializeProcess();
      _manager.awaitSystemPhaseCompletion();

      _manager.runFlow();
      _manager.awaitSystemPhaseCompletion();

      _manager.tearDownProcess();
      _manager.awaitSystemPhaseCompletion();
    }
    catch(Throwable t) {
      throw new RuntimeException(t);
    }
    notifyListenersOnDone();
  }
  
  private void notifyListenersOnDone() {
    for(ProcessListener listener : _listeners) {
      listener.notifyProcessFinished();
    }
  }
  
  public void finishComputation() {
    _manager.finishComputation();
  }
  
  @Override
  protected void initialize() {
    // nothing
  }
  
  public RuntimeProcessConfiguration getConfig() {
    return _config;
  }
  
}
