/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;



public class OhuaOSProcess extends AbstractOhuaOSProcess
{
  public static void main(String[] args) throws Throwable
  {
    OhuaOSProcess process = new OhuaOSProcess();
    process.execute(args);
  }
  
}
