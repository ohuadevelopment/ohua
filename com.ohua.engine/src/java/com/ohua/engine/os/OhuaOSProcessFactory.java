/*
 * Copyright � Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public abstract class OhuaOSProcessFactory
{
  private static String[] _debug =
      new String[] { "java",
                    "-Xdebug",
                    "-Xnoagent",
                    "-Djava.compiler=NONE",
                    "-Xrunjdwp:transport=dt_socket,address=localhost:8000,server=y,suspend=y",
                    "-ea",
                    "-Xmx512m",
                    // FIXME needs to look for the system parameter to pass it along!
                    "-Djava.util.logging.config.file=./com.ohua.engine/config/consoleLogging.properties",
                    "-classpath" };

  private static String[] _noDebug =
      new String[] { "java",
                    "-Xnoagent",
                    "-Djava.compiler=NONE",
                    "-ea",
                    "-Xmx2048m",
//                    "-Dcom.sun.management.jmxremote", 
//                    "-Dcom.sun.management.jmxremote.ssl=false",
//                    "-Dcom.sun.management.jmxremote.authenticate=false",
//                    "-Dcom.sun.management.jmxremote.port=1100",

                    // "-Xmx5120m",
                    // "-XX:+UseParallelGC",
//                    "-Xloggc:gc.log",
                    // "-XX:+UseConcMarkSweepGC",
//                    "-XX:+PrintGCTimeStamps",
//                    "-XX:+PrintGC",
//                    "-XX:+PrintGCDetails",
//                    "-XX:+PrintGCApplicationConcurrentTime",
//                    "-XX:+PrintGCApplicationStoppedTime",
                    // FIXME needs to look for the system parameter to pass it along!
//                    "-Djava.util.logging.config.file=./com.ohua.experiments/config/consoleLogging.properties",
//                     "-Djava.util.logging.config.file=./com.ohua.engine/config/consoleLogging.properties",
                     "-Djava.util.logging.config.file=./com.ohua.engine/config/consoleLogging-run.properties", // no logging
                     "-Djava.library.path=" + System.getProperty("java.library.path"),
                     "-classpath" };

  public static OhuaOSProcessHandle createOSProcess(String loggingConfiguration,
                                                    boolean debugMode,
                                                    List<String> commands) throws IOException
  {
    OhuaOSProcessHandle processHandle = new OhuaOSProcessHandle();
    processHandle._processBuilder = new ProcessBuilder();
    
    // prepare input I/O redirection
    Properties props = new Properties();
    FileReader reader = new FileReader(loggingConfiguration);
    props.load(reader);
    performStdIOVerification(props);

    String systemInput = props.getProperty("system.in");
    new File(systemInput).mkdirs();
    File processRedirect = new File(systemInput + "/system.in.log");
    FileWriter redirect = new FileWriter(processRedirect);
    processHandle._inputDrainer = new ProcessInputDrainer(redirect);
    
    // create the child process
    String classPath = getOhuaClassPath();
    List<String> command = new ArrayList<String>();
    if(debugMode)
    {
      Collections.addAll(command, _debug);
    }
    else
    {
//      _noDebug[5] = "-Xloggc:" + systemInput + "/gc.log";
      Collections.addAll(command, _noDebug);
    }
    command.add(classPath);
    command.addAll(commands);
    processHandle._processBuilder.command(command);

    return processHandle;
  }

  private static String getOhuaClassPath()
  {
    if(System.getProperties().containsKey("ohua.class.path"))
    {
      // ant
      return System.getProperty("ohua.class.path");
    }
    else
    {
      // eclipse
      return System.getProperty("java.class.path");
    }
  }

  public static void startOSProcess(OhuaOSProcessHandle processHandle) throws IOException
  {
    processHandle._ohuaChildProcess = processHandle._processBuilder.start();
    
    // child process input IO handling
    InputStream processInput = processHandle._ohuaChildProcess.getInputStream();
    processHandle._inputDrainer.setProcessInput(processInput);
    Thread drainer = new Thread(processHandle._inputDrainer, "process-input-drainer");
    drainer.start();
  }
  
  private static void performStdIOVerification(Properties props)
  {
    if(!props.containsKey("system.in"))
    {
      throw new IllegalArgumentException("Missing system.in configuration");
    }
    if(!props.containsKey("system.out"))
    {
      throw new IllegalArgumentException("Missing system.out configuration");
    }
    if(!props.containsKey("system.err"))
    {
      throw new IllegalArgumentException("Missing system.err configuration");
    }
  }
}
