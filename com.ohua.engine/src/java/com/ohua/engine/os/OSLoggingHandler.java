/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ohua.engine.utils.OhuaLoggingFormatter;
import com.ohua.engine.utils.OhuaLoggingUtils;

public class OSLoggingHandler
{
  private String _loggingConfig = null;
  
  private List<FileHandler> _loggers = new ArrayList<FileHandler>();
  
  public OSLoggingHandler(String loggingConfig)
  {
    _loggingConfig = loggingConfig;
  }

  public void loggingSetup() throws FileNotFoundException, IOException
  {
    Properties loggingProps = new Properties();
    FileReader reader = new FileReader(_loggingConfig);
    loggingProps.load(reader);
    
    handleLoggingRedirect(loggingProps);
    loggerSetup(loggingProps);
    
    reader.close();
  }
  
  private void loggerSetup(Properties loggingProps)
  {
    // redirect the logging to System.out and System.err files stored in the output directory of
    // this test case.
    Logger defaultLogger = Logger.getLogger("");
    defaultLogger.setLevel(Level.ALL);
    try
    {
      FileHandler defaultLogFileHandler =
          OhuaLoggingUtils.createLogFileHandler("Process-Console.out",
                                                Level.ALL,
                                                loggingProps.getProperty("system.out",
                                                                         "test-output"));
      defaultLogFileHandler.setFormatter(new OhuaLoggingFormatter());
      defaultLogger.addHandler(defaultLogFileHandler);
    }
    catch(Exception e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  private void handleLoggingRedirect(Properties loggingProps) throws IOException
  {
    for(Map.Entry<Object, Object> entry : loggingProps.entrySet())
    {
      String loggerName = entry.getKey().toString();
      String logDir = entry.getValue().toString();
      if(loggerName.equals("system.err"))
      {
        new File(logDir).mkdirs();
        // don't use buffered output here because if you fail the error might get stuck in the
        // buffer instead of being printed to the log!
        PrintStream newOut = new PrintStream(new FileOutputStream(logDir + "/system.err.log"));
        System.setErr(newOut);
      }
      else if(loggerName.equals("system.out"))
      {
        new File(logDir).mkdirs();
        // don't use buffered output here because if you fail the error might get stuck in the
        // buffer instead of being printed to the log!
        PrintStream newOut = new PrintStream(new FileOutputStream(logDir + "/system.out.log"));
        System.setOut(newOut);
      }
      else if(loggerName.equals("system.in"))
      {
        // the drainer thread in the parent process takes care of that
        continue;
      }
      else
      {
        FileHandler writer =
            OhuaLoggingUtils.outputLogToFile(entry.getKey().toString(), Level.ALL, logDir);
        _loggers.add(writer);
      }
    }
  }

  public void loggingTearDown()
  {
    for(FileHandler handler : _loggers)
    {
      handler.flush();
      handler.close();
    }
  }

}
