/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import com.ohua.engine.utils.parser.OhuaFlowParser;



public class CommandLineParser
{
  public String _loggingConfig = null;
  protected String _pathToFlow = null;
  private String _pathToRuntimeConfiguration = null;
  
  public void parseComandLineArgs(String[] args)
  {
    _pathToFlow = args[0];
    _pathToRuntimeConfiguration = args[1];
    _loggingConfig = args[2];
  }
  
  public void apply(AbstractOhuaOSProcess process)
  {
    process.setFlowLoader(new OhuaFlowParser(_pathToFlow));
    process._pathToRuntimeConfiguration = _pathToRuntimeConfiguration;
  }

}
