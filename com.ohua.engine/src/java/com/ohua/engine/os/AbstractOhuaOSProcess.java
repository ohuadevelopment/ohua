/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.os;

import java.io.IOException;

import com.ohua.engine.ProcessRunner;
import com.ohua.engine.flowgraph.DataFlowComposition;

public abstract class AbstractOhuaOSProcess
{
  private DataFlowComposition _loader = null;
  protected String _pathToRuntimeConfiguration = null;
  
  private OSLoggingHandler _logging = null;

  protected void initialize() throws Exception
  {
//     loggingSetup();
  }
  
  public final void execute(String[] args) throws Throwable
  {
    CommandLineParser parser = createCommandLineParser();
    parser.parseComandLineArgs(args);
    parser.apply(this);

    _logging = new OSLoggingHandler(parser._loggingConfig);
    _logging.loggingSetup();

    try
    {
      initialize();
      run();
      // FIXME needs to be in a finally clause!
      tearDown();
    }
    catch(RuntimeException re)
    {
      System.err.println("A fatal error occured during execution: ");
      re.printStackTrace();
      handleFatalError();
      if(re.getCause() != null)
      {
        throw re.getCause();
      }
      else
      {
        throw re;
      }
    }
    
  }

  protected CommandLineParser createCommandLineParser()
  {
    return new CommandLineParser();
  }

  protected void handleFatalError()
  {
    _logging.loggingTearDown();
  }

  public void tearDown() throws IOException
  {
    _logging.loggingTearDown();
  }

  protected void run() throws IOException, ClassNotFoundException
  {
    ProcessRunner runner = createProcessRunner();
    runner.loadRuntimeConfiguration(_pathToRuntimeConfiguration);
    runner.run();
  }

  protected ProcessRunner createProcessRunner()
  {
    ProcessRunner runner = new ProcessRunner(_loader);
    return runner;
  }
  
  public void setFlowLoader(DataFlowComposition loader)
  {
    _loader = loader;
  }

  public DataFlowComposition getFlowLoader(){
    return _loader;
  }
}
