/*
 * Copyright � Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

import com.ohua.engine.utils.OhuaLoggerFactory;

public abstract class AbstractOhuaProcessRunner
{
  protected Logger _logger = OhuaLoggerFactory.getLogger(getClass());
  
  private ProcessBuilder _processBuilder = new ProcessBuilder();
  
  private String _loggingConfiguration = null;
  
  private String _pathToFlow = null;
  private String _pathToRuntimeConfiguration = null;
  
  public int forkAndWait(boolean debug) throws IOException, InterruptedException
  {
    OhuaOSProcessHandle childProcess = forkAndReturn(debug);
    int result = childProcess._ohuaChildProcess.waitFor();
    childProcess._inputDrainer.done();
    
    _logger.info("Process finished: " + childProcess._ohuaChildProcess.exitValue());
    return result;
  }
  
  public OhuaOSProcessHandle forkAndReturn(boolean debug) throws IOException,
                                                         InterruptedException
  {
//    printProcessInformation();
    
    OhuaOSProcessHandle childProcess =
        OhuaOSProcessFactory.createOSProcess(_loggingConfiguration, debug, getRunCommands());
    OhuaOSProcessFactory.startOSProcess(childProcess);
    
    return childProcess;
  }

  protected List<String> getRunCommands()
  {
    List<String> commands = new ArrayList<String>();
    commands.add(getProcessClassReference());
    commands.add(_pathToFlow);
    commands.add(_pathToRuntimeConfiguration);
    commands.add(_loggingConfiguration);
    return commands;
  }

  protected String getProcessClassReference()
  {
    return "com/ohua/engine/os/OhuaOSProcess";
  }
  
  protected void printProcessInformation()
  {
    System.out.println("System environment configuration:");
    Map<String, String> sysEnv = System.getenv();
    for(Map.Entry<String, String> entry : sysEnv.entrySet())
    {
      System.out.println(entry.getKey() + "=" + entry.getValue());
    }
    
    System.out.println("System properties configuration:");
    Properties sysProps = System.getProperties();
    for(Map.Entry<Object, Object> entry : sysProps.entrySet())
    {
      System.out.println(entry.getKey().toString() + "=" + entry.getValue().toString());
    }
    
    System.out.println("Process Builder environment configuration:");
    sysEnv = _processBuilder.environment();
    for(Map.Entry<String, String> entry : sysEnv.entrySet())
    {
      System.out.println(entry.getKey() + "=" + entry.getValue());
    }
  }
  
  public final String getPathToFlow()
  {
    return _pathToFlow;
  }
  
  public final void setPathToFlow(String pathToFlow)
  {
    _pathToFlow = pathToFlow;
  }
  
  public final String getPathToRuntimeConfiguration()
  {
    return _pathToRuntimeConfiguration;
  }
  
  public final void setPathToRuntimeConfiguration(String pathToRuntimeConfiguration)
  {
    _pathToRuntimeConfiguration = pathToRuntimeConfiguration;
  }
  
  public final String getLoggingConfiguration()
  {
    return _loggingConfiguration;
  }
  
  public void setLoggingConfiguration(String loggingConfiguration)
  {
    _loggingConfiguration = loggingConfiguration;
  }

  public void applyDefaultConfiguration(String testMethodInputDirectory)
  {
    setPathToRuntimeConfiguration(testMethodInputDirectory + "runtime-parameters.properties");
    setLoggingConfiguration(testMethodInputDirectory + "logging-configuration.properties");
  }

}
