/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.os;

import java.io.IOException;

public class OhuaOSProcessRunner extends AbstractOhuaProcessRunner
{
  public OhuaOSProcessHandle forkAndReturn(boolean debug) throws IOException,
                                                         InterruptedException
  {
    final OhuaOSProcessHandle handle = super.forkAndReturn(debug);
    // add a shutdown hook to make sure the child process will shutdown when something happens
    // to the parent process
    Runtime.getRuntime().addShutdownHook(new Thread()
    {
      @Override
      public void run()
      {
        handle._ohuaChildProcess.destroy();
      }
    });
    return handle;
  }
}
