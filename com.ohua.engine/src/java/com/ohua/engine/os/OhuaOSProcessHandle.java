/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import java.util.List;

public class OhuaOSProcessHandle
{
  public Process _ohuaChildProcess = null;
  public ProcessInputDrainer _inputDrainer = null;
  public List<String> _commands = null;
  public ProcessBuilder _processBuilder = null;
}
