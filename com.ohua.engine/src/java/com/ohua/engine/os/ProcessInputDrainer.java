/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.os;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;

public class ProcessInputDrainer implements Runnable
{
  private InputStream _processInput = null;
  private Writer _redirect = null;
  
  public ProcessInputDrainer(InputStream processInput, Writer redirect)
  {
    _processInput = processInput;
    _redirect = redirect;
  }

  public ProcessInputDrainer(Writer redirect)
  {
    _redirect = redirect;
  }

  public void run()
  {
    InputStreamReader isr = new InputStreamReader(_processInput);
    BufferedReader br = new BufferedReader(isr);
    String line = null;
    int errorsCaught = 0;
    
    while(true)
    {
      try
      {
        if((line = br.readLine()) != null)
        {
          _redirect.write(line + "\n");
        }
        else
        {
          break;
        }
      }
      catch(IOException e)
      {
        errorsCaught++;
        e.printStackTrace();
        if(errorsCaught == 5)
        {
          throw new RuntimeException(e);
        }
      }
    }
  }
  
  public void done()
  {
    try
    {
      _processInput.close();
      _redirect.close();
    }
    catch(IOException e)
    {
      // no problem
    }
  }
  
  protected final InputStream getProcessInput()
  {
    return _processInput;
  }
  
  protected final void setProcessInput(InputStream processInput)
  {
    _processInput = processInput;
  }
}
