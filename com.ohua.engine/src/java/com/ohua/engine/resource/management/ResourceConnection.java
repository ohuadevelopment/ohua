/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import com.ohua.engine.AbstractExternalActivator;

/**
 * This class does not contain any functions. it is supposed to force every new resource to also
 * build a proxy object that hides away all access to functions with transactional powers such
 * as commit(), rollback() etc.
 * @author sertel
 * 
 */
public abstract class ResourceConnection
{
  private ConnectionID _connectionID = null;
  
  private AbstractExternalActivator _activator = null;
  
  public ResourceConnection(ConnectionID connectionID)
  {
    _connectionID = connectionID;
  }
  
  public ConnectionID getConnectionID()
  {
    return _connectionID;
  }
  
  protected void setActivator(AbstractExternalActivator activator)
  {
    _activator = activator;
  }
  
  protected AbstractExternalActivator getActivator()
  {
    return _activator;
  }
  
}
