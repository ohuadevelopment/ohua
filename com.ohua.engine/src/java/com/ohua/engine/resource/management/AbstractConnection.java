/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.ConnectionID.ConnectionIDGenerator;

public abstract class AbstractConnection extends TransactionControl
{
  private String _resourceID = null;
  private ConnectionID _connectionID = ConnectionIDGenerator.generateNewConnectionID();
  
  private int _commitFrequency = 100;

  public ConnectionID getConnectionID()
  {
    return _connectionID;
  }
  
  private OperatorID _associatedOp = null;
  
  protected final void associateOperator(OperatorID opID)
  {
    _associatedOp = opID;
  }
  
  protected final OperatorID getAssociatedOperator()
  {
    return _associatedOp;
  }

  protected final void setParentResourceID(String resourceID)
  {
    _resourceID = resourceID;
  }
  
  protected final String getParentResourceID()
  {
    return _resourceID;
  }
  
  protected final void setCommitFrequency(int commitFrequency)
  {
    _commitFrequency = commitFrequency;
  }

  @Override
  protected final int getCommitFrequency()
  {
    return _commitFrequency;
  }

  /**
   * This function should provide a proxy like object that allows accessing all but the
   * transactional functions of the connection such as commit(), rollback(), abort().
   * @return
   */
  abstract protected ResourceConnection getRestrictedConnection();

  /**
   * Closing the existing connection to the resource.
   * @throws Throwable
   */
  abstract protected void close() throws Throwable;
  
}
