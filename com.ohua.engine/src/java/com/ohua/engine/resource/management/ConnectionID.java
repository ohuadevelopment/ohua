/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import java.util.concurrent.atomic.AtomicInteger;

import com.ohua.engine.flowgraph.elements.ScopedUniqueID;

public class ConnectionID extends ScopedUniqueID
{
  public ConnectionID(int id)
  {
    super(id);
  }
  
  public static class ConnectionIDGenerator
  {
    private static AtomicInteger _connectionIDCounter = new AtomicInteger(0);
    
    public static ConnectionID generateNewConnectionID()
    {
      return new ConnectionID(_connectionIDCounter.getAndIncrement());
    }
  }
}
