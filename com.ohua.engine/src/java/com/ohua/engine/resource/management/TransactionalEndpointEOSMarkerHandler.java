/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.IEndOfStreamPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OhuaOperator;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacket;
import com.ohua.engine.flowgraph.elements.packets.functionality.PriorityLevel;
import com.ohua.engine.operators.categories.BatchEndpoint;
import com.ohua.engine.utils.OhuaLoggerFactory;

// FIXME change this name! needs to be "TransactionalIOOpEOSMarkerHandler!
public class TransactionalEndpointEOSMarkerHandler implements IEndOfStreamPacketHandler
{
  private OhuaOperator _operator = null;

  public TransactionalEndpointEOSMarkerHandler(OhuaOperator operator)
  {
    _operator = operator;
  }
  
  public void notifyMarkerArrived(InputPort port, EndOfStreamPacket packet)
  {
    ResourceConnection cnn = ResourceManager.getInstance().getConnection(_operator.getId());
    Assertion.invariant(cnn != null);
    switch(packet.getType())
    {
      case INITIALIZATION:
        // commit any preparation here such as creating or deleting a table
        commit(cnn);
        break;
//      case GRAPH_ANALYSIS:
//        // nothing to do
//        break;
      case COMPUTATION:
        // commit here any final changes that have been made to that resource during that
        // phase
        if(_operator.getUserOperator() instanceof BatchEndpoint)
        {
          ((BatchEndpoint) _operator.getUserOperator()).flush();
        }
        commit(cnn);
        
        // clearly here we also want to deregister all listeners to external resources!
        // until here it could have happened that we got activated by the external resource
        // because new packets are available. those new packets will be retrieved in this cycle
        // (or in the subsequent ones if there is more data than the arc can carry). so it might
        // happen that we have one cycle for this operator where it gets schedule although it is
        // already done (case where the op finishes in this cycle and got a notification from
        // the external resource just before the above line) but that's ok because we will not
        // activate any downstream operators again.
        ResourceManager.getInstance().unlisten(_operator.getId());
        break;
      case TEARDOWN:
        // not only commit but also close the connection here
        commit(cnn);
        ResourceManager.getInstance().closeConnection(cnn.getConnectionID());
    }
  }

  private void commit(ResourceConnection cnn)
  {
    TransactionControl control =
        ResourceManager.getInstance().takeOverTransactionControl(cnn.getConnectionID());
    try
    {
      control.commit();
    }
    catch(Throwable e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }

    OhuaLoggerFactory.getLogger(getClass()).info("Transaction committed successfully.");
  }

  /**
   * We always want to run as the very last!
   */
  public PriorityLevel getPriority(InputPortEvents event)
  {
    return PriorityLevel.LAST;
  }
  
  public void addCallback(InputPortEvents event, InputPort port)
  {
    // nothing to do
  }
  
  public void init()
  {
    // no init necessary
  }
  
  public void removeCallback(InputPortEvents event, InputPort port)
  {
    // nothing
  }

}
