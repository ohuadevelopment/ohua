/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

public abstract class TransactionControl
{
  /**
   * Rolling back all changes performed since the last commit against the resource was issued.
   * @throws Throwable
   */
  abstract protected void rollback() throws Throwable;
  
  /**
   * Making all changes to the connected resource since the last commit persistent.
   * @throws Throwable
   */
  abstract protected void commit() throws Throwable;
  
  /**
   * Exposes the desired commit frequency for this external connection.
   * @return
   */
  abstract protected int getCommitFrequency();
}
