/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

public interface TransactionalReadListener
{
  public Object notifyData(Object data);
}
