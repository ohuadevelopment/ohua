/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.resource.management;

/**
 * A marker interface for unbounded (blocking) input connections.
 * @author sertel
 * 
 */
public interface IUnboundedInput
{
  /**
   * Whenever this is being called the underlying connection is being interrupted if currently
   * blocking and any subsequent call to the connection will fail.
   */
  public void finish();
  
  /**
   * Allows for retrieval of new data again.
   */
  public void restart();
}
