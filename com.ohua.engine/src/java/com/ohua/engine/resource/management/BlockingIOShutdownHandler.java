package com.ohua.engine.resource.management;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.IEndOfStreamPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OhuaOperator;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacket;
import com.ohua.engine.flowgraph.elements.packets.functionality.PriorityLevel;
import com.ohua.engine.operators.categories.InputIOOperator;

public class BlockingIOShutdownHandler implements IEndOfStreamPacketHandler
{
  private OhuaOperator _operator = null;
  
  public BlockingIOShutdownHandler(OhuaOperator operator)
  {
    Assertion.invariant(operator.getUserOperator() instanceof InputIOOperator);
    Assertion.invariant(((InputIOOperator) operator.getUserOperator()).isUnboundedInput());
    _operator = operator;
  }
  
  @Override
  public void addCallback(InputPortEvents event, InputPort port)
  {
    // nothing
  }
  
  @Override
  public void removeCallback(InputPortEvents event, InputPort port)
  {
    // nothing
  }
  
  @Override
  public void init()
  {
    // nothing to be initialized
  }
  
  @Override
  public PriorityLevel getPriority(InputPortEvents event)
  {
    return PriorityLevel.LOW;
  }
  
  /**
   * Input operators might be stuck in blocking I/O operators. If we want to shut them down then
   * we need to interrupt them. A simple way to do so is to just call their cleanup routine.
   */
  @Override
  public void notifyMarkerArrived(InputPort port, EndOfStreamPacket packet)
  {
    // make sure that the operator state machine will allow for exceptions and does not
    // propagate those
    
    // TODO Here we really need to tamper with the operator core! Create a specific interface to
    // be exposed here!
    
    // TODO A separate marker is better here, because in order to be propagated in a concurrent
    // setting it needs to be of a special type!
    // _operator.
    
    _operator.getUserOperator().cleanup();
  }
  
}
