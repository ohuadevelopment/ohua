/*
 * Copyright � Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.XMLParserException;
import com.ohua.engine.resource.management.AbstractResource.MandatoryResourceAttributes;
import com.ohua.engine.utils.FileUtils;

public class ResourceConfigurationParser {
  private enum Elements {
    RESOURCES,
    RESOURCE
  }
  
  private ManagerProxy _ohuaProcess = null;
  
  public ResourceConfigurationParser(ManagerProxy ohuaProcess) {
    _ohuaProcess = ohuaProcess;
  }
  
  public void loadResources() throws XMLParserException {
    try {
      List<Path> registries = FileUtils.loadMetaInfFilesFromClassPath("resources**", "*-resources.xml");
      for(Path reg : registries)
        try (Reader fileReader = Files.newBufferedReader(reg)) {
          loadResource(fileReader);
        }
    }
    catch(Exception e) {
      throw new XMLParserException(e);
    }
  }
  
  private void loadResource(Reader reader) throws XMLStreamException, FactoryConfigurationError {
    XMLStreamReader xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(reader);
    while(xmlReader.hasNext()) {
      int eventType = xmlReader.next();
      switch(eventType) {
        case XMLStreamConstants.START_ELEMENT:
          QName elementName = xmlReader.getName();
          if(elementName.getPrefix().equals("ohua")) {
            throw new XMLStreamException("Invalid prefix " + elementName
                                         + " during parsing resource file detected.");
          }
          
          switch(Elements.valueOf(elementName.getLocalPart().toUpperCase())) {
            case RESOURCES:
              // nothing to do yet
              break;
            case RESOURCE:
              Map<String, String> attributes = new HashMap<String, String>();
              for(int i = 0; i < xmlReader.getAttributeCount(); i++) {
                attributes.put(xmlReader.getAttributeLocalName(i), xmlReader.getAttributeValue(i));
              }
              
              if(!attributes.containsKey(MandatoryResourceAttributes.type.name())
                 || attributes.get(MandatoryResourceAttributes.type.name()).length() < 1)
              {
                throw new XMLStreamException("Missing type attribute detected for resource: " + elementName);
              }
              
              if(!attributes.containsKey(MandatoryResourceAttributes.id.name())
                 || attributes.get(MandatoryResourceAttributes.id.name()).length() < 1)
              {
                throw new XMLStreamException("Missing id attribute detected for resource: " + elementName);
              }
              
              String clz = attributes.get(MandatoryResourceAttributes.type.name());
              AbstractResource resource = createResource(clz, attributes);
              ResourceManager.getInstance().registerResource(resource);
              break;
          }
          
          break;
        default:
          // do nothing
      }
    }
  }
  
  @SuppressWarnings("unchecked")
  private AbstractResource createResource(String clz, Map<String, String> attributes) {
    try {
      Class<? extends AbstractResource> resClz = (Class<? extends AbstractResource>) Class.forName(clz);
      return resClz.getConstructor(ManagerProxy.class, Map.class).newInstance(_ohuaProcess, attributes);
    }
    catch(InstantiationException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    catch(IllegalAccessException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    catch(ClassNotFoundException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    catch(IllegalArgumentException e) {
      e.printStackTrace();
      Assertion.impossible(e);
    }
    catch(SecurityException e) {
      e.printStackTrace();
      Assertion.impossible(e);
    }
    catch(InvocationTargetException e) {
      e.printStackTrace();
      Assertion.impossible(e);
    }
    catch(NoSuchMethodException e) {
      e.printStackTrace();
      Assertion.impossible(e);
    }
    return null;
  }
  
}
