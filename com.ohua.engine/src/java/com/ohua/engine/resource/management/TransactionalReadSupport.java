/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import com.ohua.engine.flowgraph.elements.operator.OperatorID;

public abstract class TransactionalReadSupport implements TransactionalReadListener
{
  private int _uncommittedPackets = 0;

  private OperatorID _operator = null;
  
  private TransactionControl _taControl = null;

  protected TransactionalReadSupport(OperatorID operator)
  {
    _operator = operator;
  }
  
  protected void initialize()
  {
    _taControl = ResourceManager.getInstance().takeOverTransactionControl(_operator);
  }

  public Object notifyData(Object data)
  {
    _uncommittedPackets++;
    
    if(_taControl.getCommitFrequency() <= _uncommittedPackets)
    {
      // FIXME this thing also needs to call commit in the case that the operator gives back
      // control because it is not clear when it is going to be called back again!
      commit();
      _uncommittedPackets = 0;
    }
    
    return data;
  }

  protected final void commit()
  {
    try
    {
      _taControl.commit();
    }
    catch(Throwable e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  protected final OperatorID getOperator()
  {
    return _operator;
  }
}
