/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import java.util.List;
import java.util.Map;

import com.ohua.engine.flowgraph.elements.operator.OperatorID;

public abstract class ResourceAccess
{
  private OperatorID _operatorID = null;
  
  private String _cpArtifactID = null;
  
  // meta data
  protected Map<String, String> _columnDefs = null;
  protected List<String> _keys = null;

  public ResourceAccess(OperatorID operatorID, String cpArtifactID)
  {
    _operatorID = operatorID;
    _cpArtifactID = cpArtifactID;
  }
  
  public final void setMetaData(Map<String, String> colDefs, List<String> keys)
  {
    _columnDefs = colDefs;
    _keys = keys;
  }

  protected ResourceConnection getConnection()
  {
    return ResourceManager.getInstance().getConnection(_operatorID);
  }
  
  protected AbstractResource getResource()
  {
    return ResourceManager.getInstance().getResource(getConnection().getConnectionID());
  }
  
  protected String getCPArtifactID()
  {
    return _cpArtifactID;
  }

  abstract public void initialize();

  abstract public void prepareInsert();
  
  abstract public void createArtifact() throws Throwable;
  
  abstract public void insert(Map<String, Object> data) throws Throwable;
  
  abstract public void insert(Object data) throws Throwable;

  abstract public void deleteArtifact() throws Throwable;
}
