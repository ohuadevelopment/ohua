/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import java.util.Collections;
import java.util.Map;
import java.util.logging.Logger;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.utils.OhuaLoggerFactory;

public abstract class AbstractResource
{
  public static enum MandatoryResourceAttributes
  {
    type,
    id
  }

  private Map<String, String> _attributes = null;
  
  protected Logger _logger = OhuaLoggerFactory.getLogger(getClass());
  
  protected ManagerProxy _ohuaProcess = null;

  public AbstractResource(ManagerProxy ohuaProcess, Map<String, String> attributes)
  {
    _ohuaProcess = ohuaProcess;
    _attributes = Collections.unmodifiableMap(attributes);
  }
  
  abstract public ResourceAccess getResourceAccess(OperatorID operatorID,
                                                    String opChckPtArtifactID);

  
  protected final Map<String, String> getAttributes()
  {
    return _attributes;
  }

  /**
   * Should contain code that validates the provided attributes for correctness and presence of
   * mandatory attributes.
   */
  abstract public void validate();
  
  abstract protected AbstractConnection getConnection(Object... args);

  public String getIdentifier()
  {
    Assertion.invariant(_attributes.containsKey(MandatoryResourceAttributes.id.name()));
    return _attributes.get(MandatoryResourceAttributes.id.name());
  }
  
  abstract protected AbstractExternalActivator getExternalActivator(OperatorID operatorID);
  
  protected final void commit(ResourceConnection cnn)
  {
    try
    {
      ResourceManager.getInstance().takeOverTransactionControl(cnn.getConnectionID()).commit();
    }
    catch(Throwable e)
    {
      e.printStackTrace();
    }
  }

}
