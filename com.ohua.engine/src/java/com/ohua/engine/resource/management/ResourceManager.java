/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.ProcessID;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.XMLParserException;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.utils.OhuaLoggerFactory;

/**
 * Note that the resource manager controls all access to the resources the flow interacts with.
 * However the transaction coordination is business of the engine itself and the checkpointing
 * algorithm.
 * @author sertel
 * 
 */
public class ResourceManager
{
  private Logger _logger = OhuaLoggerFactory.getLogger(getClass());

  private Map<String, AbstractResource> _resources = new HashMap<String, AbstractResource>();
  private Map<ConnectionID, AbstractConnection> _connections =
      new HashMap<ConnectionID, AbstractConnection>();
  
  private Map<OperatorID, ResourceConnection> _operatorConnections =
      new HashMap<OperatorID, ResourceConnection>();

  private static ResourceManager _resourceManager = new ResourceManager();
  
  private ResourceManager()
  {
    // singleton
  }
  
  public static ResourceManager getInstance()
  {
    return _resourceManager;
  }
  
  /**
   * Allows to retrieve the connection for the operator. If none exists null is being returned.
   * <p>
   * Note again: Operators are restricted to only one resource connection per operator.
   * 
   * @param operatorID
   * @return
   */
  public ResourceConnection getConnection(OperatorID operatorID)
  {
    return _operatorConnections.get(operatorID);
  }
  
  /**
   * This associates the created connection directly with the operator and will further on
   * provide the possibility to have external activators for this operator.
   * @param opID
   * @param resourceID
   * @param args
   * @return
   */
  public ResourceConnection openDirectConnection(OperatorID operatorID,
                                           String resourceID,
                                           Object... args)
  {
    return openConnection(operatorID, resourceID, false, -1, args);
  }
  
  public ResourceConnection openConnection(OperatorID operatorID,
                                           String resourceID,
                                           boolean activatorNeeded,
                                           int commitFrequency)
  {
    return openConnection(operatorID, resourceID, activatorNeeded, commitFrequency, null);
  }

  public ResourceConnection openConnection(OperatorID operatorID,
                                           String resourceID,
                                           boolean activatorNeeded,
                                           int commitFrequency,
                                           Object[] args)
  {
    
    if(_operatorConnections.containsKey(operatorID))
    {
      return _operatorConnections.get(operatorID);
      // throw new
      // RuntimeException("Operators are allowed to have only a single resource connection! Bad boy: "
      // + operatorID);
    }

    ResourceConnection cnn = openConnection(resourceID, args);
    _connections.get(cnn.getConnectionID()).associateOperator(operatorID);
    _operatorConnections.put(operatorID, cnn);
    
    // scope the connection and update the reference in the map
    AbstractConnection unresConn = _connections.remove(cnn.getConnectionID());
    cnn.getConnectionID().associate(operatorID.getScope());
    _connections.put(cnn.getConnectionID(), unresConn);
    unresConn.setCommitFrequency(commitFrequency);
    
    if(activatorNeeded)
    {
      AbstractExternalActivator activator =
          _resources.get(resourceID).getExternalActivator(operatorID);
      cnn.setActivator(activator);
    }

    return cnn;
  }
  
  public ResourceConnection openConnection(OperatorID operatorID,
                                           String resourceID,
                                           boolean activatorNeeded,
                                           Object[] args)
  {
    return openConnection(operatorID, resourceID, activatorNeeded, -1, args);
  }
  
  /**
   * This function should only be used in test cases! Operators should always use the function
   * above in order to associate the connection with the operator.
   * @param resourceID
   * @param args
   * @return
   */
  public ResourceConnection openConnection(String resourceID, Object... args)
  {
    if(!_resources.containsKey(resourceID))
    {
      throw new RuntimeException("No resource found with ID: " + resourceID);
    }
    
    AbstractResource resource = _resources.get(resourceID);
    AbstractConnection connection = resource.getConnection(args);
    connection.setParentResourceID(resourceID);
    _connections.put(connection.getConnectionID(), connection);
    ResourceConnection userConnection = connection.getRestrictedConnection();
    return userConnection;
  }

  public void registerResource(AbstractResource resource)
  {
    _resources.put(resource.getIdentifier(), resource);
  }
  
  public AbstractResource getResource(String resourceID)
  {
    return _resources.get(resourceID);
  }
  
  public void closeConnection(ConnectionID connectionID)
  {
    if(!_connections.containsKey(connectionID))
    {
      throw new IllegalArgumentException("No connection found with ID: "
                                         + connectionID.getIDInt());
    }
    
    AbstractConnection cnnToClose = _connections.remove(connectionID);
    try
    {
      cnnToClose.close();
    }
    catch(Throwable e)
    {
      e.printStackTrace();
    }
    
    if(cnnToClose.getAssociatedOperator() != null)
    {
      _operatorConnections.remove(cnnToClose.getAssociatedOperator());
    }
  }
  
  public void finishUnboundedConnection(ConnectionID connectionID)
  {
    if(!_connections.containsKey(connectionID))
    {
      throw new IllegalArgumentException("No connection found with ID: "
                                         + connectionID.getIDInt());
    }

    AbstractConnection cnnToFinish = _connections.get(connectionID);
    Assertion.invariant(cnnToFinish instanceof IUnboundedInput, "Connection was of type: " + cnnToFinish.getClass());
    try
    {
      ((IUnboundedInput)cnnToFinish).finish();
    }
    catch(Throwable e)
    {
      e.printStackTrace();
    }
    
  }

  public void closeAllConnections(ProcessID processID)
  {
    List<ConnectionID> connections = new ArrayList<ConnectionID>(_connections.keySet());
    for(ConnectionID connection : connections)
    {
      if(processID == null || connection.isInScope(processID))
      {
        closeConnection(connection);
      }
    }
  }

  public void loadResources()
  {
    loadResources(null);
  }

  // FIXME this is a problem when multiple processes are being run in one JVM! mapping back from
  // a connection to a resource is now impossible because it might have the wrong manager proxy.
  public void loadResources(ManagerProxy ohuaProcess)
  {
    ResourceConfigurationParser parser = new ResourceConfigurationParser(ohuaProcess);
    try
    {
      parser.loadResources();
    }
    catch(XMLParserException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  /**
   * This function is very critical! It should only be used if you intend to replace Ohua's
   * existing resource management algorithm to handle with side-effects to transactional
   * resources etc. So please think twice before calling this function in your code!
   * <p>
   * Once called this function will provide a view on the requested connection that provides
   * access to transactional control and management functions such as commit(), rollback() and
   * abort().
   * @param connectionID
   * @return
   */
  protected TransactionControl takeOverTransactionControl(ConnectionID connectionID)
  {
    if(!_connections.containsKey(connectionID))
    {
      throw new IllegalArgumentException("No connection found with ID: "
                                         + connectionID.getIDInt());
    }
    
    _logger.info("Giving away transaction control for connection: " + connectionID);
    return _connections.get(connectionID);
  }

  protected TransactionControl takeOverTransactionControl(OperatorID id)
  {
    if(!_operatorConnections.containsKey(id))
    {
      throw new IllegalArgumentException("No operator connection found for operator with ID: "
                                         + id);
    }

    _logger.info("Giving away transaction control for operator: " + id);
    return takeOverTransactionControl(_operatorConnections.get(id).getConnectionID());
  }

  /**
   * A source operator might wait for external resource callbacks. This is function for the
   * framework to activate such a listener.
   * @param operatorID
   */
  public void listen(OperatorID operatorID)
  {
    if(_operatorConnections.containsKey(operatorID))
    {
      if(_operatorConnections.get(operatorID).getActivator() != null)
      {
        _operatorConnections.get(operatorID).getActivator().listen();
      }
      else
      {
        _logger.fine("No activator found for connection: "
                         + _operatorConnections.get(operatorID).getConnectionID()
                         + " on operator: " + operatorID);
      }
    }
  }
  
  /**
   * Deactivate the listener for the external resource of this operator.
   * @param operatorID
   */
  public void unlisten(OperatorID operatorID)
  {
    if(_operatorConnections.containsKey(operatorID))
    {
      if(_operatorConnections.get(operatorID).getActivator() != null)
      {
        _operatorConnections.get(operatorID).getActivator().unlisten();
      }
    }
  }

  public boolean hasExternalActivators()
  {
    for(ResourceConnection cnn : _operatorConnections.values())
    {
      if(cnn.getActivator() != null)
      {
        return true;
      }
    }
    
    return false;
  }
  
  public void printExternalActivators(Logger logger)
  {
    for(ResourceConnection cnn : _operatorConnections.values())
    {
      if(cnn.getActivator() != null)
      {
        logger.info("activator: " + cnn.getActivator());
      }
    }
  }

  public boolean isOpenConnection(ConnectionID connectionID)
  {
    return _connections.containsKey(connectionID);
  }

  public OperatorID getAssociatedOperator(ConnectionID cnnID)
  {
    Assertion.invariant(_connections.containsKey(cnnID));
    return _connections.get(cnnID).getAssociatedOperator();
  }
  
  public AbstractResource getResource(ConnectionID cnnID)
  {
    if(!_connections.containsKey(cnnID))
    {
      throw new IllegalArgumentException("No connection found for ID: " + cnnID);
    }
    
    return getResource(_connections.get(cnnID).getParentResourceID());
  }

  public boolean exists(String resourceID) {
    return _resources.containsKey(resourceID);
  }
}
