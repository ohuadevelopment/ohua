/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine;

import java.util.LinkedList;

import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.sections.communication.ProcessObserver;

/**
 * An interface for all the functions provided to the user.
 */
public interface IExternalProcessManager extends ProcessObserver
{
  // FIXME feels to me like these two functions belong into a framework internal interface for
  // the process manager
  void runFlow();
  
//  void runFlowWithGraphAnalysis() throws Throwable;
//
//  void runGraphAnalysisAlgorithms();
  
  void initializeProcess();
  
  void finishComputation();
  
  void tearDownProcess();
  
  void awaitSystemPhaseCompletion() throws Throwable;

  void submitFlowInput(LinkedList<IMetaDataPacket> input);
}
