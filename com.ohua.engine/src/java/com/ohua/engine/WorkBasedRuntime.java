package com.ohua.engine;

import com.ohua.engine.flowgraph.elements.operator.*;
import com.ohua.engine.scheduler.AbstractScheduler;
import com.ohua.engine.scheduler.WorkBasedAsynchronousArc;
import com.ohua.engine.scheduler.WorkBasedTaskScheduler;
import com.ohua.engine.sections.SectionGraph;

/**
 * Created by sertel on 1/28/17.
 */
public class WorkBasedRuntime extends AbstractRuntime {

  public WorkBasedRuntime(RuntimeProcessConfiguration config) {
    super(config);
  }

  @Override
  protected AbstractOperatorRuntime createOperatorRuntime(OperatorCore op) {
    return new WorkBasedOperatorRuntime(op, _runtimeConfiguration);
  }

  @Override
  protected AbstractScheduler createScheduler() {
    return new WorkBasedTaskScheduler();
  }

  @Override
  protected AbstractArcImpl createArcImpl(Arc arc) {
    return new WorkBasedAsynchronousArc();
  }
}
