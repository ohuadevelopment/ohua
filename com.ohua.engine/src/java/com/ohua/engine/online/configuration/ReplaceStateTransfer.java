/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class ReplaceStateTransfer implements StateTransfer<UserOperator, UserOperator>
{
  private UserOperator _oldOp = null;
  
  public void setOldState(UserOperator oldOp){
    _oldOp = oldOp;
  }
  
  public void transfer(UserOperator newOp){
    transfer(_oldOp, newOp);
  }
  
  @Override public void transfer(UserOperator oldOp, UserOperator newOp) {
    // TODO
  }
  
}
