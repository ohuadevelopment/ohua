/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.AbstractPort;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.sections.communication.FlowStructureChange;

public abstract class ReplaceSubFlowChange
{
  
  public static void changeInput(SectionGraph secGraph, FlowGraph subGraph, OperatorCore operator,
                                 String[][] portReconnect)
  {
    // perform the reconnection
    for(String[] singlePortReconnect : portReconnect) {
      AbstractPort[][] oldConnections = FlowStructureChange.saveConnections(operator);
      FlowStructureChange.reconnectInput(FlowStructureChange.findInputPort(singlePortReconnect[0], operator),
                                         FlowStructureChange.findInputPort(singlePortReconnect[1], subGraph));
      OrphanHandler.handleOrphanedOperator(operator, oldConnections, secGraph);
    }
  }
  
  public static void handleOldOperator() {
    // TODO
  }
  
  public static void changeOutput(OperatorCore oldOp, OperatorCore newOp, String[][] portReconnect) {
    for(String[] singlePortReconnect : portReconnect) {
      FlowStructureChange.reconnectOutput(FlowStructureChange.findOutputPort(singlePortReconnect[0], oldOp),
                                          FlowStructureChange.findOutputPort(singlePortReconnect[1], newOp));
    }
  }

  public static void cloakSubFlowInputs(FlowGraph newGraph, String[][] reconnects) {
    for(String[] reconnect : reconnects) {
      InputPort inPort = FlowStructureChange.findInputPort(reconnect[1], newGraph);
      // the arc is overwritten with the reconnect
      inPort.setIncomingArc(new DynamicSourceArc());
    }
  }

  public static void cloakSubFlowOutputs(FlowGraph newGraph, String[][] reconnects) {
    for(String[] reconnect : reconnects) {
      FlowStructureChange.cloakOutput(newGraph, reconnect[1]);
    }
  }
  
  public static void transferNewData(OperatorCore oldOp, OperatorCore newOp, String[][] reconnects) {
    for(String[] reconnect : reconnects) {
      OutputPort oldOutPort = FlowStructureChange.findOutputPort(reconnect[0], oldOp);
      OutputPort newOutPort = FlowStructureChange.findOutputPort(reconnect[1], newOp);
      // the assumption here is that the mapping of the arcs from old to new is given by their
      // index in the array of outgoing arcs
      Assertion.invariant(oldOutPort.getOutgoingArcs().size() == newOutPort.getOutgoingArcs().size());
      for(int i = 0; i < oldOutPort.getOutgoingArcs().size(); i++) {
        newOutPort.getOutgoingArcs().get(i).transferTo(oldOutPort.getOutgoingArcs().get(i));
      }
    }
  }
  
  public static void uncloakSubFlowOutputs(OperatorCore newOp){
    for(Arc arc : newOp.getGraphNodeOutputConnections()){
      arc.detachFromSource();
    }    
  }
}
