/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

public interface StateTransfer<OLD, NEW>
{ 
  public void transfer(OLD oldOp, NEW newOp);
}
