/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.sections.MetaSection;
import com.ohua.engine.sections.SectionGraph;

/**
 * Attach the configuration operator between the Process Input operator and the User Graph
 * Entrance.
 * @author sertel
 * 
 */
public abstract class ConfigurationGraphRewrite
{
  
  public static void attachSystemComponents(SectionGraph secGraph)
  {
    OperatorCore entrance = secGraph.getUserGraphEntranceSection().getOperator();
    OperatorFactory opFactory = OperatorFactory.getInstance();
    OperatorCore op = opFactory.createSystemOperatorCore(ConfigurationOperator.class, "ReconfigInit");
    op.setOperatorName("reconfig-init");
    ((ConfigurationOperator)op.getOperatorAlgorithm()).setSectionGraph(secGraph);
    
    InputPort inPort = new InputPort(op);
    inPort.setPortName("input");
    inPort.setMetaPort(true);
    op.addInputPort(inPort);
    
    OutputPort outPort = new OutputPort(op);
    outPort.setPortName("output");
    outPort.setMetaPort(true);
    op.addOutputPort(outPort);
    
    MetaSection section = new MetaSection();
    section.createSingleOperatorSection(op);
    secGraph.addSystemSection(section);
    
    // reconnect
    Arc initialArc = entrance.getInputPort("process-control").getIncomingArc();
    initialArc.detachFromTarget();
    initialArc.setTargetPort(op.getInputPort("input"));
    Arc newArc = new Arc(op.getOutputPort("output"),entrance.getInputPort("process-control"));
//    SectionArc secArc = new SectionArc();
//    secArc.setMappedArc(newArc);
//    secGraph.addSystemArc(secArc);
  }
}
