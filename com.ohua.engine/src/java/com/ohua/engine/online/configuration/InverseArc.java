/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;

/*
 * FIXME There is a problem with this: it creates an endless loop! This is because the operator always schedules itself 
 * again and again although what we want is to just backoff. So dequeuing from that arc actually needs to be as if 
 * nothing ever happened. Apparently, this is not working because we do not get the engine to just do nothing.
 */
public class InverseArc extends Arc
{
  public OperatorCore getSource()
  {
    if(super.getSourcePort() == null)
    {
      return super.getTarget();
    }
    else
    {
      return super.getSource();
    }
  }
  
  public OperatorCore getTarget()
  {
    if(super.getTargetPort() == null)
    {
      return super.getSource();
    }
    else
    {
      return super.getTarget();
    }
  }
}
