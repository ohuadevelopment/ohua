/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.online.configuration;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.sections.communication.FlowStructureChange;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;

public abstract class DownstreamReconnectProtocol
{
    
  /**
   * @param subGraph
   * @param pendingReconnections
   * @return
   */
  public static List<IMetaDataPacket> initiateReconnectMarkers(SectionGraph secGraph,
                                                               FlowGraph subGraph,
                                                               JSONArray pendingReconnections)
  {
    try
    {
      List<IMetaDataPacket> markers = new ArrayList<IMetaDataPacket>();
      for(int i = 0; i < pendingReconnections.length(); i++)
      {
        // avoid shared memory on the list by just creating a marker for each reconnect request
        markers.add(createInputReconnectMarker(secGraph,
                                               subGraph,
                                               pendingReconnections.getJSONObject(i)));
      }
      return markers;
    }
    catch(JSONException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  /**
   * The input-reconnect marker carries only the reference to the outgoing arc that operator X
   * needs to be connected to. (Additionally, we need to attach the ID of the arc to be
   * disconnected otherwise it can not be found anymore by barely looking at the output port of
   * X which can have multiple outgoing arcs.)
   * @param subGraph
   * @param reconnect
   * @return
   * @throws JSONException
   */
  private static IMetaDataPacket createInputReconnectMarker(SectionGraph secGraph,
                                                            FlowGraph subGraph,
                                                            JSONObject reconnect) throws JSONException
  {
    JSONObject reconnectInfo = reconnect.getJSONObject("connection-info");    
    return PacketFactory.createConfigurationMarkerPacket(reconnect.getString("target-operator"),
                                                         reconnectInfo.getString("current-input"),
                                                         new Object[] { subGraph,
                                                                        reconnectInfo },
                                                         Category.RECONNECT_REQUEST);
  }
    
  public static void handleReconnectRequest(OperatorCore currentOperator,
                                                       IConfigurationMarker marker)
  {
    try
    {
      handleReconnection(currentOperator, marker);
    }
    catch(JSONException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  private static void handleReconnection(OperatorCore currentOperator,
                                         IConfigurationMarker marker) throws JSONException
  {
    FlowGraph subGraph = (FlowGraph) ((Object[]) marker.getValue())[0];
    JSONObject reconnect = (JSONObject) ((Object[]) marker.getValue())[1];
    
    // swap of cloaked input arc on "reconnect-to" and this port "current-input"
    InputPort inCurrent =
        FlowStructureChange.findInputPort(reconnect.getString("current-input"), currentOperator);
    
    InputPort inReconnect =
        FlowStructureChange.findInputPort(reconnect.getString("reconnect-to"), subGraph);
    inCurrent.getIncomingArc().reconnectTarget(inReconnect);
    
    // here the input port of the current op is not connected but that's fine since we are in a
    // port handler at the current op and hence nobody will perform any activation via this
    // port.
    
    // connect cloaked output arc from "new-input" to "current-input"
    OutputPort outNewIn =
        FlowStructureChange.findOutputPort(reconnect.getString("new-input"), subGraph);
    Assertion.invariant(outNewIn.getOutgoingArcs().size() == 1);
    outNewIn.getOutgoingArcs().get(0).setTargetPort(inCurrent);
    ((LazyActivationArcImpl) outNewIn.getOutgoingArcs().get(0).getImpl())._disabled = false;
  }
 
}
