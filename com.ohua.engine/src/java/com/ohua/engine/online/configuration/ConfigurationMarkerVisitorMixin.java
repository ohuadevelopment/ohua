package com.ohua.engine.online.configuration;

import com.ohua.engine.Maybe;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.InputPort.VisitorReturnType;
import com.ohua.engine.flowgraph.elements.packets.functionality.VisitorMixin;

public class ConfigurationMarkerVisitorMixin extends
                                            VisitorMixin<IConfigurationMarker, IConfigurationMarkerHandler>
{
  
  public ConfigurationMarkerVisitorMixin(InputPort in)
  {
    super(in);
  }
  
  @Override
  public void notifyHandlers(IConfigurationMarker packet)
  {
    for(IConfigurationMarkerHandler handler : getAllHandlers())
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }
  
  @Override
  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.CONFIGURATION_PACKET_ARRIVAL;
  }
  
  @Override
  public void handlePacket(IConfigurationMarker packet)
  {
    notifyHandlers(packet);
    
    _inputPort.setCurrentPacketToBeReturned(Maybe.empty());
    // the handler needs to decide for itself because it might request a re-schedule
    if(_inputPort.getVisitorReturnStatus() == VisitorReturnType.NOT_MY_BUISNESS)
    {
      _inputPort.setVisitorReturnStatus(VisitorReturnType.DEQUEUE_NEXT_PACKET);
    }    
  }
  
}
