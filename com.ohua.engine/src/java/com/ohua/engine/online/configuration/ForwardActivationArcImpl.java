/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.AsynchronousArcImpl;

public class ForwardActivationArcImpl extends AsynchronousArcImpl
{ 
  public ForwardActivationArcImpl(Arc arc) {
    super(arc);
  }

  protected void activateUpstream()
  {
    // never do that
  }  
}
