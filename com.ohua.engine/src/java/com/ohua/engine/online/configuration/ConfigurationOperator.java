/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.SystemOperator;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.sections.ConcurrentPipelineScheduling;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;

@Deprecated // redo online config
public class ConfigurationOperator extends SystemOperator implements IConfigurationMarkerHandler {
  
  private SectionGraph _secGraph = null;
  
  public void setSectionGraph(SectionGraph graph) {
    _secGraph = graph;
  }
  
  @Override
  public void prepare() {
    takeoverEvents(InputPortEvents.CONFIGURATION_PACKET_ARRIVAL);
  }
  
  @Override
  public void runProcessRoutine() {
    // TODO it seems that this is the common loop for any non-periodic system operator. hence we
    // might want to put this into the framework.
    while(getDataLayer().getInputPortController("input").next()) {
      // there is no data flowing in the system graph, so we should actually never enter here!
      Assertion.impossible();
    }
  }
  
  @Override
  public void cleanup() {
    // nothing
  }
  
  @Override
  public void addCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override
  public void removeCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override
  public void init() {
    // nothing
  }
  
  @Override
  public void notifyMarkerArrived(InputPort port, IConfigurationMarker packet) {
    System.out.println("Initializing subflow (offline) ...");
    switch(packet.getCategory()) {
      case EXCHANGE_FLOW:
      case EXTEND_FLOW:
        broadcast(prepareSubFlow(packet));
        break;
      case RECONNECT_REQUEST:
        Assertion.impossible();
        break;
      case REPLACE_INPUT:
        for(IMetaDataPacket marker : prepareUpdate(packet)) {
          broadcast(marker);
        }
        break;
      default:
        broadcast(packet);
    }
  }
  
  /**
   * Currently we give in real data structures which means that this can not be transfered to
   * other nodes in a cluster. There is another marker missing that carries this information to
   * the according node such that it can perform create these data structures.
   * @param packet
   * @return
   */
  private List<IMetaDataPacket> prepareUpdate(IConfigurationMarker packet) {
    RuntimeProcessConfiguration config = (RuntimeProcessConfiguration) ((Object[]) packet.getValue())[0];
    FlowGraph newGraph = (FlowGraph) ((Object[]) packet.getValue())[1];
    String[][] portReconnects = (String[][]) ((Object[]) packet.getValue())[2];
    @SuppressWarnings("unchecked") Map<String, String> newToOld =
        (Map<String, String>) ((Object[]) packet.getValue())[3];
    @SuppressWarnings("rawtypes")
    StateTransfer transfer = (StateTransfer) ((Object[]) packet.getValue())[4];
    
    // block all ops whose state needs to be transfered (at the very least we have to block
    // the last one) by creating new input ports and arcs to receive the "activation marker"
    Map<String, Object[]> activationMarkers =
        constructOperatorPreservationInfrastructure(newToOld, transfer, newGraph);
    
    // section graph preparation
    SubGraphLoader.loadSections(super.getOperatorID(),
                                config,
                                newGraph,
                                _secGraph);
    
    // with the addition of the state transfer activations, ops are activated as soon as the
    // state transfer marker arrives. hence, at that moment the sub flow must be ready to run.
    // if the input ports are not connect then skimming those ports will fail with an NPE. so we
    // make it a valid graph.
    ReplaceSubFlowChange.cloakSubFlowInputs(newGraph, portReconnects);
    
    // the outputs of the new flow graph are not connected yet, but we nevertheless allow it to
    // run. so make sure the output has a place to go.
    ReplaceSubFlowChange.cloakSubFlowOutputs(newGraph, ((UpdateMarker) packet.getExtension()).getReconnects());
    
    // create a new extension
    UpdateMarker extension = (UpdateMarker) packet.getExtension();
    IConfigurationMarker newExtension =
        PacketFactory.createUpdateMarkerPacket(extension.getOperatorID(),
                                               extension.getCategory(),
                                               extension.getNewGraph(),
                                               extension.getReconnects(),
                                               activationMarkers);
    
    // create as many marker as there are ops that need to be reconnected. (a marker join will
    // ignite the final output replace.)
    Map<String, List<String[]>> opsToReconnect = new HashMap<>();
    for(String[] portReconnect : portReconnects) {
      String opName = FlowGraph.parsePortReference(portReconnect[0])[0];
      if(!opsToReconnect.containsKey(opName)) opsToReconnect.put(opName, new ArrayList<String[]>());
      opsToReconnect.get(opName).add(portReconnect);
    }
    
    List<IMetaDataPacket> markers = new ArrayList<>();
    for(Map.Entry<String, List<String[]>> entry : opsToReconnect.entrySet()) {
      String[][] r = entry.getValue().toArray(new String[entry.getValue().size()][]);
      System.out.println("Calculated target op: " + entry.getKey());
      markers.add(PacketFactory.createUpdateMarkerPacket(entry.getKey(),
                                                         Category.REPLACE_INPUT,
                                                         newExtension,
                                                         newGraph,
                                                         r));
    }
    return markers;
  }
  
  private Map<String, Object[]> constructOperatorPreservationInfrastructure(Map<String, String> newToOld,
                                                                            @SuppressWarnings("rawtypes") StateTransfer transfer,
                                                                            FlowGraph newGraph)
  {
    Map<String, Object[]> preserveInfo = new HashMap<>();
    for(Map.Entry<String, String> entry : newToOld.entrySet()) {
      OperatorCore newOp = newGraph.getOperator(entry.getKey());
      OperatorCore oldOp = _secGraph.findOperator(entry.getValue());
      
      /*
       * 1. step: construct the additional input ports and add them to the new ops. the input
       * port needs to have a single online-reconfig-marker-handler!
       */
      InputPort inPort = new InputPort(newOp);
      inPort.setPortName("update-sync");
      newOp.addInputPort(inPort);
      ConfigurationMarkerVisitorMixin visitor = new ConfigurationMarkerVisitorMixin(inPort);
      // the marker that arrives on this port is an operator update which does not perform any
      // changes to the graph!
      // FIXME needs the runtime
//      visitor.registerMarkerHandler(new OnlineConfigurationMarkerHandler(OnlineConfigurationMarkerHandler.aquireFeatures(newOp),
//                                                                         null));
      inPort.registerPacketVisitor(visitor);
      
      /*
       * 2. step: construct arcs with forward activation only
       */
      Arc arc = new DynamicSourceArc();
      arc.setImpl(new ForwardActivationArcImpl(arc));
      arc.setTargetPort(inPort);
      // FIXME needs the runtime
//    arc.registerArcEventListener(new ConcurrentPipelineScheduling(arc, _secGraph.getActivationService()));
      
      /*
       * 3. step: create update markers that are sent via these arcs and transfer the state.
       * these state handlers must be able to also correct the state of the schema matcher
       * accordingly.
       */
      preserveInfo.put(oldOp.getOperatorName(), new Object[] { arc,
                                                              transfer });
      
      /*
       * 4. step: later we send this marker before the operator is connected and therewith valid
       * to run. hence, we need to make it a valid operator already be connecting the
       * unconnected input ports with empty arcs. -> see calling function.
       */
      
    }
    return preserveInfo;
  }
  
  private IConfigurationMarker prepareSubFlow(IConfigurationMarker packet) {
    
    Object[] graphs =
        SubGraphLoader.loadSections(packet,
                                    super.getOperatorID(),
                                    packet.getPropertyName(),
                                    _secGraph);
    
    IConfigurationMarker result =
        PacketFactory.createConfigurationMarkerPacket(packet.getOperatorID(),
                                                      packet.getPropertyName(),
                                                      new Object[] { graphs,
                                                                    packet.getValue() },
                                                      packet.getCategory(),
                                                      packet.getExtension());
    return result;
  }
  
  @SuppressWarnings("unused")
  private void prepareUserGraphEntranceReconnect() {
    // TODO
  }
  
  @SuppressWarnings("unused")
  private void prepareUserGraphExitReconnect() {
    // TODO
  }
  
}
