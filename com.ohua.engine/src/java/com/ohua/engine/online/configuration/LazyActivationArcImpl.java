/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.AsynchronousArcImpl;

public class LazyActivationArcImpl extends AsynchronousArcImpl
{
  // TODO we can actually exchange this arc implementation for a normal one. this needs to be
  // triggered in the first step of the DownstreamReconnectProtocol. There we actually have the
  // chance to switch the arc implementation, I believe.
  public boolean _disabled = true;
  
  // note that this actually would need to be an atomic boolean but loosing one or two
  // notifications is not too bad for us.
  
  public LazyActivationArcImpl(Arc arc)
  {
    super(arc);
  }
  
  protected void activateUpstream()
  {
    if(!_disabled)
    {
      super.activateUpstream();
    }
  }
  
  protected void activateDownstream()
  {
    if(!_disabled)
    {
      super.activateDownstream();
    }
  } 
}
