/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.online.configuration;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import com.ohua.engine.Maybe;
import com.ohua.engine.flowgraph.elements.operator.*;
import org.json.JSONArray;

import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.IDataPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.InputPort.VisitorReturnType;
import com.ohua.engine.flowgraph.elements.packets.ExclusivePacketForward;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.sections.communication.FlowStructureChange;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;
import com.ohua.engine.operators.system.UserGraphEntranceOperator;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.engine.utils.ReflectionUtils;

public class OnlineConfigurationMarkerHandler implements IConfigurationMarkerHandler, IDataPacketHandler

{
  private static class ExchangeOperator extends OhuaOperator {
    private NotificationBasedOperatorRuntime _runtime;
    private OperatorCore _core = null;
    
    protected ExchangeOperator(AbstractOperatorRuntime runtime) {
      super(runtime);
      _runtime = (NotificationBasedOperatorRuntime) runtime;
      _core = runtime.getOp();
    }
    
    protected void exchangeOperator(UserOperator newOp) {
      OperatorFactory.getInstance().replaceUserOperator(_core, newOp);
      // we need to do this here because the prepare routine might actually want to access the
      // core operator features (to acquire connections or port handlers and such).
      newOp.prepare();
      // TODO Should we shut down the old operator at this point? -> this has something to do
      // with the case that the update fails!
    }
    
    @SuppressWarnings({ "unchecked",
     "rawtypes" })
    protected void exchangeOperator(UserOperator newOp, StateTransfer t) {
      UserOperator oldOp = OperatorFactory.getInstance().replaceUserOperator(_core, newOp);
      // we transfer state before we run or even prepare this operator because even preparation
      // might require some compilation info or such.
      t.transfer(oldOp, newOp);
      // we need to do this here because the prepare routine might actually want to access the
      // core operator features (to acquire connections or port handlers and such).
      newOp.prepare();
      // TODO Should we shut down the old operator at this point? -> this has something to do
      // with the case that the update fails!
    }
  }
  
  private ExclusivePacketForward _forwarder = null;
  private ExchangeOperator _operator = null;
  
  // FIXME this is actually shared memory!!! either this thing becomes protected by locks or we
  // need another approach to apply changes to this thing.
  // Currently it is used to get access to the scheduler of the target section and to register
  // newly created operators, arcs and sections. The latter can probably be done in a system
  // operator that we plan anyways for initialization of new sub flows and tear down of
  // exchanged ones! OR we get rid of the list and make functions like getAllArcs() to be
  // services.
  
  // FIXME As a matter of fact, is this thing actually something that is used somewhere else
  // during runtime??? -> The only runtime entity using it is the SectionScheduler, but only
  // during initialization. It is not used during runtime for decision making or such!
  private SectionGraph _secGraph = null;
  
  public OnlineConfigurationMarkerHandler(ExchangeOperator op, SectionGraph sectionGraph) {
    _operator = op;
    _secGraph = sectionGraph;
    _forwarder = new ExclusivePacketForward(_operator);
  }
  
  @Override
  public void addCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override
  public void removeCallback(InputPortEvents event, InputPort port) {
    // nothing
  }
  
  @Override
  public void init() {
    // nothing
  }
  
  @Override
  public void notifyMarkerArrived(InputPort port, IConfigurationMarker packet) {
    // FIXME put into its own handler!
    if(_operator.getUserOperator() instanceof UserGraphEntranceOperator) {
      // When we prepared in an online flow graph then this is the right point to create the
      // connection to the entrance. but make sure we do not send this marker down the new
      // connection!
      _forwarder.broadcast(port, packet);
      // switch condition for backward compatibility
      if((Arrays.asList(new Category[] { Category.EXCHANGE_FLOW,
                                        Category.EXTEND_FLOW }).contains(packet.getCategory()) && ((Object[]) packet.getValue())[0] instanceof String))
      {
        System.out.println("Connecting system input op to entrance ...");
        @SuppressWarnings("unchecked") Set<OperatorCore> systemOutputOps =
            (Set<OperatorCore>) ((Object[]) ((Object[]) packet.getValue())[0])[3];
        SubGraphLoader.connectSystemOutputOperator(systemOutputOps, _operator._core);
      }
    } else {
      // FIXME We really have to perform a marker join here!
      
      boolean isTarget = _operator.getUserOperator().getOperatorName().equals(packet.getOperatorID());
      if(isTarget) {
        System.out.println("Reconfiguration request received at operator "
                           + _operator.getUserOperator().getOperatorName());
      }
      IConfigurationMarker extension = packet.getExtension();
      switch(packet.getCategory()) {
        case PROPERTY:
          if(isTarget) reconfigureProperty(packet);
          break;
        case OPERATOR:
          if(isTarget) exchangeOperator(port, packet);
          break;
        case EXCHANGE_FLOW:
          if(isTarget) exchangeFlow(packet);
          break;
        case EXTEND_FLOW:
          if(isTarget) extendFlow(packet);
          break;
        case RECONNECT_REQUEST:
          if(isTarget) reconnectFlow(packet);
          break;
        case REPLACE_INPUT:
          if(isTarget) extension = replaceInput((UpdateMarker) packet);
          break;
        case REPLACE_OUTPUT:
          replaceOutput((UpdateMarker) packet, isTarget);
          break;
        case REPLACE_TRANSFER:
          replaceTransfer(port, packet);
          break;
        case REPLACE_FINALIZE:
          replaceFinalize(port, packet);
          break;
        default:
          break;
      }
      
      if(isTarget && extension != null) {
        _operator.broadcast(extension);
      }
      
      if(!isTarget) {
        _forwarder.broadcast(port, packet);
      }
    }
  }
  
  private IConfigurationMarker replaceInput(UpdateMarker packet) {
    // we have to broadcast the output-replace before we cloak/close the outgoing arc(s)
    _operator.broadcast(packet.getExtension());
    
    // this op's state might also need to be preserved -> we send this before we change the
    // input port to the old one in order to make sure that the state transfer will be run
    // before any data processing happens. (this is true because operators handle meta data
    // first.)
    handleStateTransfer(((UpdateMarker) packet.getExtension()).getTransfers());
    
    // reconnect this input operator -> after this call the operator will receive data and start
    // processing
    ReplaceSubFlowChange.changeInput(_secGraph, packet.getNewGraph(), _operator._core, packet.getReconnects());
    
    // FIXME this actually removes the whole section which we can not perform yet
    // OrphanHandler.deleteOrphanedOperator(_operator._core, _secGraph);
    System.out.println("Input replace performed!");
    
    // we broadcasted the extension marker already as the first step
    return null;
  }
  
  private void replaceOutput(UpdateMarker packet, boolean isTarget) {
    // TODO marker join necessary here!
    
    // send marker to initiate the state transfer
    handleStateTransfer(packet.getTransfers());
    
    // the reconnect must happen on the new op because new op might be running when we are
    // reconnecting here!
    if(isTarget) {
      Assertion.invariant(packet.getTransfers().containsKey(_operator._core.getOperatorName()));
      Object[] info = packet.getTransfers().get(_operator._core.getOperatorName());
      Arc arc = (Arc) info[0];
      
      String newOpName = arc.getTarget().getOperatorName();
      IMetaDataPacket updateMarker =
          PacketFactory.createConfigurationMarkerPacket(newOpName,
                                                        "operator",
                                                        new Object[] { _operator._core,
                                                                      packet.getReconnects() },
                                                        Category.REPLACE_FINALIZE);
      LinkedList<IMetaDataPacket> l = new LinkedList<IMetaDataPacket>();
      l.add(updateMarker);
      arc.enqueueBatch(l);
      System.out.println("Notificiation to the new output op sent.");
    }
    
    // FIXME this actually removes the whole section which we can not perform yet
    // OrphanHandler.deleteOrphanedOperator(_operator._core, _secGraph);
  }
  
  private void handleStateTransfer(Map<String, Object[]> transfers) {
    if(transfers.containsKey(_operator._core.getOperatorName())) {
      Object[] info = transfers.get(_operator._core.getOperatorName());
      Arc arc = (Arc) info[0];
      ReplaceStateTransfer transfer = (ReplaceStateTransfer) info[1];
      String newOpName = (String) arc.getTarget().getOperatorName();
      
      // get the current state of this operator!
      transfer.setOldState((UserOperator) _operator.getUserOperator());
      
      IMetaDataPacket updateMarker =
          PacketFactory.createConfigurationMarkerPacket(newOpName, "operator", transfer, Category.REPLACE_TRANSFER);
      
      LinkedList<IMetaDataPacket> l = new LinkedList<IMetaDataPacket>();
      l.add(updateMarker);
      arc.enqueueBatch(l);
    }
  }
  
  private void replaceFinalize(InputPort port, IConfigurationMarker packet) {
    System.out.println("Finalizer received!");
    OperatorCore oldOp = (OperatorCore) ((Object[]) packet.getValue())[0];
    String[][] reconnects = (String[][]) ((Object[]) packet.getValue())[1];
    
    // transfer results that might have been already created to the arcs of the original
    // outgoing arcs that will be connect to this operator in the next step
    ReplaceSubFlowChange.transferNewData(oldOp, _operator._core, reconnects);
    
    // remove the cloaked arcs again from the op
    ReplaceSubFlowChange.uncloakSubFlowOutputs(_operator._core);
    
    // output reconnect
    ReplaceSubFlowChange.changeOutput(oldOp, _operator._core, reconnects);
    
    // finally activate this op
    _operator._runtime.activateOperator(_operator._runtime);
    System.out.println("Output replace performed!");
  }
  
  /**
   * This packet carries the state from the old operator. We need to apply this state to the new
   * operator (like in an operator update) and finally evict the input port that this marker
   * arrived upon (like in an operator structure update).
   * @param packet
   */
  private void replaceTransfer(InputPort inPort, IConfigurationMarker packet) {
    // FIXME for the output op, this must be performed *after* it has received the reconnect
    // marker!
    ReplaceStateTransfer transfer = (ReplaceStateTransfer) packet.getValue();
    
    // remove the "replace" input port
    inPort.getOwner().removeInputPort(inPort);
    
    // update the operator state
    transfer.transfer((UserOperator) _operator.getUserOperator());
  }
  
  /**
   * A simple request for reconnecting one of the input connections of this operator.
   * @param packet
   */
  private void reconnectFlow(IConfigurationMarker packet) {
    DownstreamReconnectProtocol.handleReconnectRequest(_operator._core, packet);
    GraphVisualizer.printSectionGraph(_secGraph);
  }
  
  /**
   * Instead of exchanging operators or subflows, we just hook into the flow. <br>
   * @param packet
   */
  private void extendFlow(IConfigurationMarker packet) {
    System.out.println("Flow extension requested ...");
    Object[] graphs = null;
    Object reconnectionSpec = null;
    if(packet.getValue() instanceof Object[]) {
      graphs = (Object[]) ((Object[]) packet.getValue())[0];
      reconnectionSpec = ((Object[]) packet.getValue())[1];
    } else {
      graphs =
          SubGraphLoader.loadSections(packet,
                                      _operator.getId(),
//                                      _secGraph.findParentSection(_operator._core.getId()).getSectionObserver(),
                                      (String) packet.getPropertyName(),
                                      _secGraph);
      reconnectionSpec = packet.getValue();
    }
    
    SubGraphLoader.connectSystemOperators(_secGraph, (FlowGraph) graphs[0]);
    
    JSONArray pendingReconnections =
        FlowStructureChange.extendFlow(_secGraph, (FlowGraph) graphs[0], _operator._core, reconnectionSpec);
    List<IMetaDataPacket> markers =
        DownstreamReconnectProtocol.initiateReconnectMarkers(_secGraph,
                                                             (FlowGraph) graphs[0],
                                                             pendingReconnections);
    System.out.println("New flow inserted. Output reconnection pending ...");
    for(IMetaDataPacket marker : markers) {
      System.out.println("Sending reconnection marker downstream ... ");
      _operator.broadcast(marker);
    }
    if(markers.isEmpty()) {
      GraphVisualizer.printSectionGraph(_secGraph);
    }
  }
  
  /**
   * Here we actually exchange this operator with a completely new flow part.
   * @param packet
   */
  private void exchangeFlow(IConfigurationMarker packet) {
    FlowGraph newGraph = null;
    SectionGraph newSecGraph = null;
    String[][] reconnectionSpec = null;
    // switch condition for backward compatibility
    if(packet.getValue() instanceof Object[]) {
      Object[] graphs = (Object[]) ((Object[]) packet.getValue())[0];
      reconnectionSpec = (String[][]) ((Object[]) packet.getValue())[1];
      newGraph = (FlowGraph) graphs[0];
      newSecGraph = (SectionGraph) graphs[1];
    } else {
      Object[] graphs =
          SubGraphLoader.loadSections(packet,
                                      _operator.getId(),
                                      packet.getPropertyName(),
                                      _secGraph);
      newGraph = (FlowGraph) graphs[0];
      newSecGraph = (SectionGraph) graphs[1];
      reconnectionSpec = (String[][]) packet.getValue();
    }
    // TODO we have to signal this operator that there will be no more data arriving, so it
    // flushes its state to its outgoing arcs! Therefore, we need to send an EOS through its
    // input port, that gets dequeued next. as a final step we really want to make sure that
    // this operator is flushed so it needs to disregard the boundary on the outgoing arc.
    // Hence, it feels a bit like this algorithm wants to be implemented in two parts: when the
    // configuration marker arrives on the input port then we do everything related to the input
    // side. then we also register to EOS-events on the output port and only when this event
    // occurs, we actually want to do all the replacement!
    AbstractPort[][] oldConnections =
        FlowStructureChange.exchange(_secGraph, newGraph, _operator._core, reconnectionSpec, newSecGraph);
    OrphanHandler.handleOrphanedOperator(_operator._core, oldConnections, _secGraph);
    OrphanHandler.deleteOrphanedOperator(_operator._core, _secGraph);
    GraphVisualizer.printSectionGraph(_secGraph);
    System.out.println("Rewrite finished!");
  }
  
  /**
   * Clearly the operator must have the same structure as the one that is being replaced.
   * 
   * @param packet
   */
  @SuppressWarnings("rawtypes")
  private void exchangeOperator(InputPort inPort, IConfigurationMarker packet) {
    Object payload = packet.getValue();
    if(payload instanceof UserOperator) {
      _operator.exchangeOperator((UserOperator) packet.getValue());
    } else {
      Assertion.invariant(payload instanceof Object[]);
      Assertion.invariant(((Object[]) payload).length == 2);
      Assertion.invariant(((Object[]) payload)[0] instanceof UserOperator);
      Assertion.invariant(((Object[]) payload)[1] instanceof StateTransfer);
      _operator.exchangeOperator((UserOperator) ((Object[]) payload)[0], (StateTransfer) ((Object[]) payload)[1]);
    }
    
    System.out.println("Operator '" + _operator.getUserOperator().getOperatorName() + "' exchanged! \n request: "
                       + _request);
    // we need to enforce an operator scheduling cycle because the "old" operator might still be
    // on the call stack!
    inPort.setCurrentPacketToBeReturned(Maybe.empty());
    inPort.setVisitorReturnStatus(VisitorReturnType.PACKET_WAS_HANDLED);
    _operator._runtime.yield();
  }
  
  private void reconfigureProperty(IConfigurationMarker packet) {
    String[] elements = packet.getPropertyName().split("\\.");
    Object target = _operator.getUserOperator();
    int i = 0;
    while(i < elements.length - 1) {
      target = ReflectionUtils.getProperty(target, elements[i], target.getClass());
      i++;
    }
    ReflectionUtils.setProperty(target, elements[elements.length - 1], packet.getValue());
  }
  
  public static ExchangeOperator aquireFeatures(AbstractOperatorRuntime core) {
    return new ExchangeOperator(core);
  }
  
  /*
   * DEBUGGING
   */
  private String _request = "";
  
  @Override
  public void notifyMarkerArrived(InputPort port, DataPacket packet) {
    _request = packet.getData().toString();
  }
  
}
