/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.online.configuration;

import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.flowgraph.elements.packets.ISpecialMetaDataPacket;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;

/**
 * Note, a general configuration marker can be a fast traveler. However, when used in
 * combination with another configuration marker to perform changes on two depending operators then fast traveling
 * can only be done once the first operator was hit. On the rest of the way, the second marker
 * has to travel in order with the data to preserve consistency.
 * @author sertel
 * 
 */
public interface IConfigurationMarker extends
//                                     FastTraveler,
                                     IMetaDataPacket,
                                     ISpecialMetaDataPacket
{
  public String getOperatorID();
  
  public Category getCategory();
  
  public String getPropertyName();
  
  public Object getValue();
  
  public IConfigurationMarker getExtension();  
}
