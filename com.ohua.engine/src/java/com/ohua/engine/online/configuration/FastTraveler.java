/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.online.configuration;

// FIXME this probably wants to be located somewhere else!
public interface FastTraveler
{
  // this is a marker interface for packets to be propagated with higher priority
}
