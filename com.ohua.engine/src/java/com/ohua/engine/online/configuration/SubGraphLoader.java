/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.*;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.*;
import com.ohua.engine.sections.*;

import java.util.*;

/**
 * Captures the functionality to load a subflow and initialize it.
 *
 * @author sertel
 */
// FIXME I'm still not 100% happy here. What we do here is to register the new sections at the
// scheduler. This should be made more explicit. It seems there is an interface for updating
// scheduler information missing.
  @Deprecated // this implementation is too tied to the scheduling in the system -> redo!
public abstract class SubGraphLoader {
  public static ProcessRunner doLoad(String pathToFlowAndConfig) {
    String[] paths = pathToFlowAndConfig.split(":");
    String pathToFlow = paths[0];
    ProcessRunner runner = new ProcessRunner(pathToFlow);
    if (paths.length > 1) {
      try {
        System.out.println("LOADING runtime configuration...");
        runner.loadRuntimeConfiguration(paths[1]);
      } catch (Exception e) {
        // something happened during configuration loading. report and skip over it.
        e.printStackTrace();
      }
    }
    return runner;
  }

  private static ProcessRunner doLoad(String pathToFlowAndConfig, IConfigurationMarker marker) {
    String[] paths = pathToFlowAndConfig.split(":");
    if (paths[0].length() == 0) {
      Object[] values = (Object[]) marker.getValue();
      ProcessRunner runner = (ProcessRunner) values[0];
      Object[] newValues = new Object[values.length];
      System.arraycopy(values, 1, newValues, 0, newValues.length);
      ((Object[]) marker.getValue())[0] = ((Object[]) marker.getValue())[1];
      return runner;
    } else {
      String pathToFlow = paths[0];
      ProcessRunner runner = new ProcessRunner(pathToFlow);
      if (paths.length > 1) {
        try {
          System.out.println("LOADING runtime configuration...");
          runner.loadRuntimeConfiguration(paths[1]);
        } catch (Exception e) {
          // something happened during configuration loading. report and skip over it.
          e.printStackTrace();
        }
      }
      return runner;
    }
  }

  public static Object[] loadSections(IConfigurationMarker marker, OperatorID current,
                                      String pathToFlowAndConfig, SectionGraph secGraph) {
    ProcessRunner runner = doLoad(pathToFlowAndConfig, marker);
    return loadSections(current,
            runner.getConfig(),
            ((AbstractProcessManager) runner.getProcessManager()).getProcess().getGraph(),
            secGraph);
  }

  /**
   * @param current
//   * @param pathToFlowAndConfig
   * @param secGraph
   * @return the new flow graph and the associated section graph
   */
  public static Object[]
  loadSections(OperatorID current, RuntimeProcessConfiguration config,
               FlowGraph newGraph, SectionGraph secGraph) {
    DataFlowProcess process = new DataFlowProcess();
    process.setGraph(newGraph);
    AbstractProcessManager manager = config.getProcessManager(process);
//    manager.getProcess().setSectionGraph(secGraph);
    // FIXME can't support this anymore do to refactoring in runtime/scheduler
//    manager.initializeFlowGraph();
    
    /*
     * TODO: Whatever strategy chosen, the resulting flow graph will be inserted as a new
     * section. this requires during reconnection to detect whether the arcs to be reconnected
     * where previously inter-section arcs and if not turn them into inter-section arcs.
     */
    Assertion.invariant(secGraph.getComputationalSections().size() > 1,
            "Currently, we do not support updates for single threaded executions, sorry.");
    AbstractSectionGraphBuilder builder = null;
    switch (config.getExecutionMode()) {
      case MULTI_THREADED:
        builder = config.getSectionStrategy();
        break;
      case SINGLE_THREADED:
        builder = new SingleSectionMapper(config);
        break;
    }

    SectionGraph graph = handleSectionSetup(secGraph, manager, builder);
    changeOperatorOwnerships(manager.getProcess().getGraph().getContainedGraphNodes(), current);
    performInitialSectionManagement(secGraph, graph);

    enterComputationState(manager);

    return new Object[]{manager.getProcess().getGraph(),
            graph};
  }

  private static void enterComputationState(AbstractProcessManager manager) {
    // FIXME can't support this anymore do to refactoring in runtime/scheduler
//    manager.getProcess().getGraph().getContainedGraphNodes()
//            .stream()
//            .forEach(op -> Assertion.invariant(op.getOperatorState() == OperatorStateMachine.OperatorState.WAITING_FOR_COMPUTATION));
//    manager.getProcess().getGraph().getContainedArcs().stream().forEach(arc -> Assertion.invariant(arc.isQueueEmpty()));
//
//    manager.enterComputationState();
    // FIXME this is such a bad hack!
    // TODO create a valid graph and perform the activation via the scheduler/execution instead of trying to drive the operator state machine sequentially.
//    manager.getProcess().getGraph().getContainedGraphNodes().
//            forEach(op -> {
//              List<InputPort> unconnectedInputPorts =
//                      op.getInputPorts().stream().filter(inPort -> !inPort.hasIncomingArc()).collect(Collectors.toList());
//              unconnectedInputPorts.stream().forEach(inPort -> new Arc(new OutputPort(null), inPort));
//              List<OutputPort> unconnectedOutputPorts =
//                      op.getOutputPorts().stream().filter(outPort -> outPort.getOutgoingArcs().isPresent()).collect(Collectors.toList());
//              unconnectedOutputPorts.stream().forEach(outPort -> new Arc(outPort, new InputPort(null)));
//
//              op.getGraphNodeInputConnections().stream().
//                      forEach(inArc -> {
//                        inArc.disableDownstreamActivation();
//                        inArc.enqueueBatch(new LinkedList<>(Collections.singletonList(PacketFactory.createActivationMarkerPacket(SystemPhaseType.COMPUTATION))));
//                      });
//              op.runOperatorStep();
//              Assertion.invariant(op.getOperatorState() == OperatorStateMachine.OperatorState.EXECUTING_META_DATA);
//              Function<OperatorCore, Boolean> callConvention = op.getCallConvention();
//              op.defineCallingConvention(o -> {
//                // shutdown activation here because it got enable just before this is called. (hacky, I know but ...!)
//                o.getGraphNodeInputConnections().stream().forEach(Arc::disableUpstreamActivation);
//                o.getGraphNodeOutputConnections().stream().forEach(Arc::disableDownstreamActivation);
//                o.getGraphNodeOutputConnections().stream().forEach(Arc::disableUpstreamActivation);
//                return false;
//              });
//              op.runOperatorStep();
//              op.defineCallingConvention(callConvention);
//              Assertion.invariant(op.getOperatorState() == OperatorStateMachine.OperatorState.EXECUTING_EPILOGUE,
//                      () -> "" + op.getOperatorName() + " " + op.getOperatorState());
//              Assertion.invariant(op.getBackOffReason() == OperatorStateMachine.BackoffReason.NULL_DEQUEUE,
//                      () -> "" + op.getOperatorName() + " " + op.getBackOffReason());
//              op.getGraphNodeOutputConnections().stream().forEach(outArc -> {
//                        Assertion.invariant(outArc.getLoadEstimate() == 1);
//                        IStreamPacket p = outArc.getData();
//                        Assertion.invariant(p instanceof ActivationMarker);
//                      }
//              );
//              op.runOperatorStep();
//
//              unconnectedInputPorts.stream().forEach(inPort -> inPort.getIncomingArc().detachFromTarget());
//              unconnectedOutputPorts.stream().forEach(outPort -> outPort.getOutgoingArcs().clear());
//            });

    // FIXME can't support this anymore do to refactoring in runtime/scheduler
//    manager.getProcess().getGraph().getContainedGraphNodes()
//            .stream()
//            .forEach(op -> Assertion.invariant(op.getOperatorState() == OperatorStateMachine.OperatorState.WAITING_FOR_DATA,
//                    () -> op.getOperatorName() + " : " + op.getOperatorState()));
    manager.getProcess().getGraph().getContainedArcs().stream().forEach(arc -> Assertion.invariant(arc.isQueueEmpty()));
  }

  private static SectionGraph handleSectionSetup(SectionGraph secGraph,
                                                 AbstractProcessManager manager,
                                                 AbstractSectionGraphBuilder builder) {
    // FIXME Does not work because there are no source operators in the graph! (maybe easier for
    // now to just build own sections for each operator here!)
    // SectionGraph graph = builder.build(manager.getProcess().getGraph());

    // TODO this does not support single-threaded configurations! in such a case the operators
    // must be added to the single section!
    SectionGraph graph = new SectionGraph();
    List<Section> sections = new ArrayList<>();
    for (OperatorCore op : manager.getProcess().getGraph().getContainedGraphNodes()) {
      // System.out.println("Creating section for: " + op);
      Section section = builder.createSingleOpSection(op);
      // this sets the operator scheduler to the operators.
      // FIXME below code need for section scheduler
//      section.startNewSystemPhase();
      sections.add(section);
    }
    graph.setComputationalSections(sections);

//    List<SectionArc> sArcs = new ArrayList<SectionArc>();
//    for (Arc arc : manager.getProcess().getGraph().getContainedArcs()) {
//      SectionArc a = new SectionArc();
//      a.setMappedArc(arc);
//      sArcs.add(a);
//    }

    // FIXME below code need for section scheduler
//    manager.configureScheduling(graph);

//    manager.setUpInterSectionArcs(sArcs);
    // FIXME can't support this anymore do to refactoring in runtime/scheduler
//    manager.applyCustomArcConfiguration();

    // TODO this sets up pipeline activation even for inner-section arcs! too many notifications
    // might be the result. we should only set this up for inter-section arcs again.
    // make sure pipeline activation works
    for (Arc arc : manager.getProcess().getGraph().getContainedArcs()) {
      // if(arc.getTargetPort() != null)
      // System.out.println("Activating pipeline scheduling for arc:(target) " +
      // arc.getTarget());
      // else
      // System.out.println("Activating pipeline scheduling for arc:(source) " +
      // arc.getSource());
      // FIXME needs the runtime
//      arc.registerArcEventListener(new ConcurrentPipelineScheduling(arc, secGraph.getActivationService()));
    }

    // the exit is stateful, so restart it!
    // ((UserGraphExitOperator)exit.getOperatorAlgorithm()).restart();

    return graph;
  }

  /**
   * Transfer the operators and arcs into this flow graph.
   *
   * @param newOps
   * @param oldOp
   */
  private static void changeOperatorOwnerships(List<OperatorCore> newOps, OperatorID oldOp) {
    for (OperatorCore op : newOps) {
      op.getId().associate(oldOp.getScope());
    }
  }

  /**
   * Just transfer the sections and its arcs from the new graph to the old graph.
   *
   * @param secGraph
   * @param subSecGraph
   */
  private static void performInitialSectionManagement(SectionGraph secGraph, SectionGraph subSecGraph) {
    List<Section> computationalSections = new ArrayList<>();
    computationalSections.addAll(secGraph.getComputationalSections());
    computationalSections.addAll(subSecGraph.getComputationalSections());
    secGraph.setComputationalSections(computationalSections);
  }

  // FIXME this is not called apparently! another marker is probably needed to carry this final
  // piece of the update to the exit operator!
  public static void connectSystemInputOperator(Set<OperatorCore> systemInputOps, OperatorCore exit) {
    for (OperatorCore op : systemInputOps) {
      System.out.println("in: " + op.getOperatorName());
      OutputPort outPort = new OutputPort(op);
      outPort.setMetaPort(true);
      // FIXME
//      FlowGraphControl.initializeOutputPort(outPort);
      op.addOutputPort(outPort);
      // note that there are no visitors to this port at all!
      InputPort inPort = new InputPort(exit);
      inPort.setMetaPort(true);
      inPort.initComplete();
      inPort.setHasSeenLastPacket(false);
      exit.addInputPort(inPort);
      new Arc(outPort, inPort);
    }
  }

  // FIXME the name seems wrong! this connects the entrance to new source ops
  public static void connectSystemOutputOperator(Set<OperatorCore> systemOutputOps, OperatorCore entrance) {
    for (OperatorCore op : systemOutputOps) {
      System.out.println("out: " + op.getOperatorName());
      OutputPort outPort = new OutputPort(entrance);
      outPort.setMetaPort(true);
      // FIXME
//      FlowGraphControl.initializeOutputPort(outPort);
      entrance.addOutputPort(outPort);
      InputPort inPort = new InputPort(op);
      inPort.setMetaPort(true);
      inPort.initComplete();
      inPort.setHasSeenLastPacket(false);
      op.addInputPort(inPort);
      new Arc(outPort, inPort);
    }
  }

  public static void connectSystemOperators(SectionGraph secGraph, FlowGraph subflow) {
    Set<OperatorCore> systemInputOps = new HashSet<OperatorCore>();
    Set<OperatorCore> systemOutputOps = new HashSet<OperatorCore>();
    OperatorCore entrance = secGraph.getUserGraphEntranceSection().getOperator();
    OperatorCore exit = secGraph.getUserGraphExitSection().getOperator();
    for (OperatorCore op : subflow.getContainedGraphNodes()) {
      if (op.isSystemInputOperator()) {
        systemInputOps.add(op);
      } else if (op.isSystemOutputOperator()) {
        systemOutputOps.add(op);
      }
    }

    connectSystemInputOperator(systemInputOps, exit);
    connectSystemOutputOperator(systemOutputOps, entrance);
  }
}
