/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import java.util.HashMap;
import java.util.Map;

import com.ohua.engine.flowgraph.elements.FlowGraph;

public class UpdateMarker extends OnlineConfigurationMarker
{
  
  private FlowGraph _graph = null;
  private String[][] _reconnects = null;
  private Map<String,Object[]> _transfers = new HashMap<>();

  public UpdateMarker(String operatorID,
                      Category category,
                      String propertyName,
                      Object value,
                      IConfigurationMarker extension)
  {
    super(operatorID, category, propertyName, value, extension);
  }

  public UpdateMarker(String operatorID,
                      Category category,
                      IConfigurationMarker extension,
                      FlowGraph graph,
                      String[][] reconnects)
  {
    super(operatorID, category, null, null, extension);
    _graph = graph;
    _reconnects = reconnects;
  }

  public UpdateMarker(String operatorID,
                      Category category,
                      FlowGraph graph,
                      String[][] reconnects,
                      Map<String,Object[]> transfers)
  {
    super(operatorID, category, null, null, null);
    _graph = graph;
    _reconnects = reconnects;
    _transfers = transfers;
  }

  public FlowGraph getNewGraph() {
    return _graph;
  }

  public String[][] getReconnects() {
    return _reconnects;
  }
  
  public Map<String, Object[]> getTransfers(){
    return _transfers;
  }

}
