/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.online.configuration;

import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.packets.IStreamPacket;

public class OnlineConfigurationMarker implements IConfigurationMarker
{
  public enum Category {
    PROPERTY,
    OPERATOR,
    EXCHANGE_FLOW,
    EXTEND_FLOW,
    RECONNECT_REQUEST,
    REPLACE_INPUT,
    REPLACE_OUTPUT,
    REPLACE_TRANSFER,
    REPLACE_FINALIZE
  }
  
  private String _operatorID, _propertyName = null;
  private Object _value = null;
  private Category _category = null;
  
  private IConfigurationMarker _extension = null;
      
  public OnlineConfigurationMarker(String operatorID,
                                   Category category,
                                   String propertyName,
                                   Object value,
                                   IConfigurationMarker extension)
  {
    _operatorID = operatorID;
    _category = category;
    _propertyName = propertyName;
    _value = value;
    _extension = extension;
  }
  
  /**
   * No need to deep copy this thing as it is read-only.
   */
  @Override public IStreamPacket deepCopy() {
    return this;
  }
  
  /**
   * @return the _operatorID
   */
  public String getOperatorID() {
    return _operatorID;
  }
  
  /**
   * @return the _propertyName
   */
  public String getPropertyName() {
    return _propertyName;
  }
  
  /**
   * @return the _value
   */
  public Object getValue() {
    return _value;
  }
  
  @Override public Category getCategory() {
    return _category;
  }
  
  @Override public IConfigurationMarker getExtension() {
    return _extension;
  }
  
  @Override public InputPortEvents getEventType() {
    return InputPortEvents.CONFIGURATION_PACKET_ARRIVAL;
  }
  
}
