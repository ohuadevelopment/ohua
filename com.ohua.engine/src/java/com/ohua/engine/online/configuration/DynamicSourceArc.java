/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import java.util.LinkedList;

import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.AsynchronousArcImpl;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.packets.IStreamPacket;

public class DynamicSourceArc extends Arc
{ 
  /**
   * Prevent upstream notifications.
   */
  public OperatorCore getSource()
  {
    if(super.getSourcePort() == null)
    {
      return super.getTarget();
    }
    else
    {
      return super.getSource();
    }
  }

  public void enqueueBatch(LinkedList<? extends IStreamPacket> batch)
  {
    super.enqueueBatch(batch);
    ((AsynchronousArcImpl) super.getImpl()).notifyDequeueNeeded();
  }
}
