/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.online.configuration;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.AbstractPort;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.sections.SectionGraph;

public abstract class OrphanHandler
{
  /**
   * A shadow arc is known only to either its source or its target, but not to both.
   * @author sertel
   * 
   */
  private static class ShadowArc extends Arc
  {
    private boolean _sourceShadow = true;
    
    private ShadowArc(boolean sourceShadow)
    {
      _sourceShadow = sourceShadow;
    }
    
    public void setSourcePort(OutputPort sourcePort)
    {
      if(_sourceShadow)
      {
        _sourcePort.set(sourcePort);
      }
      else
      {
        super.setSourcePort(sourcePort);
      }
    }
    
    public void setTargetPort(InputPort targetPort)
    {
      if(_sourceShadow)
      {
        super.setTargetPort(targetPort);
      }
      else
      {
        _targetPort.set(targetPort);
      }
    }
    
  }
  
  /**
   * Only a single operator was replaced.
   * @param orphan
   * @param oldConnections
   * @param secGraph 
   */
  protected static void handleOrphanedOperator(OperatorCore orphan,
                                               AbstractPort[][] oldConnections, SectionGraph secGraph)
  {
    // to allow further dequeues and finishing activations of downstream and upstream operators,
    // we turn the old connections into shadow arcs.
    for(AbstractPort[] arc : oldConnections)
    {
      Assertion.invariant(arc[0].getOwner() == orphan);
      
      if(arc[0] instanceof InputPort)
      {
        ShadowArc sArc = new ShadowArc(true);
        sArc.setSourcePort((OutputPort) arc[1]);
        sArc.setTargetPort((InputPort) arc[0]);
      }
      else
      {
        System.out.println("Creating shadow arc for output of operator: " + orphan);
        ShadowArc sArc = new ShadowArc(false);
        sArc.setSourcePort((OutputPort) arc[0]);
        sArc.setTargetPort((InputPort) arc[1]);
      }
      // at this point the only one having a reference to this arc is the orphaned operator.
      // meaning: when this operator is garbage collected then so is this arc.
      
      // FIXME The Operator scheduler might also have another reference to this operator and
      // might therefore schedule it again. This should be ok because the operator is DONE but
      // we need to check what the impact of scheduling this operator again is. Maybe we really
      // need to deactivate this operator. Probably the operator state machine does so already
      // when it is backing out! But there is a call in the OperatorScheduler that activates
      // every operator again that is retrieved from the scheduler queue! Check if this call is
      // really needed still.
    }    
  }
  
  protected static void deleteOrphanedOperator(OperatorCore orphan, SectionGraph secGraph){
    // delete the orphan from the flow graph
    secGraph.remove(orphan);
  }
}
