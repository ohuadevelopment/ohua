/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine;

public interface RWReference<T> extends ReadReference<T>, WriteReference<T>
{
  // nothing
}
