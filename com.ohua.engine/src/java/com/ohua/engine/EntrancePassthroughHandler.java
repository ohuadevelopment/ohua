/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine;

import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OhuaOperator;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.io.IUnboundedIOShutdownHandler;
import com.ohua.engine.io.IUnboundedIOShutdownMarker;
import com.ohua.engine.online.configuration.IConfigurationMarker;
import com.ohua.engine.online.configuration.IConfigurationMarkerHandler;

public class EntrancePassthroughHandler implements
                                       IConfigurationMarkerHandler,
                                       IUnboundedIOShutdownHandler
{
  private OhuaOperator _op = null;
  
  protected EntrancePassthroughHandler(OhuaOperator op)
  {
    _op = op;
  }
  
  @Override
  public void addCallback(InputPortEvents event, InputPort port)
  {
    // nothing
  }
  
  @Override
  public void removeCallback(InputPortEvents event, InputPort port)
  {
    // nothing
  }
  
  @Override
  public void init()
  {
    // nothing
  }
  
  private void passthrough(IMetaDataPacket packet)
  {
    // differentiate between Fast and Slow Travelers here!
    for(OutputPort outPort : _op.getOutputPorts())
    {
      _op.enqueueMetaData(outPort, packet);
    }
    
//    _op.broadcast(packet);
  }
  
  @Override
  public void notifyMarkerArrived(InputPort port, IUnboundedIOShutdownMarker packet)
  {
    passthrough(packet);
  }
  
  // TODO activate this!
  @Override
  public void notifyMarkerArrived(InputPort port, IConfigurationMarker packet)
  {
    passthrough(packet);
  }
  
}
