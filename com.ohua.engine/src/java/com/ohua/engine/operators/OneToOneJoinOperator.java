/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.operators;

import java.util.ArrayList;
import java.util.List;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class OneToOneJoinOperator extends UserOperator
{
  private int _currentIdx = 0;
  private List<InputPortControl> _inControllers = null;
  private OutputPortControl _outControl = null;
  
  @Override
  public void prepare()
  {
    _inControllers = new ArrayList<InputPortControl>();
    for(String inPort : getInputPorts())
    {
      _inControllers.add(getDataLayer().getInputPortController(inPort));
    }
    _outControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inControllers.get(_currentIdx).next())
    {
      _currentIdx = (_currentIdx + 1) % _inControllers.size();
      if(_currentIdx == 0 && join())
      {
        break;
      }
    }
    
  }
  
  private boolean join()
  {
    getDataLayer().transferInputToOutput(_inControllers.get(0).getPortName(), "output");
    for(int i = 1; i < _inControllers.size(); i++)
    {
      InputPortControl inControl = _inControllers.get(i);
      for(String leaf : inControl.getLeafs())
      {
        getDataLayer().transfer(inControl.getPortName(), "output", leaf);
      }
    }
    return _outControl.send();
  }
  
  @Override
  public void cleanup()
  {
    // nothing to be done
  }
  
  @Override
  public Object getState()
  {
    return _currentIdx;
  }
  
  @Override
  public void setState(Object state)
  {
    _currentIdx = (Integer) state;
  }
  
}
