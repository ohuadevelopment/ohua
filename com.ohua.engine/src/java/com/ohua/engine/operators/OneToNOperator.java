/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators;

import java.util.logging.Level;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;


public class OneToNOperator extends UserOperator
{
  
  // the default behavior is a peek!
  private int N = 1;
  
  // current packet count
  private int _pushedPackets = N;
  
  private String _currentData = null;
  
  private InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;

  @Override
  public void cleanup()
  {
    // nothing to be cleaned up
  }
  
  @Override
  public void prepare()
  {
    _pushedPackets = N;
    
    preparePortControls();
  }

  private void preparePortControls()
  {
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  // TODO refactor the two loops!
  @Override
  public void runProcessRoutine()
  {
    // finish last push
    for(int i = _pushedPackets; i < N; i++)
    {
      createNewDataPacket(i);
      _pushedPackets++;
      if(_outPortControl.send())
      {
        return;
      }
      
    }
    
    while(_inPortControl.next())
    {
      _currentData = _inPortControl.getData("/testValue").toString();
      _pushedPackets = 0;
      
      for(int i = 0; i < N; i++)
      {
        createNewDataPacket(i);
        _pushedPackets++;
        if(_outPortControl.send())
        {
          return;
        }
        
      }
    }
  }
  
  private void createNewDataPacket(int someNumber)
  { 
    _outPortControl.newPacket();
    _outPortControl.setData("/testValue", _currentData + "_" + someNumber);
  }
  
  
  public Object getState()
  {
    return new Object[] {_currentData, N, _pushedPackets};
  }
  
  public void setState(Object state)
  {
    Object[] s = (Object[]) state;
    _currentData = (String) s[0];
    N = (int) s[1];
    _pushedPackets = (int) s[2];
    
    preparePortControls();
  }

  public int getN()
  {
    return N;
  }

  public void setN(int n)
  {
    N = n;
  }
  
}
