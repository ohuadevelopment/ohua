/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;

public interface TransactionalIOEndpoint extends TransactionalIOOperator
{
  // public int getCommitPeriod();
  //  
  // public boolean strictCommitRequirements();
}
