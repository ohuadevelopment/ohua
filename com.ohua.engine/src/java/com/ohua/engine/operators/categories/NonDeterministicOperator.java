/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;

public interface NonDeterministicOperator// extends Operator
{
  // every operator in the system that implements some form of non-determinism needs to be
  // markered with this interface
  // TODO later on there should be some functions here that each of this operators should
  // provide in order for the checkpoint framework to retrieve minicheckpoint state properly.
  // for now we only handle the non-deterministic merge operator!
  
  // for now this function resides here but it should be somewhere else for sure. in the end the
  // commit frequency should be computed automatically by a graph analysis algorithm.
  public int getCommitFrequency();
}
