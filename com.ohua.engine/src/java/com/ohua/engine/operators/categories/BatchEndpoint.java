/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;


public interface BatchEndpoint extends TransactionalIOEndpoint
{
  /**
   * This routine is called by the framework and is suppose to tell the operator to flush out
   * all buffered state in preparation of a commit.
   */
  public void flush();

}
