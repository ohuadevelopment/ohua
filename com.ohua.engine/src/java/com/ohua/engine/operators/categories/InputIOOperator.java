/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;

//FIXME this interface should probably go away and the function needs to go onto the connection itself
public interface InputIOOperator extends IOOperator
{
  // this is a marker interface for the decision process on which operator gets its own thread
  public boolean isUnboundedInput();
}
