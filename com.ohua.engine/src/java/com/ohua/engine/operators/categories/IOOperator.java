/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;

public interface IOOperator
{
  // barely a marker interface for now
  // TBD if we tack down during initialization which operator requests a resource then we might not need this interface anymore.
}
