/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.operators.categories;

import java.util.concurrent.TimeUnit;

/**
 * The interfaces symbolizes that this operator wants to be scheduled periodically. Hence it's
 * input is the time itself. The scheduler will implement the notion of time and assure that the
 * section containing this operator will be scheduled for execution every 'period' time units.
 * <p>
 * Note though that period starts only once this section has been scheduled at the beginning of
 * a computational system phase or during that.
 * <p>
 * For each section with a periodic operator defining a time period x it accounts that:<br>
 * <ol>
 * <li>On start of the system phase the operator scheduled without delay.
 * <li>The delay time d of the section is defined by x-n with n being the computation time of
 * the section for the scheduled period.
 * <li>Two cases are important:
 * <ol>
 * <li>d >= 0, the next execution of the section will be delayed by d time units.
 * <li>d < 0, that means the computation took longer than the period. As a result the section
 * will be scheduled again without any delay.
 * <ol>
 * </ol> The latter case can occur when a lot of data is being fetched from a resource such as a
 * file or a network (website). If the idea of the periodic operator is to execute a certain
 * action by a given frequency then the right thing to do is to put a Timer operator upstream.
 * It will map each period to a packet on the stream which will be input to the actual operator
 * handling the period. The Timer should never enter the second case as its computation time is
 * absolutely minimal. (If it still is then you do not really want to have a periodic operator!)
 * @author sebastian
 * 
 */
public interface Periodic
{
  public long getTimePeriod();
  
  public TimeUnit getTimeUnit();
}
