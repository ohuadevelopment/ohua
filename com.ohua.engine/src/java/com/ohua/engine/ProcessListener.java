/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine;

// Because it belongs to a Process interface that is also deprecated.
@Deprecated
public interface ProcessListener
{
  public void notifyProcessFinished();
}
