/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.functionality.ActivationMarkerVisitorMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.DataPollOnBlockedInputPortSignalMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.DataSignalVisitorMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.EndStreamSignalVisitorMixin;
import com.ohua.engine.io.UnboundedIOShutdownMarkerVisitorMixin;

public class VisitorFactory
{
  public static EndStreamSignalVisitorMixin createEndStreamPacketVisitor(InputPort port)
  {
    EndStreamSignalVisitorMixin mixin = new EndStreamSignalVisitorMixin(port);
    return mixin;
  }
  
  public static DataSignalVisitorMixin createDataPacketVisitor(InputPort port)
  {
    DataSignalVisitorMixin mixin = new DataSignalVisitorMixin(port);
    return mixin;
  }
  
  public static DataPollOnBlockedInputPortSignalMixin createDataPollOnBlockedPortEventVisitor(InputPort port)
  {
    DataPollOnBlockedInputPortSignalMixin mixin = new DataPollOnBlockedInputPortSignalMixin(port);
    return mixin;
  }

  public static ActivationMarkerVisitorMixin createActivationMarkerVisitor(InputPort inPort)
  {
    ActivationMarkerVisitorMixin mixin = new ActivationMarkerVisitorMixin(inPort);
    return mixin;
  }

  public static UnboundedIOShutdownMarkerVisitorMixin createUnboundedIOShutdownMarkerVisitor(InputPort inPort)
  {
    UnboundedIOShutdownMarkerVisitorMixin mixin = new UnboundedIOShutdownMarkerVisitorMixin(inPort);
    return mixin;
  }

}
