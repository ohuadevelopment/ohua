/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.flowgraph.elements.operator.InputPort;

public interface IDataPacketHandler extends IPacketHandler
{
  public void notifyMarkerArrived(InputPort port, DataPacket packet);
}
