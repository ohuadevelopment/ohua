/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.ActivationMarker;

public interface IActivationMarkerHandler extends IPacketHandler, IPrioritizedPacketHandler
{
  public void notifyMarkerArrived(InputPort port, ActivationMarker packet);
}
