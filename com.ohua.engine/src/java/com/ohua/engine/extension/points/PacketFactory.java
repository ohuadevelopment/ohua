/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import java.util.List;
import java.util.Map;

import com.ohua.engine.SystemPhaseType;
import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.PortID;
import com.ohua.engine.flowgraph.elements.packets.ActivationMarker;
import com.ohua.engine.flowgraph.elements.packets.ActivationMarkerImpl;
import com.ohua.engine.flowgraph.elements.packets.DataPollOnBlockedInputPortEvent;
import com.ohua.engine.flowgraph.elements.packets.DataPollOnBlockedInputPortSignal;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacket;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacketImpl;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.io.UnboundedIOShutdownMarker;
import com.ohua.engine.online.configuration.IConfigurationMarker;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;
import com.ohua.engine.online.configuration.UpdateMarker;

public class PacketFactory
{
  public static DataPacket createDataPacket(DataPacket packet) {
    return packet;
  }
  
  public static EndOfStreamPacket createEndSignalPacket(OperatorCore operator, SystemPhaseType type) {
    return createEndSignalPacket(operator.getLevel() + 2, type);
  }
  
  public static EndOfStreamPacket createEndSignalPacket(int levelToClose, SystemPhaseType type) {
    return new EndOfStreamPacketImpl(levelToClose, type);
  }
  
  public static EndOfStreamPacket createConditionalEndSignalPacket(int levelToClose, SystemPhaseType type,
                                                                   List<PortID> eosNeeded)
  {
    return new EndOfStreamPacketImpl(levelToClose, type, eosNeeded);
  }
  
  public static EndOfStreamPacket createEndSignalPacket(int levelToClose) {
    return createEndSignalPacket(levelToClose, SystemPhaseType.COMPUTATION);
  }
  
  public static DataPollOnBlockedInputPortEvent createDataPollOnBlockedInputPortEvent() {
    return new DataPollOnBlockedInputPortSignal();
  }
  
  public static ActivationMarker createActivationMarkerPacket(SystemPhaseType phaseType) {
    return new ActivationMarkerImpl(phaseType);
  }
  
  public static IMetaDataPacket createConfigurationMarkerPacket(String operatorID, String propertyName,
                                                                Object value)
  {
    return new OnlineConfigurationMarker(operatorID, Category.PROPERTY, propertyName, value, null);
  }
  
  public static IConfigurationMarker createConfigurationMarkerPacket(String operatorID, String propertyName,
                                                                     Object value, Category category)
  {
    return new OnlineConfigurationMarker(operatorID, category, propertyName, value, null);
  }
  
  public static IConfigurationMarker createConfigurationMarkerPacket(String operatorID, String propertyName,
                                                                     Object value, Category category,
                                                                     IConfigurationMarker responseMarker)
  {
    return new OnlineConfigurationMarker(operatorID, category, propertyName, value, responseMarker);
  }

  public static IConfigurationMarker createUpdateInitMarkerPacket(Object value,
                                                                     IConfigurationMarker responseMarker)
  {
    return new OnlineConfigurationMarker(null, Category.REPLACE_INPUT, null, value, responseMarker);
  }

  public static IConfigurationMarker createUpdateMarkerPacket(String operatorID, Category category,
                                                              IConfigurationMarker extension, FlowGraph graph,
                                                              String[][] reconnects)
  {
    return new UpdateMarker(operatorID, category, extension, graph, reconnects);
  }
  
  public static IConfigurationMarker createUpdateMarkerPacket(String operatorID, Category category,
                                                              FlowGraph graph, String[][] reconnects,
                                                              Map<String, Object[]> transfers)
  {
    return new UpdateMarker(operatorID, category, graph, reconnects, transfers);
  }
  
  public static IMetaDataPacket createUnboundedIOShutdownSignalPacket() {
    return new UnboundedIOShutdownMarker();
  }
  
}
