/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacket;

public interface IEndOfStreamPacketHandler extends IPacketHandler, IPrioritizedPacketHandler
{
  public void notifyMarkerArrived(InputPort port, EndOfStreamPacket packet);
}
