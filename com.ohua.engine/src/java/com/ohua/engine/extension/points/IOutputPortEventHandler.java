/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

import java.util.Set;

import com.ohua.engine.flowgraph.elements.operator.OutputPort;

public interface IOutputPortEventHandler
{
  public int getPriority(OutputPortEvents event);
  public Set<OutputPortEvents> getOutputPortEventInterest();
  public void notifyOutputEvent(OutputPort port, OutputPortEvents event);
}
