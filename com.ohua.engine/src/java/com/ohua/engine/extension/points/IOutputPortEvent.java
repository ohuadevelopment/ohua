/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.extension.points;

public interface IOutputPortEvent
{
  // an interface for the enumerations of output port events.
}
