/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine;

import java.util.Set;

import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.extension.points.VisitorFactory;
import com.ohua.engine.flowgraph.elements.operator.*;
import com.ohua.engine.flowgraph.elements.packets.functionality.ActivationMarkerVisitorMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.DataPollOnBlockedInputPortSignalMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.EndStreamSignalVisitorMixin;
import com.ohua.engine.flowgraph.elements.packets.functionality.handers.ActivationMarkerHandler;
import com.ohua.engine.flowgraph.elements.packets.functionality.handers.EndOfStreamPacketHandler;
import com.ohua.engine.io.UnboundedIOShutdownMarkerVisitorMixin;
import com.ohua.engine.operators.categories.TransactionalIOOperator;
import com.ohua.engine.resource.management.TransactionalEndpointEOSMarkerHandler;
import com.ohua.engine.operators.system.UserGraphEntranceOperator;

/**
 * Initialization functionality for operators and arcs.
 * @author sertel
 *         
 */
public abstract class FlowGraphControl {

  /**
   * Calls the initialization routines of the operators and transitions their state to WAITING_FOR_COMPUTATION.
   * @param ops
   */
  public static void prepareAndEnterComputationState(Set<AbstractOperatorRuntime> ops) {
    // TODO could be done in parallel
    ops.stream().forEach(AbstractOperatorRuntime::prepareAndEnterComputationState);
  }

  public static void performPortHandlerSetup(OhuaOperator op, OperatorCore core,
                                             EndOfStreamPacketHandler endHandler
//          , SectionGraph graph
  )
  {
    // we need somebody that handles the data and somebody that handles end-of-stream signals
    ActivationMarkerHandler activationHandler = new ActivationMarkerHandler(op);
//    OnlineConfigurationMarkerHandler onlineConfigHandler =
//        new OnlineConfigurationMarkerHandler(OnlineConfigurationMarkerHandler.aquireFeatures(core), graph);
    EntrancePassthroughHandler entrancePassthrough = new EntrancePassthroughHandler(op);
    
    for(InputPort inPort : op.getInputPorts()) {
      // TODO re-enable the DataSignalVisitor once it is needed (checkpointing). I hope at that
      // point, there is traits available in Java.
      
      EndStreamSignalVisitorMixin endStreamPacketVisitor = VisitorFactory.createEndStreamPacketVisitor(inPort);
      endStreamPacketVisitor.registerMarkerHandler(endHandler);
      inPort.registerPacketVisitor(endStreamPacketVisitor);
      
      // we also want a DataPollOnBlockedInputPort visitor on all input ports
      DataPollOnBlockedInputPortSignalMixin dataPollVisitor =
          VisitorFactory.createDataPollOnBlockedPortEventVisitor(inPort);
      inPort.registerPacketVisitor(dataPollVisitor);
      
      if(op.getUserOperator() instanceof UserGraphEntranceOperator
         && !inPort.getPortName().equals("process-control"))
      {
        continue;
      }
      
      // stuff for user graph and input path to user graph (ProcessController, etc.)
      
      ActivationMarkerVisitorMixin activationPacketVisitor = VisitorFactory.createActivationMarkerVisitor(inPort);
      inPort.registerPacketVisitor(activationPacketVisitor);
      activationPacketVisitor.registerMarkerHandler(activationHandler);
      activationPacketVisitor.registerMarkerHandler(endHandler);
      
      if(op.getUserOperator() instanceof UserGraphEntranceOperator) {
        UnboundedIOShutdownMarkerVisitorMixin shutdownVisitor =
            VisitorFactory.createUnboundedIOShutdownMarkerVisitor(inPort);
        inPort.registerPacketVisitor(shutdownVisitor);
        shutdownVisitor.registerMarkerHandler(entrancePassthrough);
      } else if(op.isSourceOperator()) {
        UnboundedIOShutdownMarkerVisitorMixin shutdownVisitor =
            VisitorFactory.createUnboundedIOShutdownMarkerVisitor(inPort);
        inPort.registerPacketVisitor(shutdownVisitor);
//        shutdownVisitor.registerMarkerHandler(new UnboundedIOShutdownHandler(UnboundedIOShutdownHandler.aquireFeatures(core)));
      }

      if(op.getUserOperator() instanceof TransactionalIOOperator) {
        assert op.getInputPorts().size() == 1;
        inPort.registerForEvent(InputPortEvents.END_OF_STREAM_PACKET_ARRIVAL,
                                                   new TransactionalEndpointEOSMarkerHandler(op));
      }
      
//      if(runtimeConfiguration.enableOnlineConfigurationSupport()) {
//        ConfigurationMarkerVisitorMixin mixin = new ConfigurationMarkerVisitorMixin(inPort);
//        inPort.registerPacketVisitor(mixin);
//        mixin.registerMarkerHandler(onlineConfigHandler);
//        // for debugging
//        // dataPacketVisitor.registerMarkerHandler(onlineConfigHandler);
//      }
      
    }
  }

  public static void tearDownAndFinishComputation(Set<AbstractOperatorRuntime> ops){
    // TODO could be done in parallel
    ops.stream().forEach(AbstractOperatorRuntime::tearDownAndFinishComputation);
  }
}
