/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.utils.parser;

import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.utils.DeepCopy;
import com.ohua.engine.utils.ReflectionUtils;

public abstract class AbstractParallelFlowSupport
{
  protected final static OperatorCore replicateOperator(FlowGraph graph, OperatorCore op, Object value, int i)
  {
    // replicate this operator by taking the user operator type
    String newOperatorName = op.getOperatorName() + "-" + i;
    OperatorCore newReplica = OperatorFactory.cloneOperator(graph, op);
    newReplica.setOperatorName(newOperatorName);
    
    if(value != null)
    {
      Object clone = DeepCopy.copy(value);
      ReflectionUtils.setProperty(newReplica.getOperatorAlgorithm(), "properties", clone);
    }
    return newReplica;
  }

}
