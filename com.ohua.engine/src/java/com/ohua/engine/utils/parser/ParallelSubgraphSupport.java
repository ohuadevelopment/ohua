/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.utils.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.flowgraph.elements.operator.OperatorID.OperatorIDGenerator;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.utils.ReflectionUtils;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;

public class ParallelSubgraphSupport extends AbstractParallelFlowSupport
{
  private static int _splitCounter = 0;
  private static int _opCounter = 0;
  private static int _mergeCounter = 0;
  
  private DataParallelSplitMergeFactory _splitMergeFactory = null;
  
  public ParallelSubgraphSupport()
  {
    this(new DataParallelSplitMergeFactory());
  }
  
  public ParallelSubgraphSupport(DataParallelSplitMergeFactory splitMergeFactory)
  {
    _splitMergeFactory = splitMergeFactory;
  }
  
  /**
   * We replicate all the operators in the subgraph. All operators with incoming connections
   * will have a split in front of them. All operators with outgoing connections will have a
   * non-deterministic merge in front of them.
   * 
   * @param parallel
   * @param graph
   */
  public void performParallelRewrite(ReplicationProperties parallel, FlowGraph graph)
  {
    resetOperatorIDCounter(graph);
    List<List<OperatorID>> edges = findSubgraphEdges(parallel, graph);
    Map<OperatorID, List<OperatorCore>> replicas =
        replicateSubgraph(parallel, graph, edges.get(1));
    List<OperatorID> splits = handleSubgraphInputs(parallel, graph, edges.get(0), replicas);
    List<OperatorID> merges = handleSubgraphOutputs(parallel, graph, edges.get(1), replicas);
    handleNestedParallelism(parallel, replicas, splits, merges);
  }
  
  private static void resetOperatorIDCounter(FlowGraph graph)
  {
    int max = 0;
    for(OperatorCore op : graph.getContainedGraphNodes())
    {
      max = Math.max(op.getId().getIDInt(), max);
    }
    OperatorIDGenerator.setStartValue(max + 1);
  }
  
  private static void handleNestedParallelism(ReplicationProperties parallel,
                                              Map<OperatorID, List<OperatorCore>> replicas,
                                              List<OperatorID> splits,
                                              List<OperatorID> merges)
  {
    if(parallel._enclosing != null)
    {
      parallel._enclosing._subgraph.addAll(replicas.keySet());
      for(List<OperatorCore> opReplicas : replicas.values())
      {
        for(OperatorCore replica : opReplicas)
        {
          parallel._enclosing._subgraph.add(replica.getId());
        }
      }
      parallel._enclosing._subgraph.addAll(splits);
      parallel._enclosing._subgraph.addAll(merges);
    }
  }
  
  @SuppressWarnings("unchecked")
  private List<List<OperatorID>> findSubgraphEdges(ReplicationProperties parallel,
                                                   FlowGraph graph)
  {
    List<OperatorID> inputWithCNN = new ArrayList<OperatorID>();
    List<OperatorID> outputWithCNN = new ArrayList<OperatorID>();
    
    for(OperatorID op : parallel._subgraph)
    {
      OperatorCore operator = graph.getOperator(op);
      
      for(OperatorCore upstream : operator.getAllPreceedingGraphNodes())
      {
        if(!parallel._subgraph.contains(upstream.getId()))
        {
          // needs a split connection
          inputWithCNN.add(op);
          break;
        }
      }
      
      for(OperatorCore downstream : operator.getAllSucceedingGraphNodes())
      {
        if(!parallel._subgraph.contains(downstream.getId()))
        {
          // needs a merge connection
          outputWithCNN.add(op);
          break;
        }
      }
    }
    
    return Arrays.asList(inputWithCNN, outputWithCNN);
  }
  
  private Map<OperatorID, List<OperatorCore>> replicateSubgraph(ReplicationProperties parallel,
                                                                FlowGraph graph,
                                                                List<OperatorID> outputEdges)
  {
    Map<OperatorID, List<OperatorCore>> replicas =
        new HashMap<OperatorID, List<OperatorCore>>();
    int opID = 0;
    // replicate the operators
    for(OperatorID id : parallel._subgraph)
    {
      opID = _opCounter;
      replicas.put(id, new ArrayList<OperatorCore>());
      OperatorCore op = graph.getOperator(id);
      
      // get original properties
      Object value =
          ReflectionUtils.getProperty(op.getOperatorAlgorithm(),
                                      "properties",
                                      op.getOperatorAlgorithm().getClass());
      for(int i = 0; i < parallel._replicationCount - 1; i++)
      {
        replicas.get(id).add(replicateOperator(graph, op, value, opID++));
      }
    }
    _opCounter = opID;
    
    // reconnect operators
    for(Map.Entry<OperatorID, List<OperatorCore>> entry : replicas.entrySet())
    {
      if(!outputEdges.contains(entry.getKey()))
      {
        OperatorCore original = graph.getOperator(entry.getKey());
        for(Arc originalArc : original.getGraphNodeOutputConnections())
        {
          List<OperatorCore> downstreamReplicas = replicas.get(originalArc.getTarget().getId());
          for(int j = 0; j < downstreamReplicas.size(); j++)
          {
            Arc arc =
                new Arc(entry.getValue().get(j).getOutputPort(originalArc.getSourcePort().getPortName()),
                        downstreamReplicas.get(j).getInputPort(originalArc.getTargetPort().getPortName()));
            graph.addArc(arc);
          }
        }
      }
    }
    
    return replicas;
  }
  
  private List<OperatorID> handleSubgraphInputs(ReplicationProperties parallel,
                                                FlowGraph graph,
                                                List<OperatorID> inputEdges,
                                                Map<OperatorID, List<OperatorCore>> replicas)
  {
    List<OperatorID> splits = new ArrayList<OperatorID>();
    int splitID = _splitCounter;
    for(OperatorID inputEdge : inputEdges)
    {
      OperatorCore original = graph.getOperator(inputEdge);
      List<OperatorCore> edgeReplicas = replicas.get(inputEdge);
      for(Arc originalArc : original.getGraphNodeInputConnections())
      {
        Assertion.invariant(!parallel._subgraph.contains(originalArc.getSource().getId()),
                            "Not yet supported");
        
        // create a Split operator
        try
        {
          OperatorCore split = _splitMergeFactory.getSplitOperator(graph, parallel);
          splits.add(split.getId());
          split.setOperatorName("Split-" + splitID++);
          
          // connect upstream to the split
          originalArc.detachFromTarget();
          originalArc.setTargetPort(split.getInputPorts().get(0));
          
          // connect to downstream
          int index = 0;
          for(OperatorCore edgeReplica : edgeReplicas)
          {
            OutputPort out = new OutputPort(split);
            out.setPortName("out-" + index++);
            split.addOutputPort(out);
            Arc arc =
                new Arc(out,
                        edgeReplica.getInputPort(originalArc.getTargetPort().getPortName()));
            graph.addArc(arc);
          }
          
          // finally connect the original op to the split
          
          OutputPort out = new OutputPort(split);
          out.setPortName("out-" + index++);
          split.addOutputPort(out);
          original.getInputPort(originalArc.getTargetPort().getPortName());
          Arc arc =
              new Arc(out, original.getInputPort(originalArc.getTargetPort().getPortName()));
          graph.addArc(arc);
        }
        catch(OperatorLoadingException e)
        {
          Assertion.impossible(e);
        }
        
      }
    }
    _splitCounter = splitID;
    return splits;
  }
  
  private List<OperatorID> handleSubgraphOutputs(ReplicationProperties parallel,
                                                 FlowGraph graph,
                                                 List<OperatorID> outputEdges,
                                                 Map<OperatorID, List<OperatorCore>> replicas)
  {
    List<OperatorID> merges = new ArrayList<OperatorID>();
    int mergeID = _mergeCounter;
    for(OperatorID outputEdge : outputEdges)
    {
      OperatorCore original = graph.getOperator(outputEdge);
      List<OperatorCore> edgeReplicas = replicas.get(outputEdge);
      for(Arc originalArc : original.getGraphNodeOutputConnections())
      {
        // create a Merge operator
        try
        {
          OperatorCore merge = _splitMergeFactory.getMergeOperator(graph, parallel);
          merge.setOperatorName("Merge-" + mergeID++);
          merges.add(merge.getId());
          
          // connect downstream to the merge
          originalArc.detachFromSource();
          originalArc.setSourcePort(merge.getOutputPorts().get(0));
          
          // connect to upstream
          int index = 0;
          for(OperatorCore edgeReplica : edgeReplicas)
          {
            InputPort in = new InputPort(merge);
            in.setPortName("in-" + index++);
            merge.addInputPort(in);
            Arc arc =
                new Arc(edgeReplica.getOutputPort(originalArc.getSourcePort().getPortName()),
                        in);
            graph.addArc(arc);
          }
          
          // finally connect the original op to the merge
          InputPort in = new InputPort(merge);
          in.setPortName("in-" + index++);
          merge.addInputPort(in);
          Arc arc =
              new Arc(original.getOutputPort(originalArc.getSourcePort().getPortName()), in);
          graph.addArc(arc);
          
        }
        catch(OperatorLoadingException e)
        {
          Assertion.impossible(e);
        }
        
      }
    }
    _mergeCounter = mergeID;
    return merges;
  }
  
  public static void resetCounters()
  {
    _opCounter = 0;
    _mergeCounter = 0;
    _splitCounter = 0;
  }
  
}
