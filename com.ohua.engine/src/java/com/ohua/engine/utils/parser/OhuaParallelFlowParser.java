/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.utils.parser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ohua.engine.DataFlowProcess;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.exceptions.XMLParserException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.utils.ReflectionUtils;

public class OhuaParallelFlowParser extends OhuaFlowParser
{

  public static class ReplicationProperties
  {
    public List<OperatorID> _subgraph = new ArrayList<OperatorID>();
    public int _replicationCount = 0;
    public String _pipelineProps = null;
    public ReplicationProperties _enclosing = null;
  }
  
  enum AdditionalProcessElementTags
  {
    PIPELINE,
    PARALLEL;
    
    public static boolean contains(String s)
    {
      try
      {
        AdditionalProcessElementTags.valueOf(s);
        return true;
      }
      catch(IllegalArgumentException t)
      {
        return false;
      }
    }
  }
  
  enum ReplicationAttributes
  {
    NUMBER,
    PROPERTIES
  }
  
  private List<ReplicationProperties> _pipelines = new ArrayList<ReplicationProperties>();
  private List<ReplicationProperties> _parallels = new ArrayList<ReplicationProperties>();
  
  private Stack<ReplicationProperties> _nestedParallels = new Stack<ReplicationProperties>();
  
  private ReplicationProperties _currentReplicationProperties = null;

  public OhuaParallelFlowParser(String pathToFlow) {
    super(pathToFlow);
  }
  
  @Override
  public void startElement(String uri, String localName, String name, Attributes atts) throws SAXException
  {
    if(AdditionalProcessElementTags.contains(localName.toUpperCase()))
    {
      AdditionalProcessElementTags elementTag =
          AdditionalProcessElementTags.valueOf(localName.toUpperCase());
      
      switch(elementTag)
      {
        case PIPELINE:
          _currentReplicationProperties = new ReplicationProperties();
          parseReplicationAttributes(atts, _currentReplicationProperties);
          break;
        case PARALLEL:
          ReplicationProperties props = new ReplicationProperties();
          if(!_nestedParallels.isEmpty())
          {
            props._enclosing = _nestedParallels.peek();
          }
          _nestedParallels.push(props);
          parseReplicationAttributes(atts, _nestedParallels.peek());
          break;
      }
    }
    else
    {
      super.startElement(uri, localName, name, atts);
      if(!_nestedParallels.isEmpty())
      {
        if(ProcessElementTags.OPERATOR.toString().equalsIgnoreCase(localName))
        {
          _nestedParallels.peek()._subgraph.add(((OperatorCore) getCurrentStackTop()).getId());
        }
      }
      else if(_currentReplicationProperties != null)
      {
        if(ProcessElementTags.OPERATOR.toString().equalsIgnoreCase(localName))
        {
          _currentReplicationProperties._subgraph.add(((OperatorCore) getCurrentStackTop()).getId());
        }
      }
      
    }
  }
  
  private void parseReplicationAttributes(Attributes atts, ReplicationProperties properties)
  {
    for(ReplicationAttributes propAtt : ReplicationAttributes.values())
    {
      String value = atts.getValue(propAtt.name().toLowerCase());
      switch(propAtt)
      {
        case NUMBER:
          properties._replicationCount = Integer.parseInt(value);
          break;
        case PROPERTIES:
          properties._pipelineProps = value;
          break;
      }
    }
    
    // assert false;
    return;
  }
  
  @Override
  public void endElement(String uri, String localName, String name) throws SAXException
  {
    if(AdditionalProcessElementTags.contains(localName.toUpperCase()))
    {
      AdditionalProcessElementTags elementTag =
          AdditionalProcessElementTags.valueOf(localName.toUpperCase());
      
      switch(elementTag)
      {
        case PIPELINE:
          _pipelines.add(_currentReplicationProperties);
          _currentReplicationProperties = null;
          break;
        case PARALLEL:
          _parallels.add(_nestedParallels.pop());
          break;
      }
    }
    else
    {
      super.endElement(uri, localName, name);
    }
  }
  
  @Override
  public DataFlowProcess parse(InputSource source) throws SAXException,
                                                  ParserConfigurationException,
                                                  IOException,
                                                  XMLParserException
  {
    DataFlowProcess process = super.parse(source);
    try
    {
      performGraphRewrite(process);
    }
    catch(OperatorLoadingException e)
    {
      e.printStackTrace();
      throw new XMLParserException(e);
    }
    return process;
  }
  
  private void applyAdditionalProperties(FlowGraph graph, String pipelineProps) throws IOException
  {
    if(pipelineProps == null)
    {
      return;
    }
    
    Properties props = new Properties();
    FileReader reader = new FileReader(pipelineProps);
    props.load(reader);
    
    for(Map.Entry<Object, Object> entry : props.entrySet())
    {
      String key = entry.getKey().toString();
      String[] ref = key.split("\\.");
      OperatorCore op = graph.getOperator(ref[0]);
      Object opProps =
          ReflectionUtils.getProperty(op.getOperatorAlgorithm(),
                                      "properties",
                                      op.getOperatorAlgorithm().getClass());
      // FIXME This is of course nothing more than a hack. We should use castor here again to get the correct type.
      String[] value = entry.getValue().toString().split(":");
      Object convertedValue = value[1];
      if(value[0].equals("long"))
      {
        convertedValue = Long.parseLong(value[1]);
      }
      else if(value[0].equals("int"))
      {
        convertedValue = Integer.parseInt(value[1]);
      }
      ReflectionUtils.setProperty(opProps, ref[1], convertedValue);
      
    }
  }
  
  private void performGraphRewrite(DataFlowProcess process) throws OperatorLoadingException,
                                                           IOException
  {
    for(ReplicationProperties pipeline : _pipelines)
    {
      PipelineSubgraphSupport.performPipelineRewrite(pipeline, process.getGraph());
      applyAdditionalProperties(process.getGraph(), pipeline._pipelineProps);
    }
    
    ParallelSubgraphSupport.resetCounters();
    ParallelSubgraphSupport parallelSupport = new ParallelSubgraphSupport();
    // no need for ordering here because the innermost parallels are always the nested ones!
    for(ReplicationProperties parallel : _parallels)
    {
      parallelSupport.performParallelRewrite(parallel, process.getGraph());
      applyAdditionalProperties(process.getGraph(), parallel._pipelineProps);
    }
    
  }
}
