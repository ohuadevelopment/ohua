/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.utils.parser;

import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.operators.NonDeterministicMergeOperator;
import com.ohua.engine.operators.SplitOperator;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;

public class DataParallelSplitMergeFactory
{
  protected OperatorCore getSplitOperator(FlowGraph graph, ReplicationProperties props) throws OperatorLoadingException
  {
    OperatorCore split =
        OperatorFactory.getInstance().createUserOperatorCore(graph,
                                                             SplitOperator.class,
                                                             "Split");
    return split;
  }
  
  protected OperatorCore getMergeOperator(FlowGraph graph, ReplicationProperties props) throws OperatorLoadingException
  {
    OperatorCore merge =
        OperatorFactory.getInstance().createUserOperatorCore(graph,
                                                             NonDeterministicMergeOperator.class,
                                                             "NonDeterministicMerge");
    return merge;
  }
}
