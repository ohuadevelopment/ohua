/*
 * Copyright (c) Sebastian Ertel 2008-2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.utils.parser;

public abstract class OperatorDescriptorRegistry
{
  // TODO has a map with the operator alias as the key and the deserialized operator description
  // as the value. deserialization should happen on demand.
}
