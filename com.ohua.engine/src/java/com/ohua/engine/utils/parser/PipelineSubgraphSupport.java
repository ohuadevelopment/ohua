/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.utils.parser;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.utils.ReflectionUtils;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;

public class PipelineSubgraphSupport extends AbstractParallelFlowSupport
{
  /**
   * Currently, we only support pipeline replication for a single operator!
   * 
   * @param pipeline
   * @param graph
   * @throws OperatorLoadingException
   */
  protected static void performPipelineRewrite(ReplicationProperties pipeline, FlowGraph graph) throws OperatorLoadingException
  {
    Assertion.invariant(pipeline._subgraph.size() == 1);
    // find the original
    OperatorCore op = graph.getOperator(pipeline._subgraph.get(0));
    
    // detach from target
    Assertion.invariant(op.getOutputPorts().size() == 1
                        && op.getOutputPorts().get(0).getOutgoingArcs().size() == 1);
    Arc targetArc = op.getOutputPorts().get(0).getOutgoingArcs().get(0);
    targetArc.detachFromSource();
    
    // get original properties
    Object value =
        ReflectionUtils.getProperty(op.getOperatorAlgorithm(),
                                    "properties",
                                    op.getOperatorAlgorithm().getClass());
    
    OperatorCore replica = op;
    for(int i = 0; i < pipeline._replicationCount; i++)
    {
      OperatorCore newReplica = replicateOperator(graph, op, value, i);
      
      // reconnect
      Arc arc = new Arc(replica.getOutputPorts().get(0), newReplica.getInputPorts().get(0));
      graph.addArc(arc);
      
      replica = newReplica;
    }
    
    targetArc.setSourcePort(replica.getOutputPorts().get(0));
  }

}
