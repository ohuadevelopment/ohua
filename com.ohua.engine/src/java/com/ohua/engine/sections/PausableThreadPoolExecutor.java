/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.sections;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Pausable ThreadPoolExecutor pattern taken from JavaDoc of ThreadPoolExectuor.
 */
public abstract class PausableThreadPoolExecutor extends ThreadPoolExecutor
{
  /**
   * Here we use a lock and therefore also synchronize the tasks, which is not required. If this
   * becomes a performance problem just use a semaphore with one lock per task and all locks
   * require for pausing.
   */
  private boolean _isPaused = false;
  private ReentrantLock _pauseLock = new ReentrantLock();
  private Condition _unpaused = _pauseLock.newCondition();
  
  private ReentrantLock _runningTasksLock = new ReentrantLock();
  private Condition _pausedTasks = _runningTasksLock.newCondition();
  
  private AtomicInteger _runningTaskCount = new AtomicInteger(0);
  
  public PausableThreadPoolExecutor(int corePoolSize,
                                    int maximumPoolSize,
                                    long keepAliveTime,
                                    TimeUnit unit,
                                    BlockingQueue<Runnable> workQueue,
                                    ThreadFactory threadFactory,
                                    RejectedExecutionHandler handler)
  {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
  }
  
  public PausableThreadPoolExecutor(int corePoolSize,
                                    int maximumPoolSize,
                                    long keepAliveTime,
                                    TimeUnit unit,
                                    BlockingQueue<Runnable> workQueue,
                                    ThreadFactory threadFactory)
  {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
  }
  
  public PausableThreadPoolExecutor(int corePoolSize,
                                    int maximumPoolSize,
                                    long keepAliveTime,
                                    TimeUnit unit,
                                    BlockingQueue<Runnable> workQueue,
                                    RejectedExecutionHandler handler)
  {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
  }
  
  public PausableThreadPoolExecutor(int corePoolSize,
                                    int maximumPoolSize,
                                    long keepAliveTime,
                                    TimeUnit unit,
                                    BlockingQueue<Runnable> workQueue)
  {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
  }
  
  public void pause()
  {
    // 1. step: don't take new tasks
    _pauseLock.lock();
    try
    {
      _isPaused = true;
    }
    finally
    {
      _pauseLock.unlock();
    }
    
    // 2. step: interrupt the tasks themselves
    pauseRunningTasks();
    
    // 3. step: wait until all running tasks stopped processing
    _runningTasksLock.lock();
    try
    {
      while(_runningTaskCount.get() > 0)
      {
        _pausedTasks.await();
      }
    }
    catch(InterruptedException e)
    {
      assert false;
    }
    finally
    {
      _runningTasksLock.unlock();
    }
  }
  
  abstract protected void pauseRunningTasks();
  
  public void resume()
  {
    _pauseLock.lock();
    try
    {
      _isPaused = false;
      _unpaused.signalAll();
    }
    finally
    {
      _pauseLock.unlock();
    }
  }
  
  @Override
  protected void beforeExecute(Thread t, Runnable r)
  {
    super.beforeExecute(t, r);
    
    awaitUnpaused();
    
    _runningTaskCount.incrementAndGet();
  }

  protected void awaitUnpaused()
  {
    _pauseLock.lock();
    try
    {
      while(_isPaused)
      {
        _unpaused.await();
      }
    }
    catch(InterruptedException e)
    {
      assert false;
    }
    finally
    {
      _pauseLock.unlock();
    }
  }
  
  @Override
  protected void afterExecute(Runnable r, Throwable t)
  {
    super.afterExecute(r, t);
    
    signalPausedRunningTask();
  }

  protected void signalPausedRunningTask()
  {
    int running = _runningTaskCount.decrementAndGet();
    
    if(running > 0)
    {
      return;
    }
    
    try
    {
      _runningTasksLock.lock();
      _pausedTasks.signalAll();
    }
    finally
    {
      _runningTasksLock.unlock();
    }
  }
  
}
