/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.sections.communication;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.ConcurrentArcQueue;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.AbstractPort;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.AsynchronousArcImpl;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.online.configuration.InverseArc;
import com.ohua.engine.online.configuration.LazyActivationArcImpl;

/**
 * Used in the online reconfiguration handler, this class resembles the functionality needed to
 * exchange part of an executing flow graph.
 * 
 * @author sertel
 * 
 */
// FIXME see FIXME below
public abstract class FlowStructureChange
{
  
  public static final AbstractPort[][] exchange(SectionGraph secGraph,
                                                FlowGraph subGraph,
                                                OperatorCore operator,
                                                String[][] portReconnect,
                                                SectionGraph subSecGraph)
  {
    AbstractPort[][] oldConnections = saveConnections(operator);
    
    reconnectInput(findInputPort(portReconnect[0][0], operator),
                   findInputPort(portReconnect[0][1], subGraph));
    reconnectOutput(findOutputPort(portReconnect[1][0], operator),
                    findOutputPort(portReconnect[1][1], subGraph));
    
    activateInput(subGraph, portReconnect);
    return oldConnections;
  }
  
  public static JSONArray extendFlow(SectionGraph secGraph,
                                     FlowGraph subGraph,
                                     OperatorCore operator,
                                     Object reconnectionSpec)
  {    
    try
    {
      JSONArray reconnections = getReconnection(reconnectionSpec);
      int handledReconnect = -1;
      for(int i = 0; i < reconnections.length(); i++)
      {
        JSONObject reconnect = reconnections.getJSONObject(i);
        String target = reconnect.getString("target-operator");
        if(target.equals(operator.getOperatorName()))
        {
          reconnectOwner(subGraph, operator, reconnect);
          handledReconnect = i;
        }
        else
        {
          JSONObject cnnInfo = reconnect.getJSONObject("connection-info");
          cloakInput(subGraph, cnnInfo.getString("reconnect-to"));
          cloakOutput(subGraph, cnnInfo.getString("new-input"));
        }
      }
      reconnections.remove(handledReconnect);
      // TODO activate the new input operator since data might already be located in its
      // incoming arc
      return reconnections;
    }
    catch(JSONException e)
    {
      throw new RuntimeException(e);
    }
  }
    
  private static void cloakInput(FlowGraph subGraph, String target)
  {
    InputPort inPort = findInputPort(target, subGraph);
    
    // create input plug
    // we need the inverse arc for defining the scheduling priority.
    // we need the lazy activation arc implementation to deactivate operator activation.
    InverseArc arc = new InverseArc();
    arc.setImpl(new LazyActivationArcImpl(arc));
    ((AsynchronousArcImpl) arc.getImpl()).exchangeQueue(new ConcurrentArcQueue());
    
    // arc is already created in the sub graph because we need to define the ports of the
    // operators in the subgraph already there
    inPort.getIncomingArc().detachFromTarget();
    arc.setTargetPort(inPort);
  }
  
  public static void cloakOutput(FlowGraph subGraph, String target)
  {
    OutputPort outPort = findOutputPort(target, subGraph);
    
    InverseArc arc = new InverseArc();
    arc.setImpl(new LazyActivationArcImpl(arc));
    ((AsynchronousArcImpl) arc.getImpl()).exchangeQueue(new ConcurrentArcQueue());
    arc.setSourcePort(outPort);
    // we throw away this arc later in the last step of the rewrite, so don't register it.
  }

  /**
   * Note, in order to perform this operation, the flow graph must already specify the new arcs
   * that need to be reconnected to the original flow.
   * @param subGraph
   * @param operator
   * @param reconnect
   * @throws JSONException
   */
  private static void reconnectOwner(FlowGraph subGraph,
                                     OperatorCore operator,
                                     JSONObject reconnect) throws JSONException
  {
    JSONObject reconnectInfo = reconnect.getJSONObject("connection-info");
    InputPort inPort = findInputPort(reconnectInfo.getString("current-input"), operator);
    InputPort newInPort = findInputPort(reconnectInfo.getString("reconnect-to"), subGraph);
    reconnectInput(inPort, newInPort);
    OutputPort outPort = findOutputPort(reconnectInfo.getString("new-input"), subGraph);
    
    // arc is already created in the sub graph because we need to define the ports of the
    // operators in the subgraph already there
    Assertion.invariant(outPort.getOutgoingArcs().size() == 1);
    outPort.getOutgoingArcs().get(0).setTargetPort(inPort);
    // TODO register arc at flow graph level
  }
  
  /**
   * A reconnection request always transitions the a graph in a valid state into a graph which
   * is also in a valid state. A valid graph state is defined as a state where all input ports
   * and output ports are properly connected. </p> JSON format for reconnection information:<br>
   * [{<br>
   * "target-operator" : "X" ,<br>
   * "connection-info" :<br>
   * {"current-input" : "X.input", "reconnect-to" : "Y.input", "new-input" : "Z.output"}<br>
   * }]<br>
   * Note that a reconnection request is an upstream rewrite and targeted at an operator X's
   * input port. As such, X is the target operator of the arc to be reconnected. Y is the
   * operator of the new subflow where the arc is to be reconnected to. Finally, Z is the
   * operator in the new subflow that provides the new arc for the disconnected input port of X.
   * 
   * @param reconnectionSpec
   * @return
   */
  // TODO We should check that the above statement is true for all reconnection requests!
  private static JSONArray getReconnection(Object reconnectionSpec)
  {
    if(reconnectionSpec instanceof String)
    {
      try
      {
        return new JSONArray(new JSONTokener((String) reconnectionSpec));
      }
      catch(JSONException e)
      {
        e.printStackTrace();
        throw new RuntimeException(e);
      }
    }
    else
    {
      return (JSONArray) reconnectionSpec;
    }
  }
  
  /**
   * We need to activate the new input operator because data might already be waiting in its
   * incoming arc.
   * @param subGraph
   * @param portReconnect
   */
  public static void activateInput(FlowGraph subGraph, String[][] portReconnect)
  {
    OperatorCore newInput = findInputPort(portReconnect[0][1], subGraph).getOwner();
    System.out.println("Activating input: " + newInput.getOperatorName());
    // FIXME needs the runtime!
//    newInput.activateOperator(newInput);
  }
  
  public static AbstractPort[][] saveConnections(OperatorCore operator)
  {
    AbstractPort[][] connections =
        new AbstractPort[operator.getNumGraphNodeInputs() + operator.getNumGraphNodeOutputs()][2];
    int i = 0;
    for(Arc arc : operator.getGraphNodeInputConnections())
    {
      connections[i][0] = arc.getTargetPort();
      connections[i++][1] = arc.getSourcePort();
    }
    
    for(Arc arc : operator.getGraphNodeOutputConnections())
    {
      connections[i][0] = arc.getSourcePort();
      connections[i++][1] = arc.getTargetPort();
    }
    return connections;
  }

  public static InputPort findInputPort(String portReference, OperatorCore op)
  {
    String[] path = FlowGraph.parsePortReference(portReference);
    Assertion.invariant(path[0].equals(op.getOperatorName()));
    return op.getInputPort(path[1]);
  }
  
  public static InputPort findInputPort(String portReference, FlowGraph graph)
  {
    String[] path = FlowGraph.parsePortReference(portReference);
    OperatorCore op = graph.getOperator(path[0]);
    return op.getInputPort(path[1]);
  }
  
  public static OutputPort findOutputPort(String portReference, OperatorCore op)
  {
    String[] path = FlowGraph.parsePortReference(portReference);
    Assertion.invariant(path[0].equals(op.getOperatorName()));
    return op.getOutputPort(path[1]);
  }
  
  public static OutputPort findOutputPort(String portReference, FlowGraph graph)
  {
    String[] path = FlowGraph.parsePortReference(portReference);
    OperatorCore op = graph.getOperator(path[0]);
    return op.getOutputPort(path[1]);
  }
  
  /**
   * Reconnect the input arc to the input port of the new operator.
   * @param inPort
   * @param newInPort
   */
  public static void reconnectInput(InputPort inPort, InputPort newInPort)
  {
    Arc inArc = inPort.getIncomingArc();
    inArc.reconnectTarget(newInPort);
  }
  
  /**
   * Reconnect the outgoing arcs of the old output port to the new one.
   * @param outPort
   * @param newOutPort
   */
  public static void reconnectOutput(OutputPort outPort, OutputPort newOutPort)
  {
    List<Arc> outArcs = new ArrayList<Arc>(outPort.getOutgoingArcs());
    for(Arc outArc : outArcs)
    {
      outArc.reconnectSource(newOutPort);
    }
  }  
}
