/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.sections.communication;

import com.ohua.engine.IOperatorEventListener;
import com.ohua.engine.OperatorEvents;
import com.ohua.engine.SystemPhaseType;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;

// FIXME feels to me like all of the functions in this interface should just go away!
public interface ProcessObserver
{
  public SystemPhaseType getProcessState();

  /**
   * This interface function will later on be extended when handling multiple processes in
   * parallel by one ProcessObserver. (In that case just add processID as a parameter.)
   */
  // FIXME this call just doesn't seem to belong into this interface and the process manager
  // should not notify anybody. should be the business of the component that runs the Ohua
  // process.
  public void notifyOnDoneStatus();

  // FIXME those methods clearly belong into another interface!
  // FIXME should this method be local to the operator?!
  public void registerForOperatorEvent(OperatorEvents opEvent,
                                       IOperatorEventListener opEventListener,
                                       OperatorCore op);
}
