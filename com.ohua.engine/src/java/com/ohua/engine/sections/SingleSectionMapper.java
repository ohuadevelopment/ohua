/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.sections;

import java.util.Collections;
import java.util.List;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;

public class SingleSectionMapper extends AbstractSectionGraphBuilder
{
  
  public SingleSectionMapper(RuntimeProcessConfiguration config) {
    super(config);
  }
  
  @Override protected SectionGraph buildSectionGraph(FlowGraph graphToConvert) {
    SectionGraph secGraph = new SectionGraph();
    
    FlowGraph flow = graphToConvert;
    Section sec = createSection();
    
    List<OperatorCore> containedOperators = flow.getContainedGraphNodes();
    sec.setOperators(containedOperators);

    List<Section> all = Collections.singletonList(sec);
    if(_inputOps.isEmpty() && _outputOps.isEmpty()) {
      secGraph.setComputationalSections(all);
    }
    else {
      secGraph.setIoSections(all);
    }
    secGraph.setInputSections(all);
    secGraph.setOutputSections(all);
    
    return secGraph;
  }
  
  protected Section createSection() {
    return new Section();
  }
  
}
