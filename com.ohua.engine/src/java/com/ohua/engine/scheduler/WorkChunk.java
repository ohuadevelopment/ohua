package com.ohua.engine.scheduler;

import java.util.Deque;
import java.util.LinkedList;

import com.ohua.engine.flowgraph.elements.AbstractArcQueue;
import com.ohua.engine.flowgraph.elements.packets.IStreamPacket;

/**
 * Instead of a direct arc, this is a chunk of work which gets assigned to an arc
 * implementation.
 * @author sertel
 * 
 */
public class WorkChunk extends AbstractArcQueue
{
  /**
   * This is pending work that is not present in this work chunk. This is needed during arc boundary checking.
   */
  private int _lowerWorkBound = 0;
  private int _memoizedSize = 0;

  // FIXME use an ArrayList of fixed length or even better an ArrayDeque!
  @Override
  protected Deque<Object> newDataQueue()
  {
    return new LinkedList<>();
  }

  protected WorkChunk memorize(){
    _memoizedSize = super.getDataPacketAmount();
    return this;
  }

  public int getMemoizedSize() { return _memoizedSize; }

  protected WorkChunk setLowerWorkBound(int bound){
    _lowerWorkBound = bound;
    return this;
  }

  protected int getWorkSize(){
    return _lowerWorkBound + super.size();
  }

  protected void addFirst(Object o){
    _queue.addFirst(o);
  }
}
