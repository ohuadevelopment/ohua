/*
 * Copyright (c) Sebastian Ertel 2017. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.scheduler;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.SystemPhaseType;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.sections.SectionGraph;

import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by sertel on 1/21/17.
 */
public interface IRuntime {
  void launch(Set<OperatorCore> ops);

  void start(SystemPhaseType systemPhase);
  void teardown();

  void onDone(Consumer<Optional<Throwable>> onDone);

  void activate(OperatorCore op);
}
