/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.flowgraph.elements.operator;

/**
 * Created by sertel on 3/24/16.
 */
public interface IDone {
  boolean isComputationComplete();
}
