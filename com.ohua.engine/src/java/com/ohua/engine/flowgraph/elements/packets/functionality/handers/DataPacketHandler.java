/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality.handers;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ohua.engine.Maybe;
import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.extension.points.IDataPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.AbstractArcImpl;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.InputPort.VisitorReturnType;


public class DataPacketHandler implements IDataPacketHandler
{
  private Logger _logger = Logger.getLogger(getClass().getCanonicalName());
  
  public void notifyMarkerArrived(InputPort port, DataPacket packet)
  {
    _logger.log(Level.ALL, "DataPacketMixin of input port " + port.getOwner().getOperatorName() + " handles data packet!");

    // TODO just change the interface and reuse the Maybe object
    port.setCurrentPacketToBeReturned(Maybe.value(packet));
    port.setVisitorReturnStatus(VisitorReturnType.PACKET_WAS_HANDLED);
  }
  
  public void addCallback(InputPortEvents event, InputPort port)
  {
    // nothing to register here!
  }

  public void init()
  {
    // nothing for init here
  }

  public void restartInit()
  {
    // nothing here
  }
  
  public void removeCallback(InputPortEvents event, InputPort port)
  {
    // nothing here
  }
  
}
