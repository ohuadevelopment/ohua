/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets;

import com.ohua.engine.SystemPhaseType;

public interface ActivationMarker extends IMetaDataPacket
{
  SystemPhaseType getPhaseType();
}
