/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.operator;


public abstract class UserOperator extends AbstractOperatorAlgorithm implements
                                                                    OperatorStateAccess
{
  @Override
  protected final void setOperatorAlgorithmAdapter(OperatorAlgorithmAdapter adapter)
  {
    super.setOperatorAlgorithmAdapter(adapter);
  }
}
