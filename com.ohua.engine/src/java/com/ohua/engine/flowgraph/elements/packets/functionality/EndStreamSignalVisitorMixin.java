/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.ohua.engine.extension.points.IEndOfStreamPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.EndOfStreamPacket;

public class EndStreamSignalVisitorMixin extends
                                        VisitorMixin<EndOfStreamPacket, IEndOfStreamPacketHandler>
{
  public EndStreamSignalVisitorMixin(InputPort in)
  {
    super(in);
  }
  
  // TODO: A cleaner separation is needed! There should be only one guy that handles the packet,
  // but there can be multiple guys registered that receive a notification once one of those
  // packets arrives! (For now, I just make sure that really only one of the registered handlers
  // actually handles the packet!)
  @Override
  public void handlePacket(EndOfStreamPacket packet)
  {
    notifyHandlers(packet);
  }
  
  /**
   * We notify the attached handlers. The "root" EOS packet handler will be notified last,
   * always!
   */
  public void notifyHandlers(EndOfStreamPacket packet)
  {
    List<IEndOfStreamPacketHandler> handlers =
        new ArrayList<IEndOfStreamPacketHandler>(getAllHandlers());
    // sorts in ascending order (that's why the comparator function is actually the other way
    // around than in the doc described!)
    Collections.sort(handlers,
                     new PriorityComparator(InputPortEvents.END_OF_STREAM_PACKET_ARRIVAL));
    for(IEndOfStreamPacketHandler handler : handlers)
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }
  
  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.END_OF_STREAM_PACKET_ARRIVAL;
  }
  
}
