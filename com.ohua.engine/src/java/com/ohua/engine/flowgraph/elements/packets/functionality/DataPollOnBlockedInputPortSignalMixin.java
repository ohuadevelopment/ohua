/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality;

import com.ohua.engine.extension.points.IDataPollOnBlockedInputPortHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.DataPollOnBlockedInputPortEvent;

public class DataPollOnBlockedInputPortSignalMixin extends
                                                  VisitorMixin<DataPollOnBlockedInputPortEvent, IDataPollOnBlockedInputPortHandler>
{
  
  public DataPollOnBlockedInputPortSignalMixin(InputPort in)
  {
    super(in);
  }
  
  @Override
  public void handlePacket(DataPollOnBlockedInputPortEvent packet)
  {
    notifyHandlers(packet);
  }
  
  public void notifyHandlers(DataPollOnBlockedInputPortEvent packet)
  {
    for(IDataPollOnBlockedInputPortHandler handler : getAllHandlers())
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }
  
  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.DATA_POLL_ON_BLOCKED_INPUT_PORT;
  }
  
}
