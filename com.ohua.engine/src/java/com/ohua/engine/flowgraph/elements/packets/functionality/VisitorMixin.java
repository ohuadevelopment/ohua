/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ohua.engine.extension.points.IPacketHandler;
import com.ohua.engine.extension.points.IPacketVisitor;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.packets.IPacket;

/**
 * Too bad, we can not make those mixins stateless!<br>
 * <p>
 * In the end there will be a visitor registered for every INPUT PORT of each operator. All
 * visitors of an operator will report to one handler, that cares about the actual functionality
 * and cares about the forwarding of the marker!
 * <p>
 * The mixin layer should decide what to do next, the notification is just for all guys that
 * need the information that a packet of a certain type has arrived!
 * @param <T>
 */
public abstract class VisitorMixin<T extends IPacket, S extends IPacketHandler> implements
                                                                                IPacketVisitor<S, T>
{
  protected InputPort _inputPort = null;
  protected List<S> _handlers = new ArrayList<S>();
  
  public VisitorMixin(InputPort in)
  {
    _inputPort = in;
  }
  
  public abstract void handlePacket(T packet);
  
  public void registerMarkerHandler(S handler)
  {
    assert handler != null;
    if(_handlers.contains(handler))
    {
      return;
    }
    
    _handlers.add(handler);
    handler.addCallback(getEventResponsibility(), _inputPort);
  }
  
  public void unregisterMarkerHandler(S handler)
  {
    handler.removeCallback(getEventResponsibility(), _inputPort);
    _handlers.remove(handler);
  }
  
  public Set<S> getAllHandlers()
  {
    return new HashSet<S>(_handlers);
  }
  
}
