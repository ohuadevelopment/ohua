/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality;

import java.util.Set;

import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.extension.points.IDataPacketHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;

/**
 * Mind that at that level we do not need access to the data directly but only to the meta
 * information of the packets!!!
 */
public class DataSignalVisitorMixin extends VisitorMixin<DataPacket, IDataPacketHandler>
{
  
  public DataSignalVisitorMixin(InputPort in)
  {
    super(in);
  }
  
  @Override
  public void handlePacket(DataPacket packet)
  {
    notifyHandlers(packet);
  }
  
  // TODO REFACTORING this loop should be generic in the super class and the function maybe does
  // need to be in the interface! --> problem: can not push the notifyMarkerArrived() function
  // into PacketHandlerInterface!
  public void notifyHandlers(DataPacket packet)
  {
    Set<IDataPacketHandler> allHandlers = getAllHandlers();
    for(IDataPacketHandler handler : allHandlers)
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }
  
  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.DATA_PACKET_ARRIVAL;
  }
  
}
