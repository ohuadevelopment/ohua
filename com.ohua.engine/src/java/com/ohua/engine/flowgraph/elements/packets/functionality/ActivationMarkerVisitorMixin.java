/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.flowgraph.elements.packets.functionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ohua.engine.Maybe;
import com.ohua.engine.extension.points.IActivationMarkerHandler;
import com.ohua.engine.extension.points.InputPortEvents;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.InputPort.VisitorReturnType;
import com.ohua.engine.flowgraph.elements.packets.ActivationMarker;

public class ActivationMarkerVisitorMixin extends
                                         VisitorMixin<ActivationMarker, IActivationMarkerHandler>
{
  
  public ActivationMarkerVisitorMixin(InputPort in)
  {
    super(in);
  }
  
  public void notifyHandlers(ActivationMarker packet)
  {
    List<IActivationMarkerHandler> handlers =
        new ArrayList<IActivationMarkerHandler>(getAllHandlers());
    // sorts in ascending order (that's why the comparator function is actually the other way
    // around than in the doc described!)
    Collections.sort(handlers,
                     new PriorityComparator(InputPortEvents.ACTIVATION_PACKET_ARRIVAL));
    for(IActivationMarkerHandler handler : handlers)
    {
      handler.notifyMarkerArrived(_inputPort, packet);
    }
  }

  public InputPortEvents getEventResponsibility()
  {
    return InputPortEvents.ACTIVATION_PACKET_ARRIVAL;
  }
  
  @Override
  public void handlePacket(ActivationMarker packet)
  {
    notifyHandlers(packet);
    
    _inputPort.setCurrentPacketToBeReturned(Maybe.empty());
    _inputPort.setVisitorReturnStatus(VisitorReturnType.DEQUEUE_NEXT_PACKET);
  }
  
}
