/*
 * Copyright � Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.distributed.connection.resource;

import java.util.Map;

import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Poller;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.AbstractResource;
import com.ohua.engine.resource.management.ResourceAccess;

// TODO As a matter of fact, we somehow need to able to create more connections to that.
public class ZeroMQResource extends AbstractResource
{
  
  public ZeroMQResource(ManagerProxy ohuaProcess, Map<String, String> attributes)
  {
    super(ohuaProcess, attributes);
  }
  
  @Override
  public void validate()
  {
    // TODO validate! there is only a predefined set available in 0mq
    getAttributes().get("protocol");
    // TODO validate IP and port
    getAttributes().get("address");
    getAttributes().get("port");
  }
  
  @Override
  protected AbstractConnection getConnection(Object... args)
  {
    AbstractConnection cnn = null;
    ZMQ.Socket socket = null;
    ZMQ.Context context = ZMQ.context(1);
    
    String address =
        getAttributes().get("protocol") + "://" + getAttributes().get("address") + ":"
            + getAttributes().get("port");
    System.out.println("Address: " + address);
    
    if(((Integer) args[0]) == ZMQ.PULL)
    {
      socket = context.socket(ZMQ.PULL);
      socket.setRcvHWM(1);
      socket.bind(address);
      
      /*
       * In order to take this thing down even when blocked we use another (local) connection
       * and the polling concept of ZeroMQ.
       */
      ZMQ.Socket pub = context.socket(ZMQ.PUB);
      pub.bind("inproc://endpoint");
      ZMQ.Socket sub = context.socket(ZMQ.SUB);
      sub.connect("inproc://endpoint");
      sub.subscribe("RETURN".getBytes());
      
      Poller poller = context.poller(2);
      poller.register(socket, ZMQ.Poller.POLLIN);
      poller.register(sub, ZMQ.Poller.POLLIN);
      
      cnn = new ZeroMQPullConnection(socket, poller, pub, sub);
    }
    else if(((Integer) args[0]) == ZMQ.PUSH)
    {
      /*
       * Note the following two options: 
       * 
       * sock.setsockopt(zmq.HWM, 1000)
       * sock.setsockopt(zmq.SWAP, 200)
       * 
       * The first means that the socket will block when the memory buffer reaches 1000 messages.
       * The second means that the socket will spill messages to disk if the buffer reaches 2000 messages.
       */
      
      socket = context.socket(ZMQ.PUSH);
      socket.setSndHWM(1);
      cnn = new ZeroMQPushConnection(socket);
      socket.connect(address);
    }
    else
    {
      throw new UnsupportedOperationException();
    }
    
    return cnn;
  }
  
  /**
   * Just don't use one and make the connection blocking. This is fine now that we have support
   * to handle blocking I/O.</br> Maybe this hook should just go away.
   */
  @Override
  protected AbstractExternalActivator getExternalActivator(OperatorID operatorID)
  {
    return null;
  }

  @Override
  public ResourceAccess getResourceAccess(OperatorID operatorID, String opChckPtArtifactID) {
    throw new UnsupportedOperationException();
  }
  
}
