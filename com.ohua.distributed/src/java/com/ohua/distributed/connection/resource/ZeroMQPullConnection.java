package com.ohua.distributed.connection.resource;

import org.zeromq.ZMQ;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.ConnectionID;
import com.ohua.engine.resource.management.IUnboundedInput;
import com.ohua.engine.resource.management.ResourceConnection;

public class ZeroMQPullConnection extends AbstractConnection implements IUnboundedInput
{
  public interface ZeroMQPull
  {
    public String pull();
  }
  
  private class ZeroMQPullResourceConnection extends ResourceConnection implements ZeroMQPull
  {
    public ZeroMQPullResourceConnection(ConnectionID connectionID)
    {
      super(connectionID);
    }
    
    public String pull()
    {
      // this hopefully means that the call is blocking!
//      System.out.println("Pull");
      // ZMsg msg = ZMsg.recvMsg(_puller);
      // System.out.println("Received message: " + msg);
      return new String(_puller.recv(0));
    }
  }
  
  private class AdvancedZeroMQPullResourceConnection extends ZeroMQPullResourceConnection
  {
    public AdvancedZeroMQPullResourceConnection(ConnectionID connectionID)
    {
      super(connectionID);
    }
    
    public String pull()
    {
      if(!_finished)
      {
        _poller.poll();
        if(_poller.pollin(0))
        {
          return super.pull();
        }
        if(_poller.pollin(1))
        {
          String msg = new String(_controlOut.recv(0));
          Assertion.invariant(msg.equals("RETURN"));
          throw new RuntimeException("Shutdown!");
        }
      }
      else
      {
        throw new RuntimeException("Shutdown!");
      }
      Assertion.impossible();
      return null;
    }
  }
  
  private ZMQ.Socket _puller = null;
  
  // advanced shutdown support
  private ZMQ.Poller _poller = null;
  private ZMQ.Socket _controlIn = null;
  private ZMQ.Socket _controlOut = null;
  
  private boolean _finished = false;
  
  protected ZeroMQPullConnection(ZMQ.Socket puller)
  {
    _puller = puller;
  }
  
  protected ZeroMQPullConnection(ZMQ.Socket puller,
                                 ZMQ.Poller poller,
                                 ZMQ.Socket controlIn,
                                 ZMQ.Socket controlOut)
  {
    this(puller);
    _poller = poller;
    _controlIn = controlIn;
    _controlOut = controlOut;
  }
  
  @Override
  protected ResourceConnection getRestrictedConnection()
  {
    if(_poller == null)
    {
      return new ZeroMQPullResourceConnection(getConnectionID());
    }
    else
    {
      return new AdvancedZeroMQPullResourceConnection(getConnectionID());
    }
  }
  
  @Override
  public void finish()
  {
    _finished = true;
    _controlIn.send("RETURN".getBytes(), ZMQ.NOBLOCK);
  }
  
  @Override
  public void restart()
  {
    _finished = false;
  }
  
  @Override
  protected void close() throws Throwable
  {
    _puller.close();
  }
  
  @Override
  protected void rollback() throws Throwable
  {
    throw new UnsupportedOperationException();
  }
  
  @Override
  protected void commit() throws Throwable
  {
    throw new UnsupportedOperationException();
  }
  
}
