package com.ohua.distributed.connection.resource;

import org.zeromq.ZMQ;

import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.ConnectionID;
import com.ohua.engine.resource.management.ResourceConnection;

public class ZeroMQPushConnection extends AbstractConnection
{
  public interface ZeroMQPush
  {
    public void push(String data);
  }
  
  private class ZeroMQPushResourceConnection extends ResourceConnection implements ZeroMQPush
  {
    public ZeroMQPushResourceConnection(ConnectionID connectionID)
    {
      super(connectionID);
    }
    
    public void push(String data)
    {
      byte[] b = data.getBytes();
      _pusher.send(b,0);
    }
  }
  
  private ZMQ.Socket _pusher = null;
  
  protected ZeroMQPushConnection(ZMQ.Socket pusher)
  {
    _pusher = pusher;
  }
  
  @Override
  protected ResourceConnection getRestrictedConnection()
  {
    return new ZeroMQPushResourceConnection(getConnectionID());
  }
  
  @Override
  protected void close() throws Throwable
  {
    _pusher.close();
  }
  
  @Override
  protected void rollback() throws Throwable
  {
    throw new UnsupportedOperationException();
  }
  
  @Override
  protected void commit() throws Throwable
  {
    throw new UnsupportedOperationException();
  }
  
}
