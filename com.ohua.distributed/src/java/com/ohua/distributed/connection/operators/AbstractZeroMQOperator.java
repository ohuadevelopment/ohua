/*
 * Copyright � Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.distributed.connection.operators;

import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.resource.management.ResourceManager;

public abstract class AbstractZeroMQOperator<T> extends UserOperator
{
  public ZeroMQOperatorProperties _properties = null;
  
  protected T _cnn = null;
  
  @SuppressWarnings("unchecked")
  @Override
  public void prepare()
  {
    _cnn =
        (T) ResourceManager.getInstance().openDirectConnection(getOperatorID(),
                                                                  _properties.resourceID,
                                                                  getMode());
  }
  
  protected abstract int getMode();
  
  @Override
  public void cleanup()
  {
    // nothing to be done
  }
  
  @Override
  public Object getState()
  {
    return null;
  }
  
  @Override
  public void setState(Object checkpoint)
  {
    // stateless operator
  }
  

}
