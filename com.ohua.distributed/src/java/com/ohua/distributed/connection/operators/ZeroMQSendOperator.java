/*
 * Copyright � Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.distributed.connection.operators;

import org.zeromq.ZMQ;

import com.ohua.distributed.connection.resource.ZeroMQPushConnection.ZeroMQPush;
import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.operators.categories.OutputIOOperator;

public class ZeroMQSendOperator extends AbstractZeroMQOperator<ZeroMQPush> implements OutputIOOperator
{
  private InputPortControl _inControl = null;
  
  @Override
  public void prepare()
  {
    super.prepare();
    _inControl = getDataLayer().getInputPortController("input");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inControl.next())
    {
//      System.out.println("Sent: " + _inControl.dataToString("JSON"));
      _cnn.push(_inControl.dataToString("JSON"));
    }
  }
  
  @Override
  protected int getMode()
  {
    return ZMQ.PUSH;
  }
}
