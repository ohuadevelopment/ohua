/*
 * Copyright � Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.distributed.connection.operators;

import org.zeromq.ZMQ;

import com.ohua.distributed.connection.resource.ZeroMQPullConnection.ZeroMQPull;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.operators.categories.InputIOOperator;

public class ZeroMQReceiveOperator extends AbstractZeroMQOperator<ZeroMQPull> implements InputIOOperator
{
  private OutputPortControl _outControl = null;
  
  @Override
  public void prepare()
  {
    super.prepare();
    _outControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(true)
    {
      String msg = _cnn.pull();
//      System.out.println("Received: " + msg);
      _outControl.newPacket();
      _outControl.parse(msg, "JSON");
      if(_outControl.send())
      {
        break;
      }
    }
  }

  @Override
  protected int getMode()
  {
    return ZMQ.PULL;
  }

  @Override
  public boolean isUnboundedInput()
  {
    return true;
  }
}
