/*
 * Copyright � Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.distributed.connection.operators;

import java.io.Serializable;

public class ZeroMQOperatorProperties implements Serializable
{
    public String resourceID = null;
}
