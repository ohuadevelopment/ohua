/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.DataFlowProcess;
import com.ohua.engine.OhuaProcessManager;
import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.engine.operators.GeneratorOperator;
import com.ohua.engine.operators.GeneratorOperator.GeneratorProperties;
import com.ohua.engine.operators.PeekOperator;
import com.ohua.engine.resource.management.ResourceConnection;
import com.ohua.engine.resource.management.ResourceManager;
import com.ohua.engine.resource.management.TestingResourceAccess;
import com.ohua.engine.utils.OhuaLoggerFactory;
import com.ohua.operators.io.AbstractDatabaseOperator.DatabaseProperties;
import com.ohua.operators.io.DatabaseNonBatchWriterOperator;
import com.ohua.operators.io.DatabaseReaderOperator;
import com.ohua.operators.io.resources.DerbyServerUtils;

@Ignore
public class AbstractIOTestCase extends AbstractFlowTestCase
{
  @BeforeClass
  public static void testStartDerbyServer() throws Throwable
  {
    DerbyServerUtils.startServer(new String[] {});
  }
  
  @AfterClass
  public static void testStopDerbyServer() throws Throwable
  {
    // close all connections!
    // DatabaseResource resource =
    // (DatabaseResource) ResourceManager.getInstance().getResource("derbyDatabase");
    // resource.closeAllConnections();
    
    DerbyServerUtils.shutdown();
    Thread.sleep(1000);
  }
  
  public void fillTable(String tableName, int packetCount, Map<String, String> columns) throws Throwable
  {
    String tmpSchema = prepareSourceSchema(columns);
    
    runFillTableFlow(tableName, packetCount, tmpSchema, columns.keySet());
    
    tableRegressionCheck(tableName, packetCount);
  }

  public void fillTable(String tableName, int packetCount, String... columns) throws Throwable
  {
    Map<String, String> cols = new HashMap<>();
    for(String column : columns)
    {
      cols.put(column, "");
    }
    
    fillTable(tableName, packetCount, cols);
  }

  private String prepareSourceSchema(Map<String, String> columns) throws IOException
  {
    String tmpSchema = getTestMethodOutputDirectory() + "tmp.json";
    FileWriter writer = new FileWriter(tmpSchema);
    writer.write("{\"context\":{");
    for(Map.Entry<String, String> columnEntry : columns.entrySet())
    {
      writer.write(columnEntry.getKey() + ":\"" + columnEntry.getValue() + "\",");
    }
    writer.write("}}");
    writer.close();
    return tmpSchema;
  }

  private void runFillTableFlow(String tableName,
                                int packetCount,
                                String tmpSchema,
                                Collection<String> columns) throws Throwable
  {
    DataFlowProcess proc = new DataFlowProcess();
    AbstractProcessManager manager =
        new OhuaProcessManager(proc, new RuntimeProcessConfiguration());
    
    OperatorCore gen =
        OperatorFactory.getInstance().createUserOperatorCore(proc.getGraph(), "Generator");
    gen.setOperatorName("Generator");
    GeneratorProperties props = new GeneratorProperties();
    props.setAmountToGenerate(packetCount);
    props.setPathToSchemaFile(tmpSchema);
    ((GeneratorOperator) gen.getOperatorAlgorithm()).setProperties(props);
    
    OperatorCore dbWriter =
        OperatorFactory.getInstance().createUserOperatorCore(proc.getGraph(),
                                                             "DatabaseNonBatchWriter");
    dbWriter.setOperatorName("DatabaseWriter");
    
    ((DatabaseNonBatchWriterOperator) dbWriter.getOperatorAlgorithm()).getProperties().tableName =
        tableName;

    Map<String, String> colDefs = new HashMap<>();
    for(String column : columns)
    {
      colDefs.put(column, "VARCHAR(32)");
    }
    ((DatabaseNonBatchWriterOperator) dbWriter.getOperatorAlgorithm()).getProperties().columns =
        colDefs;
    
    Map<String, String> adaptations = new HashMap<>();
    for(String column : columns)
    {
      adaptations.put("context/" + column, column);
    }
    ((DatabaseNonBatchWriterOperator) dbWriter.getOperatorAlgorithm()).getProperties().adaptation =
        adaptations;

    Arc arc1 = new Arc(gen.getOutputPort("output"), dbWriter.getInputPort("input"));
    proc.getGraph().addArc(arc1);
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();
    manager.runFlow();
    manager.awaitSystemPhaseCompletion();
    manager.tearDownProcess();
    manager.awaitSystemPhaseCompletion();
  }
  
  protected void fillTable(String tableName, String... columns) throws Throwable
  {
    fillTable(tableName, 100, columns);
  }
  
  protected void tableRegressionCheck(Map<String, Long> outputTables) throws Throwable
  {
    for(Map.Entry<String, Long> outputTable : outputTables.entrySet())
    {
      tableRegressionCheck(outputTable.getKey(), outputTable.getValue());
    }
  }

  public static void tableRegressionCheck(String tableName, long expectedRowCount) throws Throwable
  {
    tableRegressionCheck("derby01", tableName, expectedRowCount);
  }
  
  public static void tableRegressionCheck(String resourceID,
                                          String tableName,
                                          long expectedRowCount) throws Throwable
  {
    Connection cnn = (Connection) ResourceManager.getInstance().openConnection(resourceID);
    Statement stmt = cnn.createStatement();
    ResultSet rs = stmt.executeQuery("select count(*) from " + tableName);
    
    rs.next();
    
    int actualRowCount = rs.getInt(1);
    if(actualRowCount != expectedRowCount)
    {
//      System.out.println("TEST FAILED!");
//      System.out.println("Table: " + tableName + " expected row count: " + expectedRowCount
//                         + " actual row count: " + actualRowCount);
      Assert.fail("expected row count: " + expectedRowCount + " actual row count: "
                     + actualRowCount);
    }
    else
    {
//      System.out.println("TEST PASSED!");
//      System.out.println("Table: " + tableName + " expected row count: " + expectedRowCount
//                         + " actual row count: " + actualRowCount);
      
    }
    try
    {
      rs.close();
      stmt.close();
      TestingResourceAccess.getInstance().commitAndCloseTransaction((ResourceConnection) cnn);
    }
    catch(Throwable t)
    {
      // don't care. fix this later
    }
  }
  
  public static final void columnBoundaryCheck(String tableName,
                                               String columnName,
                                               double expectedLowerBoundary,
                                               double expectedUpperBoundary) throws Throwable
  {
    Connection cnn = (Connection) ResourceManager.getInstance().openConnection("derby01");
    Statement stmt = cnn.createStatement();

    ResultSet rs = stmt.executeQuery("select min(" + columnName + ") from " + tableName);
    rs.next();
    double actualLowerBoundary = rs.getDouble(1);
    if(actualLowerBoundary != expectedLowerBoundary)
    {
//      System.out.println("TEST FAILED!");
//      System.out.println("Table: " + tableName + " Column: " + columnName
//                         + " expected lower boundary: " + expectedLowerBoundary
//                         + " actual lower boundary: " + actualLowerBoundary);
      Assert.fail("Table: " + tableName + " Column: " + columnName
                  + " expected lower boundary: " + expectedLowerBoundary
                  + " actual lower boundary: " + actualLowerBoundary);
    }
    else
    {
//      System.out.println("TEST PASSED!");
//      System.out.println("Table: " + tableName + " Column: " + columnName
//                         + " expected lower boundary: " + expectedLowerBoundary
//                         + " actual lower boundary: " + actualLowerBoundary);
    }
    
    try
    {
      rs.close();
    }
    catch(Throwable t)
    {
      // don't care. fix this later
    }
    
    rs = stmt.executeQuery("select max(" + columnName + ") from " + tableName);
    rs.next();
    double actualUpperBoundary = rs.getInt(1);
    if(actualUpperBoundary != expectedUpperBoundary)
    {
//      System.out.println("TEST FAILED!");
//      System.out.println("Table: " + tableName + " Column: " + columnName
//                         + " expected upper boundary: " + expectedUpperBoundary
//                         + " actual upper boundary: " + actualUpperBoundary);
      Assert.fail("Table: " + tableName + " Column: " + columnName
                  + " expected upper boundary: " + expectedUpperBoundary
                  + " actual upper boundary: " + actualUpperBoundary);
    }
    else
    {
//      System.out.println("TEST PASSED!");
//      System.out.println("Table: " + tableName + " Column: " + columnName
//                         + " expected upper boundary: " + expectedUpperBoundary
//                         + " actual upper boundary: " + actualUpperBoundary);
    }

    try
    {
      rs.close();
      stmt.close();
      TestingResourceAccess.getInstance().commitAndCloseTransaction((ResourceConnection) cnn);
    }
    catch(Throwable t)
    {
      // don't care. fix this later
    }
  }
  
  protected void fillTablewithRealData(String tableName,
                                       LinkedHashMap<String, String> colNameType,
                                       List<List<?>> dataRows) throws Throwable
  {
    Connection cnn = (Connection) ResourceManager.getInstance().openConnection("derby01");
    
    Statement stmt = null;
    stmt = cnn.createStatement();
    try
    {
      stmt.execute("drop table " + tableName);
      stmt.close();
    }
    catch(Throwable t)
    {
      //
      stmt.close();
    }
    
    stmt = cnn.createStatement();
    StringBuffer createQuery = new StringBuffer("create table " + tableName + " ( ");
    
    for(Map.Entry<String, String> col : colNameType.entrySet())
    {
      createQuery.append(col.getKey() + " " + col.getValue() + " ,");
    }
    
    createQuery.deleteCharAt(createQuery.length() - 1);
    
    createQuery.append(")");
    
    stmt.execute(createQuery.toString());
    stmt.close();
    
    // insert the data into the table
    StringBuffer colNames = new StringBuffer();
    colNames.append("insert into " + tableName + " (");
    
    for(Map.Entry<String, String> col : colNameType.entrySet())
    {
      colNames.append(col.getKey() + ", ");
    }
    
    colNames.deleteCharAt(colNames.length() - 2);
    colNames.append(" ) values ( ");
    
    for(int i = 0; i < colNameType.size(); i++)
    {
      colNames.append("? ,");
    }
    
    colNames.deleteCharAt(colNames.length() - 1);
    colNames.append(")");
    
    try
    {
      PreparedStatement insertStmt = cnn.prepareStatement(colNames.toString());
      for(List<?> row : dataRows)
      {
        for(int j = 0; j < row.size(); j++)
        {
          insertStmt.setObject(j + 1, row.get(j));
        }
        insertStmt.addBatch();
      }
      
      insertStmt.executeBatch();
      insertStmt.close();
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    
    stmt.close();
    
    TestingResourceAccess.getInstance().commitAndCloseTransaction((ResourceConnection) cnn);
  }
  
  protected final void printData(String tableName) throws Throwable
  {
    outputLogToFile(OhuaLoggerFactory.getLogIDForOperator(PeekOperator.class,
                                                          "Packet-Output",
                                                          "2"));
    outputLogToFile(OhuaLoggerFactory.getLogIDForOperator(ConsumerOperator.class,
                                                          "TableContent-Output",
                                                          "3"));
    String pathToFlow =
        getTestClassInputDirectory() + "../genericFlows/read-table-contents-flow.xml";
    AbstractProcessManager manager = loadProcess(pathToFlow);
    DatabaseProperties properties = new DatabaseProperties();
    properties.tableName = tableName;
    ((DatabaseReaderOperator) (manager.getProcess().getGraph().getOperator("Reader")).getOperatorAlgorithm()).setProperties(properties);
    runFlowNoAssert(manager);
  }
}
