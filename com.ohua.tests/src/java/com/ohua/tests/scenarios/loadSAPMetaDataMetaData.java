/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.scenarios;

import org.junit.Test;

import com.ohua.tests.AbstractIOTestCase;

public class loadSAPMetaDataMetaData extends AbstractIOTestCase
{
  /**
   * Get a somewhat more readable overview of the columns and the data.
   */
  @Test
  public void fromFileToFile() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-metadata-metadata-flow.xml");
  }
}
