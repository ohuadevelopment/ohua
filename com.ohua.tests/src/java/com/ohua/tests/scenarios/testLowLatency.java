/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.scenarios;

import java.util.Collections;
import java.util.LinkedList;

import org.junit.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.OhuaProcessRunner;
import com.ohua.engine.OhuaTestProcessRunner;
import com.ohua.engine.SimpleProcessListener;
import com.ohua.engine.UserRequest;
import com.ohua.engine.UserRequestType;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.tests.features.testOnlineReconfiguration.ReconfigPropertyOperator;
import com.ohua.tests.multithreading.AbstractNonBlockIOTestCase;

/**
 * Currently the style of processing is defined by the arc size. If the arc size is big then we
 * favor high throughput because we reduce the scheduling in the system by passing data in
 * between the operators around in batches.</p>
 * 
 * Another scenario that Ohua needs to handle is low latency. In these cases, whenever there is
 * data on an inter-section arc the downstream section needs to be scheduled immediately.
 * 
 * Furthermore, in such a scenario the assignment of operators to sections is very important.
 * The major question is: Should the I/O section contain only the input operator or another one?
 * The intuitive answer is no, but who knows?!
 * 
 * Finally, there exists yet a fundamental problem still: If we do allow blocking I/O operations
 * then how do we make sure that an operator is still responsive, e.g. can still propagate
 * markers?
 * 
 * @author sertel
 * 
 */
public class testLowLatency extends AbstractNonBlockIOTestCase
{
  
  /**
   * We use the non-blocking operator defined in the abstract class and leverage the operator
   * configuration overlay network to show that we still can process markers even though we are
   * blocked.
   * @throws Throwable
   */
  @Ignore
  @Test
  // FIXME this test case is broken because the non-blocking IO support is broken (see work item #39)
  public void testBasic() throws Throwable
  {
    BlockingInputProducer.BLOCKING_TIME = 10000;
    patch("Long-IO-Generator");
    
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "blocking-low-latency-flow.xml",
                    getTestMethodInputDirectory() + "runtime.properties");
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();
    System.out.println("Preparation finished!");
    
    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-op-reconfig-process").start();
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("ReconfigurationTarget");
    ReconfigPropertyOperator reconfigOp =
        (ReconfigPropertyOperator) core.getOperatorAlgorithm();
    Assert.assertEquals("world", reconfigOp._properties.someTestProperty2);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(1000);
    System.out.println("Reconfiguring sub property ...");
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT,
                                             new LinkedList<IMetaDataPacket>(
                                                                Collections.singletonList(PacketFactory.createConfigurationMarkerPacket("ReconfigurationTarget",
                                                                                                              "properties.someTestProperty2",
                                                                                                              "earth")))));
    System.out.println("Waiting a bit ...");
    Thread.sleep(5000);
    Assert.assertEquals("earth", reconfigOp._properties.someTestProperty2);
    
    System.out.println("Waiting for processing to finish ...");
    listener.awaitAndReset();
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
}
