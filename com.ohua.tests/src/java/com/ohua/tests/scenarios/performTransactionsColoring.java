/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.scenarios;

import java.util.logging.Level;

import org.junit.Test;

import com.ohua.engine.operators.PeekOperator;
import com.ohua.engine.utils.OhuaLoggerFactory;
import com.ohua.operators.io.DatabaseBatchWriterOperator;
import com.ohua.operators.io.DatabaseReaderOperator;
import com.ohua.tests.AbstractIOTestCase;

/**
 * This is a test scenario that uses the SAP sales data and goes into the BI direction.
 * @author sertel
 * 
 */
public class performTransactionsColoring extends AbstractIOTestCase
{
  @Test
  public void colorSAPSalesTransactions() throws Throwable
  {
    outputOperatorLogToFile(PeekOperator.class, "Log", "18", Level.INFO);
    runFlowNoAssert(getTestClassInputDirectory() + "transcations-coloring-flow.xml");
    tableRegressionCheck("derby03", "BI.SHORT_TA", 8882);
    tableRegressionCheck("derby03", "BI.LONG_TA", 22);
  }
  
  @Test
  public void colorSAPSalesTAsCP() throws Throwable
  {
    runFlowNoAssert(getTestClassInputDirectory() + "transcations-coloring-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    tableRegressionCheck("derby03", "BI.SHORT_TA", 8882);
    tableRegressionCheck("derby03", "BI.LONG_TA", 22);
  }

  @Test
  public void colorSAPSalesTAsMT() throws Throwable
  {
    runFlowNoAssert(getTestClassInputDirectory() + "transcations-coloring-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    tableRegressionCheck("derby03", "BI.SHORT_TA", 8882);
    tableRegressionCheck("derby03", "BI.LONG_TA", 22);
  }
  
  @Test
  public void colorSAPSalesTAsMTDebug() throws Throwable
  {
    outputLogToFile(OhuaLoggerFactory.getLogIDForOperator(DatabaseReaderOperator.class,
                                                          "ResultInput",
                                                          "8")
                    + "-result", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Long-Categorization-Log", "16", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Short-Categorization-Log", "17", Level.INFO);
    
    outputOperatorLogToFile(PeekOperator.class, "Rejoin1-drop", "20", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Rejoin2-drop", "21", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-MVG-AVG", "22", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Rejoin-1-Match", "23", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Rejoin-2-Match", "24", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Categorization-12-Long", "25", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Categorization-12-Short", "26", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Categorization-13-Long", "27", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Log-Categorization-13-Short", "28",
                    Level.INFO);

    outputLogToFile(OhuaLoggerFactory.getLogIDForOperator(DatabaseBatchWriterOperator.class,
                                                          "DatabaseOutput-Long",
                                                          "18")
                    + "-result", Level.INFO);
    outputLogToFile(OhuaLoggerFactory.getLogIDForOperator(DatabaseBatchWriterOperator.class,
                                                          "DatabaseOutput-Short",
                                                          "19")
                    + "-result", Level.INFO);
    
    runFlowNoAssert(getTestClassInputDirectory() + "transcations-coloring-debugging-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    tableRegressionCheck("derby03", "BI.SHORT_TA", 8882);
    tableRegressionCheck("derby03", "BI.LONG_TA", 22);
  }
  
  @Test
  public void colorSAPSalesTAsCPMT() throws Throwable
  {
    
    runFlowNoAssert(getTestClassInputDirectory() + "transcations-coloring-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    tableRegressionCheck("derby03", "BI.SHORT_TA", 8882);
    tableRegressionCheck("derby03", "BI.LONG_TA", 22);
  }

}
