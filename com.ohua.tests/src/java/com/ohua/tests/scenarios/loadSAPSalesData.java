/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.scenarios;

import org.junit.Test;

import com.ohua.tests.AbstractIOTestCase;

public class loadSAPSalesData extends AbstractIOTestCase
{
  /**
   * This flow will take the meta data file as input and help to create a first cut on the
   * column definitions of each Sales table. The results will be stored in separate output files
   * for each table.
   * @throws Throwable
   */
  @Test
  public void createColumnDefinitions() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-table-metadata-extraction-flow.xml");
  }

  @Test
  public void createSourceSchemas() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-source-schema-creation-flow.xml");
  }
  
  /**
   * Load the master data table for the Customer data.
   * @throws Throwable
   */
  @Test
  public void loadKNA1Master() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-kna1-load-flow.xml");
    tableRegressionCheck("debry02", "SAP.KNA1", 8291);
  }
  
  /**
   * Loads the main Sales document (Header).
   * @throws Throwable
   */
  @Test
  public void loadVBAK() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-vbak-load-flow.xml");
    tableRegressionCheck("derby02", "SALES.VBAK", 8904);
  }
  
  /**
   * Loads the VBAP Sales document (Item Info).
   * @throws Throwable
   */
  @Test
  public void loadVBAP() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-vbap-load-flow.xml");
    tableRegressionCheck("derby02", "SALES.VBAP", 17320);
  }
  
  /**
   * Loads the VBPA Sales document (Partners).
   * @throws Throwable
   */
  @Test
  public void loadVBPA() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "SAP-vbpa-load-flow.xml");
    tableRegressionCheck("derby02", "SALES.VBPA", 201719);
  }

}
