/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.exceptions;

/**
 * Use this exception for testing purposes to advanced the state of the program to a certain
 * point that you want to analyse.
 * @author sertel
 * 
 */
public class StopException extends RuntimeException
{
  // nothing special
}
