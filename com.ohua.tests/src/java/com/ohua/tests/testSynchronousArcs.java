/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.ProcessRunner;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser;

@Ignore // this is obsolete. fusion is done differently in Ohua now (/soon).
public class testSynchronousArcs extends AbstractFlowTestCase
{
  @Test(timeout = 10000)
  // normally runs in below 2ms
  public void testSingleSection() throws Throwable
  {
    FlowGraph graph =
        runFlowGetGraph(getTestMethodInputDirectory() + "flow.xml",
                        getTestMethodInputDirectory() + "runtime.properties");
    Assert.assertEquals(1000,
                        ((ConsumerOperator) graph.getOperator("Consumer").getOperatorAlgorithm()).getSeenPackets());
  }
  
  @Test(timeout=10000)
  // normally runs in below 3ms
  public void testMultipleSections() throws Throwable
  {
    FlowGraph graph =
        runFlowGetGraph(getTestMethodInputDirectory() + "flow.xml",
                        getTestMethodInputDirectory() + "runtime.properties");
    Assert.assertEquals(1000,
                        ((ConsumerOperator) graph.getOperator("Consumer").getOperatorAlgorithm()).getSeenPackets());
  }
  
  protected ProcessRunner createProcessRunner(String pathToFlow)
  {
    return new ProcessRunner(new OhuaParallelFlowParser(pathToFlow));
  }

}
