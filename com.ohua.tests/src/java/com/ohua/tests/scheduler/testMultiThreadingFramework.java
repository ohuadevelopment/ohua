/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.junit.Assert;

import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.OhuaProcessManager;
import com.ohua.engine.sections.OneOpOneSectionGraphBuilder;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.operators.PeekOperator;
import com.ohua.engine.utils.DebugReporter;
import com.ohua.engine.utils.OhuaLoggerFactory;
import com.ohua.operators.io.DatabaseBatchWriterOperator;
import com.ohua.tests.AbstractIOTestCase;

public class testMultiThreadingFramework extends AbstractIOTestCase
{
  /**
   * Generator -> Consumer
   */
  @Test(timeout = 10000)
  public void testOneOpOneSectionBuilder1() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory()
                    + "1-Op-1-Section-Builder-simple-correctness-flow.xml");
    
    OneOpOneSectionGraphBuilder builder = new OneOpOneSectionGraphBuilder(null);
    SectionGraph sectionGraph = builder.build(manager.getProcess().getGraph());
    
    Assert.assertEquals(2, sectionGraph.getAllSections().size());
    Assert.assertEquals(2, sectionGraph.getAllOperators().size());
    Assert.assertEquals(1, sectionGraph.getInputSections().size());
    Assert.assertEquals(1, sectionGraph.getOutputSections().size());
  }
  
  /**
   * More complex flow with merge and split
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testOneOpOneSectionBuilder2() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory()
                    + "1-Op-1-Section-Builder-complex-correctness-flow.xml");
    
    OneOpOneSectionGraphBuilder builder = new OneOpOneSectionGraphBuilder(null);
    SectionGraph sectionGraph = builder.build(manager.getProcess().getGraph());
    
    Assert.assertEquals(11, sectionGraph.getAllSections().size());
    Assert.assertEquals(11, sectionGraph.getAllOperators().size());
    Assert.assertEquals(2, sectionGraph.getInputSections().size());
    Assert.assertEquals(2, sectionGraph.getOutputSections().size());
  }
  
  /**
   * Generator -> DatabaseWriter
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testSimpleFlow() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "1-Op-1-Section-simple-correctness-flow.xml",
            getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    tableRegressionCheck("table_writer", 100);
  }
  
  /**
   * More complex flow with non-deterministic merge and split.<br/>
   * 2 threads
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testComplexFlow() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "1-Op-1-Section-complex-correctness-flow.xml",
            getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }
  
  /**
   * More complex flow with deterministic merge and split.<br/>
   * 2 threads
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testComplexFlow2() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "1-Op-1-Section-complex-correctness-flow-2.xml",
            getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }
  
  /**
   * More complex flow with non-deterministic merge and split.<br/>
   * More threads than operators.
   * @throws Throwable
   */
  @Test(timeout = 16000)
  public void testComplexFlow3() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "1-Op-1-Section-complex-correctness-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }

  /**
   * More complex flow with deterministic merge and split.<br/>
   * More threads than operators.
   * @throws Throwable
   */
  @Test(timeout = 16000)
  public void testComplexFlow4() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "1-Op-1-Section-complex-correctness-flow-2.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }

}
