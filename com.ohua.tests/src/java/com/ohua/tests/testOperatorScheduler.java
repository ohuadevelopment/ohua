/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests;

import java.util.logging.Level;

import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.OperatorPriorityComparator;
import com.ohua.engine.scheduler.OperatorScheduler;
import com.ohua.engine.operators.PeekOperator;
import com.ohua.engine.utils.OhuaLoggerFactory;

public class testOperatorScheduler extends AbstractIOTestCase
{
  
  /**
   * Rule 1: Operators that are not blocked get higher scheduling priority.
   * <p>
   * We enforce this behavior by having two independent sources where each is followed by a
   * Peek. Both branches get merged by a Deterministic Merge. We start scheduling with the
   * opposite branch the merge starts with and hence buffering happens in this branch's arcs. As
   * a result this branch's logger should not be scheduled anymore.
   */
  @Test//(timeout = 8000)
  public void testSchedulingRuleOne() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory()
                        + "OperatorScheduler-Rule1-correctness-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();
    performSystemPhaseAssertions(manager);

    manager.runProcessStartUp();
    
    manager.runDataPhase();
    manager.awaitSystemPhaseCompletion();
    performDataPhaseAssertions(manager);
    
    manager.tearDownProcess();
    manager.awaitSystemPhaseCompletion();
    performTeardownAssertions(manager);

    
    // runFlow(getTestMethodInputDirectory() + "OperatorScheduler-Rule1-correctness-flow.xml",
    // getTestMethodInputDirectory() + "RuntimeProcessConfiguration.xml");
    
    tableRegressionCheck("table_writer", 100);
  }
  
}
