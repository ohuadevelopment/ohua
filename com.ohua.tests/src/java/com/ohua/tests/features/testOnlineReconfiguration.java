/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.features;

import java.io.FileReader;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;

import org.junit.Assert;

import org.json.JSONArray;
import org.json.JSONTokener;
import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.OhuaProcessRunner;
import com.ohua.engine.OhuaTestProcessRunner;
import com.ohua.engine.ProcessRunner;
import com.ohua.engine.SimpleProcessListener;
import com.ohua.engine.UserRequest;
import com.ohua.engine.UserRequestType;
import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.online.configuration.IConfigurationMarker;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;
import com.ohua.engine.online.configuration.ReplaceStateTransfer;
import com.ohua.engine.online.configuration.SubGraphLoader;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.tests.AbstractFlowTestCase;
import com.ohua.tests.features.testOnlineReconfiguration.ReconfigPropertyOperator.ReconfigPropertyOperatorProperties;

//FIXME this needs to be re-implemented once we support updates to flow graphs as a standard feature.
@Ignore
public class testOnlineReconfiguration extends AbstractFlowTestCase
{
  @Ignore public static class ReconfigPropertyOperator extends UserOperator
  {
    @Ignore public static class ReconfigPropertyOperatorProperties implements Serializable
    {
      public String someTestProperty1 = null;
      public String someTestProperty2 = null;
    }
    
    public ReconfigPropertyOperatorProperties _properties = null;
    
    @Override public Object getState() {
      return null;
    }
    
    @Override public void setState(Object state) {
      // not needed
    }
    
    @Override public void prepare() {
      // not needed
    }
    
    @Override public void runProcessRoutine() {
      InputPortControl inPortControl = getDataLayer().getInputPortController("input");
      OutputPortControl outPortControl = getDataLayer().getOutputPortController("output");
      while(inPortControl.next()) {
        getDataLayer().transferInputToOutput("input", "output");
        outPortControl.setData("someProperty1", _properties.someTestProperty1);
        outPortControl.setData("someProperty2", _properties.someTestProperty2);
        if(outPortControl.send()) {
          break;
        }
      }
    }
    
    @Override public void cleanup() {
      // not needed
    }
  }
  
  /**
   * The flow is very simple and processes a certain amount of messages. It contains an operator
   * that always outputs one of its current configuration parameters. During processing, we
   * change this parameter and when the run finishes we check the result of the output operator
   * to contain the redefined property value.
   * @throws Throwable
   */
  @Test(timeout = 60000)// normally finishes in ~26s
      public
      void testOperatorReconfig() throws Throwable {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "operator-reconfiguration-flow.xml",
                    getTestMethodInputDirectory() + "runtime.properties");
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();

    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-op-reconfig-process").start();
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("ReconfigurationTarget");
    ReconfigPropertyOperator reconfigOp = (ReconfigPropertyOperator) core.getOperatorAlgorithm();
    Assert.assertEquals("hello", reconfigOp._properties.someTestProperty1);
    Assert.assertEquals("world", reconfigOp._properties.someTestProperty2);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(6000);
    System.out.println("Reconfiguring whole property ...");
    ReconfigPropertyOperatorProperties props = new ReconfigPropertyOperatorProperties();
    props.someTestProperty1 = "goodbye";
    props.someTestProperty2 = "world";
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT,
                                             new LinkedList<>(Collections.singletonList(PacketFactory.createConfigurationMarkerPacket("ReconfigurationTarget",
                                                                                                                                                     "properties",
                                                                                                                                                     props)))));
    Thread.sleep(6000);
    System.out.println("Reconfiguring sub property ...");
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT,
                                             new LinkedList<>(Collections.singletonList(PacketFactory.createConfigurationMarkerPacket("ReconfigurationTarget",
                                                                                                                                                     "properties.someTestProperty2",
                                                                                                                                                     "earth")))));
    System.out.println("Waiting for processing to finish ...");
    listener.awaitAndReset();
    
    Assert.assertEquals("goodbye", reconfigOp._properties.someTestProperty1);
    Assert.assertEquals("earth", reconfigOp._properties.someTestProperty2);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
  
  /**
   * A test case to show that swapping an operator online works.
   * @throws Throwable
   */
  @Test(timeout = 60000)// normally finishes in ~23s
      public
      void testOperatorExchange() throws Throwable {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "operator-exchange-flow.xml", getTestMethodInputDirectory()
                                                                                  + "runtime.properties");
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();

    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-op-exchange-process").start();
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("ReconfigurationTarget");
    ReconfigPropertyOperator reconfigOp = (ReconfigPropertyOperator) core.getOperatorAlgorithm();
    Assert.assertEquals("hello", reconfigOp._properties.someTestProperty1);
    Assert.assertEquals("world", reconfigOp._properties.someTestProperty2);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(6000);
    System.out.println("Switching operator ...");
    
    ReconfigPropertyOperator newOp =
        (ReconfigPropertyOperator) OperatorFactory.getInstance().createUserOperatorInstance("Reconfig");
    newOp._properties = new ReconfigPropertyOperatorProperties();
    newOp._properties.someTestProperty1 = "goodbye";
    newOp._properties.someTestProperty2 = "earth";
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT,
                                             new LinkedList<IMetaDataPacket>(Collections.singletonList(PacketFactory.createConfigurationMarkerPacket("ReconfigurationTarget",
                                                                                                                                                     "operator",
                                                                                                                                                     newOp,
                                                                                                                                                     Category.OPERATOR)))));
    System.out.println("Waiting for processing to finish ...");
    listener.awaitAndReset();
    
    core = manager.getProcess().getGraph().getOperator("ReconfigurationTarget");
    reconfigOp = (ReconfigPropertyOperator) core.getOperatorAlgorithm();
    Assert.assertEquals("goodbye", reconfigOp._properties.someTestProperty1);
    Assert.assertEquals("earth", reconfigOp._properties.someTestProperty2);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
  
  /**
   * Instead of handing over a reference to an already existing memory object, this test case
   * assumes that the operator to be replaced is actually located on a different node.
   * @throws Throwable
   */
  @Ignore @Test public void testOperatorExchangeRemote() throws Throwable {
    // TODO
    // This requires reflection and therewith the initialization of the operator inside the port
    // handler.
  }
  
  /**
   * A test case to show that online reconfiguring as in extending a flow graph works. (This
   * feature is essential to the scalability aspect of Ohua.)<br>
   * (This test case reuses the old operator.)
   * @throws Throwable
   */
  @Test(timeout = 60000)// normally finishes in ~23s
      public
      void testFlowReconfiguration() throws Throwable {
    // When we want to exchange a whole flow then this requires a reconnection of the output
    // port of the upstream operator! Another question is: What happens with the orphaned sub
    // flow? Should we just initiate a shutdown? Or should we connect it to the
    // UserGraphEntrance operator such that it gets shut down whenever the whole flow is being
    // torn down?
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "subflow-exchange-flow.xml", getTestMethodInputDirectory()
                                                                                 + "runtime.properties");
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("Consumer");
    ConsumerOperator consumer = (ConsumerOperator) core.getOperatorAlgorithm();
    consumer.keepLastPacket();
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();

    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-subflow-exchange-process").start();
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(6000);
    System.out.println("Switching operator with sub flow ...");
    
    String[][] portReconnect = new String[][] { new String[] { "ReconfigurationTarget.input",
                                                              "ReconfigurationTarget-1.input" },
                                               new String[] { "ReconfigurationTarget.output",
                                                             "ReconfigurationTarget-2.output" } };
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT,
                                             new LinkedList<IMetaDataPacket>(Collections.singletonList(PacketFactory.createConfigurationMarkerPacket("ReconfigurationTarget",
                                                                                                                                                     getTestMethodInputDirectory()
                                                                                                                                                         + "subflow.xml",
                                                                                                                                                     portReconnect,
                                                                                                                                                     Category.EXCHANGE_FLOW)))));
    System.out.println("Waiting for processing to finish ...");
    listener.awaitAndReset();
    
    Assert.assertEquals(20000, consumer.getSeenPackets());
    Assert.assertEquals("{\n" + "    \"someProperty1\": \"bye\",\n" + "    \"someProperty2\": \"moon\",\n"
                        + "    \"test1\": \"testValue-20000\",\n" + "    \"test2\": \"testValue-20000\",\n"
                        + "    \"test3\": \"testValue-20000\",\n" + "    \"test4\": \"testValue-20000\",\n"
                        + "    \"test5\": \"testValue-20000\"\n" + "}", consumer.getLastPacket());
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
  
  private void testComplexFlowExtension(String initialFlow) throws Throwable {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + initialFlow, getTestMethodInputDirectory()
                                                                 + "runtime.properties");
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("Consumer");
    ConsumerOperator consumer = (ConsumerOperator) core.getOperatorAlgorithm();
    consumer.keepLastPacket();
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();

    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-flow-extension-process").start();
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(6000);
    System.out.println("Requesting flow graph extension ...");
    
    FileReader reader = new FileReader(getTestMethodInputDirectory() + "reconnection-spec.json");
    JSONArray reconnectSpec = new JSONArray(new JSONTokener(reader));
    reader.close();
    
    LinkedList<IMetaDataPacket> markers = new LinkedList<IMetaDataPacket>();
    markers.add(PacketFactory.createConfigurationMarkerPacket("Dummy-1",
                                                              getTestMethodInputDirectory() + "subflow.xml:"
                                                                  + getTestMethodInputDirectory()
                                                                  + "runtime.properties",
                                                              reconnectSpec,
                                                              Category.EXTEND_FLOW));
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT, markers));
    System.out.println("Waiting for processing to finish ...");
    
    // Thread.sleep(5000);
    // GraphVisualizer.PRINT_SECTION_GRAPH = getTestMethodOutputDirectory() + "rewrite-graph";
    // // GraphVisualizer.printSectionGraph(manager.getSectionGraph());
    // GraphVisualizer.printFlowGraph(manager.getProcess().getGraph(),
    // getTestMethodOutputDirectory() + "rewrite-graph");
    
    listener.awaitAndReset();
    
    Assert.assertEquals(10000, consumer.getSeenPackets());
    // Assert.assertEquals("{\n" + "    \"someProperty1\": \"bye\",\n"
    // + "    \"someProperty2\": \"moon\",\n"
    // + "    \"test1\": \"testValue-20000\",\n"
    // + "    \"test2\": \"testValue-20000\",\n"
    // + "    \"test3\": \"testValue-20000\",\n"
    // + "    \"test4\": \"testValue-20000\",\n"
    // + "    \"test5\": \"testValue-20000\"\n" + "}",
    // consumer.getLastPacket());
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
  
  /**
   * Hooking another flow into an existing one is not particularly difficult. However, if the
   * new subflow reconnects to the existing flow, then things get more challenging.
   * @throws Throwable
   */
  @Test(timeout = 60000)// typically finishes after 25s
      public
      void testComplexFlowExtension() throws Throwable {
    testComplexFlowExtension("flow-extension-flow.xml");
  }
  
  /**
   * For matters of testing marker forwarding during rewrite, we include a triangle into the
   * flow graph to be rewritten.
   * @throws Throwable
   */
  @Test(timeout = 60000) public void testExclusiveMarkerForwarding() throws Throwable {
    testComplexFlowExtension("triangle-flow-extension-flow.xml");
  }
  
  @Test(timeout = 60000) public void testReplaceSubFlow() throws Throwable {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
    
    OperatorCore core = manager.getProcess().getGraph().getOperator("Consumer");
    ConsumerOperator consumer = (ConsumerOperator) core.getOperatorAlgorithm();
    consumer.keepLastPacket();
    
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();

    OhuaProcessRunner runner = new OhuaTestProcessRunner(manager);
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "online-flow-extension-process").start();
    
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    System.out.println("Computation started ...");
    Thread.sleep(6000);
    System.out.println("Requesting flow graph extension ...");
    
    String[][] portReconnectIn = new String[][] { new String[] { "ReconfigurationSource.input",
                                                                "ReconfigurationSource-1.input" } };
    String[][] portReconnectOut = new String[][] { new String[] { "ReconfigurationTarget.output",
                                                                 "ReconfigurationTarget-1.output" } };
    ProcessRunner r = SubGraphLoader.doLoad(getTestMethodInputDirectory() + "subflow.xml");
    // FlowGraph oldGraph = manager.getProcess().getGraph();
    FlowGraph newGraph = ((AbstractProcessManager) r.getProcessManager()).getProcess().getGraph();
    LinkedList<IMetaDataPacket> markers = new LinkedList<IMetaDataPacket>();
    IConfigurationMarker secondMarker =
        PacketFactory.createUpdateMarkerPacket("ReconfigurationTarget",
                                               Category.REPLACE_OUTPUT,
                                               null,
                                               newGraph,
                                               portReconnectOut);
    IMetaDataPacket firstMarker =
        PacketFactory.createConfigurationMarkerPacket("ReconfigurationSource",
                                                      ":" + getTestMethodInputDirectory() + "runtime.properties",
                                                      new Object[] { r.getConfig(),
                                                                    ((AbstractProcessManager) r.getProcessManager()).getProcess().getGraph(),
                                                                    portReconnectIn,
                                                                    Collections.singletonMap("ReconfigurationTarget-1",
                                                                                             "ReconfigurationTarget"),
                                                                    new ReplaceStateTransfer() },
                                                      Category.REPLACE_INPUT,
                                                      secondMarker);
    markers.add(firstMarker);
    
    runner.submitUserRequest(new UserRequest(UserRequestType.FLOW_INPUT, markers));
    System.out.println("Waiting for processing to finish ...");
    
    // Thread.sleep(5000);
    // GraphVisualizer.PRINT_SECTION_GRAPH = getTestMethodOutputDirectory() + "rewrite-graph";
    // // GraphVisualizer.printSectionGraph(manager.getSectionGraph());
    // GraphVisualizer.printFlowGraph(manager.getProcess().getGraph(),
    // getTestMethodOutputDirectory() + "rewrite-graph");
    
    listener.awaitAndReset();
    
    Assert.assertEquals(40000, consumer.getSeenPackets());
    Assert.assertEquals("{\n" + "    \"someProperty1\": \"bye\",\n" + "    \"someProperty2\": \"moon\",\n"
                        + "    \"test1\": \"testValue-40000\",\n" + "    \"test2\": \"testValue-40000\",\n"
                        + "    \"test3\": \"testValue-40000\",\n" + "    \"test4\": \"testValue-40000\",\n"
                        + "    \"test5\": \"testValue-40000\"\n" + "}", consumer.getLastPacket());
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
  }
  
  @Test (timeout = 60000) public void testSingleThreadedReplace() throws Throwable{
    // TODO
  }
  
  /**
   * A test case to show that reconfiguration of an handler in an overlay network works.
   * @throws Throwable
   */
  @Test @Ignore public void testHandlerReconfiguration() throws Throwable {
    // TODO
  }
  
  /**
   * A test case to show that configuring sections at runtime works. This feature is essential
   * to low latency and efficient execution.
   * @throws Throwable
   */
  @Test @Ignore public void testSectionReconfiguration() throws Throwable {
    // TODO
  }
}
