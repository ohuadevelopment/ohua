/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.features;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

import com.ohua.engine.Maybe;
import com.ohua.engine.SystemPhaseType;
import com.ohua.engine.flowgraph.elements.operator.OperatorStateMachine;
import com.ohua.engine.flowgraph.elements.packets.ActivationMarker;
import org.junit.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.data.model.map.MapDataFormat;
import com.ohua.data.model.map.MapDataPacket;
import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.data.model.daapi.DataPacket;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.AbstractPort.PortState;
import com.ohua.engine.flowgraph.elements.operator.Arc;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.packets.IStreamPacket;
import com.ohua.tests.AbstractFlowTestCase;

/**
 * Functionality testing for loading and initialization of sub flows. Sub flows are defined as
 * flows with no source or target operator.
 * @author sertel
 * 
 */
@Ignore
public class testSubFlows extends AbstractFlowTestCase
{
  /**
   * A simple subflow consisting of two peek operators.
   */
  @Test
  public void testSimpleSubFlow() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "peek-subflow.xml");
    RuntimeProcessConfiguration processConfig = new RuntimeProcessConfiguration();
    Properties properties = new Properties();
    properties.setProperty("data-format", MapDataFormat.class.getName());
    processConfig.setProperties(properties);
    manager.setRuntimeProcessConfiguration(processConfig);
    // initialize the sub flow yourself
    // FIXME
//    manager.initializeFlowGraph();
    
    // perform some assertion checks
    performGeneralSystemPhaseAssertions(manager);
    checkAllOutputPorts(manager, PortState.NORMAL);
    
    // send some packets manually
    FlowGraph graph = manager.getProcess().getGraph();
    OperatorCore logger1 = graph.getOperator("Logger-1");
    OperatorCore logger2 = graph.getOperator("Logger-2");
    Arc inArc = new Arc();
    inArc.setTargetPort(logger1.getInputPort("input"));
    Arc outArc = new Arc();
    outArc.setSourcePort(logger2.getOutputPort("output"));
    // this is just to trick the activation mechanism
    outArc.setTargetPort(new InputPort(logger1));
    LinkedList<IStreamPacket> packets = new LinkedList<>();
    for(int i = 0; i < 5; i++)
    {
      MapDataPacket packet = new MapDataPacket();
      packet.setData(Collections.singletonMap("key", "value-" + i));
      packets.add(PacketFactory.createDataPacket(packet));
    }
    inArc.enqueueBatch(new LinkedList<>(Collections.singletonList(PacketFactory.createActivationMarkerPacket(SystemPhaseType.COMPUTATION))));
    inArc.enqueueBatch(packets);
    for(OperatorCore op : new OperatorCore[] { logger1,
                                        logger2 })
    {
      // FIXME
//      Assert.assertTrue(op.getOperatorState() == OperatorStateMachine.OperatorState.WAITING_FOR_COMPUTATION);
//      op.runOperatorStep(); // WAITING_FOR_COMPUTATION -> EXECUTING_META_DATA
//      Assert.assertTrue(op.getOperatorState() == OperatorStateMachine.OperatorState.EXECUTING_META_DATA);
//      op.runOperatorStep(); // EXECUTING_META_DATA -> EXECUTING
//      Assert.assertTrue(op.getOperatorState() == OperatorStateMachine.OperatorState.EXECUTING);
//      op.runOperatorStep(); // EXECUTING -> WAITING_FOR_DATA
//      Assert.assertTrue(op.getOperatorState() == OperatorStateMachine.OperatorState.EXECUTING_EPILOGUE);
    }
    IStreamPacket activation = (IStreamPacket) outArc.getData().get();
    Assert.assertTrue(activation instanceof ActivationMarker);
    Assert.assertEquals(packets.size(), outArc.getLoadEstimate());
    DataPacket sig = (DataPacket) outArc.getData().get();
    int j = 0;
    while(sig != null)
    {
      @SuppressWarnings("unchecked")
      Map<String, Object> arrived = (Map<String, Object>) sig.getData();
      Assert.assertEquals("value-" + j++, arrived.get("key"));
      Maybe<Object> maybe = outArc.getData();
      sig = maybe.isPresent() ? (DataPacket) maybe.get() : null;
    }
  }
}
