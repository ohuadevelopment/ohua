/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.lang;

import com.ohua.backend.JavaBackendProvider;
import com.ohua.lang.OhuaRuntime;
import com.ohua.lang.Tuple;
import com.ohua.lang.defsfn;
import com.ohua.tests.AbstractFlowTestCase;
import com.ohua.tests.lang.testIfThenElseOperator.MultiProducer;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class testExceptions extends AbstractFlowTestCase{
  
  @Test(expected=ArrayIndexOutOfBoundsException.class)
  public void testRuntimeExceptionInFunction() throws Throwable{
      registerFunc("func-prod", MultiProducer.class.getDeclaredMethod("produce", List.class));
      registerFunc("fail", Failure.class.getDeclaredMethod("fail", String.class));
      JavaBackendProvider.loadCoreOperators();

    OhuaRuntime runtime = new OhuaRuntime();
      runtime.createOperator(testNS + "/func-prod", 100);
      runtime.createOperator(testNS + "/fail", 102);

    runtime.registerDependency(100, 0, 102, 0);

    List<Object[]> input = new ArrayList<>();
    input.add(new Object[] { "some" });
    runtime.setArguments(100, new Tuple[] { new Tuple(0, input) });

      runtime.execute();
  }
  
  public void testCompileTimeException() throws Throwable{
    // TODO
  }

    public static class Failure {
        @defsfn
        public Object[] fail(String arg) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }
}
