/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.lang;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.ohua.engine.flowgraph.elements.operator.DataflowFunction;
import com.ohua.lang.*;
import org.junit.Assert;
import org.junit.Test;

import aua.analysis.qual.Linear;

import com.ohua.engine.flowgraph.elements.operator.OperatorLibrary;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.operators.NonDeterministicMergeOperator;
import com.ohua.lang.compile.Linker;
import com.ohua.lang.update.OhuaUpdates;
import com.ohua.tests.AbstractFlowTestCase;
import com.ohua.tests.lang.testCycles.IncOperator;
import com.ohua.tests.lang.testFunctionalOperator.FunctionalConsumer;
import com.ohua.tests.lang.testFunctionalOperator.ResultCapture;

public class testUpdates extends AbstractFlowTestCase
{
  interface ScheduledUpdate
  {
    public void update() throws Throwable;
  }
  
  public static class UpdateInc
  {
    @defsfn
    public Object[] updateInc(int i, String s) {
      return new Object[] { -i, s };
    }
  }

  public static class MutualDepUpdateInc
  {
    @defsfn
    public Object[] mutualUpdateInc(int i, String s) {
      return new Object[] { -i, s , false};
    }
  }

  public static class MutualDepConsumer extends FunctionalConsumer
  {
    @defsfn
    public void updateConsume(int i, String s, boolean nonesense, ResultCapture capture) {
      super.consume(i, s, capture);
    }
  }
  
  public static class ListInput
  {
    @Linear private int _pos = 0;

    @DataflowFunction
    @defsfn
    public Either.EitherObjectArrayOrFinish produce(List<Object[]> input) {
      if(_pos < input.size()) return new Either.EitherObjectArrayOrFinish().add(input.get(_pos++));
      else return new Either.EitherObjectArrayOrFinish().add(DataflowFunction.Finish.DONE);
    }
  }
  
  public static List<Object[]> createTestData(int amount) {
    List<Object[]> data = new ArrayList<>();
    for(int i = 0; i < amount; i++) {
      data.add(new Object[] { i,
                             "some-" + i });
    }
    return data;
  }
  
  private static OhuaRuntime initializeFlow(int dataAmount, String incName, ResultCapture capture, String testOutDir) throws Throwable {
    Linker.clear();
    Map<String, String> ops = new HashMap<>();
    ops.put("func-prod", ListInput.class.getName());
    ops.put("nd-merge", NonDeterministicMergeOperator.class.getName());
    ops.put(incName, IncOperator.class.getName());
    ops.put("updateConsume", FunctionalConsumer.class.getName());
    OperatorLibrary.registerOperators(ops, testOutDir + "test-registry.xml");
    Linker.loadCoreOperators();
    Linker.loadAppOperators(testOutDir + "test-registry.xml");
    
    OhuaRuntime runtime = new OhuaRuntime();
    runtime.createOperator("func-prod", 100);
    runtime.createOperator(incName, 101);
    runtime.createOperator("updateConsume", 102);
    
    runtime.registerDependency(100, -1, 101, 0);
    runtime.registerDependency(101, -1, 102, 0);
    
    runtime.setArguments(100, new Tuple[] { new Tuple(0, createTestData(dataAmount)) });
    runtime.setArguments(102, new Tuple[] { new Tuple(1, capture) });
    return runtime;
  }
  
  private static void scheduleUpdate(final long timeout, final ScheduledUpdate u) {
    // set the update on hold
    new Thread(new Runnable() {
      
      @Override public void run() {
        try {
          Thread.sleep(timeout);
          u.update();
        }
        catch(Throwable e) {
          e.printStackTrace();
          throw new RuntimeException(e);
        }
      }
    }).start();
    
  }

  public static class t1 extends AbstractFlowTestCase {

    @Test(timeout = 80000)
    public void testOperatorUpdate() throws Throwable {
      ResultCapture finalResult = new ResultCapture();
      final OhuaRuntime runtime = initializeFlow(10000000, "updateInc", finalResult, getTestMethodOutputDirectory());

      // schedule the update on hold
      scheduleUpdate(3000, new ScheduledUpdate() {
        @Override
        public void update() throws Throwable {
          // update the linker with the new operator
          Linker.reload("com.ohua.tests.lang");

          // prepare and issue an update
          Map<String, String> updates = new HashMap<String, String>();
          updates.put("updateInc", "updateInc");
          LinkedList<IMetaDataPacket> requests =
                  OhuaUpdates.update(updates, runtime.getCompileTimeView(), runtime.getRuntimeView());
          System.out.println("Injecting update ... " + requests.size());
          runtime.inject(requests);
          System.out.println("Update injected!");
        }
      });

      // start the computation
      runtime.execute();

      System.out.println(finalResult._iResult);
      // make sure the update arrived
      Assert.assertTrue(finalResult._iResult < 0);
    }
  }

  public static class t2 extends AbstractFlowTestCase {

    @Test(timeout = 80000)
    public void testMutualDependency() throws Throwable {
      ResultCapture finalResult = new ResultCapture();
      final OhuaRuntime runtime = initializeFlow(10000000, "mutualUpdateInc", finalResult, getTestMethodOutputDirectory());

      // schedule the update on hold
      scheduleUpdate(3000, new ScheduledUpdate() {
        @Override
        public void update() throws Throwable {
          // update the linker with the new operator
          Linker.reload("com.ohua.tests.lang");

          // prepare and issue an update
          Map<String, String> updates = new HashMap<String, String>();
          updates.put("mutualUpdateInc", "mutualUpdateInc");
          updates.put("updateConsume", "updateConsume");
          LinkedList<IMetaDataPacket> requests =
                  OhuaUpdates.update(updates, runtime.getCompileTimeView(), runtime.getRuntimeView());
          System.out.println("Injecting update ... " + requests.size());
          runtime.inject(requests);
          System.out.println("Update injected!");
        }
      });

      // start the computation
      runtime.execute();

      System.out.println(finalResult._iResult);
      // make sure the update arrived
      Assert.assertTrue(finalResult._iResult < 0);
    }
  }

  @Test public void testAlgoExtension() throws Throwable {
    // TODO
  }
  
  @Test public void testStateTransformer() throws Throwable {
    // TODO
  }
  
}
