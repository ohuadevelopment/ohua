/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.lang;

import clojure.lang.Compiler;
import com.ohua.backend.JavaBackendProvider;
import com.ohua.engine.flowgraph.elements.operator.OperatorLibrary;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.lang.Tuple;
import com.ohua.lang.compile.FlowGraphCompiler;
import com.ohua.lang.compile.Linker;
import com.ohua.lang.defsfn;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.exceptions.CompilationException.CAUSE;
import com.ohua.tests.AbstractRegressionTestCase;
import com.ohua.tests.lang.ClojureTestOps.Access;
import com.ohua.tests.lang.ClojureTestOps.ListCollect;
import com.ohua.tests.lang.ClojureTestOps.Pipe;
import com.ohua.tests.lang.ClojureTestOps.Update;
import com.ohua.tests.lang.testFunctionalOperator.FunctionalConsumer;
import com.ohua.tests.lang.testFunctionalOperator.ResultCapture;
import com.ohua.tests.lang.testUpdates.ListInput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class testFlowCompilation extends AbstractRegressionTestCase {
  
  @Before
  public void load() throws Throwable {
    clearCache();
    registerFunc("func-prod", ListInput.class.getDeclaredMethod("produce", List.class));
    registerFunc("peek", Peeker.class.getDeclaredMethod("peek", List.class));
    registerFunc("consume", FunctionalConsumer.class.getDeclaredMethod("consume", int.class, String.class, ResultCapture.class));
    registerFunc("consume-mutable", MutableStateConsumer.class.getDeclaredMethod("consume", MyState.class, List.class, ResultCapture.class));
    JavaBackendProvider.loadCoreOperators();
  }

  /**
   * Uses consumer with parameters of immutable type.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testNoneLeakingFlow() throws Throwable {
    FlowGraphCompiler original = new FlowGraphCompiler();
    createOp(original, "func-prod", 100);
    createOp(original, "consume", 101);
    createOp(original, "consume", 102);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.registerDependency(100, 0, 102, 0);
    original.registerDependency(100, 1, 102, 1);
    original.setArguments(100, new Tuple[] { new Tuple(0, new ArrayList<>()) });
    original.setArguments(101, new Tuple[] { new Tuple(2, new ResultCapture()) });
    original.setArguments(102, new Tuple[] { new Tuple(2, new ResultCapture()) });
    original.compile(false);
  }

  /**
   * Uses consumer with parameters of mutable type and must therefore raise an exception.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testLeakingFlow() throws Throwable {
    FlowGraphCompiler original = new FlowGraphCompiler();
    createOp(original, "func-prod", 100);
    createOp(original, "consume-mutable", 101);
    createOp(original, "consume-mutable", 102);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.registerDependency(100, 0, 102, 0);
    original.registerDependency(100, 1, 102, 1);
    original.setArguments(100, new Tuple[] { new Tuple(0, new ArrayList<>()) });
    original.setArguments(101, new Tuple[] { new Tuple(2, new ResultCapture()) });
    original.setArguments(102, new Tuple[] { new Tuple(2, new ResultCapture()) });

    try {
      original.compile(false);
      Assert.fail();
    }
    catch(CompilationException ce) {
      Assert.assertTrue(ce.getCauze() == CAUSE.SHARED_DEPENDENCY_DETECTED);
      Assert.assertTrue(ce.getOpId().equals("produce"));
    }
  }

  // TODO @sertel Please have a look and see if we can replace `merge` here
  @Ignore
  @Test(timeout = 10000)
  public void testSharedInput() throws Throwable {
    GraphVisualizer.PRINT_FLOW_GRAPH = super.getTestMethodOutputDirectory() + "graph";
    Map<String, String> ops = new HashMap<>();
    ops.put("pipe", Pipe.class.getName());
    ops.put("get", Access.class.getName());
    ops.put("replace", Update.class.getName());
    ops.put("l-collect", ListCollect.class.getName());
    OperatorLibrary.registerOperators(ops, getTestMethodOutputDirectory() + "test-registry.xml");
    Linker.loadCoreOperators();
    Linker.loadAppOperators(getTestMethodOutputDirectory() + "test-registry.xml");

    String code =
        "(doto (new com.ohua.lang.compile.FlowGraphCompiler)"
            + "(.createOperator \"pipe\" 100)"
            + "(.createOperator \"pipe\" 101)"
            + "(.createOperator \"merge\" 102)"
            + "(.createOperator \"ifThenElse\" 103)"
            + "(.createOperator \"get\" 105)"
            + "(.createOperator \"replace\" 106)"
            + "(.createOperator \"l-collect\" 107)"
            + "(.registerDependency 100 -1 105 0)"
            + "(.registerDependency 100 -1 106 0)"
            + "(.registerDependency 101 -1 103 1)"
            + "(.registerDependency 105 -1 102 0)"
            + "(.registerDependency 106 -1 102 1)"
            + "(.registerDependency 102 1 107 0)"
            + "(.registerDependency 103 0 105 -1)"
            + "(.registerDependency 103 1 106 -1)"
            + "(.setArguments 100 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) '\"java.lang.Object\"))))"
            + "(.setArguments 101 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) '\"java.lang.Object\"))))"
            + "(.setArguments 103 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))"
            + "(.setArguments 105 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 1) '\"java.lang.Object\"))))"
            + "(.setArguments 106 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 1) 'java.lang.String) (com.ohua.lang.Tuple. (clojure.core/int 2) '\"java.lang.Object\"))))"
            + "(.setArguments 107 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 1) '\"java.lang.Object\"))))" + ")";
    new clojure.lang.RT(); // needed by Clojure
    FlowGraphCompiler compiler = (FlowGraphCompiler) Compiler.load(new StringReader(code));
    compiler.compile(true);
  }

  /**
   * Compilation should succeed because the targets are on different branches of a switch
   * statement. Both functions that share the output reference are top-level, i.e., they have
   * direct connections to the switch.
   *
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testSharedOutputTopLevel() throws Throwable {
    String code =
        "(doto (new com.ohua.lang.compile.FlowGraphCompiler)"
                + "(.createOperator \"" + testNS + "/func-prod\" 100)"
                + "(.createOperator \"com.ohua.lang/ifThenElse\" 102)"
                + "(.createOperator \"" + testNS + "/consume-mutable\" 104)"
                + "(.createOperator \"" + testNS + "/consume-mutable\" 105)"
            + "(.registerDependency 100 0 102 1)"
            + "(.registerDependency 100 0 104 0)"
            + "(.registerDependency 100 1 104 1)"
            + "(.registerDependency 100 0 105 0)"
            + "(.registerDependency 100 1 105 1)"
            + "(.registerDependency 102 0 104 -1)"
            + "(.registerDependency 102 1 105 -1)"
            + "(.setArguments 100 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'input1))))"
            + "(.setArguments 104 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 2) '\"com.ohua.tests.lang.testFunctionalOperator$ResultCapture\"))))"
            + "(.setArguments 105 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 2) '\"com.ohua.tests.lang.testFunctionalOperator$ResultCapture\"))))"
            + "(.setArguments 102 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))"
            + ")";
    new clojure.lang.RT(); // needed by Clojure
    FlowGraphCompiler compiler = (FlowGraphCompiler) Compiler.load(new StringReader(code));
    compiler.compile(true);
  }

  /**
   * The output is shared among the two branches of a switch statement but is not input to
   * top-level functions.
   *
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testSharedOutputNoneTopLevel() throws Throwable {
    String code =
        "(doto (new com.ohua.lang.compile.FlowGraphCompiler)"
                + "(.createOperator \"" + testNS + "/func-prod\" 100)"
                + "(.createOperator \"" + testNS + "/func-prod\" 101)"
                + "(.createOperator \"com.ohua.lang/ifThenElse\" 103)"
                + "(.createOperator \"" + testNS + "/consume-mutable\" 105)"
                + "(.createOperator \"" + testNS + "/peek\" 106)"
                + "(.createOperator \"" + testNS + "/consume-mutable\" 107)"
                + "(.createOperator \"" + testNS + "/peek\" 108)"
            + "(.registerDependency 100 -1 105 0)"
            + "(.registerDependency 100 -1 107 0)"
            + "(.registerDependency 101 -1 103 1)"
            + "(.registerDependency 101 -1 106 0)"
            + "(.registerDependency 101 -1 108 0)"
            + "(.registerDependency 106 -1 105 1)"
            + "(.registerDependency 108 -1 107 1)"
            + "(.registerDependency 103 0 106 -1)"
            + "(.registerDependency 103 1 108 -1)"
            + "(.setArguments 100 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'input1))))"
            + "(.setArguments 101 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'input2))))"
            + "(.setArguments 107 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 2) '\"com.ohua.tests.lang.testFunctionalOperator$ResultCapture\"))))"
            + "(.setArguments 105 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 2) '\"com.ohua.tests.lang.testFunctionalOperator$ResultCapture\"))))"
            + "(.setArguments 103 (clojure.core/into-array com.ohua.lang.Tuple (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))"
            + ")";
    new clojure.lang.RT(); // needed by Clojure
    FlowGraphCompiler compiler = (FlowGraphCompiler) Compiler.load(new StringReader(code));
    compiler.compile(true);
  }

  protected static class MyState {
    public MyState _stateField = null;
    public int _field = 1;
  }

  public static class MutableStateConsumer {
    @defsfn
    public Object[] consume(MyState s, @SuppressWarnings("rawtypes") List l, ResultCapture capture) {
      // System.out.println("Args: " + num + " : " + s);
      // capture._iResult = num;
      // capture._sResult = s;
      return new Object[0];
    }
  }

  public static class Peeker {
    @defsfn
    public List peek(@SuppressWarnings("rawtypes") List l) {
      return l;
    }
  }
}
