/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.lang;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.RuntimeProcessConfiguration.Parallelism;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.lang.OhuaRuntime;
import com.ohua.lang.Tuple;
import com.ohua.lang.compile.FlowGraphCompiler;
import com.ohua.lang.defsfn;
import com.ohua.lang.exceptions.MultiCompilationException;
import com.ohua.lang.functions.FunctionFlowGraph;
import com.ohua.lang.update.FlowGraphUpdate;
import com.ohua.tests.AbstractFlowTestCase;
import com.ohua.tests.lang.testCycles.IncOperator;
import com.ohua.tests.lang.testFunctionalOperator.FunctionalConsumer;
import com.ohua.tests.lang.testFunctionalOperator.FunctionalPeek;
import com.ohua.tests.lang.testFunctionalOperator.ResultCapture;
import com.ohua.tests.lang.testUpdates.ListInput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.*;

public class testFlowGraphUpdate extends AbstractFlowTestCase
{
  @Before public void load() throws Throwable {

    clearCache();
    registerFunc("func-prod", ListInput.class.getDeclaredMethod("produce", List.class));

    // TODO add this back once we have a way to add operators directly again
    //registerFunc("nd-merge", NonDeterministicMergeOperator.class.getDeclaredMethod(""));

    registerFunc("inc", IncOperator.class.getDeclaredMethod("inc", int.class, String.class));
    registerFunc("neg", NegationOperator.class.getDeclaredMethod("inc", int.class, String.class));
    registerFunc("peek", FunctionalPeek.class.getDeclaredMethod("peek", int.class, String.class));
    registerFunc("consume", FunctionalConsumer.class.getDeclaredMethod("consume", int.class, String.class, ResultCapture.class));

    loadCoreOps();
  }
  
  @Test(timeout = 20000) public void testSimpleInsertionDetection() throws Throwable {
    // original graph
    FlowGraphCompiler original = new FlowGraphCompiler();
    createOp(original, "func-prod", 100);
    createOp(original, "consume", 101);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    original.setArguments(101, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    original.compile(false);

    FlowGraph oldGraph = original.load().getGraph();

    // new graph
    FlowGraphCompiler insertion = new FlowGraphCompiler();
    createOp(insertion, "func-prod", 100);
    createOp(insertion, "inc", 101);
    createOp(insertion, "consume", 102);
    insertion.registerDependency(100, 0, 101, 0);
    insertion.registerDependency(100, 1, 101, 1);
    insertion.registerDependency(101, 0, 102, 0);
    insertion.registerDependency(101, 1, 102, 1);
    insertion.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    insertion.setArguments(102, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    insertion.compile(false);

    FlowGraph newGraph = insertion.load().getGraph();

    // find the mapping between old and new graph
    Map<String, String> result = FlowGraphUpdate.mapOldToNew(oldGraph, newGraph);

    Assert.assertEquals(2, result.size());
  }
  
  @Test(timeout = 20000) public void testSimpleDeletionDetection() throws Throwable {
    // original graph
    FlowGraphCompiler original = new FlowGraphCompiler();
    createOp(original, "func-prod", 100);
    createOp(original, "inc", 101);
    createOp(original, "consume", 102);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.registerDependency(101, 0, 102, 0);
    original.registerDependency(101, 1, 102, 1);
    original.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    original.setArguments(102, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    original.compile(false);
    FlowGraph oldGraph = original.load().getGraph();

    // new graph
    FlowGraphCompiler insertion = new FlowGraphCompiler();
    createOp(insertion, "func-prod", 100);
    createOp(insertion, "consume", 101);
    insertion.registerDependency(100, 0, 101, 0);
    insertion.registerDependency(100, 1, 101, 1);
    insertion.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    insertion.setArguments(101, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    insertion.compile(false);
    FlowGraph newGraph = insertion.load().getGraph();

      // find the mapping between old and new graph
    Map<String, String> result = FlowGraphUpdate.mapOldToNew(oldGraph, newGraph);

      Assert.assertEquals(2, result.size());
  }
  
  @Test(timeout = 20000) public void testSimpleReplaceDetection() throws Throwable {
    // original graph
    FlowGraphCompiler original = new FlowGraphCompiler();
    createOp(original, "func-prod", 100);
    createOp(original, "inc", 101);
    createOp(original, "consume", 102);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.registerDependency(101, 0, 102, 0);
    original.registerDependency(101, 1, 102, 1);
    original.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    original.setArguments(102, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    original.compile(false);
    FlowGraph oldGraph = original.load().getGraph();

      // new graph
    FlowGraphCompiler insertion = new FlowGraphCompiler();
    createOp(insertion, "func-prod", 100);
    createOp(insertion, "neg", 101);
    createOp(insertion, "consume", 102);
    insertion.registerDependency(100, 0, 101, 0);
    insertion.registerDependency(100, 1, 101, 1);
    insertion.registerDependency(101, 0, 102, 0);
    insertion.registerDependency(101, 1, 102, 1);
    insertion.setArguments(100, new Tuple[]{ new Tuple(0, ArrayList.class) });
    insertion.setArguments(102, new Tuple[]{ new Tuple(2, ResultCapture.class) });
    insertion.compile(false);
    FlowGraph newGraph = insertion.load().getGraph();

      // find the mapping between old and new graph
    Map<String, String> result = FlowGraphUpdate.mapOldToNew(oldGraph, newGraph);

      Assert.assertEquals(2, result.size());
  }

    @Ignore // this does not work anymore. put it back in once updates are back on the road map.
    @Test(timeout = 20000)
  public void testSimpleReplace() throws Throwable {
    // original graph
    OhuaRuntime original = new OhuaRuntime();
      createOp(original, "func-prod", 100);
      createOp(original, "peek", 101);
      createOp(original, "inc", 102);
      createOp(original, "consume", 103);
    original.registerDependency(100, 0, 101, 0);
    original.registerDependency(100, 1, 101, 1);
    original.registerDependency(101, 0, 102, 0);
    original.registerDependency(101, 1, 102, 1);
    original.registerDependency(102, 0, 103, 0);
    original.registerDependency(102, 1, 103, 1);
    original.setArguments(100, new Tuple[] { new Tuple(0, testUpdates.createTestData(800000)) });
    ResultCapture capture = new ResultCapture();
    original.setArguments(103, new Tuple[] { new Tuple(2, capture) });
    RuntimeProcessConfiguration oldConfig = new RuntimeProcessConfiguration();
    Properties props = new Properties();
    props.setProperty("execution-mode", Parallelism.MULTI_THREADED.name());
    oldConfig.setProperties(props);
    original.executeNoWait(oldConfig);

        Thread.sleep(4000);

        FlowGraph oldGraph = original.load().getGraph();

        System.out.println("Preparing update ...");
    // new graph
    FlowGraphCompiler insertion = new FlowGraphCompiler();
      createOp(insertion, "peek", 100);
      createOp(insertion, "neg", 101);
    insertion.registerDependency(201, 0, 202, 0);
    insertion.registerDependency(201, 1, 202, 1);
    try {
      insertion.compile(false);
    }catch(MultiCompilationException mce){
      // this is a non-valid program, so all we can do is prepare it nevertheless and throw away the compiler exceptions.
      // later this will work on the abstraction of an algorithm.
    }
    FlowGraph newGraph = insertion.load().getGraph();

        // RuntimeProcessConfiguration config =
    // ProcessConfigurationLoader.load(new File(getTestMethodInputDirectory() +
    // "runtime.properties"));
    RuntimeProcessConfiguration config = new RuntimeProcessConfiguration();
    FunctionFlowGraph oldGraphMeta = new FunctionFlowGraph(new String[] { "peek-101.in-0" }, "inc-102");
    oldGraphMeta.setOutputActuals(new String[] { "inc-102.out-0" });
    FunctionFlowGraph newGraphMeta = new FunctionFlowGraph(new String[] { "peek-201.in-0" }, "neg-202");
    /*
     * currently we create these ports manually but later these ports will be established
     * automatically via registerDependency(200 0 201 0) where 200 is the function itself!
     */
    OperatorCore peek = newGraph.getOperator("peek-201");
    InputPort inPort = new InputPort(peek);
    inPort.setPortName("in-0");
    peek.addInputPort(inPort);

        LinkedList<IMetaDataPacket> markers =
        FlowGraphUpdate.update(oldGraphMeta, oldGraph, newGraphMeta, newGraph, config);
    original.inject(markers);
    System.out.println("Update submitted!");

        original.awaitCompletion();

        Assert.assertTrue(capture._iResult < 0);
  }

    public static class NegationOperator {
        @defsfn
        public Object[] inc(int i, String s) {
            return new Object[]{-i, s};
        }
  }

}
