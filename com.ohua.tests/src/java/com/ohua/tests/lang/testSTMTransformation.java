/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.lang;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import transactified.runtime.Initializable;

import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.FunctionalOperatorFactory;
import com.ohua.engine.flowgraph.elements.operator.OperatorLibrary;
import com.ohua.lang.defsfn;
import com.ohua.lang.compile.Linker;
import com.ohua.lang.runtime.ProtectedFunctionExecution;
import com.ohua.tests.AbstractRegressionTestCase;

public class testSTMTransformation extends AbstractRegressionTestCase {
  
  public static class MemLoadTestOperator {
    private Map<String, String> c = new HashMap<>();
    
    @defsfn
    public Object[] doSomething() {
      c.put("key-1", "value-1");
      return new Object[] { c.get("key-1") };
    }
  }
    
  @Test(timeout = 20000)
  public void testMemClassLoading() throws Throwable {
    Map<String, String> ops = new HashMap<>();
    ops.put("do-something", MemLoadTestOperator.class.getName());
    OperatorLibrary.registerOperators(ops, getTestMethodOutputDirectory() + "test-registry.xml");
    Linker.loadCoreOperators();
    Linker.loadAppOperators(getTestMethodOutputDirectory() + "test-registry.xml");
    
    AbstractFunctionalOperator collOp =
        (AbstractFunctionalOperator) FunctionalOperatorFactory.getInstance().createUserOperatorInstance("do-something");
    ProtectedFunctionExecution.loadAndCharge(Collections.singletonList(collOp));
    Assert.assertTrue(Initializable.class.isAssignableFrom(collOp.getFunctionType()));
  }  
}
