/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.parser;

import org.junit.Assert;

import org.junit.Test;

import com.ohua.engine.DataFlowProcess;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser;
import com.ohua.tests.AbstractFlowTestCase;

public class testParallelParserSupport extends AbstractFlowTestCase
{
  @Test
  public void testPipeline() throws Throwable
  {
    OhuaParallelFlowParser parser = new OhuaParallelFlowParser(getTestMethodInputDirectory() + "pipeline-flow.xml");
    DataFlowProcess process = parser.load();
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "process";
    GraphVisualizer.printFlowGraph(process.getGraph());
    Assert.assertEquals(8, process.getGraph().getContainedGraphNodes().size());
  }
  
  /**
   * Simple parallelization of one operator.
   * @throws Throwable
   */
  @Test
  public void testParallel() throws Throwable
  {
    OhuaParallelFlowParser parser = new OhuaParallelFlowParser(getTestMethodInputDirectory() + "parallel-flow.xml");
    DataFlowProcess process = parser.load();
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "process";
    GraphVisualizer.printFlowGraph(process.getGraph());
    Assert.assertEquals(9, process.getGraph().getContainedGraphNodes().size());
  }

  /**
   * An input operator is marked to be parallelized.
   * @throws Throwable
   */
  @Test
  public void testParallelInput() throws Throwable
  {
    OhuaParallelFlowParser parser = new OhuaParallelFlowParser(getTestMethodInputDirectory() + "parallel-input-flow.xml");
    DataFlowProcess process = parser.load();
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "process";
    GraphVisualizer.printFlowGraph(process.getGraph());
    Assert.assertEquals(12, process.getGraph().getContainedGraphNodes().size());
  }

  /**
   * An output operator is marked to be parallelized.
   * @throws Throwable
   */
  @Test
  public void testParallelOutput() throws Throwable
  {
    OhuaParallelFlowParser parser = new OhuaParallelFlowParser(getTestMethodInputDirectory() + "parallel-output-flow.xml");
    DataFlowProcess process = parser.load();
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "process";
    GraphVisualizer.printFlowGraph(process.getGraph());
    Assert.assertEquals(12, process.getGraph().getContainedGraphNodes().size());
  }
  
  @Test
  public void testNestedParallelism() throws Throwable
  {
    OhuaParallelFlowParser parser = new OhuaParallelFlowParser(getTestMethodInputDirectory() + "nested-parallel-flow.xml");
    DataFlowProcess process = parser.load();
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "process";
    GraphVisualizer.printFlowGraph(process.getGraph());
    Assert.assertEquals(16, process.getGraph().getContainedGraphNodes().size());
  }
}
