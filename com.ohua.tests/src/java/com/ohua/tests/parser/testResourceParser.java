/*
 * Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.parser;

import java.nio.file.Paths;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.AbstractResource;
import com.ohua.engine.resource.management.ResourceAccess;
import com.ohua.engine.resource.management.ResourceManager;
import com.ohua.tests.AbstractRegressionTestCase;
import com.ohua.tests.testClasspathFileLoading.TestURLClassLoader;

public class testResourceParser extends AbstractRegressionTestCase {
  
  public static class TestResource extends AbstractResource{

    public TestResource(ManagerProxy ohuaProcess, Map<String, String> attributes) {
      super(ohuaProcess, attributes);
      // TODO Auto-generated constructor stub
    }

    @Override
    public ResourceAccess getResourceAccess(OperatorID operatorID, String opChckPtArtifactID) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    public void validate() {
      // TODO Auto-generated method stub
      
    }

    @Override
    protected AbstractConnection getConnection(Object... args) {
      // TODO Auto-generated method stub
      return null;
    }

    @Override
    protected AbstractExternalActivator getExternalActivator(OperatorID operatorID) {
      // TODO Auto-generated method stub
      return null;
    }
    
  }
  
  @Test public void testClasspathExtension() throws Throwable {
    // put the test META-INF folder onto the class path
    TestURLClassLoader.addToClasspath(Paths.get(getTestMethodInputDirectory() + "META-INF").toAbsolutePath().normalize().toUri().toURL());
    
    // load the resources
    ResourceManager manager = ResourceManager.getInstance();
    manager.loadResources();
    
    // make sure the new resource is in there
    Assert.assertTrue(manager.exists("testResource"));

  }
}
