/*
 * Copyright (c) Sebastian Ertel 2008-2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.parser;

import java.io.File;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.tests.AbstractRegressionTestCase;

@Ignore
public class testSchemaValidatiy extends AbstractRegressionTestCase
{
  
  private void loadSchema(String schemaFileName) throws Throwable
  {
    SAXParserFactory parserFactory = SAXParserFactory.newInstance();
    parserFactory.setNamespaceAware(true);
    
    // validation
    SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
    File schemaFile = new File(schemaFileName);
    assert schemaFile.exists();
    
    Schema newSchema = schemaFactory.newSchema(new Source[] { new StreamSource(schemaFile) });
    parserFactory.setSchema(newSchema);
  }

  @Test
  public void operatorDeserializerSchema() throws Throwable
  {
    loadSchema("com.ohua.engine/src/java/META-INF/schemas/OperatorDescriptor.xsd");
  }
  
  @Test
  public void operatorMappingSchema() throws Throwable
  {
    loadSchema("com.ohua.engine/src/java/META-INF/schemas/OhuaOperatorRegistry.xsd");
  }
  
  @Test
  public void flowSchema() throws Throwable
  {
    loadSchema("com.ohua.engine/src/java/META-INF/schemas/OhuaFlow.xsd");
  }
}
