/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.sections.OneOpOneSectionGraphBuilder;
import com.ohua.engine.sections.Section;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.tests.AbstractFlowTestCase;

public class testSectionGraphBuilder extends AbstractFlowTestCase
{
      
  @Test
  public void testOneOpOneSection() throws Throwable
  { 
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "transactions-coloring-flow.xml");
    OneOpOneSectionGraphBuilder builder = new OneOpOneSectionGraphBuilder(null);
    SectionGraph graph = builder.build(manager.getProcess().getGraph());
    List<Section> sections = graph.getContainedGraphNodes();
    Assert.assertEquals(sections.size(), manager.getProcess().getGraph().getContainedGraphNodes().size());
  }
  
}
