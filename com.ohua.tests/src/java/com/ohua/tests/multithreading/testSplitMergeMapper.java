/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.multithreading;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.system.sections.mappers.SplitMergeMapper;
import com.ohua.tests.AbstractFlowTestCase;

@Ignore// probably tests functionality that is not used anymore. delete?
public class testSplitMergeMapper extends AbstractFlowTestCase
{
  private void basicTest() throws Throwable 
  {
    AbstractProcessManager manager = loadProcess(getTestMethodInputDirectory() + "flow.xml");
    SplitMergeMapper builder = new SplitMergeMapper(null);
    SectionGraph graph = builder.build(manager.getProcess().getGraph());
    GraphVisualizer.PRINT_SECTION_GRAPH = getTestMethodOutputDirectory() + "section-graph";
    GraphVisualizer.printSectionGraph(graph);
    assertBaselines();
  }
  
  @Test
  public void testBasicMerge() throws Throwable
  {
    basicTest();
  }

  @Test
  public void testBasicSplit() throws Throwable
  {
    basicTest();
  }
  
  @Test
  public void testBasicSplitMerge() throws Throwable
  {
    basicTest();
  }
}
