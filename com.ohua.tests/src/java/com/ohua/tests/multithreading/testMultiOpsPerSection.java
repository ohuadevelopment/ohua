/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.ProcessRunner;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser;
import com.ohua.tests.AbstractFlowTestCase;

public class testMultiOpsPerSection extends AbstractFlowTestCase
{
  @Override
  protected ProcessRunner createProcessRunner(String pathToFlow)
  {
    return new ProcessRunner(new OhuaParallelFlowParser(pathToFlow));
  }

  @Test(timeout=10000)
  public void test1() throws Throwable
  {
    FlowGraph graph = runFlowGetGraph(getTestMethodInputDirectory() + "pipeline.xml", getTestClassInputDirectory()
                                                            + "runtime-parameters.properties");
    int seen = ((ConsumerOperator) graph.getOperator("Consumer").getOperatorAlgorithm()).getSeenPackets();
    Assert.assertEquals(1000, seen);
  }
  
  /**
   * Same flow but more data detects lost notification in scheduling algorithm.
   * @throws Throwable
   */
  // this is (at least) four-times as long as this test case should normally take.
  @Ignore // FIXME with the SectionScheduler, this test case takes super long for some reason.
  // (probably bad scheduling)
  @Test(timeout = 200000)
  public void test2() throws Throwable
  {    
    FlowGraph graph = runFlowGetGraph(getTestMethodInputDirectory() + "pipeline.xml", getTestClassInputDirectory()
                                                            + "runtime-parameters.properties");
    int seen = ((ConsumerOperator) graph.getOperator("Consumer").getOperatorAlgorithm()).getSeenPackets();
    Assert.assertEquals(1000, seen);
  }

}
