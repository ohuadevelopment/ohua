/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import org.junit.Ignore;
import org.junit.Test;

/**
 * We test the functionality here that makes sure whenever an I/O section is scheduled, the
 * thread pool is being enhanced such that the other section can make further progress.
 * 
 * @author sertel
 * 
 */
public class testSequentialNonblockingIOSupport extends AbstractNonBlockIOTestCase
{
  
  /**
   * This test case has a predefined time. If it did not reach it then that means non-blocking
   * I/O is not working properly.
   * 
   * @throws Throwable
   */
  @Ignore
  // FIXME to be fixed when the support for unbounded I/O is back in place!
  @Test(timeout = 16000) // Normally, processing took around 14000 ms.
  public void testBasicNonblocking() throws Throwable 
  {
    super.performTest("Generator");
  }

}
