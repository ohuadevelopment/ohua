/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.OhuaProcessRunner;
import com.ohua.engine.SimpleProcessListener;
import com.ohua.engine.UserRequest;
import com.ohua.engine.UserRequestType;
import com.ohua.tests.AbstractFlowTestCase;

/**
 * We have to interrupt unbounded IO streams in order to properly shut down a flow. In this test
 * we will test all of the unbounded (blocking) IO sources that we support.
 * @author sertel
 * 
 */
public class testUnboundedIO extends AbstractFlowTestCase
{
  /**
   * Just run a flow with a connected ZeroMQ endpoint and try to shut it down.
   */
  @Ignore
  // FIXME to be fixed when the support for unbounded I/O is back in place!
  @Test
  public void testZeroMQ() throws Throwable
  {    
    // run the flow
    OhuaProcessRunner runner =
        new OhuaProcessRunner(getTestMethodInputDirectory() + "unbounded-input-zeromq.xml");
    runner.loadRuntimeConfiguration(getTestMethodInputDirectory() + "runtime.properties");
    SimpleProcessListener listener = new SimpleProcessListener();
    runner.register(listener);
    new Thread(runner, "zeromq-receive-process").start();
    runner.submitUserRequest(new UserRequest(UserRequestType.INITIALIZE));
    listener.awaitAndReset();
   
    runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
    
    // wait some time
    Thread.sleep(2000);
    
    System.out.println("Shutting down the flow ...");
    // now we try to take down the flow
    runner.submitUserRequest(new UserRequest(UserRequestType.FINISH_COMPUTATION));
    listener.awaitAndReset();
    
    runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    listener.awaitAndReset();
    
  }
}
