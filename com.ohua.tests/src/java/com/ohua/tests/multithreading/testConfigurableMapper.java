/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.multithreading;

import org.junit.Assert;

import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.ManagerAccess;
import com.ohua.engine.ProcessRunner;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.sections.SectionGraph;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser;
import com.ohua.system.sections.mappers.ConfigurableSectionMapper;
import com.ohua.tests.AbstractFlowTestCase;

public class testConfigurableMapper extends AbstractFlowTestCase
{
  private SectionGraph basicTest() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
    ConfigurableSectionMapper builder =
        new ConfigurableSectionMapper(ManagerAccess.getRuntimeConfig(manager));
    SectionGraph graph = builder.build(manager.getProcess().getGraph());
    GraphVisualizer.PRINT_SECTION_GRAPH = getTestMethodOutputDirectory() + "section-graph";
    GraphVisualizer.printSectionGraph(graph);
    return graph;
  }
  
  protected ProcessRunner createProcessRunner(String pathToFlow)
  {
    return new ProcessRunner(new OhuaParallelFlowParser(pathToFlow));
  }
  
  @Test
  public void testBasic() throws Throwable
  {
    SectionGraph graph = basicTest();
    
    Assert.assertEquals(1, graph.getComputationalSections().size());
    Assert.assertEquals(1, graph.getInputSections().size());
    Assert.assertEquals(1, graph.getOutputSections().size());
    Assert.assertEquals(3, graph.getAllSections().size());
  }
  
  @Test
  public void testRegExpressions() throws Throwable
  {
    SectionGraph graph = basicTest();
    
    Assert.assertEquals(1, graph.getComputationalSections().size());
    Assert.assertEquals(1, graph.getInputSections().size());
    Assert.assertEquals(1, graph.getOutputSections().size());
    Assert.assertEquals(3, graph.getAllSections().size());
  }
  
  @Test
  public void testExecution() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
  }
  
  /**
   * Tests execution in case of parallelism.
   * @throws Throwable
   */
  @Test
  public void testParallelExecution() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
  }
  
  /**
   * We use the same test case as the above but specify parallelism this time via the section
   * configuration. If the algorithm to enable parallelism in the section configuration does not
   * work then the algorithm for section configuration will fail.
   * 
   * @throws Throwable
   */
  @Test
  public void testParallelsInSectionConfig() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
  }
  
  @Test
  public void testNestedParallelsInSectionConfig() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
  }
  
  /**
   * Instead of a split now a switch can be used to break the stream.
   * @throws Throwable
   */
  @Test
  public void testSwitchParallelismInSectionConfig() throws Throwable
  {
    GraphVisualizer.PRINT_SECTION_GRAPH =
        getTestMethodOutputDirectory() + "section-graph";
    FlowGraph graph =
        runFlowGetGraph(getTestMethodInputDirectory() + "flow.xml",
                        getTestMethodInputDirectory() + "runtime.properties");
    Assert.assertEquals(2000,
                        ((ConsumerOperator) graph.getOperator("Consumer-0").getOperatorAlgorithm()).getSeenPackets());
    Assert.assertEquals(4000,
                        ((ConsumerOperator) graph.getOperator("Consumer").getOperatorAlgorithm()).getSeenPackets());
  }
  
}
