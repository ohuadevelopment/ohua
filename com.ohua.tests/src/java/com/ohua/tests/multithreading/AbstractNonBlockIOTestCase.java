/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.operators.GeneratorOperator;
import com.ohua.engine.utils.parser.OperatorDescription;
import com.ohua.engine.utils.parser.OperatorDescriptorDeserializer;
import com.ohua.tests.AbstractFlowTestCase;

/**
 * I do not exactly know why but when JUnit runs tests in the same class then the performance
 * test cases seem to fail. Therefore, I had to put each test method into its own class.
 * 
 * @author sertel
 * 
 */
public abstract class AbstractNonBlockIOTestCase extends AbstractFlowTestCase
{
  /*
   * An operator for testing purposes. Whenever this operator is being scheduled, it will wait
   * for a bit until it produces more output.
   */
  public static class BlockingInputProducer extends GeneratorOperator
  {
    public static long BLOCKING_TIME = 1000;
    
    @Override
    public void runProcessRoutine()
    {
      try
      {
        System.out.println("BLOCKING >>>> " + BLOCKING_TIME);
        Thread.sleep(BLOCKING_TIME);
      }
      catch(InterruptedException i)
      {
        assert false;
      }
      super.runProcessRoutine();
    }
  }
  
  class TestOperatorDescriptorDeserializer extends OperatorDescriptorDeserializer
  {
    public OperatorDescription deserialize(String operatorName) throws OperatorLoadingException
    {
      if(operatorName.endsWith("BlockingInputProducer"))
      {
        operatorName = "GeneratorOperator";
      }
      return super.deserialize(operatorName);
    }
  }
  
  protected void patch(String replacementName)
  {
    // overload the generator to use the testing class
    OperatorFactory.getInstance().setOperatorImplementationClass(replacementName,
                                                                 BlockingInputProducer.class);
    OperatorFactory.getInstance().setOperatorDescriptorDeserializer(new TestOperatorDescriptorDeserializer());    
  }
  
  protected void performTest(String replacementName) throws Throwable 
  {
    patch(replacementName);
    runFlowNoAssert(getTestMethodInputDirectory() + "flow.xml", getTestMethodInputDirectory()
                                                                + "runtime.properties");
  }

  protected void performTest(String replacementName, long blockingTime) throws Throwable 
  {
    BlockingInputProducer.BLOCKING_TIME = blockingTime;
    performTest(replacementName);
  }
  
}
