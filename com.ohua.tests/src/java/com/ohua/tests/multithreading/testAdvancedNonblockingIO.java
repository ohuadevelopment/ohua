/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.multithreading;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Testing this feature needs the following two things:
 * <ul>
 * <li>a long io operation
 * <li>an overlay network with a special marker.
 * </ul>
 * We borrow the "fake" io operation from the other I/O test cases and use one of our marker
 * algorithms to test this functionality.
 * @author sertel
 *         
 */
public class testAdvancedNonblockingIO extends AbstractNonBlockIOTestCase {
  /**
   * The basic test case for this feature. With a long blocking I/O operation.
   */
  @Ignore // broke when removing the notion of sentData=true for data packets only from the
          // OperatorStateMachine. fix this once non-blocking IO is back on the road map.
  @Test(timeout = 10000) // normal processing time: ~6000
  public void testLongIO() throws Throwable {
    performTest("Fake_IO_Generator", 3000);
  }
  
  /**
   * Here we should actually see that this feature only kicks in when there was a long period of
   * inactivity in the operator.
   */
  @Test
  public void testNotSoLongIO() {
    // TODO
  }
  
  /**
   * A test case that shows, we can interrupt a long blocking IO operation and shutdown the
   * flow.
   */
  @Test
  public void testShutdownLongBlocking() {
    // TODO
  }
}
