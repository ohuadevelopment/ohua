/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import com.ohua.tests.AbstractFlowTestCase;

import org.junit.Test;

public class testMovingAverageOperator extends AbstractFlowTestCase
{
  @Test
  public void testMovingAverage() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "MovingAverage-correctness-flow.xml");
  }
}
