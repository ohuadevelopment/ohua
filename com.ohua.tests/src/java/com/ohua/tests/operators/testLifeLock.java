/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.junit.Test;

import com.ohua.engine.operators.PeekOperator;
import com.ohua.engine.utils.OhuaLoggerFactory;
import com.ohua.operators.io.DatabaseBatchWriterOperator;
import com.ohua.tests.AbstractIOTestCase;

public class testLifeLock extends AbstractIOTestCase
{
  /**
   * The flow consists of a DeterministicMerge and resulting life-lock situation is the
   * following:<br>
   * The Merge wants data from its right incoming branch but there is no data available. Hence
   * it retreats and schedules ALL(!) upstream operators to push more data. The preceding
   * operator on the right incoming arc gets scheduled but has also no data so it schedules its
   * previous op X in return. The preceding operator on its left incoming arc gets scheduled but
   * his outgoing arc to the Merge is blocked (=full). As a result it activates the Merge again
   * to take data from this arc. Hence operator X never gets scheduled because the scheduling
   * priority of the Merge and both of its preceding operators is always higher than the
   * scheduling priority of operator X due to output-favored scheduling.
   * <p>
   * The solution is of course to not activate all upstream operators but only those who really
   * are needed to push data in order to make forward progress.
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testLifelockOperatorLevel() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "Operator-lifelock-correctness-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }
  
  /**
   * The same can happen for section. In order to test for that we say that the thread count can
   * only be 1. Hence only one section is running at a time. The flow is the same as used to
   * test the operator case with each operator running in its own section.
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testLifelockSectionLevel() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "Section-lifelock-correctness-flow.xml",
                    getTestMethodInputDirectory() + "runtime-parameters.properties");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }

}
