/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Test;

import com.ohua.tests.AbstractFlowTestCase;


public class testThroughputOperator extends AbstractFlowTestCase
{
  @Test
  public void testBasic() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "basic-throughput-correctness-process.xml");
  }
}
