/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.junit.Test;

import com.ohua.engine.operators.SplitOperator;
import com.ohua.operators.io.DatabaseBatchWriterOperator;
import com.ohua.tests.AbstractIOTestCase;

public class testSplitOperator extends AbstractIOTestCase
{
  
  /**
   * Scheduling interval = 1.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testSplit() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "Split-correctness-flow.xml");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 50L);
    outputTables.put("table_writer_2", 50L);
    tableRegressionCheck(outputTables);
  }
  
  /**
   * Scheduling interval = 12.<br>
   * In the end the right database op should have 4 records less than the left one.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testSplitInterval() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "Split-interval-correctness-flow.xml");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 52L);
    outputTables.put("table_writer_2", 48L);
    tableRegressionCheck(outputTables);
  }

  /**
   * Scheduling interval = 10.<br>
   * In the end both endpoints should have received the same amount of packets.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testSplitInterval2() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "Split-interval-correctness-flow-2.xml");
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", 100L);
    outputTables.put("table_writer_2", 100L);
    tableRegressionCheck(outputTables);
  }

}
