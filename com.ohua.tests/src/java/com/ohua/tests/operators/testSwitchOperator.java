/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.engine.operators.PeekOperator;
import com.ohua.tests.AbstractIOTestCase;

public class testSwitchOperator extends AbstractIOTestCase
{
  /**
   * A basic flow with a Switch and 3 transactional targets.
   * @throws Throwable
   */
  @Test
  public void testBasic() throws Throwable
  {
    outputOperatorLogToFile(PeekOperator.class, "Logger-1", "3", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Logger-2", "4", Level.INFO);
    outputOperatorLogToFile(PeekOperator.class, "Logger-3", "5", Level.INFO);
    
    runFlowNoAssert(getTestMethodInputDirectory() + "basic-switch-correctness-process.xml");
    
    Map<String, Long> tables = new HashMap<String, Long>();
    tables.put("match_table_under_1500", new Long(1499));
    tables.put("match_table_1500", new Long(1));
    tables.put("match_table_over_1500", new Long(1500));
    tableRegressionCheck(tables);
  }
  
  @Test
  public void testPatternMatch() throws Throwable
  {
    FlowGraph graph = runFlowGetGraph(getTestMethodInputDirectory() + "advanced-switch-process.xml",
                    getTestMethodInputDirectory() + "runtime.properties");
    Assert.assertEquals(2000, ((ConsumerOperator)graph.getOperator("Consumer-Left").getOperatorAlgorithm()).getSeenPackets());
    Assert.assertEquals(4000, ((ConsumerOperator)graph.getOperator("Consumer-Right").getOperatorAlgorithm()).getSeenPackets());
  }
  
  @Ignore
  @Test
  public void testPattern()
  {
    Pattern pattern = Pattern.compile("^test");
    Matcher matcher = pattern.matcher("test-right-1");
    Assert.assertTrue(matcher.find());
  }
}
