/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Test;

import com.ohua.tests.AbstractIOTestCase;

public class testWeightedMovingAverageOperator extends AbstractIOTestCase
{
  @Test
  public void testBasic() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
                    + "WeightedMovingAverage-correctness-flow.xml");
    
    tableRegressionCheck("mv_avg_table", 1600);
    columnBoundaryCheck("mv_avg_table", "TA_mv_avg", 1, 1596);
  }

}
