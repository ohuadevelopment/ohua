/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.tests.operators;

import java.util.List;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.operators.ConsumerOperator;
import com.ohua.tests.AbstractFlowTestCase;

public class testFileOperators extends AbstractFlowTestCase
{
  
  @Test
  public void testFileReader() throws Throwable
  {
    AbstractProcessManager manager = loadProcess(getTestMethodInputDirectory() + "FileReader-correctness-flow.xml");
    runFlowNoAssert(manager);
    List<OperatorCore> ops = manager.getProcess().getGraph().getOperators("Consumer*");
    Assert.assertTrue(ops.size() == 1);
    Assert.assertEquals(100, ((ConsumerOperator) ops.get(0).getOperatorAlgorithm()).getSeenPackets());
  }

  @Test
  public void testFileWriter() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "FileWriter-correctness-flow.xml");
  }
  
  @Ignore
  @Test
  public void testWriteWithIndex() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "flow.xml");
  }
}
