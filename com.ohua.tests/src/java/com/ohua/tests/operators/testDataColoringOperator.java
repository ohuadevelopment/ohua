/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Test;

import com.ohua.tests.AbstractFlowTestCase;

public class testDataColoringOperator extends AbstractFlowTestCase
{
  /**
   * Simple test making sure the operator is working properly.
   */
  @Test
  public void test1() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "data-coloring-test-flow.xml");
  }
}
