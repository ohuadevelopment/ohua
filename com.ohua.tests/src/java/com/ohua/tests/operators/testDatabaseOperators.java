/*
 * Copyright (c) Sebastian Ertel 2008-2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.tests.AbstractIOTestCase;

public class testDatabaseOperators extends AbstractIOTestCase
{
  
  @Test
  public void testBatchTAWriter() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "BatchTAWriter-correctness-flow.xml");
    
    tableRegressionCheck("test_writer_table", 100);
  }
  
  @Test
  public void testUnevenBatchSize() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "BatchTAWriter-correctness-batch-size-flow.xml");
    
    tableRegressionCheck("test_writer_table", 100);
  }

  @Test
  public void testNonBatchTAWriter() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "NonBatchTAWriter-correctness-flow.xml");
    
    tableRegressionCheck("test_writer_table", 100);
  }
  
  /**
   * This test case also uses a writer as an endpoint because that way we can make sure no
   * packet was duplicated.
   * @throws Throwable
   */
  @Test
  public void testReader() throws Throwable
  {
    fillTable("test_reader_table", "NAME", "ADDRESS", "PHONE");
    runFlowNoAssert(getTestMethodInputDirectory() + "TAReader-correctness-flow.xml");
    
    tableRegressionCheck("test_writer_table", 100);
  }
  
  @Ignore
  @Test
  public void testSortedRead() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "load-data-flow.xml",
                    getTestMethodInputDirectory() + "runtime.properties");
    runFlowNoAssert(getTestMethodInputDirectory() + "sorted-read-flow.xml",
                    getTestMethodInputDirectory() + "runtime.properties");
  }
    
}
