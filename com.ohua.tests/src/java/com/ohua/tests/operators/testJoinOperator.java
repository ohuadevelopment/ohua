/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.tests.AbstractFlowTestCase;

public class testJoinOperator extends AbstractFlowTestCase
{
  /**
   * Just a simple test case using 2 file readers and a file writer. No orphans.
   */
  @Test
  public void testBasic() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "basic-join-correctness-process.xml");
  }
  
  /**
   * The same flow as above with the same input but left and right switched.
   */
  @Test
  public void testBasicSwitched() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "basic-join-correctness-switched-process.xml");
  }

  /**
   * Test where the left input starts with no matches.
   * @throws Throwable
   */
  @Test
  public void testStartLeftNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-left-start-no-match-process.xml");
  }
  
  /**
   * Same as above but now the right starts with no-match items.
   * @throws Throwable
   */
  @Test
  public void testStartRightNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-right-start-no-match-process.xml");
  }
  
  /**
   * Test where the left input starts with no matches.
   * @throws Throwable
   */
  @Test
  public void testEndLeftNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-left-end-no-match-process.xml");
  }
  
  /**
   * Same as above but now the right starts with no-match items.
   * @throws Throwable
   */
  @Test
  public void testEndRightNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-right-end-no-match-process.xml");
  }
  
  /**
   * This test case will contain matches and no-matches everywhere and serves as the final
   * functionality test scenario for the Join operator.
   * @throws Throwable
   */
  @Test
  public void testFullMatchNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-all-process.xml");
  }
  
  /**
   * This test case will run the join on a bigger amount of data and will make sure that the
   * back-off mechanism for sending the data downstream is working for the case where we have to
   * back-off on producing no-matches.
   * @throws Throwable
   */
  @Test
  public void testBackoffNoMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-backoff-no-match-process.xml",
            getTestMethodInputDirectory() + "runtime-parameters.properties");
  }
  
  /**
   * This test case will run the join on a bigger amount of data and will make sure that the
   * back-off mechanism for sending the data downstream is working for the case where we have to
   * back-off on producing big matches (one left item has many right item matches).
   * @throws Throwable
   */
  @Ignore //FIXME fix if the data buffer/replay API ever gets interesting again
  @Test
  public void testBackoffMatch() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "join-correctness-backoff-match-process.xml",
            getTestMethodInputDirectory() + "runtime-parameters.properties");
  }

}
