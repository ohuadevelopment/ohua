/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Test;

import com.ohua.tests.AbstractFlowTestCase;

public class testLeftOuterJoinOperator extends AbstractFlowTestCase
{
  /**
   * Just a simple test case using 2 file readers and a file writer.
   */
  @Test
  public void testBasic() throws Throwable
  {
    runFlow(getTestMethodInputDirectory() + "basic-leftjoin-correctness-process.xml");
  }

}
