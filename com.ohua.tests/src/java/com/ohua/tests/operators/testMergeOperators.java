/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.operators;

import org.junit.Test;

import com.ohua.tests.AbstractIOTestCase;

public class testMergeOperators extends AbstractIOTestCase
{
  
  /**
   * DequeueBatchSize = 1.
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testDeterministicMerge() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory() + "DeterministicMerge-correctness-flow.xml");
    
    tableRegressionCheck("table_writer", 100);
  }
  
  /**
   * DequeueBatchSize = 12.
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testDeterministicMergeDequeueBatchSize() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
            + "DeterministicMerge-DequeueBatchSize-correctness-flow.xml");
    
    tableRegressionCheck("table_writer", 100);
  }
  
  /**
   * DequeueBatchSize = 1.<br>
   * Check the output of the logger and you can see that the output is not alternating. That is
   * because the input port the ND merge starts with has no data yet and therefore stays with
   * the second input port until this is exhausted.
   * 
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testNonDeterministicMerge() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
                    + "NonDeterministicMerge-correctness-flow.xml");
    
    tableRegressionCheck("table_writer", 100);
  }
  
  @Test(timeout = 8000)
  public void testNonDeterministicMergeDequeueBatchSize() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
            + "NonDeterministicMerge-DequeueBatchSize-correctness-flow.xml");
    
    tableRegressionCheck("table_writer", 200);
  }
  
  /**
   * DequeueBatchSize = -1 means that the operator schedules only if the data among one arc is
   * exhausted.
   * @throws Throwable
   */
  @Test(timeout = 8000)
  public void testEagerNonDeterministicMerge() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
                    + "Eager-NonDeterministicMerge-correctness-flow.xml");
    
    tableRegressionCheck("table_writer", 100);
  }
  
  /**
   * A merge of more than 2 branches.
   * @throws Throwable
   */
  @Test(timeout = 10000)
  public void testNonDeterministicMergeThreeInputs() throws Throwable
  {
    runFlowNoAssert(getTestMethodInputDirectory()
                    + "NonDeterministicMerge-three-inputs-flow.xml");
    
    tableRegressionCheck("table_writer", 1500);
  }
  
  @Test
  // (timeout=10000)
  public void testPrioritizedNDMerge() throws Throwable
  {    
    runFlowNoAssert(getTestMethodInputDirectory() + "PrioNDMerge-flow.xml");
    
    tableRegressionCheck("table_writer", 2000);
  }
}
