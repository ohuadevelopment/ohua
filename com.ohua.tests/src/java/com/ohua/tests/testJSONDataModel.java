/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import com.ohua.data.model.json.JSONDataAccessor;
import com.ohua.data.model.json.JSONPath;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.XML;
import org.junit.Test;

public class testJSONDataModel extends AbstractRegressionTestCase
{
  /**
   * This test will serialize a JSON file that contains an example for each simple type
   * supported in JSON.
   * @throws Throwable
   */
  @Test
  public void testLoadPrimitveTypes() throws Throwable
  {
    outputLogToFile(_newLogger);

    String data = loadFileContents("primitve-types.json");
    JSONObject context = new JSONObject(new JSONTokener(data));

    JSONObject root = context.getJSONObject("root");

    Assert.assertTrue(root.get("int") instanceof Integer);
    Assert.assertTrue(root.get("long") instanceof Long);
    // although JSON supports floats as values its deserializer turns those into Double
    Assert.assertTrue(root.get("float") instanceof Double);
    // bytes are being treated as integers
    Assert.assertTrue(root.get("byte") instanceof Integer);
    Assert.assertTrue(root.get("double") instanceof Double);
    Assert.assertTrue(root.get("boolean") instanceof Boolean);
    Assert.assertTrue(root.get("string") instanceof String);
    Assert.assertTrue(root.get("NULL") == JSONObject.NULL);

    _newLogger.info(root.toString());
    _newLogger.info(XML.toString(root));
  }
  
  /**
   * Loads hierarchy data such as nested subtrees and dimensionality such as vectors/arrays.
   */
  @Test
  public void testLoadHierarchy() throws Throwable
  {
    outputLogToFile(_newLogger);

    String data = loadFileContents("hierarchical-types.json");
    JSONObject context = new JSONObject(new JSONTokener(data));
    
    JSONObject root = context.getJSONObject("root");
    
    Assert.assertNotNull(root.optJSONArray("dimensionality"));
    JSONArray firstLevelVector = root.getJSONArray("dimensionality");
    Assert.assertEquals(1, firstLevelVector.length());

    // the vector has a complex type ...
    Assert.assertNotNull(firstLevelVector.optJSONObject(0));
    JSONObject vectorElement = firstLevelVector.getJSONObject(0);
    Assert.assertNotNull(vectorElement.opt("string"));
    Assert.assertNotNull(vectorElement.opt("int"));
    
    // ... which in turn again contains a vector (of simple type string this time).
    Assert.assertNotNull(vectorElement.optJSONArray("dimensionality"));
    JSONArray simpleTypeVector = vectorElement.getJSONArray("dimensionality");
    Assert.assertEquals(3, simpleTypeVector.length());
    
    // the second item under "root" is a JSONObject with a nested vector
    Assert.assertNotNull(root.optJSONObject("hierarchy"));
    JSONObject hierarchy = root.getJSONObject("hierarchy");
    Assert.assertNotNull(hierarchy.opt("string"));
    Assert.assertNotNull(hierarchy.opt("int"));
    
    Assert.assertNotNull(hierarchy.optJSONArray("dimensionality"));
    simpleTypeVector = vectorElement.getJSONArray("dimensionality");
    Assert.assertEquals(3, simpleTypeVector.length());

    _newLogger.info(root.toString());
    _newLogger.info(XML.toString(root));
  }
  
  @Test
  public void testJPathResolution() throws Throwable
  {
    String data = loadFileContents("jpath-resolution.json");
    JSONObject context = new JSONObject(new JSONTokener(data));

    resolveAndAssert(context, "root");
    resolveAndAssert(context, "root/hierarchy");
    resolveAndAssert(context, "root/hierarchy/string");
    resolveAndAssert(context, "root/hierarchy/int");
    // TODO not supported yet
    // resolveAndAssert(context, "root/hierarchy/dimensionality");
    resolveAndAssert(context, "root/hierarchy/dimensionality[0]");
    resolveAndAssert(context, "root/hierarchy/dimensionality[1]");
    resolveAndAssert(context, "root/hierarchy/dimensionality[2]");

    // TODO not supported yet
    // resolveAndAssert(context, "root/dimensionality");
    resolveAndAssert(context, "root/dimensionality[0]");
    resolveAndAssert(context, "root/dimensionality[1]");
    resolveAndAssert(context, "root/dimensionality[0]/string");
    resolveAndAssert(context, "root/dimensionality[0]/int");
    resolveAndAssert(context, "root/dimensionality[0]/dimensionality[1]");
    resolveAndAssert(context, "root/dimensionality[1]/string");
    resolveAndAssert(context, "root/dimensionality[1]/int");
    resolveAndAssert(context, "root/dimensionality[1]/dimensionality[2]");
  }

  private void resolveAndAssert(JSONObject context, String path)
  {
    JSONPath jpath = new JSONPath(path);
    jpath.resolvePath(context);
    Assert.assertNotNull(jpath.getLeaf());
  }
  
  @Test
  public void testPatternMatcher()
  {
    Pattern _vectorPattern = Pattern.compile("\\[(\\d)\\]$");
    Matcher m = _vectorPattern.matcher("root/hierarchy/dimensionality[0]");
    Assert.assertTrue(m.find());
//    System.out.println("The group count is: " + m.groupCount());
    String match = m.group(1);
//    System.out.println("The match is: " + match);
    Integer index = Integer.valueOf(match);
//    System.out.println("The index is: " + index);
    Assert.assertEquals(0, index.intValue());
  }

  @Test
  public void testDataAccess() throws Throwable
  {
    JSONDataAccessor accessor = new JSONDataAccessor();
    accessor.load(new File(getTestMethodInputDirectory() + "data-access.json"));

    // initial insert
    accessor.setData("root/dimensionality[0]/description1", "ohua data streaming");
    accessor.setData("root/dimensionality[1]/description2", "streaming ohua data");
    
    // get the data again
    List<Object> data1 = accessor.getData("root/dimensionality[0]/description1");
    List<Object> data2 = accessor.getData("root/dimensionality[1]/description2");
    Assert.assertEquals(1, data1.size());
    Assert.assertEquals(1, data2.size());
    Assert.assertEquals("ohua data streaming", data1.get(0));
    Assert.assertEquals("streaming ohua data", data2.get(0));
    
    // override the above written value
    accessor.setData("root/dimensionality[0]/description1", "ohua data streaming is new");
    accessor.setData("root/dimensionality[1]/description2", "streaming ohua data is fast");
    
    // and finally get the data again
    data1 = accessor.getData("root/dimensionality[0]/description1");
    data2 = accessor.getData("root/dimensionality[1]/description2");
    Assert.assertEquals(1, data1.size());
    Assert.assertEquals(1, data2.size());
    Assert.assertEquals("ohua data streaming is new", data1.get(0));
    Assert.assertEquals("streaming ohua data is fast", data2.get(0));
  }
  
  @Test(expected = RuntimeException.class)
  // this is not implemented yet
  public void testDataConstruction() throws Throwable
  {
    JSONDataAccessor accessor = new JSONDataAccessor();
    accessor.load(new File(getTestMethodInputDirectory() + "data-construction.json"));

    // add another item into the vector
    // a special thing in JSON is that the elements of an array do not have to have the same
    // structure. this is a big advantage in terms of memory usage because you really only
    // allocate what you need. no null elements.
    accessor.setData("root/dimensionality[3]/someNumber", 12);
    
    List<Object> data1 = accessor.getData("root/dimensionality[3]/someNumber");
    Assert.assertEquals(12, data1.get(0));
  }
  
}
