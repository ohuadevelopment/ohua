/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.flows;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ohua.engine.flowgraph.elements.operator.OperatorGraphIterator;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.flowgraph.elements.GenericGraphIterator;
import com.ohua.engine.flowgraph.elements.GraphIterator;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.tests.AbstractFlowTestCase;

@Ignore
public class testFlowGraphIterator extends AbstractFlowTestCase
{
  /**
   * This graph has two branches that reference each other at the end. The GraphIterator needs
   * to detect this case.
   * @throws Throwable
   */
  @Ignore
  // FIXME just remove the GraphIterator already!
  @Test
  public void testManyBranchesAndJoins() throws Throwable
  {
    outputLogToFile(_newLogger);
    
    AbstractProcessManager manager =
        loadProcess(getTestClassInputDirectory() + "transcations-coloring-flow.xml");
    GraphIterator it = new GraphIterator(manager.getProcess().getGraph());
    while(it.hasNext())
    {
      _newLogger.info(it.next().toString());
    }
    
    assertBaselines();
  }
  
  /**
   * Same as above but this time we use the GenericGraphIterator.
   * @throws Throwable
   */
  // FIXME the graph used in this test case is just way too complicated. it should test for a specific graph structure!
  @Test
  public void testManyBranchesAndJoinsGI() throws Throwable
  {
    String[] expected = new String[] { "InitialInput-1",
                                      "Parallel_1_Entry-2",
                                      "Acc_Parallel_",
                                      "ResultInput-9",
                                      "Parallel_2_Entry_2-10",
                                      "Acc_Parallel_",
                                      "Parallel_1_Exit-5",
                                      "Acc_Final-6",
                                      "30_Day_Moving_Avg-7",
                                      "Log-19",
                                      "Parallel_2_Entry_1-8",
                                      "Rejoin-Data_Parallel_2-12",
                                      "Categorization-14",
                                      "Rejoin-Data_Parallel_1-11",
                                      "Categorization-13",
                                      "Parallel_2_Exit_2-16",
                                      "DatabaseOutput-18",
                                      "Parallel_2_Exit_1-15",
                                      "DatabaseOutput-17" };
    AbstractProcessManager manager =
        loadProcess(getTestClassInputDirectory() + "transcations-coloring-flow.xml");
    List<OperatorCore> operators = manager.getProcess().getGraph().getContainedGraphNodes();
    operators = new ArrayList<>(operators);
    Collections.sort(operators, (o1, o2) -> o1.getId().compareTo(o2.getId()));
    System.out.println(operators);
    GenericGraphIterator<OperatorCore> it = new OperatorGraphIterator(operators);
    int i = 0;
    while(it.hasNext())
    {
      String id = it.next().toString();
//      System.out.println("No match: " + id  + " expected: " + expected[i++]);
      Assert.assertTrue("No match: " + id  + " expected: " + expected[i], id.startsWith(expected[i++]));
    }

    // THIS IS ALSO CORRECT!
//    "[InitialInput-1, " +
//            "Parallel_1_Entry-2, " +
//            "Acc_Parallel_1-3, " +
//            "Acc_Parallel_2-4, " +
//            "Parallel_1_Exit-5, " +
//            "Acc_Final-6, " +
//            "30_Day_Moving_Avg-7, " +
//            "Parallel_2_Entry_1-8, " +
//            "ResultInput-9, " +
//            "Parallel_2_Entry_2-10, " +
//            "Rejoin-Data_Parallel_1-11, " +
//            "Rejoin-Data_Parallel_2-12, " +
//            "Categorization-13, " +
//            "Categorization-14, " +
//            "Parallel_2_Exit_1-15, " +
//            "Parallel_2_Exit_2-16, " +
//            "DatabaseOutput-17, " +
//            "DatabaseOutput-18, Log-19]"
  }
}
