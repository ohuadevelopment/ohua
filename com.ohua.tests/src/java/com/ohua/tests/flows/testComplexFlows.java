/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.tests.flows;

import java.util.HashMap;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

import com.ohua.engine.AbstractProcessManager;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.tests.AbstractIOTestCase;

public class testComplexFlows extends AbstractIOTestCase
{
  /**
   * This flow contains a merge operator whose incoming branches do not have the same length.
   * The test case makes sure that the "level-concept" in the EndOfStreamPacketHandler works
   * properly. (No cycle support code branch is executed, as it is not yet back in the plan.)
   * @throws Throwable
   */
  @Test//(timeout=40000)
  public void testTwoNDTwoTAOutput() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "Fast-Travelers-Graph-Analysis-flow.xml");

    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();
    performInitPhaseAssertions(manager);

    manager.runProcessStartUp();
    manager.runDataPhase();
    manager.awaitSystemPhaseCompletion();
    performDataPhaseAssertions(manager);
    
    manager.tearDownProcess();
    manager.awaitSystemPhaseCompletion();
    performTeardownAssertions(manager);
    
    Map<String, Long> outputTables = new HashMap<>();
    outputTables.put("table_writer_1", new Long(5000));
    outputTables.put("table_writer_2", new Long(10000));
    tableRegressionCheck(outputTables);
  }
  
  @Ignore
  @Test
  // FIXME this test case requires the SAP data to be loaded
  public void testMulitpleSplitMultipleNDMerge() throws Throwable
  {
    AbstractProcessManager manager =
        loadProcess(getTestMethodInputDirectory() + "transcations-coloring-flow.xml");
    System.out.println("INITIALIZATION");
    manager.initializeProcess();
    manager.awaitSystemPhaseCompletion();
    performInitPhaseAssertions(manager);
    
    GraphVisualizer.PRINT_FLOW_GRAPH = getTestMethodOutputDirectory() + "transformed-process";
    GraphVisualizer.printFlowGraph(manager.getProcess().getGraph());
  }
}
