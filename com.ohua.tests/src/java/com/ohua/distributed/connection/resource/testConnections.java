/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.distributed.connection.resource;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

import com.ohua.distributed.connection.resource.ZeroMQPullConnection.ZeroMQPull;
import com.ohua.distributed.connection.resource.ZeroMQPushConnection.ZeroMQPush;
import com.ohua.tests.AbstractOhuaTestCase;

public class testConnections extends AbstractOhuaTestCase
{
  @Ignore
  private class Puller implements Runnable
  {
    private ZeroMQPull _pull = null;
    private String _result = null;
    
    private Puller(ZeroMQPull pull)
    {
      _pull = pull;
    }
    
    @Override
    public void run()
    {
      _result = _pull.pull();
    }
  }
  
  @Ignore //TODO put it back in when this is back on the road map
  @Test(timeout=20000)
  public void testZMQ() throws Throwable
  {
    System.out.println(System.getProperty("java.library.path"));
    System.out.println(System.getProperty("java.class.path"));
       
    ZMQ.Context context = ZMQ.context(1);
    
    Socket socket2 = context.socket(ZMQ.PUSH);
    ZeroMQPushConnection sender = new ZeroMQPushConnection(socket2);
    socket2.bind("tcp://127.0.0.1:9080");

    Socket socket1 = context.socket(ZMQ.PULL);
    ZeroMQPullConnection receiver = new ZeroMQPullConnection(socket1);
    socket1.connect("tcp://127.0.0.1:9080");

    ZeroMQPush push = (ZeroMQPush)sender.getRestrictedConnection();
    ZeroMQPull pull = (ZeroMQPull)receiver.getRestrictedConnection();

    BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(10);
    ThreadPoolExecutor executor = new ThreadPoolExecutor(1, 1, Long.MAX_VALUE, TimeUnit.NANOSECONDS, workQueue);
    Puller p = new Puller(pull);
    Future<?> t = executor.submit(p);

    Thread.sleep(2000);
    push.push("Hello World");
    System.out.println("Pushed message");
    
    t.get();
    Assert.assertEquals("Hello World", p._result);
  }
}
