/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine;

public class OhuaTestProcessRunner extends OhuaProcessRunner
{

  public OhuaTestProcessRunner(String pathToFlow, String pathToConfig) throws Throwable
  {
    super(pathToFlow);
    super.loadRuntimeConfiguration(pathToConfig);
  }
  
  public OhuaTestProcessRunner(IExternalProcessManager manager)
  {
    super((String)null);
    super._manager = manager;
  }
}
