/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine;

public abstract class ManagerAccess
{
  public static RuntimeProcessConfiguration getRuntimeConfig(AbstractProcessManager manager)
  {
    return manager.getRuntimeProcessConfiguration();
  }
}
