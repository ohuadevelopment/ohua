/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.resource.management;


public class TestingResourceAccess
{
  private static TestingResourceAccess _access = new TestingResourceAccess();
  
  private TestingResourceAccess()
  {
    // singleton
  }
  
  public static TestingResourceAccess getInstance()
  {
    return _access;
  }
  
  public void commitTransaction(ResourceConnection cnn) throws Throwable
  {
    TransactionControl dbConnection =
        ResourceManager.getInstance().takeOverTransactionControl(cnn.getConnectionID());
    dbConnection.commit();
  }
  
  public void commitAndCloseTransaction(ResourceConnection cnn) throws Throwable
  {
    TransactionControl dbConnection =
        ResourceManager.getInstance().takeOverTransactionControl(cnn.getConnectionID());
    dbConnection.commit();
    ResourceManager.getInstance().closeConnection(cnn.getConnectionID());
  }
}
