/*
 * Copyright (c) Sebastian Ertel 2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.utils.regression;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.AssertionFailedError;

public class FileRegressionHandler
{
  
  public List<RegressionException> assertBaselines(String baselineDirectoryPath,
                                                   String testOutputDirectoryPath) throws Throwable
  {
    return assertBaselines(new File(baselineDirectoryPath), new File(testOutputDirectoryPath));
  }
  
  public List<RegressionException> assertBaselines(File baseline, File testOutput) throws Throwable
  {
    if(!baseline.exists())
    {
      throw new AssertionFailedError("No baselines defined for test case: " + baseline);
    }
    
    if(!testOutput.exists())
    {
      throw new AssertionFailedError("No test output found for test case: " + testOutput);
    }

    assert baseline.isDirectory() && testOutput.isDirectory();
    
    List<RegressionException> regressionFailures = new ArrayList<RegressionException>();
    
    for(File baselineFile : baseline.listFiles())
    {
      String outputFilePath = testOutput + File.separator + baselineFile.getName();
      
      File outputFile = new File(outputFilePath);
      if(outputFile.isDirectory())
      {
        continue;
      }
      
      if(outputFile.getName().startsWith("."))
      {
        continue;
      }

      if(outputFile.exists())
      {
        areFilesEqual(baselineFile, outputFile, regressionFailures);
      }
      else
      {
        regressionFailures.add(new MissingOutputFile(outputFile.getAbsolutePath()));
      }
    }
    
    return regressionFailures;
  }
  
  private void areFilesEqual(File file1, File file2, List<RegressionException> errorList) throws IOException
  {
    BufferedReader file1Reader = new BufferedReader(new FileReader(file1));
    BufferedReader file2Reader = new BufferedReader(new FileReader(file2));
    
    int lineCounter = 1;
    String file1Line = file1Reader.readLine();
    String file2Line = file2Reader.readLine();
    if(file2Line == null)
    {
      errorList.add(new ComparisonDifference(file1.getAbsolutePath(),
                                             file2.getAbsolutePath(),
                                             file1Line,
                                             file2Line,
                                             lineCounter));
    }
    while(file1Line != null && file2Line != null)
    {
      if(!file1Line.equals(file2Line))
      {
        errorList.add(new ComparisonDifference(file1.getAbsolutePath(),
                                               file2.getAbsolutePath(),
                                               file1Line,
                                               file2Line,
                                               lineCounter));
      }
      
      file1Line = file1Reader.readLine();
      file2Line = file2Reader.readLine();
      lineCounter++;
    }
    
    if(file1Reader.read() > -1)
    {
      errorList.add(new ComparisonDifference(file1.getAbsolutePath(),
                                             file2.getAbsolutePath(),
                                             file1Reader.readLine(),
                                             null,
                                             ++lineCounter));
    }
    else if(file2Reader.read() > -1)
    {
      errorList.add(new ComparisonDifference(file1.getAbsolutePath(),
                                             file2.getAbsolutePath(),
                                             null,
                                             file2Reader.readLine(),
                                             ++lineCounter));
    }
    
    file1Reader.close();
    file2Reader.close();
  }
}
