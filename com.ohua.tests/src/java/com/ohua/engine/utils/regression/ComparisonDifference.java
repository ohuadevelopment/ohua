/*
 * Copyright (c) Sebastian Ertel 2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.utils.regression;

public class ComparisonDifference extends RegressionException
{
  private String _baselineFile = null;
  private String _outputFile = null;
  
  private String _baselineContent = null;
  private String _outputContent = null;
  
  private int _line = -1;
  
  public ComparisonDifference(String baselineFile,
                              String outputFile,
                              String baselineContent,
                              String outputContent,
                              int line)
  {
    _baselineFile = baselineFile;
    _outputFile = outputFile;
    _baselineContent = baselineContent;
    _outputContent = outputContent;
    _line = line;
  }
  
  @Override
  public String getMessage()
  {
    StringBuffer print = new StringBuffer();
    print.append("DIFFERENCE FOUND: \n");
    print.append("baseline file = " + _baselineFile + "\n");
    print.append("test output file = " + _outputFile + "\n");
    print.append("line number = " + _line + "\n");
    print.append("baseline content = " + _baselineContent + "\n");
    print.append("test output content = " + _outputContent + "\n");
    return print.toString();
  }
}
