/*
 * Copyright (c) Sebastian Ertel 2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.utils.regression;

public class MissingOutputFile extends RegressionException
{
  private String _missingFile = null;
  
  public MissingOutputFile(String missingFile)
  {
    _missingFile = missingFile;
  }
  
  @Override
  public String getMessage()
  {
    return "MISSING OUTPUT FILE: " + _missingFile;
  }
  
}
