/*
 * Copyright (c) Sebastian Ertel 2009. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.engine.utils.regression;

abstract public class RegressionException extends AssertionError
{
  // might turn into an interface, not sure yet
}
