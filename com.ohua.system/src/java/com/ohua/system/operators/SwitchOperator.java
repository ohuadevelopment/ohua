/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.system.operators;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ohua.engine.data.model.daapi.DataUtils;
import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class SwitchOperator extends UserOperator
{
  // FIXME refactor with DataUtils.Operation
  public static enum Operation
  {
    LESS_THAN
    {
      @Override
      public boolean evalResult(int compareResult)
      {
        return compareResult < 0;
      }
    },
    EQUAL
    {
      @Override
      public boolean evalResult(int compareResult)
      {
        return compareResult == 0;
      }
    },
    GREATER_THAN
    {
      @Override
      public boolean evalResult(int compareResult)
      {
        return compareResult > 0;
      }
    },
    BETWEEN
    {
      @Override
      public boolean evalResult(int compareResult)
      {
        return compareResult == 0;
      }
      
      @Override
      public int compare(DataUtils utils, Object data, SimpleExpression expr)
      {
        int insideBoundary1 = utils.compare(data, expr.constant);
        if(insideBoundary1 < 0)
        {
          return -1;
        }
        int insideBoundary2 = utils.compare(data, expr.range);
        if(insideBoundary2 > 0)
        {
          return 1;
        }

        return insideBoundary1 + insideBoundary2;
      }
    },
    MATCH
    {
      @Override
      public void prepare(SimpleExpression expr)
      {
        Pattern pattern = Pattern.compile(expr.constant);
        expr._attach = pattern;
      }
      
      @Override
      public int compare(DataUtils utils, Object data, SimpleExpression expr)
      {
        Matcher matcher = ((Pattern)expr._attach).matcher(data.toString());
        return matcher.find() ? 1 : 0;
      }
      
      @Override
      public boolean evalResult(int compareResult)
      {
        return compareResult == 1;
      }
    };
    
    abstract public boolean evalResult(int compareResult);
    
    public void prepare(SimpleExpression expr)
    {
      // hook to be used for preparation
    }
    
    public int compare(DataUtils utils, Object data, SimpleExpression expr)
    {
      int comparisonResult = utils.compare(data, expr.constant);
      return comparisonResult;
    }
  }
  
  public static class SimpleExpression implements Serializable
  {
    public String input = null;
    public Operation operation = null;
    public String constant = null;
    public String range = null;
    // a bag to be used for advanced expressions
    private Object _attach = null;
  }
  
  public static class SwitchProperties implements Serializable
  {
    public Map<String, SimpleExpression> expressions = new HashMap<String, SimpleExpression>();
  }
  
  public SwitchProperties _properties = new SwitchProperties();
  
  private InputPortControl _inPortControl = null;
  
  @Override
  public void cleanup()
  {
    // nothing yet
  }
  
  @Override
  public void prepare()
  {
    _inPortControl = getDataLayer().getInputPortController("input");
    for(SimpleExpression expr : _properties.expressions.values())
    {
      expr.operation.prepare(expr);
    }
  }
  
  @Override
  public void runProcessRoutine()
  {
    boolean backoff = false;
    while(!backoff && _inPortControl.next())
    {
      for(Map.Entry<String, SimpleExpression> expressionEntry : _properties.expressions.entrySet())
      {
        List<Object> data = _inPortControl.getData(expressionEntry.getValue().input);
        Assertion.invariant(data.size() == 1);
        int comparisonResult =
            expressionEntry.getValue().operation.compare(getDataLayer().getDataUtils(),
                                                         data.get(0),
                                                         expressionEntry.getValue());
        if(expressionEntry.getValue().operation.evalResult(comparisonResult))
        {
          OutputPortControl outputPortControl =
              getDataLayer().getOutputPortController(expressionEntry.getKey());
          getDataLayer().transferInputToOutput(_inPortControl.getPortName(),
                                               outputPortControl.getPortName());
          backoff |= outputPortControl.send();
        }
      }
    }
  }
  
  public Object getState()
  {
    return null;
  }
  
  public void setState(Object state)
  {
    prepare();
  }
  
}
