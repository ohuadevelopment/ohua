/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.system.sections.mappers;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.ohua.engine.ConfigurationExtension;
import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;

public class SectionConfigurationParser {
  protected static class SectionConfiguration implements ConfigurationExtension {
    private Properties _props = null;
    
    @Override
    public void setProperties(Properties properties) {
      _props = properties;
    }
  }
  
  protected class ParallelsConfig {
    int replicas = 0;
    List<String> ops = null;
    String splitType = "Split";
    Object _bag = null;// to be used as an extension point for additional information
    ReplicationProperties _props = null; // to be assigned during conversion
  }

  private List<List<String>> _allSections = null;
  private List<ParallelsConfig> _allParallels = new ArrayList<>();
  
  protected List<List<String>> getSections() {
    return _allSections;
  }
  
  protected List<ParallelsConfig> getParallels() {
    return _allParallels;
  }
  
  @SuppressWarnings("unchecked")
  protected void parseConfiguration(RuntimeProcessConfiguration runtimeConfig) {
    SectionConfiguration sectionsConfig = new SectionConfiguration();
    runtimeConfig.aquirePropertiesAccess(sectionsConfig);
    
    Object sections = sectionsConfig._props.get("section-config");
    if(sections instanceof String) {
      JSONObject root = loadConfiguration((String) sections);
      parseSectionConfigurations(root);
      parseParallelsConfiguration(root);
    } else if(sections instanceof List) {
      _allSections = (List<List<String>>) sections;
    } else {
      throw new RuntimeException("Unsupported section configuration (type): " + sectionsConfig._props.get("section-config"));
    }
  }
  
  /**
   * The JSON format for describing parallel parts of a flow graph is as follows:<br>
   * {"parallels":<br>
   * [<br>
   * {replicas:2, flow-part: [op1,op2]},<br>
   * {replicas:3, flow-part: [op3]},<br>
   * {replicas:2, flow-part: [op4,op5]<br>
   * ]<br>
   * }
   * <p/>
   * 
   * Optionally, we now support to select the operator to be used for the graph split. The
   * formatting is as follows:<br>
   * .. <br>
   * {<br>
   * replicas:2,<br>
   * flow-part: [op1,op2]<br>
   * split : { "type": "Switch", "input" : "/root/something", "conditions" :
   * ["^test-left","^test-right"]} }<br>
   * ..
   * @return
   */
  private void parseParallelsConfiguration(JSONObject root) {
    if(!root.has("parallels")) {
      return;
    }
    
    try {
      JSONArray parallels = root.getJSONArray("parallels");
      _allParallels = new ArrayList<ParallelsConfig>();
      for(int i = 0; i < parallels.length(); i++) {
        JSONObject config = parallels.getJSONObject(i);
        ParallelsConfig pConfig = new ParallelsConfig();
        pConfig.replicas = config.getInt("replicas");
        JSONArray col = config.getJSONArray("flow-parts");
        List<String> flowPart = new ArrayList<String>();
        for(int j = 0; j < col.length(); j++) {
          flowPart.add(col.getString(j));
        }
        JSONObject splitConfig = config.optJSONObject("split");
        if(splitConfig != null) {
          pConfig.splitType = splitConfig.getString("type");
          JSONArray conditions = splitConfig.getJSONArray("conditions");
          String input = splitConfig.getString("input");
          String[][] conds = new String[conditions.length()][2];
          for(int j = 0; j < conditions.length(); j++) {
            conds[j][0] = input;
            conds[j][1] = conditions.getString(j);
          }
          pConfig._bag = conds;
        }
        
        pConfig.ops = flowPart;
        _allParallels.add(pConfig);
      }
    }
    catch(JSONException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  /**
   * The JSON format for describing sections is as follows: {"sections":
   * [[op1,op2],[op3],[op4,op5]]}
   * @return
   */
  private void parseSectionConfigurations(JSONObject root) {
    JSONArray sections;
    try {
      sections = root.getJSONArray("sections");
      _allSections = new ArrayList<List<String>>();
      for(int i = 0; i < sections.length(); i++) {
        JSONArray col = sections.getJSONArray(i);
        List<String> section = new ArrayList<String>();
        for(int j = 0; j < col.length(); j++) {
          section.add(col.getString(j));
        }
        _allSections.add(section);
      }
    }
    catch(JSONException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  private JSONObject loadConfiguration(String file) {
    if(file == null) {
      throw new RuntimeException("Section configuration missing!");
    }
    try {
      return new JSONObject(new JSONTokener(new FileReader(file)));
    }
    catch(Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
}
