/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.system.sections.mappers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ohua.engine.flowgraph.elements.AbstractUniqueID;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.utils.parser.OhuaFlowParser;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;
import com.ohua.engine.utils.parser.ParallelSubgraphSupport;
import com.ohua.system.sections.mappers.SectionConfigurationParser.ParallelsConfig;

public class DataParallelSupport
{
  protected static void perform(List<ParallelsConfig> parallels, FlowGraph graph)
  {
    List<ReplicationProperties> props = convert(parallels, graph);
    ParallelSubgraphSupport parallelSupport = new ParallelSubgraphSupport(new AdvancedDataParallelSplitMergeFactory(parallels));
    for(ReplicationProperties parallel : props)
    {
      parallelSupport.performParallelRewrite(parallel, graph);
    }
    
    // scope the replicas
    AbstractUniqueID scope = props.get(0)._subgraph.get(0).getScope();
    OhuaFlowParser.unifyOperatorIDs(graph, scope);
  }
  
  /**
   * In order to enable nested parallelism, the order of the parallels must adhere to the order
   * they would be discovered during a normal parse operation.
   * @param parallels
   * @param graph
   * @return
   */
  private static List<ReplicationProperties> convert(List<ParallelsConfig> parallels,
                                                     FlowGraph graph)
  {
    // simple conversion first
    List<ReplicationProperties> props = new ArrayList<ReplicationProperties>();
    for(ParallelsConfig config : parallels)
    {
      ReplicationProperties prop = new ReplicationProperties();
      prop._replicationCount = config.replicas;
      for(String op : config.ops)
      {
        prop._subgraph.add(graph.getOperator(op).getId());
      }
      props.add(prop);
      config._props = prop;
    }
    
    // now understand who encloses whom
    Collections.sort(props, new Comparator<ReplicationProperties>()
    {
      @Override
      public int compare(ReplicationProperties o1, ReplicationProperties o2)
      {
        // ascending sort
        return o1._subgraph.size() - o2._subgraph.size();
      }
    });
    
    for(int i = props.size(); i > -1; i--)
    { // child
      for(int j = i + 1; j < props.size(); j++)
      { // potential parent
        if(props.get(j)._subgraph.containsAll(props.get(i)._subgraph))
        {
          props.get(i)._enclosing = props.get(j);
          props.get(j)._subgraph.removeAll(props.get(i)._subgraph);
          break;
        }
      }
    }
    return props;
  }
  
}
