/*
 * Copyright (c) Sebastian Ertel 2012. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.system.sections.mappers;

import java.util.ArrayList;
import java.util.List;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.sections.OneOpOneSectionGraphBuilder;
import com.ohua.engine.sections.Section;
import com.ohua.engine.operators.AbstractMergeOperator;
import com.ohua.engine.operators.SplitOperator;

public class SplitMergeMapper extends OneOpOneSectionGraphBuilder
{
  public SplitMergeMapper(RuntimeProcessConfiguration config)
  {
    super(config);
  }
    
  public Section createSingleOpSection(OperatorCore source)
  {
    if(source.getOperatorAlgorithm() instanceof SplitOperator)
    {
      // TBD if the previous section was an I/O section then we might not want this!
      Assertion.invariant(source.getAllPreceedingGraphNodes().size()==1);
      OperatorCore upstreamOp = source.getAllPreceedingGraphNodes().get(0);
      return appendToUpstreamSection(upstreamOp, source);      
    }
    else if(updstreamIsMerge(source))
    {
      Assertion.invariant(source.getAllPreceedingGraphNodes().size() == 1);
      OperatorCore merge = source.getAllPreceedingGraphNodes().get(0);
      return appendToUpstreamSection(merge, source);
    }
    else
    {
      return super.createSingleOpSection(source);
    }
  }
  
  private Section appendToUpstreamSection(OperatorCore upstreamOp, OperatorCore current)
  {
    Section s = _sectionMap.get(upstreamOp.getId());
    // correct the section
    List<OperatorCore> c = new ArrayList<OperatorCore>();
    c.addAll(s.getOperators());
    c.add(current);
    s.setOperators(c);
    _sectionMap.put(current.getId(), s);
    return s;
  }
  
  private boolean updstreamIsMerge(OperatorCore source)
  {
    for(OperatorCore upstream : source.getAllPreceedingGraphNodes())
    {
      if(upstream.getOperatorAlgorithm() instanceof AbstractMergeOperator)
      {
        return true;
      }
    }
    return false;
  }
  
}
