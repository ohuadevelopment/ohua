/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.system.sections.mappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorFactory;
import com.ohua.engine.utils.parser.DataParallelSplitMergeFactory;
import com.ohua.engine.utils.parser.OhuaParallelFlowParser.ReplicationProperties;
import com.ohua.system.operators.SwitchOperator;
import com.ohua.system.operators.SwitchOperator.SimpleExpression;
import com.ohua.system.operators.SwitchOperator.SwitchProperties;
import com.ohua.system.sections.mappers.SectionConfigurationParser.ParallelsConfig;

public class AdvancedDataParallelSplitMergeFactory extends DataParallelSplitMergeFactory
{
  private Map<ReplicationProperties, ParallelsConfig> _directory =
      new HashMap<ReplicationProperties, ParallelsConfig>();
  
  protected AdvancedDataParallelSplitMergeFactory(List<ParallelsConfig> configs)
  {
    for(ParallelsConfig config : configs)
    {
      _directory.put(config._props, config);
    }
  }
  
  protected OperatorCore getSplitOperator(FlowGraph graph, ReplicationProperties props) throws OperatorLoadingException
  {
    ParallelsConfig config = _directory.get(props);
    if(config.splitType.equals("Switch"))
    {
      OperatorCore switchOp =
          OperatorFactory.getInstance().createUserOperatorCore(graph, "Switch");
      String[][] conditions = (String[][]) config._bag;
      // TODO This probably wants to also be done via Castor!
      SwitchProperties switchProps = new SwitchProperties();
      for(int i = 0; i < conditions.length; i++)
      {
        SimpleExpression expression = new SimpleExpression();
        expression.operation = SwitchOperator.Operation.MATCH;
        expression.input = conditions[i][0];
        expression.constant = conditions[i][1];
        // this naming matches the naming in the ParallelSubgraph algorithm
        switchProps.expressions.put("out-" + i, expression);
      }
      ((SwitchOperator) switchOp.getOperatorAlgorithm())._properties = switchProps;
      return switchOp;
    }
    else
    {
      return super.getSplitOperator(graph, props);
    }
  }
}
