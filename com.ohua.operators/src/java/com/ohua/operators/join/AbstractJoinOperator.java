/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.join;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public abstract class AbstractJoinOperator extends UserOperator {
  public final static String LEFT_INPUT = "input-left";
  public final static String RIGHT_INPUT = "input-right";
  public final static String MATCH_OUTPUT = "output-match";
  public final static String NO_MATCH_OUTPUT = "output-no-match";
  
  /*
   * Properties:
   */
  public static class JoinProperties implements Serializable {
    public Map<String, String> keyPairs = new HashMap<String, String>();
  }
  
  public JoinProperties _properties = new JoinProperties();
  
  protected InputPortControl _leftInControl = null;
  protected InputPortControl _rightInControl = null;
  protected OutputPortControl _matchOutControl = null;
  protected OutputPortControl _noMatchOutControl = null;
  
  protected Set<String> _leftKeys = null;
  protected Set<String> _rightKeys = null;
  
  /*
   * State
   */
  protected boolean _unfinishedWrite = false;
  
  protected void prepareInitialState() {
    _leftInControl = getDataLayer().getInputPortController(LEFT_INPUT);
    _rightInControl = getDataLayer().getInputPortController(RIGHT_INPUT);
    _matchOutControl = getDataLayer().getOutputPortController(MATCH_OUTPUT);
    _noMatchOutControl = getDataLayer().getOutputPortController(NO_MATCH_OUTPUT);
    
    _leftKeys = new HashSet<String>(_properties.keyPairs.keySet());
    _rightKeys = new HashSet<String>(_properties.keyPairs.values());
  }
  
  @Override
  public void cleanup() {
    // nothing to do
  }
  
  protected boolean produceMatchOutput(InputPortControl match, Set<String> leafs, Set<String> keys) {
    getDataLayer().transferInputToOutput(match.getPortName(), _matchOutControl.getPortName());
    for(String leaf : leafs) {
      if(!keys.contains(leaf)) {
        List<Object> data = _rightInControl.getData(leaf);
        if(data.size() == 1) {
          _matchOutControl.setData(leaf, data.get(0));
        } else {
          _matchOutControl.setData(leaf, data);
        }
      }
    }
    return _matchOutControl.send();
  }
  
  /**
   * Here we need to add the missing fields from either the left or the right input and fill
   * them with NULL values.
   * @param rightInput
   */
  protected boolean produceNonMatchOutput(InputPortControl noMatch, Set<String> leafs, Set<String> keys,
                                          boolean copy)
  {
    if(copy) {
      getDataLayer().copyInputToOutput(noMatch.getPortName(), _noMatchOutControl.getPortName());
    } else {
      getDataLayer().transferInputToOutput(noMatch.getPortName(), _noMatchOutControl.getPortName());
    }
    for(String leaf : leafs) {
      if(!keys.contains(leaf)) {
        _noMatchOutControl.setData(leaf, null);
      }
    }
    return _noMatchOutControl.send();
  }
  
  public Object getState() {
    return _unfinishedWrite;
  }
  
  public void setState(Object state) {
    prepareInitialState();
    
    _unfinishedWrite = (Boolean) state;
  }
  
}
