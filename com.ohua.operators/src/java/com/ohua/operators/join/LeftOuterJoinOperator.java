/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.join;

import java.util.Set;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.ReplayMode;

/**
 * This join will first buffer all the data on its right port and then stream the data on its
 * left input port. When processing the data from the left input port the data is joined with
 * the buffered data of the right input port.
 * <p>
 * NOTE that here the idea is that the data arriving on the right input port does fit
 * into memory.
 * @author sertel
 * 
 */
public class LeftOuterJoinOperator extends AbstractJoinOperator
{
  @Override
  public void prepare()
  {
    super.prepareInitialState();
  }

  @Override
  public void runProcessRoutine()
  {
    if(bufferRightInput())
    {
      if(handleUnfinishedOutput())
      {
        return;
      }

      // now we stream the left input
      while(_leftInControl.next())
      {
        // do we have buffered right items matching items for this packet?
        String key = createHeapKey(_leftInControl, _leftKeys);
        _rightInControl.query(key);
        _rightInControl.replay(ReplayMode.HEAP);
        
        boolean isOrphan = true;
        while(_rightInControl.next())
        {
          isOrphan = false;
          if(produceMatchOutput(_leftInControl, _rightInControl.getLeafs(), _rightKeys))
          {
            _unfinishedWrite = true;
            return;
          }
        }
        
        _rightInControl.stop();

        if(isOrphan)
        {
          if(produceNonMatchOutput(_leftInControl, _rightInControl.getLeafs(), _rightKeys, false))
          {
            return;
          }
        }
      }
      return;
    }
  }
  
  private boolean handleUnfinishedOutput()
  {
    if(!_unfinishedWrite)
    {
      return false;
    }

    while(_rightInControl.next())
    {
      if(produceMatchOutput(_leftInControl, _rightInControl.getLeafs(), _rightKeys))
      {
        _unfinishedWrite = true;
        return true;
      }
    }
    _rightInControl.stop();
    _unfinishedWrite = false;
    return false;
  }

  private boolean bufferRightInput()
  {
    if(_rightInControl.hasSeenLastPacket())
    {
      // we are done buffering
      return true;
    }
    
    // buffer all packets from the right input port
    while(_rightInControl.next())
    {
      // buffer together with key
      String key = createHeapKey(_rightInControl, _rightKeys);
      _rightInControl.store(key);
    }
    return _rightInControl.hasSeenLastPacket();
  }
  
  private String createHeapKey(InputPortControl control, Set<String> keys)
  {
    StringBuilder combinedKey = new StringBuilder();
    for(String key : keys)
    {
      combinedKey.append(control.getData(key).toString());
      combinedKey.append("-");
    }
    
    combinedKey.deleteCharAt(combinedKey.length() - 1);

    return combinedKey.toString();
  }
  
}
