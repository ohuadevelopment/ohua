/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.join;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.ReplayMode;
import com.ohua.engine.exceptions.Assertion;

/**
 * This operator assumes that the data arriving the two input ports is ordered in an ascending
 * fashion.
 * @author sertel
 * 
 */
public class StreamingJoinOperator extends AbstractJoinOperator {
  
  /*
   * State
   */
  
  // FIXME to me like this is just a missing service of the data access layer. something along
  // the lines of "boolean hasPortData()".
  private ArrayList<InputPortControl> _needsData = new ArrayList<InputPortControl>();
  
  private boolean _clearBuffer = false;
  
  @Override
  public void prepare() {
    prepareInitialState();
    
    _needsData.add(_leftInControl);
    _needsData.add(_rightInControl);
  }
  
  @Override
  public void runProcessRoutine() {
    handleUnfinishedMatchOutput();
    
    boolean backoff = false;
    // we can process data if either one port was closed (-> produce orphans) or the ports have
    // data.
    while(retrieveData() && openInputPortsExist() && !backoff) {
      // compare current packet on left input with current packet on right input
      int compare = compareJoinKeys();
      if(compare < 0) {
        /*
         * left is smaller than right => we stop iterating right and iterate left until we see a
         * value change to the current value of left. we replay the right buffer for all the
         * items of left that do not have a value change.
         */
        _rightInControl.replay(ReplayMode.BUFFER);
        if(_rightInControl.next() && bufferMatch()) {
          // replay the buffer and produce the join result(s) for this left item
          backoff = produceMatchOutput();
          if(backoff) {
            // don't reset the buffer. when we come in the needsMoreData is empty and the
            // replay() function is idempotent. hence the above next() call will advance the
            // buffer cursor and we pick up where we left off
            continue;
          }
        } else {
          // produce no-match output for this left item
          backoff = produceNonMatchOutput(_leftInControl, _rightInControl.getLeafs(), _rightKeys, true);
        }
        finishBufferReplay();
        _needsData.add(_leftInControl);
      } else if(compare > 0) {
        /*
         * left is greater than right => we advance right and output what we see until we have a
         * match again or right gets greater than left (no-match output)
         */
        backoff = produceNonMatchOutput(_rightInControl, _leftInControl.getLeafs(), _leftKeys, true);
        _needsData.add(_rightInControl);
      } else {
        // left equals right => we just buffer the right item here and advance the right
        bufferRightItem();
        _needsData.add(_rightInControl);
      }
    }
  }
  
  private boolean handleUnfinishedMatchOutput() {
    // an unfinished output can exist either if the we got pushed back by the downstream op or
    // if all the ports have seen the EOS but still have buffered data to be output. in the
    // latter case we run again until we output all matches and then the finishBufferReplay()
    // function directly sweeps the buffer.
    if(!_unfinishedWrite && openInputPortsExist()) {
      return false;
    }
    
    // if there are no open ports anymore, there might be data still in the buffer but we have
    // already produced the output to the data and have requested a clean up in the next cycle
    // which did not come yet.
    if(_clearBuffer && !openInputPortsExist()) {
      return false;
    }
    
    _rightInControl.replay(ReplayMode.BUFFER);
    if(_rightInControl.next()) {
      if(produceMatchOutput()) {
        // back-off again
        return true;
      }
    }
    
    _unfinishedWrite = false;
    finishBufferReplay();
    _needsData.add(_leftInControl);
    return false;
  }
  
  private void finishBufferReplay() {
    // stop the buffer replay
    _rightInControl.stop();
    _rightInControl.reset();
    
    if(openInputPortsExist()) {
      // clear the right item buffer on the next match
      _clearBuffer = true;
    } else {
      // otherwise just sweep that thing! (so the next() on the buffer will also return null and
      // we can finish up)
      _rightInControl.clear();
    }
  }
  
  private void bufferRightItem() {
    if(_clearBuffer) {
      _rightInControl.clear();
      _clearBuffer = false;
    }
    _rightInControl.buffer();
  }
  
  private boolean openInputPortsExist() {
    return !_leftInControl.hasSeenLastPacket() || !_rightInControl.hasSeenLastPacket();
  }
  
  private boolean bufferMatch() {
    int compare = compareKeys();
    return compare == 0;
  }
  
  @SuppressWarnings("unused") private int _producedOutput = 0;
  
  /**
   * 
   */
  private boolean produceMatchOutput() {
    Set<String> leftLeafs = _leftInControl.getLeafs();
    
    // the cursor position is already at item one of the buffer
    do {
      // take the data item from the right input port and add the fields from the left input
      // port
      getDataLayer().copyInputToOutput(_rightInControl.getPortName(), _matchOutControl.getPortName());
      for(String leftLeaf : leftLeafs) {
        if(!_leftKeys.contains(leftLeaf)) {
          List<Object> leftData = _leftInControl.getData(leftLeaf);
          if(leftData.size() == 1) {
            _matchOutControl.setData(leftLeaf, leftData.get(0));
          } else {
            _matchOutControl.setData(leftLeaf, leftData);
          }
        }
      }
      _producedOutput++;
      if(_matchOutControl.send()) {
        _unfinishedWrite = true;
        return true;
      }
    }
    while(_rightInControl.next());
    
    return false;
  }
  
  private boolean retrieveData() {
    Iterator<InputPortControl> it = _needsData.iterator();
    while(it.hasNext()) {
      InputPortControl inPort = it.next();
      if(inPort.next() || inPort.hasSeenLastPacket()) {
        it.remove();
      }
    }
    
    return _needsData.isEmpty();
  }
  
  private Object[][] retrieveJoinValues() {
    Object[][] joinValues = new Object[_properties.keyPairs.size()][2];
    int index = 0;
    for(Map.Entry<String, String> keyPair : _properties.keyPairs.entrySet()) {
      List<Object> left = _leftInControl.getData(keyPair.getKey());
      List<Object> right = _rightInControl.getData(keyPair.getValue());
      Assertion.invariant(left.size() == 1 && right.size() == 1);
      joinValues[index][0] = left.get(0);
      joinValues[index++][1] = right.get(0);
    }
    return joinValues;
  }
  
  private int compareJoinKeys() {
    if(_rightInControl.hasSeenLastPacket()) {
      return -1;
    }
    
    if(_leftInControl.hasSeenLastPacket()) {
      return 1;
    }
    
    return compareKeys();
  }
  
  private int compareKeys() {
    Object[][] join = retrieveJoinValues();
    for(Object[] joinPair : join) {
      int compare = getDataLayer().getDataUtils().compare(joinPair[0], joinPair[1]);
      if(compare != 0) {
        return compare;
      }
    }
    return 0;
  }
  
  @Override
  public Object getState() {
    List<String> needsData = new ArrayList<String>();
    for(InputPortControl inPortControl : _needsData) {
      needsData.add(inPortControl.getPortName());
    }
    return new Object[] { super.getState(),
                         _clearBuffer,
                         needsData };
  }
  
  @Override
  @SuppressWarnings("unchecked")
  public void setState(Object state) {
    Object[] s = (Object[]) state;
    super.setState(s[0]);
    
    _clearBuffer = (Boolean) s[1];
    
    List<String> needsData = (List<String>) s[2];
    for(String needData : needsData) {
      if(needData.equals(LEFT_INPUT)) {
        _needsData.add(_leftInControl);
      } else {
        Assertion.invariant(needData.equals(RIGHT_INPUT));
        _needsData.add(_rightInControl);
      }
    }
  }
  
}
