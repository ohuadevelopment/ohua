/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.operators.io.resources;

import java.sql.Connection;
import java.sql.SQLException;

import com.ohua.engine.resource.management.AbstractConnection;
import com.ohua.engine.resource.management.ResourceConnection;

public class DatabaseTAConnection extends AbstractConnection
{
  private Connection _connection = null;
  
  public DatabaseTAConnection(Connection connection)
  {
    _connection = connection;
  }

  @Override
  protected void close() throws SQLException
  {
    _connection.close();
  }
  
  @Override
  protected void commit() throws SQLException
  {
    _connection.commit();
  }
  
  @Override
  protected ResourceConnection getRestrictedConnection()
  {
    return new DatabaseConnection(getConnectionID(), _connection);
  }
  
  @Override
  protected void rollback() throws SQLException
  {
    _connection.rollback();
  }
  
}
