/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractResource;
import com.ohua.engine.resource.management.ResourceAccess;

public abstract class AbstractDatabaseResource extends AbstractResource {
  public AbstractDatabaseResource(ManagerProxy ohuaProcess, Map<String, String> attributes) {
    super(ohuaProcess, attributes);
  }
  
  @Override
  public ResourceAccess getResourceAccess(OperatorID operatorID, String cpArtifactID) {
    return new DatabaseResourceAccess(operatorID, cpArtifactID);
  }
  
  /**
   * We use double-quotes for the table and columns in order to deal with fancy names. Drawback:
   * the names will be case-sensitive!
   * @param cnn
   * @param tableName
   * @param columnsAndTypes
   */
  public Map<String, Integer> createTable(DatabaseConnection cnn, String tableName,
                                          Map<String, String> columnsAndTypes)
  {
    Map<String, Integer> indexMap = new HashMap<String, Integer>();
    int index = 1;
    
    StringBuffer str = new StringBuffer();
    str.append("create table " + tableName + " ( ");
    
    for(Map.Entry<String, String> col : columnsAndTypes.entrySet()) {
      str.append(col.getKey() + " " + col.getValue() + " ,");
      indexMap.put(col.getKey(), index++);
    }
    
    str.deleteCharAt(str.length() - 1);
    
    str.append(")");
    
    createTable(cnn, str.toString());
    return indexMap;
  }
  
  public Map<String, Integer> createTable(DatabaseConnection cnn, String tableName,
                                          Map<String, String> columnsAndTypes, List<String> primaryKeys)
  {
    Map<String, Integer> indexMap = new HashMap<String, Integer>();
    int index = 1;
    
    StringBuffer str = new StringBuffer();
    str.append("create table " + tableName + " ( ");
    
    for(Map.Entry<String, String> col : columnsAndTypes.entrySet()) {
      str.append("" + col.getKey() + " " + col.getValue() + " ,");
      indexMap.put(col.getKey(), index++);
    }
    
    // add primary key
    str.append(" PRIMARY KEY(");
    for(String primaryKey : primaryKeys) {
      str.append("" + primaryKey + "");
      str.append(", ");
    }
    str.delete(str.length() - 2, str.length());
    str.append("))");
    
    createTable(cnn, str.toString());
    _logger.info("Connection-" + cnn.getConnectionID() + " - Table created: " + tableName);
    return indexMap;
  }
  
  protected void createTable(DatabaseConnection cnn, String createQuery) {
//    System.out.println("Table query: " + createQuery);
    _logger.info("Table query: " + createQuery);
    try {
      Statement stmt = cnn.createStatement();
      stmt.execute(createQuery);
      stmt.close();
    }
    catch(SQLException e) {
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  public Map<String, Integer> createIndexMap(Map<String, String> columnsAndTypes) {
    Map<String, Integer> indexMap = new HashMap<>();
    int index = 1;
    for(Map.Entry<String, String> col : columnsAndTypes.entrySet()) {
      indexMap.put(col.getKey(), index++);
    }
    return indexMap;
  }
  
  public void deleteTable(DatabaseConnection cnn, String tableName) {
    try {
      Statement stmt = cnn.createStatement();
      stmt.execute("drop table " + tableName);
      stmt.close();
    }
    catch(SQLException e) {
      // FIXME this can not be decided on this level. the caller of this method needs to
      // understand whether the operation was successful or not. hence we need to throw
      // something here! (Maybe make it depending on the SQL error code!)
      // it's okay, the table just did not exist. just print something
      _logger.warning("Connection-" + cnn.getConnectionID() + " - Trying to drop table[" + tableName
                      + "] encountered problem: " + e.getMessage() + " SQL state: " + e.getSQLState()
                      + " SQL ERROR Code:" + e.getErrorCode());
      // FIXME Postgres is not so graceful here and aborts the transaction right away. Need to
      // close out on the TA here!
      commit(cnn);
    }
  }
  
}
