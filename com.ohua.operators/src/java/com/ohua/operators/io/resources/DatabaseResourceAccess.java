/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractResource;
import com.ohua.engine.resource.management.ResourceAccess;
import com.ohua.engine.resource.management.ResourceConnection;
import com.ohua.engine.utils.OhuaLoggerFactory;

public class DatabaseResourceAccess extends ResourceAccess
{
  private DatabaseConnection _dbCnn = null;
  private AbstractDatabaseResource _dbRes = null;
  
  private PreparedStatement _insertStmt = null;

  private Map<String, Integer> _colIndexMap = null;

  public DatabaseResourceAccess(OperatorID operatorID, String cpArtifactID)
  {
    super(operatorID, cpArtifactID);
  }
  
  @Override
  public void initialize()
  {
    _dbCnn = getConnection();
    _dbRes = getResource();
    _colIndexMap = _dbRes.createIndexMap(_columnDefs);
  }

  @Override
  protected AbstractDatabaseResource getResource()
  {
    AbstractResource resource = super.getResource();
    Assertion.invariant(resource instanceof AbstractDatabaseResource);
    return (AbstractDatabaseResource) resource;
  }

  @Override
  protected DatabaseConnection getConnection()
  {
    ResourceConnection cnn = super.getConnection();
    Assertion.invariant(cnn instanceof DatabaseConnection);
    return (DatabaseConnection) cnn;
  }
  
  /**
   * We put double-quotes around the column names because otherwise some query parsers will
   * automatically lower case them and the columns will not be found because they are defined
   * case-sensitive.
   */
  @Override
  public void prepareInsert()
  {
    StringBuffer insertQuery = new StringBuffer();

    insertQuery.append("insert into " + getCPArtifactID() + " (");
    StringBuffer params = new StringBuffer();
    for(String col : _columnDefs.keySet())
    {
      insertQuery.append("" + col + "" + ", ");
      params.append("?, ");
    }
    insertQuery.delete(insertQuery.length() - 2, insertQuery.length());
    params.delete(params.length() - 2, params.length());
    
    insertQuery.append(") values (");
    insertQuery.append(params);
    insertQuery.append(")");
    OhuaLoggerFactory.getLogger(getClass()).info("Insert statement: " + insertQuery);
    try
    {
      _insertStmt = _dbCnn.prepareStatement(insertQuery.toString());
    }
    catch(SQLException e)
    {
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }

  @Override
  public void createArtifact()
  {
    _dbRes.createTable(_dbCnn, getCPArtifactID(), _columnDefs, _keys);
  }

  @Override
  public void deleteArtifact()
  {
    _dbRes.deleteTable(_dbCnn, getCPArtifactID());
  }
  
  @Override
  public void insert(Map<String, Object> data) throws Throwable
  {
    Assertion.invariant(!_insertStmt.isClosed());
    
    // TODO performance measurements needed: setObject vs. setInt
    // _insertStmt.setInt(1, idx);
    for(Map.Entry<String, Object> entry : data.entrySet())
    {
      _insertStmt.setObject(_colIndexMap.get(entry.getKey()), entry.getValue());
    }

    // execute the insert statement
    _insertStmt.executeUpdate();
  }
  
  @Override
  public void insert(Object data) throws Throwable
  {
    throw new UnsupportedOperationException("Direct insert does not exist for databases!");
  }
  
}
