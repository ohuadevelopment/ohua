/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;

//FIXME refactoring with derby
public class PostgresDatabaseResource extends AbstractDatabaseResource
{
  public PostgresDatabaseResource(ManagerProxy ohuaProcess, Map<String, String> attributes)
  {
    super(ohuaProcess, attributes);
  }
  
  @Override
  public AbstractConnection getConnection(Object... args)
  {
    return getDatabaseConnection();
  }
  
  private AbstractConnection getDatabaseConnection()
  {
    try
    {
      Class.forName("org.postgresql.Driver").newInstance();
    }
    catch(Exception e1)
    {
      e1.printStackTrace();
    }
    
    Properties properties = new java.util.Properties();
    properties.setProperty("user", getAttributes().get("userName"));
    properties.setProperty("password", getAttributes().get("password"));
    
    String host = "";
    if(getAttributes().containsKey("host"))
    {
      host = getAttributes().get("host");
    }
    else
    {
      host = "localhost";
    }
    
    String port = "";
    if(getAttributes().containsKey("port"))
    {
      port = getAttributes().get("port");
    }
    else
    {
      port = "5432";
    }
    
    String clientURL =
        "jdbc:postgresql://" + host + ":" + port + "/" + getAttributes().get("databaseName");
    System.out.println("Connection URL: " + clientURL);
    Connection c = null;
    try
    {
      c = DriverManager.getConnection(clientURL, properties);
      c.setAutoCommit(false);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    
    return new DatabaseTAConnection(c);
  }
  
  @Override
  protected void createTable(DatabaseConnection cnn, String createQuery)
  {
    super.createTable(cnn, createQuery);
    commit(cnn);
  }

  @Override
  protected AbstractExternalActivator getExternalActivator(OperatorID operatorID)
  {
    return null;
  }
  
  @Override
  public void validate()
  {
    // TODO Auto-generated method stub
  }

}
