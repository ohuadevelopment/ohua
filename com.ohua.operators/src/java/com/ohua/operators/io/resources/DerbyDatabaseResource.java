/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.exceptions.ResourceValidationException;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;

public class DerbyDatabaseResource extends AbstractDatabaseResource
{
  public DerbyDatabaseResource(ManagerProxy ohuaProcess, Map<String, String> attributes)
  {
    super(ohuaProcess, attributes);
  }
  
  @Override
  public AbstractConnection getConnection(Object... args)
  {
    return getDatabaseConnection();
  }
  
  private AbstractConnection getDatabaseConnection()
  {
    try
    {
      Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
    }
    catch(Exception e1)
    {
      e1.printStackTrace();
    }
    
    Properties properties = new java.util.Properties();
    properties.setProperty("user", getAttributes().get("userName"));
    properties.setProperty("password", getAttributes().get("password"));
    
    // FIXME This seems to not be enough to run the tests in parallel! Maybe also randomize the
    // database name!
    // FIXME Must assign this to a test otherwise a regression will never work!
    int port = -1;
//    if(super.getAttributes().get("id").equals("derby01"))
//    {
//      // testing database
//      port = new Random().nextInt();
//    }
//    else
//    {
      port = DerbyServerUtils.NETWORKSERVER_PORT;
//    }
    
    String clientURL =
        "jdbc:derby://localhost:" + port + "/" + getAttributes().get("databaseName")
            + ";create=true;";
    
    Connection c = null;
    try
    {
      c = DriverManager.getConnection(clientURL, properties);
      c.setAutoCommit(false);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }
    
    return new DatabaseTAConnection(c);
  }
  
  @Override
  public void validate()
  {
    if(getAttributes().containsKey("userName") || getAttributes().get("userName").length() < 1)
    {
      throw new ResourceValidationException("Missing attribute for resource '"
                                            + getAttributes().get("id") + "' missing: username");
    }
    
    if(getAttributes().containsKey("databaseName")
       || getAttributes().get("databaseName").length() < 1)
    {
      throw new ResourceValidationException("Missing attribute for resource '"
                                            + getAttributes().get("id")
                                            + "' missing: databaseName");
    }
    
    if(getAttributes().containsKey("password") || getAttributes().get("password").length() < 1)
    {
      throw new ResourceValidationException("Missing attribute for resource '"
                                            + getAttributes().get("id") + "' missing: password");
    }
    
  }
  
  /**
   * Derby needs special treatment here because it has problems during regression updating the
   * SYSTABLES and runs into a deadlock.
   */
  @Override
  protected void createTable(DatabaseConnection cnn, String createQuery)
  {
    super.createTable(cnn, createQuery);
    commit(cnn);
  }
  
  /**
   * Derby needs special treatment here because it has problems during regression updating the
   * SYSTABLES and runs into a deadlock.
   */
  @Override
  public void deleteTable(DatabaseConnection cnn, String tableName)
  {
    super.deleteTable(cnn, tableName);
    commit(cnn);
  }
  
  @Override
  protected AbstractExternalActivator getExternalActivator(OperatorID operatorID)
  {
    return null;
  }
  
}
