/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.io.PrintWriter;
import java.net.InetAddress;

import org.apache.derby.drda.NetworkServerControl;

public class DerbyServerUtils
{
  static
  {
    System.setProperty("derby.system.home", "./test-resources/derby");
  }

  // network server control specific
  public static int NETWORKSERVER_PORT = 1527;
    
  private class NetworkServerUtils
  {
    private NetworkServerControl serverControl;
    private PrintWriter _pw;
    private int _portNum;

    public NetworkServerUtils(int port){
      this(port, null);
    }

    public NetworkServerUtils(int port, PrintWriter pw)
    {
      _portNum = port;
      _pw = pw;
      try
      {
        serverControl = new NetworkServerControl(InetAddress.getByName("localhost"), _portNum);
//        pw.println("Derby Network Server created");
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
    
    /**
     * trace utility of server
     */
    @SuppressWarnings("unused")
    public void trace(boolean onoff)
    {
      try
      {
        serverControl.trace(onoff);
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
    
    /**
     * Try to test for a connection Throws exception if unable to get a connection
     */
    public void testForConnection() throws Exception
    {
      serverControl.ping();
    }
    
    /**
     * Shutdown the NetworkServer
     */
    public void shutdown()
    {
      try
      {
        serverControl.shutdown();
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
    
    /**
     * Start Derby Network server
     * 
     */
    public void start()
    {
      try
      {
        serverControl.start(_pw);
      }
      catch(Exception e)
      {
        e.printStackTrace();
      }
    }
    
  }
  
  private static NetworkServerUtils nwServer;
  
  public static void startServer(String[] args) throws Exception
  {
//    PrintWriter pw = new PrintWriter(System.out, true); // to print messages
    try
    {
//      startDerbyNetworkServer(pw);
      startDerbyNetworkServer();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    
  }
  
  public static void shutdown()
  {
//    System.out.println("Derby Server shutdown called ...");
    // Shutdown Derby network server
    nwServer.shutdown();
  }
  
  @SuppressWarnings("static-access")
//  private static void startDerbyNetworkServer(PrintWriter pw) throws InterruptedException
  private static void startDerbyNetworkServer() throws InterruptedException
  {
    /*
     * Start - In order to start the network server do the following In case you want to start
     * the server as a script or another program comment out the next block of code (i.e. until
     * the comment line 'End - network server started') Also, comment out the 'Shutdown Derby
     * Network Server' line of code at the bottom In case you decide to comment out the starting
     * of the network server, make sure that the client thread is not making an embedded
     * connection but instead making only a client connection. Also note, the server logs
     * messages to the file derby.log in the directory you run this program
     */

//      nwServer = DerbyServerUtils.getInstance().new NetworkServerUtils(NETWORKSERVER_PORT, pw);
      nwServer = DerbyServerUtils.getInstance().new NetworkServerUtils(NETWORKSERVER_PORT);
      nwServer.start();
      
      boolean knowIfServerUp = false;
      int numTimes = 5;
      
      // Test to see if server is ready for connections, for 15 seconds.
      while(!knowIfServerUp && numTimes > 0)
      {
        try
        {
          // testing for connection to see if the network server is up and running
          // if server is not ready yet, this method will throw an exception
          numTimes--;
          nwServer.testForConnection();
          knowIfServerUp = true;
        }
        catch(Exception e)
        {
          System.out.println("[NsSample] Unable to obtain a connection to network server, trying again after 3000 ms.");
          Thread.currentThread().sleep(3000);
        }
      }
      if(!knowIfServerUp)
      {
        System.err.println("[NsSample] Exiting, since unable to connect to Derby Network Server.");
        System.err.println("[NsSample] Please try to increase the amount of time to keep trying to connect to the Server.");
        System.exit(1);
      }
      
//      pw.println("[NsSample] Derby Network Server started.");
    
  }
  
  private DerbyServerUtils()
  {
    // singleton
  }

  private static DerbyServerUtils _serverUtils = new DerbyServerUtils();
  private static DerbyServerUtils getInstance()
  {
    return _serverUtils;
  }
  
}
