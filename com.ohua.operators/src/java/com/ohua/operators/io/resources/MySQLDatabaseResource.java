/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import com.ohua.engine.AbstractExternalActivator;
import com.ohua.engine.AbstractExternalActivator.ManagerProxy;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.engine.resource.management.AbstractConnection;

//FIXME refactor with other DB resources
public class MySQLDatabaseResource extends AbstractDatabaseResource
{
  
  public MySQLDatabaseResource(ManagerProxy ohuaProcess, Map<String, String> attributes)
  {
    super(ohuaProcess, attributes);
  }

  @Override
  protected AbstractConnection getConnection(Object... args)
  {
    return getDatabaseConnection();
  }
  
  private AbstractConnection getDatabaseConnection()
  {
    try
    {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
    }
    catch(Exception e1)
    {
      e1.printStackTrace();
    }
    
    Properties properties = new java.util.Properties();
    properties.setProperty("user", getAttributes().get("userName"));
    properties.setProperty("password", getAttributes().get("password"));
    
    String host = "";
    if(getAttributes().containsKey("host"))
    {
      host = getAttributes().get("host");
    }
    else
    {
      host = "localhost";
    }
    
    String port = "";
    if(getAttributes().containsKey("port"))
    {
      port = getAttributes().get("port");
    }
    else
    {
      port = "3306";
    }
    
    String clientURL =
        "jdbc:mysql://" + host + ":" + port + "/" + getAttributes().get("databaseName");
    System.out.println("Connection URL: " + clientURL);
    Connection c = null;
    try
    {
      c =
          DriverManager.getConnection(clientURL,
                                      getAttributes().get("userName"),
                                      getAttributes().get("password"));
      // c = DriverManager.getConnection(clientURL, properties);
      c.setAutoCommit(false);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    
    return new DatabaseTAConnection(c);
  }
  
  @Override
  protected AbstractExternalActivator getExternalActivator(OperatorID operatorID)
  {
    return null;
  }
  
  @Override
  public void validate()
  {
    // TODO Auto-generated method stub
    
  }
  
}
