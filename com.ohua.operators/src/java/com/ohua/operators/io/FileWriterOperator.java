/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.operators.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.operators.categories.OutputIOOperator;

/**
 * This thing needs to become something very similar to the database output operator.
 * @author sertel
 */
public class FileWriterOperator extends UserOperator implements OutputIOOperator
{
  public static class FileWriterProperties
  {
    public String filePath = null;
    public String delimiter = "";
    public ArrayList<String> columns = null;
    public boolean writeHeader = false;
    public boolean writeIndex = false;
  }

  private FileWriterProperties _properties = null;
  
  private BufferedWriter _writer = null;
  
  private InputPortControl _inPortControl = null;
  
  private long _index = 1;

  @Override
  public void prepare()
  {
    File f = new File(_properties.filePath);
    f.delete();
    try
    {
      f.createNewFile();
      _writer = new BufferedWriter(new FileWriter(_properties.filePath));
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    
    if(_properties.writeHeader)
    {
      String header = Arrays.deepToString(_properties.columns.toArray());
      header.replace(", ", _properties.delimiter);
      writeLine(header.substring(1, header.length() - 1));
    }

    _inPortControl = getDataLayer().getInputPortController("input");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inPortControl.next())
    {
      StringBuilder builder = new StringBuilder();
      if(_properties.writeIndex)
      {
        builder.append(_index++);
        builder.append(_properties.delimiter);
      }
      for(String column : _properties.columns)
      {
        builder.append(_inPortControl.getData(column).get(0));
        builder.append(_properties.delimiter);
      }
      // remove last line delimiter
      builder.delete(builder.length() - _properties.delimiter.length(), builder.length());
      
      // write to file
      writeLine(builder.toString());
    }
  }
  
  private void writeLine(String line)
  {
    try
    {
      _writer.write(line);
      _writer.newLine();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  @Override
  public void cleanup()
  {
    try
    {
      _writer.flush();
      _writer.close();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
  }

  public Object getState()
  {
    try
    {
      _writer.flush();
    }
    catch(IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException(e);
    }

    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  public void setState(Object state)
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException();
  }
  
  public FileWriterProperties getProperties()
  {
    return _properties;
  }
  
  public void setProperties(FileWriterProperties properties)
  {
    _properties = properties;
  }
  
}
