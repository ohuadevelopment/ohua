/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.operators.categories.BatchEndpoint;
import com.ohua.engine.operators.categories.OutputIOOperator;
import com.ohua.engine.operators.categories.TransactionalIOEndpoint;
import com.ohua.engine.resource.management.ResourceManager;
import com.ohua.operators.io.resources.AbstractDatabaseResource;
import com.ohua.operators.io.resources.DatabaseConnection;

public abstract class AbstractDatabaseEndpoint extends AbstractDatabaseOperator implements
                                                                               BatchEndpoint,
                                                                               OutputIOOperator,
                                                                               TransactionalIOEndpoint
{
  public static class DatabaseEndpointProperties extends DatabaseProperties implements
                                                                           Serializable
  {
    public int commitPeriod = 50;
    // TODO not yet supported properly. needs fix for "disrespect arc boundary"
    public boolean strictCommitRequirements = false;
    public Map<String, String> columns = null;
    public Map<String, String> adaptation = null;
    public ArrayList<String> primaryKeys = null;
    public boolean noPrimaryKeys = false;
  }
  
  private DatabaseConnection _cnn = null;
  
  private PreparedStatement _insertStmt = null;

  protected Map<String, Integer> _stmtMap = null;
  
  protected InputPortControl _inPortControl = null;

  @Override
  public void prepare()
  {
    Assertion.invariant(_cnn == null && _insertStmt == null);

    _cnn = getDatabaseConnection();
    
    deleteTable(getProperties().tableName);

    if(getProperties().noPrimaryKeys)
    {
      _stmtMap = createTable(getProperties().tableName, getProperties().columns);
    }
    else
    {
      List<String> primaryKeys =
          getProperties().primaryKeys == null || getProperties().primaryKeys.isEmpty() ? Collections.singletonList(getProperties().columns.keySet().iterator().next())
                                                                                      : getProperties().primaryKeys;
      _stmtMap =
          ((AbstractDatabaseResource) ResourceManager.getInstance().getResource(getProperties().resourceID)).createTable(_cnn,
                                                                                                                         getProperties().tableName,
                                                                                                                         getProperties().columns,
                                                                                                                         primaryKeys);
    }

    _insertStmt = prepareInsertStatement(getProperties().tableName, getProperties().columns);
    prepareAdaption();
    
    _inPortControl = getDataLayer().getInputPortController("input");
  }

  protected void deleteTable(String tableName)
  {
    ((AbstractDatabaseResource) ResourceManager.getInstance().getResource(getProperties().resourceID)).deleteTable(_cnn,
                                                                                                                   tableName);
  }

  protected Map<String, Integer> createTable(String tableName, Map<String, String> columns)
  {
    return ((AbstractDatabaseResource) ResourceManager.getInstance().getResource(getProperties().resourceID)).createTable(_cnn,
                                                                                                                          tableName,
                                                                                                                          columns);
  }
  
  /**
   * In case there was not adaptation specified we will use the provided columns as direct
   * adaptation.
   */
  private void prepareAdaption()
  {
    if(getProperties().adaptation != null && !getProperties().adaptation.isEmpty())
    {
      return;
    }
    
    Map<String, String> adaptation = new HashMap<String, String>();
    for(String column : getProperties().columns.keySet())
    {
      adaptation.put(column, column);
    }
    getProperties().adaptation = adaptation;
  }

  protected PreparedStatement prepareInsertStatement(String tableName,
                                                     Map<String, String> columns)
  {
    StringBuffer colNames = new StringBuffer();
    colNames.append("insert into " + tableName + " (");
    
    for(String col : columns.keySet())
    {
      colNames.append("" + col + ", ");
    }
    
    colNames.deleteCharAt(colNames.length() - 2);
    colNames.append(" ) values ( ");
    
    for(int i = 0; i < columns.size(); i++)
    {
      colNames.append("? ,");
    }
    
    colNames.deleteCharAt(colNames.length() - 1);
    colNames.append(")");
    
    PreparedStatement insertStmt = null;
    try
    {
      insertStmt = _cnn.prepareStatement(colNames.toString());
    }
    catch(SQLException e)
    {
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    return insertStmt;
  }
  
  public Object getState()
  {
    Logger.getLogger("debug_cpInitiator").log(Level.ALL, "CHECKPOINT OF DatabaseWriter taken");
    // FIXME this operator is absolutely stateless! this is a property and will be available in
    // the properties again after restart!
    // table name
    return getProperties().tableName;
  }
  
  public void setState(Object state)
  {
    getProperties().tableName = (String) state;
    _cnn = getDatabaseConnection();
    _insertStmt = prepareInsertStatement(getProperties().tableName, getProperties().columns);
    prepareAdaption();

    _stmtMap =
        ((AbstractDatabaseResource) ResourceManager.getInstance().getResource(getProperties().resourceID)).createIndexMap(getProperties().columns);
    
    _inPortControl = getDataLayer().getInputPortController("input");
  }
  
  @Override
  public void cleanup()
  {
    super.cleanup();
    try
    {
      _insertStmt.close();
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  public int getCommitPeriod()
  {
    return getProperties().commitPeriod;
  }
  
  public boolean strictCommitRequirements()
  {
    return getProperties().strictCommitRequirements;
  }

  protected PreparedStatement getInsertStmt()
  {
    return _insertStmt;
  }
  
  @Override
  public DatabaseEndpointProperties getProperties()
  {
    return (DatabaseEndpointProperties) super.getProperties();
  }

}
