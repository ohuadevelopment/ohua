/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class ZipFileReaderOperator extends FileReaderOperator
{
    
  @Override
  protected InputStream prepareReaderStream()
  {
    int endIndex = _properties.filePath.indexOf(".zip") + 4;
    File zipFile = new File(_properties.filePath.substring(0, endIndex));
    if(!zipFile.exists())
    {
      throw new RuntimeException(new FileNotFoundException(zipFile.getAbsolutePath()));
    }
    try
    {
      @SuppressWarnings("resource") // closed in the super class
      ZipFile zip = new ZipFile(zipFile);
      Enumeration<? extends ZipEntry> entries = zip.entries();
      while(entries.hasMoreElements())
      {
        ZipEntry entry = entries.nextElement();
      }

      String zippedFile = _properties.filePath.substring(endIndex + 1);
      ZipEntry zipFileEntry = zip.getEntry(zippedFile);
      if(zipFileEntry == null)
      {
        throw new RuntimeException(new FileNotFoundException("Could not find zipped file: "
                                                             + zippedFile
                                                             + ". Please check log for available entries for zip file '"
                                                             + zipFile.getName() + "'"));
      }
      return zip.getInputStream(zipFileEntry);
    }
    catch(ZipException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
}
