/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.operators.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.operators.categories.InputIOOperator;

public class FileReaderOperator extends UserOperator implements InputIOOperator
{
  public static class FileReaderProperties
  {
    public String filePath = null;
    public String delimiter = null;
    public ArrayList<String> columns = null;
    public String fileFormat = "UTF-8";
    public String outputSchema = null;
  }

  public FileReaderProperties _properties = null;
  
  private OutputPortControl _outPortControl = null;
  
  /*
   * State
   */

  private BufferedReader _reader = null;
  
  private ArrayList<String> _columns = null;

  @Override
  public void prepare()
  {
    InputStream stream = prepareReaderStream();
    InputStreamReader fileReader;
    try
    {
      fileReader = new InputStreamReader(stream, _properties.fileFormat);
    }
    catch(UnsupportedEncodingException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }

    _reader = new BufferedReader(fileReader);
    
    if(_properties.delimiter == null || _properties.delimiter.length() < 1)
    {
      if(_properties.columns.size() != 1)
      {
        throw new RuntimeException("No delimiter has been provided, hence the number of columns to be read from the file can only be 1.");
      }
    }
    
    if(_properties.columns != null && !_properties.columns.isEmpty())
    {
      _columns = _properties.columns;
    }
    
    // we have the columns already
    if(_columns == null)
    {
      readColumnNamesHeader(readLine());
    }
    
    _outPortControl = getDataLayer().getOutputPortController("output");
    if(_properties.outputSchema != null && _properties.outputSchema.length() > 0)
    {
      _outPortControl.load(new File(_properties.outputSchema));
    }
  }
  
  protected InputStream prepareReaderStream()
  {
    try
    {
      return new FileInputStream(_properties.filePath);
    }
    catch(FileNotFoundException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  private String readLine()
  {
    try
    {
      return _reader.readLine();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  @Override
  public void runProcessRoutine()
  {
    String line = null;
    while((line = readLine()) != null)
    {
      readDataColumns(line);
      
      if(_outPortControl.send())
      {
        break;
      }
    }
  }

  private void readDataColumns(String line)
  {
    _outPortControl.newPacket();
    
    String[] columns = splitLine(line);
    
    int index = 0;
    for(String column : _columns)
    {
      String value = columns[index++];
      if(value != null)
      {
        value = value.trim();
      }
      _outPortControl.setData(column, value);
    }
  }
  
  private String[] splitLine(String line)
  {
    String[] columns = null;
    if(_properties.delimiter == null || _properties.delimiter.length() < 1)
    {
      columns = new String[] { line };
    }
    else
    {
      columns = line.split(_properties.delimiter);
    }
    
    return columns;
  }
  
  /**
   * Interpret the first line of the file as the column names.
   * @param line
   */
  private void readColumnNamesHeader(String line)
  {
    _columns = new ArrayList<String>();
    String[] headers = splitLine(line);
    
    for(String columnName : headers)
    {
      _columns.add(columnName.trim());
    }
  }
  
  @Override
  public void cleanup()
  {
    try
    {
      _reader.close();
    }
    catch(IOException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public Object getState()
  {
    // FIXME this needs an own efficient implementation of RandomAccessFile using buffered
    // input.
    // emptyCP.attachAdditionalInformation("file-reader-pointer", _reader.getLineNumber());
    return null;
  }
  
  public void setState(Object state)
  {
    prepare();
    // FIXME this needs an own efficient implementation of RandomAccessFile using buffered
    // input.
    // int lineNumber = (Integer)
    // checkpoint.retrieveAdditionalInformation("file-reader-pointer");
    // _reader.setLineNumber(lineNumber);
  }

  public boolean isUnboundedInput()
  {
    return false;
  }

}
