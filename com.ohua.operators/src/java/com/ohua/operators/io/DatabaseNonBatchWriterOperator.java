/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * This operator does not execute any batches. All the rows are being added to the change set
 * and the committing is done by the framework.
 * @author sebastian
 * 
 */
public class DatabaseNonBatchWriterOperator extends AbstractDatabaseEndpoint
{
  private int _rowCount = 0;
  
  @Override
  public void runProcessRoutine()
  {
    try
    {
      while(_inPortControl.next())
      {
        for(Map.Entry<String, String> adaptation : getProperties().adaptation.entrySet())
        {
          List<Object> data = _inPortControl.getData(adaptation.getKey());
          getInsertStmt().setObject(_stmtMap.get(adaptation.getValue()), data.get(0));
        }
        getInsertStmt().executeUpdate();
        _rowCount++;
      }
    }
    catch(SQLException e)
    {
      System.out.println("Bad packet:");
      System.out.println(_inPortControl.dataToString("XML"));
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  public void flush()
  {
    // no intermediate state, so there is nothing to flush here
  }
  
  @Override
  protected DatabaseProperties getDefaultProperties()
  {
    return new DatabaseEndpointProperties();
  }
  
}
