/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import com.ohua.engine.operators.categories.TransactionalIOOperator;
import com.ohua.operators.io.resources.DatabaseConnection;

/**
 * A convenience interface, nothing more. Should never be used in the framework core but barely
 * to support database resources.
 * @author sertel
 * 
 */
public interface DatabaseIOOperator extends TransactionalIOOperator
{
  public DatabaseConnection getDatabaseConnection();
}
