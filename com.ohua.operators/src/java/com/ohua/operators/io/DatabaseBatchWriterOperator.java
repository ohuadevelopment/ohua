/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.ohua.engine.flowgraph.elements.operator.OperatorStateAccess;

/**
 * Incoming data is submitted to the database in batches. Commit handling is still all done by
 * the framework.
 * @author sertel
 * 
 */
public class DatabaseBatchWriterOperator extends AbstractDatabaseEndpoint {
  public static class DatabaseBatchEndpointProperties extends DatabaseEndpointProperties implements Serializable {
    public int batchSize = 100;
  }
  
  private int _batchCount = 0;
  private int _rowCount = 0;

//  private int total = 0;
  @Override
  public void runProcessRoutine() {
    try {
      while(_inPortControl.next()) {
        handleReceivedPacket();
//        System.out.println("DB writer total: " + total++);
        addBatch();
        
        _rowCount++;
        if(_rowCount >= ((DatabaseBatchEndpointProperties) getProperties()).batchSize) {
          writeBatch();
        }
      }
    }
    catch(SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  protected void handleReceivedPacket() throws SQLException {

    for(Map.Entry<String, String> adaptation : getProperties().adaptation.entrySet()) {
      List<Object> data = _inPortControl.getData(adaptation.getKey());
      Object val = data.get(0);
      if(val == null || val.toString().equals("null")) {
        getInsertStmt().setObject(_stmtMap.get(adaptation.getValue()), "NULL");
      } else {
        // FIXME Data and TIME etc. need to be converted here from Java to SQL! trac item
        // #24
        getInsertStmt().setObject(_stmtMap.get(adaptation.getValue()), val);
      }
    }
  }
  
  private void addBatch() throws SQLException {
    try {
      getInsertStmt().addBatch();
    }
    catch(SQLException e) {
      // DebugReporter.getInstance().printOperatorReport(this);
      throw e;
    }
  }
  
  private int[] writeBatch() throws SQLException {
    _batchCount++;
    int[] results = getInsertStmt().executeBatch();
    assert results.length == _rowCount;
    _rowCount = 0;
//    System.out.println("batch");
    return results;
  }
  
  /**
   * In the case of the DatabaseWriter that uses batch functionality for the updates to the
   * database, we need to write out the current batch at this point because otherwise the data
   * in the current batch will not be part of the transaction that gets committed with this
   * checkpoint. On restart this would lead to missing packets! Furthermore this DatabaseWriter
   * tries to write its last batch in the flush() function that is called after the checkpoint
   * framework has handled the EOS marker. Hence the data in the last batch would not be
   * committed to the database. In order to fix both scenarios we need to write out the batch
   * with every checkpoint (which seems reasonable because otherwise we would have to include
   * the data in the current batch into the checkpoint!!!)
   */
  @Override
  public Object getState() {
    return new Object[] { super.getState(),
                         _batchCount };
  }
  
  @Override
  public void setState(Object state) {
    
    // if we have seen a marker there must have been data before. hence the table must already
    // have been created! --> THIS WILL NOT BE TRUE ANYMORE ONCE WE ALLOW FOR CHECKPOINT MARKERS
    // BEFORE ANY DATA HAS BEEN PROCESSED!
    _batchCount = (int) ((Object[]) state)[1];
    
    // debugging
    // try
    // {
    // AbstractIOTestCase.tableRegressionCheck(_tableName, 100);
    // }
    // catch(Throwable e)
    // {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    
    super.setState(((Object[]) state)[0]);
  }
  
  public void flush() {
//    System.out.println("flush");
    if(_rowCount < 1) {
      return;
    }

    try {
      writeBatch();
    }
    catch(SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  
  @Override
  protected DatabaseProperties getDefaultProperties() {
    return new DatabaseBatchEndpointProperties();
  }
  
}
