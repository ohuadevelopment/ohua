/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.exceptions.Assertion;


public class DatabaseLookupOperator extends AbstractDatabaseOperator
{
  public enum Operation
  {
    EQUAL,
    GREATER_THAN,
    LESS_THAN
  }
  
  public static class DatabaseLookupProperties extends DatabaseProperties
  {
    private String _where = null;
    private List<String> _outputColumns = new LinkedList<String>();
    
    public void setWhere(String where)
    {
      _where = where;
    }
    
    public String getWhere()
    {
      return _where;
    }
    
    public void setOutputColumns(List<String> outputColumns)
    {
      _outputColumns = outputColumns;
    }
    
    public List<String> getOutputColumns()
    {
      return _outputColumns;
    }
  }

  private Connection _cnn = null;
  private PreparedStatement _selectStmt = null;
  private ResultSet _rs = null;
  
  private Map<String, Operation> _colNames = new HashMap<String, Operation>();
  
  private Map<String, List<Integer>> _predicateIndexes = new HashMap<String, List<Integer>>();
  
  private static final Pattern _paramPattern = Pattern.compile("'(\\w)'");
  
  private InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;

  @Override
  public void prepare()
  {
    _cnn = getDatabaseConnection();
    try
    {
      StringBuffer selectClause = new StringBuffer();
      selectClause.append("select ");
      for(String columnName : getProperties().getOutputColumns())
      {
        selectClause.append(columnName + ", ");
      }
      selectClause.delete(selectClause.length() - 3, selectClause.length() - 1);
      
      String whereClause = getProperties().getWhere();
      Matcher pathMatcher = _paramPattern.matcher(whereClause);
      int index = 0;
      String transformedWhereClause = whereClause;
      int deletedChars = 0;
      while(pathMatcher.find())
      {
        String path = pathMatcher.group();
        if(!_predicateIndexes.containsKey(path))
        {
          _predicateIndexes.put(path, new ArrayList<Integer>());
        }
        _predicateIndexes.get(path).add(index);
        transformedWhereClause =
            transformedWhereClause.substring(0, pathMatcher.start() - deletedChars) + "?"
                + transformedWhereClause.substring(pathMatcher.end() - deletedChars);
        deletedChars += path.length() + 2;
        index++;
      }
      
      String query =
          selectClause.toString() + " from " + getProperties().tableName + " "
              + transformedWhereClause.toString();
      _selectStmt = _cnn.prepareStatement(query);
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    
    if(!finishUpResultSet())
    {
      return;
    }
    
    while(_inPortControl.next())
    {
      try
      {
        // set the query parameters
        for(Map.Entry<String, List<Integer>> predicateInput : _predicateIndexes.entrySet())
        {
          List<Object> streamParam = _inPortControl.getData(predicateInput.getKey());
          Assertion.invariant(streamParam.size() == 1);
          for(Integer paramIndex : predicateInput.getValue())
          {
            _selectStmt.setObject(paramIndex, streamParam.get(0));
          }
        }

        // execute the query
        if(_rs == null)
        {
          _rs = _selectStmt.executeQuery();
        }

        // fetch the result
        int colCount = _rs.getMetaData().getColumnCount();
        
        while(_rs.next())
        {
          getDataLayer().transferInputToOutput("input", "output");
          for(int i = 1; i < colCount + 1; i++)
          {
            _outPortControl.setData("result/" + getProperties().getOutputColumns().get(i - 1),
                                   _rs.getObject(i));
          }
          
          if(_outPortControl.send())
          {
            return;
          }
        }
        
        _rs.close();
        _rs = null;
      }
      catch(SQLException e)
      {
        // TODO Auto-generated catch block
        e.printStackTrace();
        throw new RuntimeException("unhandled exception", e);
      }
    }
  }
  
  private boolean finishUpResultSet()
  {
    if(_rs == null)
    {
      return true;
    }
    
    try
    {
      int colCount = _rs.getMetaData().getColumnCount();
      
      while(_rs.next())
      {
        getDataLayer().transferInputToOutput("input", "output");
        for(int i = 1; i < colCount + 1; i++)
        {
          _outPortControl.setData("result/" + getProperties().getOutputColumns().get(i - 1),
                                 _rs.getObject(i));
        }
        
        if(_outPortControl.send())
        {
          return false;
        }
      }
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    
    return true;
  }
  
  @Override
  public void cleanup()
  {
    try
    {
      _rs.close();
      _selectStmt.close();
    }
    catch(SQLException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  public Object getState()
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("DatabaseReaderOperator.checkpoint(...) not yet implemented");
  }
  
  public void setState(Object checkpoint)
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("DatabaseReaderOperator.restart(...) not yet implemented");
  }
  
  public void addWhereClauseCol(String colname, Operation operation)
  {
    _colNames.put(colname, operation);
  }
  
  @Override
  protected DatabaseProperties getDefaultProperties()
  {
    return new DatabaseProperties();
  }
  
  @Override
  public DatabaseLookupProperties getProperties()
  {
    return (DatabaseLookupProperties) super.getProperties();
  }

  public int getCommitPeriod()
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("DatabaseLookupOperator.getCommitPeriod(...) not yet implemented");
  }
  
  public boolean strictCommitRequirements()
  {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("DatabaseLookupOperator.strictCommitRequirements(...) not yet implemented");
  }

}
