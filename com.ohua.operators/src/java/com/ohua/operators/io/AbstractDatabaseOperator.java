/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.io.Serializable;
import java.util.logging.Level;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.resource.management.ResourceConnection;
import com.ohua.engine.resource.management.ResourceManager;
import com.ohua.operators.io.resources.DatabaseConnection;

public abstract class AbstractDatabaseOperator extends UserOperator implements
                                                                   DatabaseIOOperator
{
  public static class DatabaseProperties implements Serializable
  {
    public String resourceID = "derby01"; // default is 'sebastianDB'
    public String tableName = null;
  }
  
  private DatabaseProperties _properties = getDefaultProperties();
  
  abstract protected DatabaseProperties getDefaultProperties();
  
  public String getResourceID()
  {
    return _properties.resourceID;
  }

  public ResourceConnection getConnection()
  {
    Assertion.invariant(!cleanUpDone);
    
    ResourceConnection connection =
        ResourceManager.getInstance().openConnection(getOperatorID(),
                                                     _properties.resourceID,
                                                     false,
                                                     getCommitPeriod());
    return connection;
  }

  public DatabaseConnection getDatabaseConnection()
  {
    ResourceConnection connection = getConnection();
    Assertion.invariant(connection instanceof DatabaseConnection);
    return (DatabaseConnection) connection;
  }
  
  // just here to enforce an invariant
  private boolean cleanUpDone = false;

  @Override
  public void cleanup()
  {
    ResourceConnection connection =
        ResourceManager.getInstance().getConnection(getOperatorID());
    if(connection == null)
    {
      return;
    }

    cleanUpDone = true;
  }
  
  public void setProperties(DatabaseProperties properties)
  {
    _properties = properties;
  }
  
  public DatabaseProperties getProperties()
  {
    return _properties;
  }
  
}
