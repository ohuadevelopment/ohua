/*
 * Copyright (c) Sebastian Ertel 2008. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.io;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.operators.categories.InputIOOperator;

/**
 * The implementation of this operator is not able to read continuous changes from a database.
 * The design of such an operator is still TBD with respect to its checkpoint/restart
 * capabilities.<br>
 * idea: There needs to be an investigation of the data that needs to be read continuously from
 * a database. I assume that this data is not ran and is ordered by some timestamp or similar.
 * For real-time flows it might even be acceptable to lose some data values. In any case our
 * checkpoint must include the timestamp (given that there is no duplicate timestamp in the
 * system).
 * @author sertel
 * 
 */
public class DatabaseReaderOperator extends AbstractDatabaseOperator implements InputIOOperator {
  public static enum SortOrder {
    ASC,
    DESC
  }
  
  public static class DatabaseReaderProperties extends DatabaseProperties implements Serializable {
    public Map<String, SortOrder> orderBy = null;
  }
  
  private Connection _cnn = null;
  private PreparedStatement _selectStmt = null;
  private ResultSet _rs = null;
  
  private int _rowsRead = 0;
  
  private OutputPortControl _outPortControl = null;
  
  @Override
  public void prepare() {
    _cnn = getDatabaseConnection();
    
    String selectClause = "select * from " + getProperties().tableName;
    
    if(((DatabaseReaderProperties) getProperties()).orderBy != null
       && !((DatabaseReaderProperties) getProperties()).orderBy.isEmpty())
    {
      StringBuilder orderByClause = new StringBuilder(" order by ");
      for(Map.Entry<String, SortOrder> col : ((DatabaseReaderProperties) getProperties()).orderBy.entrySet()) {
        orderByClause.append(col.getKey() + ", ");
        orderByClause.append(" " + col.getValue().toString().toLowerCase());
        orderByClause.append(", ");
      }
      
      orderByClause.delete(orderByClause.length() - 2, orderByClause.length());
      
      selectClause += orderByClause.toString();
    }
    
    try {
      // this assumes that the underlying data will not change therefore can not be used for
      // real-time applications where the underlying data in the database constantly changes.
      _selectStmt =
          _cnn.prepareStatement(selectClause, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    }
    catch(SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine() {
    fetchData();
  }
  
  private void fetchData() {
    try {
      if(_rs == null) {
        _rs = _selectStmt.executeQuery();
        _rs.absolute(_rowsRead);
        _rs.setFetchDirection(ResultSet.FETCH_FORWARD);
      }
      int colCount = _rs.getMetaData().getColumnCount();
      
      while(_rs.next()) {
        _rowsRead++;
        _outPortControl.newPacket();
        for(int i = 1; i < colCount + 1; i++) {
          _outPortControl.setData(_rs.getMetaData().getColumnName(i), _rs.getString(i));
        }
        
        if(_outPortControl.send()) {
          return;
        }
      }
    }
    catch(SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
    
    return;
  }
  
  @Override
  public void cleanup() {
    try {
      _rs.close();
      _selectStmt.close();
    }
    catch(SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new RuntimeException("unhandled exception", e);
    }
  }
  
  /**
   * Checkpointing for this operator is not as trivial as it might seem. This checkpoint needs
   * to be in counter of the next row to be read from the data set. The assumption is of course
   * that the underlying data set does not change and that the set is ordered.
   * <p>
   * Maybe this just wants to become another form of a destructive read. In an online
   * environment for instance we might want to be able to read from a data set that is changing.
   */
  // In order to make progress we, for now, assume that the data in the database does not change
  // and the result set retrieved will be the same until test cases proof us wrong.
  public Object getState() {
    return _rowsRead;
  }
  
  public void setState(Object state) {
    _rowsRead = (Integer) state;
    prepare();
  }
  
  @Override
  protected DatabaseProperties getDefaultProperties() {
    return new DatabaseProperties();
  }
  
  public int getCommitPeriod() {
    // TODO
    return -1;
  }
  
  public boolean strictCommitRequirements() {
    // TODO Auto-generated method stub
    throw new UnsupportedOperationException("DatabaseReaderOperator.strictCommitRequirements(...) not yet implemented");
  }
  
  public boolean isUnboundedInput() {
    return false;
  }
  
}
