/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.calc;

import java.util.LinkedList;
import java.util.List;

import com.ohua.engine.exceptions.Assertion;

public class WeightedMovingAverageOperator extends MovingAverageOperator {
  public static class WeightedMovingAverageProperties extends MovingAverageProperties {
    public String weightInput = null;
  }
  
  private LinkedList<Object> _weight = new LinkedList<Object>();
  
  @Override
  protected Object getValue() {
    List<Object> data = _inPortControl.getData(((WeightedMovingAverageProperties) _properties).weightInput);
    Assertion.invariant(data.size() == 1);
    _weight.addFirst(data.get(0));
    
    return super.getValue();
  }
  
  @Override
  protected void removeValue() {
    super.removeValue();
    
    _weight.removeLast();
  }
  
  @Override
  protected Object computeMovingAverage() {
    Assertion.invariant(_weight.size() == _values.size() && !_values.isEmpty());
    
    Object sum = 0;
    Object denominator = 0;
    for(int index = 0; index < _values.size(); index++) {
      Object value = _values.get(index);
      Object weight = _weight.get(index);
      sum = getDataLayer().getDataUtils().add(sum, getDataLayer().getDataUtils().multiply(value, weight));
      denominator = getDataLayer().getDataUtils().add(denominator, weight);
    }
    Object mvAvg = getDataLayer().getDataUtils().divide(sum, denominator);
    return mvAvg;
  }
  
  @Override
  public Object getState() {
    return new Object[] { super.getState(),
                         _weight };
  }
  
  @Override
  @SuppressWarnings("unchecked")
  public void setState(Object state) {
    super.setState(((Object[]) state)[0]);
    _weight = (LinkedList<Object>) ((Object[]) state)[1];
  }
  
}
