/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.calc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ohua.engine.data.model.daapi.DataUtils;
import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

// TODO: this guy should probably us the (Ohua) packet heap!
public class AccumulatorOperator extends UserOperator {
  public static class AccumulatorProperties implements Serializable {
    public String key = null;
    public ArrayList<String> accumulates = new ArrayList<String>();
  }
  
  public AccumulatorProperties _properties = null;
  
  private InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;
  
  class StateComparator implements Comparator<State> {
    public int compare(State o1, State o2) {
      return getDataLayer().getDataUtils().compare(o1._key, o2._key);
    }
  }
  
  public static class Accumulate implements Serializable {
    public Object _accumulate = null;
  }
  
  /*
   * state
   */
  public static class State implements Serializable {
    Object _key = null;
    Map<String, Accumulate> _totals = new HashMap<String, Accumulate>();
    int _count = 0;
    
    void initialize(Collection<String> accumulates) {
      for(String accumulate : accumulates) {
        _totals.put(accumulate, new Accumulate());
      }
    }
    
    void add(InputPortControl control, DataUtils utils) {
      for(Map.Entry<String, Accumulate> entry : _totals.entrySet()) {
        List<Object> value = control.getData(entry.getKey());
        Assertion.invariant(value.size() == 1);
        entry.getValue()._accumulate = utils.add(entry.getValue()._accumulate, value.get(0));
      }
      _count++;
    }
  }
  
  private Map<Object, State> _accumulation = new HashMap<Object, State>();
  
  private List<State> _orderedOutput = null;
  
  @Override
  public void cleanup() {
    // nothing yet
  }
  
  @Override
  public void prepare() {
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine() {
    // phase 1: accumulate
    while(_inPortControl.next()) {
      List<Object> key = _inPortControl.getData(_properties.key);
      Assertion.invariant(key.size() == 1);
      if(!_accumulation.containsKey(key)) {
        State state = new State();
        state._key = key.get(0);
        state.initialize(_properties.accumulates);
        _accumulation.put(key, state);
      }
      _accumulation.get(key).add(_inPortControl, getDataLayer().getDataUtils());
    }
    
    if(!_inPortControl.hasSeenLastPacket()) {
      return;
    }
    
    // phase 2: produce ordered output
    prepareSortedOutput();
    
    boolean backoff = false;
    while(!_orderedOutput.isEmpty() && !backoff) {
      State state = _orderedOutput.remove(0);
      _outPortControl.newPacket();
      _outPortControl.setData(_properties.key, state._key);
      for(Map.Entry<String, Accumulate> accumulate : state._totals.entrySet()) {
        _outPortControl.setData(accumulate.getKey() + "-sum", accumulate.getValue()._accumulate);
      }
      _outPortControl.setData("count", state._count);
      backoff = _outPortControl.send();
    }
  }
  
  private void prepareSortedOutput() {
    if(_accumulation == null) {
      Assertion.invariant(_orderedOutput != null);
      return;
    }
    
    _orderedOutput = new ArrayList<State>(_accumulation.values());
    Collections.sort(_orderedOutput, new StateComparator());
    _accumulation = null;
  }
  
  public Object getState() {
    // FIXME this only works because this operator has just a single input. normally a deep copy
    // would be necessary.
    return new Object[] { _accumulation,
                         _orderedOutput };
  }
  
  @SuppressWarnings("unchecked")
  public void setState(Object state) {
    prepare();
    _accumulation = (Map<Object, State>) ((Object[]) state)[0];
    _orderedOutput = (List<State>) ((Object[]) state)[1];
  }
  
}
