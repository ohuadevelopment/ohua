/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.calc;

import java.util.LinkedList;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class MovingAverageOperator extends UserOperator
{
  public static class MovingAverageProperties
  {
    public int timeWindow = 10;
    public String inputField = null;
    public String outputField = null;
  }
  
  protected LinkedList<Object> _values = null;

  public MovingAverageProperties _properties = null;
  
  protected InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;

  @Override
  public void cleanup()
  {
    // nothing to clean up
  }
  
  @Override
  public void prepare()
  {
    _values = new LinkedList<Object>();
    
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inPortControl.next())
    {
      Object inputValue = getValue();
      
      _values.addFirst(inputValue);

      if(_values.size() == _properties.timeWindow + 1)
      {
        removeValue();
      }
      
      Assertion.invariant(_values.size() < _properties.timeWindow + 1);

      Object mvAvg = computeMovingAverage();
      getDataLayer().transferInputToOutput(_inPortControl.getPortName(),
                                           _outPortControl.getPortName());
      _outPortControl.setData(_properties.outputField, mvAvg);

      if(_outPortControl.send())
      {
        break;
      }
    }
  }

  protected void removeValue()
  {
    _values.removeLast();
  }

  protected Object computeMovingAverage()
  {
    Object sum = 0;
    for(Object value : _values)
    {
      sum = getDataLayer().getDataUtils().add(sum, value);
    }
    Object mvAvg = getDataLayer().getDataUtils().divide(sum, _values.size());
    return mvAvg;
  }

  protected Object getValue()
  {
    Object data = _inPortControl.getData(_properties.inputField).get(0);
    return data;
  }
  
  public Object getState()
  {
    return _values;
  }
  
  @SuppressWarnings("unchecked")
  public void setState(Object state)
  {
    prepare();

    _values = (LinkedList<Object>) state;
  }

}
