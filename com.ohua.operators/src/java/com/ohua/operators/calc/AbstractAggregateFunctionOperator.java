/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.calc;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public abstract class AbstractAggregateFunctionOperator extends UserOperator
{
  public static class AggregateProperties
  {
    public String path = null;
  }

  protected InputPortControl _inControl = null;
  protected OutputPortControl _outControl = null;
  
  protected Object _state = null;
  
  public AggregateProperties _properties = null;
  
  private boolean _sentResult = false;

  @Override
  public void prepare()
  {
    _inControl = getDataLayer().getInputPortController("input");
    _outControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inControl.next())
    {
      Object current = _inControl.getData(_properties.path).get(0);
      if(_state == null)
      {
        _state = current;
      }
      else
      {
        int compare = getDataLayer().getDataUtils().compare(current, _state);
        if(replaceState(compare))
        {
          _state = current;
        }
      }
    }
    
    if(_inControl.hasSeenLastPacket() && !_sentResult)
    {
      sendResult();
      _sentResult = true;
    }

  }
  
  abstract protected boolean replaceState(int compareResult);

  private void sendResult()
  {
    _outControl.newPacket();
    _outControl.setData("agg-result", _state);
    _outControl.send();
  }

  @Override
  public void cleanup()
  {
    // no resources to clean up
  }
  
  public Object getState()
  {
    return _state;
  }
  
  public void setState(Object state)
  {
    _state = state;
  }

}
