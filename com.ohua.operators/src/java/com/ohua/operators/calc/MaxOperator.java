/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.calc;



public class MaxOperator extends AbstractAggregateFunctionOperator
{
  @Override
  protected boolean replaceState(int compare)
  {
    return compare > 0;
  }
}
