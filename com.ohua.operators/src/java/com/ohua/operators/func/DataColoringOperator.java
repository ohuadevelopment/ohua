/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.func;

import java.util.HashMap;
import java.util.Map;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class DataColoringOperator extends UserOperator
{
  public static class DataColoringProperties
  {
    public Map<String, String> color = new HashMap<String, String>();
  }
  
  public DataColoringProperties _properties = new DataColoringProperties();
  
  private InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;
    
  @Override
  public void prepare()
  {
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    while(_inPortControl.next())
    {
      getDataLayer().transferInputToOutput(_inPortControl.getPortName(),
                                           _outPortControl.getPortName());
      for(Map.Entry<String, String> entry : _properties.color.entrySet())
      {
        _outPortControl.setData(entry.getKey(), entry.getValue());
      }

      if(_outPortControl.send())
      {
        break;
      }
    }
  }
  
  /**
   * This is a stateless operator.
   */
  public Object getState()
  {
    return null;
  }
  
  public void setState(Object checkpoint)
  {
    prepare();
    // this is a stateless operator
  }
  
  @Override
  public void cleanup()
  {
    // nothing to clean up
  }

}
