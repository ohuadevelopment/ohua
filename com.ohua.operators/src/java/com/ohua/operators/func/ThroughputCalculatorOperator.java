/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.func;

import java.io.Serializable;
import java.util.List;

import com.ohua.engine.data.model.daapi.InputPortControl;
import com.ohua.engine.data.model.daapi.OutputPortControl;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;

public class ThroughputCalculatorOperator extends UserOperator
{
  public static class ThroughputCalculatorProperties implements Serializable
  {
    public String inputField = null;
    public int window = 1;
  }
  
  public ThroughputCalculatorProperties _properties = null;

  private InputPortControl _inPortControl = null;
  private OutputPortControl _outPortControl = null;
  
  /*
   * State
   */
  private int _count = 0;
  private long _startTimeFrame = 0;
  private long _pendingOutput = 0;
  

  @Override
  public void prepare()
  {
    _inPortControl = getDataLayer().getInputPortController("input");
    _outPortControl = getDataLayer().getOutputPortController("output");
  }
  
  @Override
  public void runProcessRoutine()
  {
    if(produceOutput(0))
    {
      return;
    }
    
    while(_inPortControl.next())
    {
      List<Object> data = _inPortControl.getData(_properties.inputField);
      Assertion.invariant(data.size() == 1);
      Object time = data.get(0);
      long aTime = Long.parseLong(time.toString());
      long timePassed = aTime - _startTimeFrame;
      if(_startTimeFrame == 0)
      {
        _startTimeFrame= aTime;
        _count++;
      }
      else if(timePassed >= _properties.window)
      {
        _pendingOutput = timePassed / _properties.window;
        long restTime = timePassed % _properties.window;
        _startTimeFrame = aTime - restTime;
        int pending = _count;
        _count = 0;
        if(restTime == 0)
        {
          pending++;
        }
        else
        {
          _count = 1;
        }

        if(produceOutput(pending))
        {
          break;
        }

      }
      else
      {
        _count++;
      }
    }
  }
  
  private boolean produceOutput(int count)
  {
    long todo = _pendingOutput;
    int firstCount = count;
    for(int i = 0; i < todo; i++)
    {
      boolean backoff =
          produceOutput(_startTimeFrame - (_properties.window * ((todo - 1) - i)), firstCount);
      _pendingOutput--;
      firstCount = 0;
      if(backoff)
      {
        return true;
      }
    }
    return false;
  }

  private boolean produceOutput(Object time, long count)
  {
    _outPortControl.newPacket();
    _outPortControl.setData("time", time);
    _outPortControl.setData("count", count);
    return _outPortControl.send();
  }
  
  @Override
  public void cleanup()
  {
    // no resources attached
  }

  public Object getState()
  {
    // TODO Auto-generated method stub
    return null;
  }
  
  public void setState(Object checkpoint)
  {
    // TODO Auto-generated method stub
    
  }
  
}
