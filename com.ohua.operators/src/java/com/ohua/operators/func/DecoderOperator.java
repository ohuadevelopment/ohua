/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.func;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ohua.engine.exceptions.Assertion;

public class DecoderOperator extends NormalizerOperator
{
  public static class DecoderProperties
  {
    public String decodingFile = null;
    public ArrayList<String> paths = null;
  }
  
  public DecoderProperties _decodings = null;

  @Override
  public void prepare()
  {
    super.prepare();
    
    try
    {
      loadDecodings();
    }
    catch(IOException e)
    {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
  
  private void loadDecodings() throws IOException
  {
    Pattern pattern = Pattern.compile("(.*)=(.*)");
    
    _properties = new NormalizerProperties();
    _properties.normalization = new HashMap<String, List<Normalization>>();

    FileInputStream in = new FileInputStream(_decodings.decodingFile);
    byte[] readBuffer = new byte[100];
    int read = 0;
    String rest = null;
    while((read = in.read(readBuffer)) != -1)
    {
      String value = new String(readBuffer, 0, read, "UTF-8");
      String[] decodingMappings = value.split("\\n");
      if(rest != null)
      {
        Assertion.invariant(decodingMappings.length > 0);
        decodingMappings[0] = rest + decodingMappings[0];
      }
      
      int boundary = 0;
      if(read < 100)
      {
        boundary = decodingMappings.length;
      }
      else
      {
        boundary = decodingMappings.length - 1;
        rest = decodingMappings[decodingMappings.length - 1];
      }
      
      for(int i = 0; i < boundary; i++)
      {
        String decoding = decodingMappings[i];
        Matcher matcher = pattern.matcher(decoding);
        if(!matcher.find())
        {
          in.close();
          throw new RuntimeException("Invalid deocoding statement found: " + decoding);
        }
        
        for(String path : _decodings.paths)
        {
          Normalization n = new Normalization();
          String sourceEncoding = matcher.group(1);
          sourceEncoding = sourceEncoding.replaceAll("\\+", "\\\\+");
          sourceEncoding = sourceEncoding.replaceAll("\\*", "\\\\*");
          sourceEncoding = sourceEncoding.replaceAll("\\?", "\\\\?");
          n.sourceFormat = "(.*)" + sourceEncoding + "(.*)";
          n.targetFormat = "$1" + matcher.group(2) + "$2";
          if(!_properties.normalization.containsKey(path))
          {
            _properties.normalization.put(path, new ArrayList<Normalization>());
          }
          _properties.normalization.get(path).add(n);
        }

        if(matcher.find())
        {
          in.close();
          throw new RuntimeException("Ambigiuous deocoding statement found: " + decoding);
        }
      }
    }
    in.close();
  }
}
