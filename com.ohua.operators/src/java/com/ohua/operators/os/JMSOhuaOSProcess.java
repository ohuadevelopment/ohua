/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.operators.os;

import java.io.IOException;

import com.ohua.engine.OhuaProcessRunner;
import com.ohua.engine.ProcessNature;
import com.ohua.engine.SimpleProcessListener;
import com.ohua.engine.UserRequest;
import com.ohua.engine.UserRequestType;
import com.ohua.engine.os.AbstractOhuaOSProcess;
import com.ohua.engine.os.CommandLineParser;

public class JMSOhuaOSProcess extends AbstractOhuaOSProcess {
  protected ProcessNature _processType = ProcessNature.SOURCE_DRIVEN;
  
  private OhuaProcessRunner _runner = null;
  
  public static void main(String[] args) throws Throwable {
    AbstractOhuaOSProcess process = new JMSOhuaOSProcess();
    process.execute(args);
  }
  
  @Override
  protected CommandLineParser createCommandLineParser() {
    return new JMSCommandLineParser();
  }
  
  public SimpleProcessListener load() throws IOException, ClassNotFoundException {
    // start
    _runner = createNewProcessRunner();
    // OhuaProcessRunner runner = new OhuaProcessRunner(_pathToFlow);
    _runner.loadRuntimeConfiguration(_pathToRuntimeConfiguration);
    SimpleProcessListener listener = new SimpleProcessListener();
    _runner.register(listener);
    new Thread(_runner, "jms-reader-process").start();
    return listener;
  }
  
  @Override
  protected void run() throws IOException, ClassNotFoundException {
    SimpleProcessListener listener = load();
    
    try {
      // initialize
      _runner.submitUserRequest(new UserRequest(UserRequestType.INITIALIZE));
      listener.awaitProcessingCompleted();
      
      // start computation
      listener.reset();
      _runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
      
      // how is computation supposed to finish?
      switch(_processType) {
        case SOURCE_DRIVEN:
          System.out.println("Waiting for source driven computation to finish.");
          listener.awaitProcessingCompleted();
          break;
        case USER_DRIVEN:
          System.out.println(this + " Waiting for user driven computation to finish." + System.currentTimeMillis());
          waitForComputationFinish();
          
          System.out.println("Requesting process to finish computation." + System.currentTimeMillis());
          // finish computation
          listener.reset();
          _runner.submitUserRequest(new UserRequest(UserRequestType.FINISH_COMPUTATION));
          System.out.println("Waiting for process to finish computation.");
          listener.awaitProcessingCompleted();
          break;
      }
      System.out.println("Computation finished.");
      
      // shutdown
      listener.reset();
      _runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
      listener.awaitProcessingCompleted();
    }
    catch(Throwable e) {
      throw new RuntimeException(e);
    }
  }
  
  protected void waitForComputationFinish() {
    // hook
  }
  
  protected OhuaProcessRunner createNewProcessRunner() {
    return new OhuaProcessRunner(getFlowLoader());
  }
  
  public void submit(UserRequestType req) {
    _runner.submitUserRequest(new UserRequest(req));
  }
}
