/*
 * Copyright (c) Sebastian Ertel 2010. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.os;

import java.io.IOException;

import com.ohua.engine.os.AbstractOhuaOSProcess;
import com.ohua.operators.io.resources.DerbyServerUtils;

public class DBOhuaOSProcess extends AbstractOhuaOSProcess
{
  public static void main(String[] args) throws Throwable
  {
    AbstractOhuaOSProcess process = new DBOhuaOSProcess();
    process.execute(args);
  }

  @Override
  protected void initialize() throws Exception
  {
    super.initialize();
    
    DerbyServerUtils.startServer(new String[] {});
  }

  @Override
  public void tearDown() throws IOException
  {
    DerbyServerUtils.shutdown();
    
    super.tearDown();
  }

}
