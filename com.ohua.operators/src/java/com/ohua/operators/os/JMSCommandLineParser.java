/*
 * Copyright (c) Sebastian Ertel 2011. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.operators.os;

import com.ohua.engine.ProcessNature;
import com.ohua.engine.os.AbstractOhuaOSProcess;
import com.ohua.engine.os.CommandLineParser;

public class JMSCommandLineParser extends CommandLineParser
{
  public ProcessNature _processType = ProcessNature.SOURCE_DRIVEN;

  @Override
  public void parseComandLineArgs(String[] args)
  {
    super.parseComandLineArgs(args);
    
    if(args.length > 3)
    {
      _processType = ProcessNature.valueOf(args[3]);
    }
  }
  
  @Override
  public void apply(AbstractOhuaOSProcess process)
  {
    super.apply(process);
    
    ((JMSOhuaOSProcess) process)._processType = _processType;
  }

}
