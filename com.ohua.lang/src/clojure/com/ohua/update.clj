;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.update
  (:require [clojure.pprint]))

;;;
;;; This namespace deals with dynamic updates to an executing Ohua flow.
;;;


(defn update 
  "Takes a vector of (old new transformer) where the latter is optional.
   The first two arguments are (namespace-based) references to Ohua functions/operators."
  [updates]
  (let [target-runtime (.getRuntime (com.ohua.lang.OhuaRuntimeRegistry/get ) "single-runtime")]
    ; TODO massage the updates to fit the formal types of the interface
    (com.ohua.lang.OhuaUpdates/update updates target-runtime)))