;;;
;;; Copyright (c) Justus Adam and Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.context
  (:require [com.ohua.ir :as ir]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [clojure.set :as set]
            [clojure.pprint :refer [pprint]]
            [com.ohua.ir :as df-ir])
  (:use com.ohua.util.assert)
  (:import (com.ohua.ir IRFunc)
           (clojure.lang PersistentArrayMap)))

;;;
;;; AST part
;;;

(defrecord AST-Ctxt [out-vars ast-ctxt-ref])

(def control-ctxts '(if com.ohua.lang/smap-fun com.ohua.lang/smap-io-fun com.ohua.lang/seq seq))

(def context-info
  {; the context for a conditional is the condition!
   'if                        (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc] (t-zip/move ctxt-loc 2 zip/right))
                                            :out-var 0}
                                           {:sub-tree-detect
                                                     (fn [ctxt-loc] (t-zip/move ctxt-loc 3 zip/right))
                                            :out-var 1}]
                                          :switch)
   ;(let [[ ] (smap-fun )]
   ;  (collect (size ) code))
   'com.ohua.lang/smap-fun    (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc] (-> ctxt-loc zip/up zip/up zip/right zip/down zip/rightmost))
                                            :out-var 0}]
                                          :op-id)
   ;(let [[ ] (smap-io-fun )]
   ;  (collect (size ) code))
   'com.ohua.lang/smap-io-fun (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc] (-> ctxt-loc zip/up zip/up zip/right zip/down zip/rightmost))
                                            :out-var 0}]
                                          :op-id)
   'com.ohua.lang/seq         (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc] (t-zip/move ctxt-loc 2 zip/right))
                                            :out-var 0}]
                                          :op-id)
   ; FIXME copied from above see issue #226
   'seq                       (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc] (t-zip/move ctxt-loc 2 zip/right))
                                            :out-var 0}]
                                          :op-id)

   ; (let [[ ] (algo-in )]
   ;   (algo-out code)
   'com.ohua.lang/algo-in     (->AST-Ctxt [{:sub-tree-detect
                                                     (fn [ctxt-loc]
                                                       (-> ctxt-loc zip/up zip/up zip/right zip/rightmost zip/down zip/rightmost))
                                            :out-var 0}]
                                          :op-id)})

(defn ctxt-matcher [loc] (and (symbol? (zip/node loc)) (contains? context-info (zip/node loc))))

(defn ctxt-editor [loc]
  (let [ctxt-info (context-info (zip/node loc))
        ctxt-id (get (meta (zip/node (zip/up loc))) (:ast-ctxt-ref ctxt-info))
        [new-loc new-c-map]
        (reduce
          (fn [[ctxt-loc c-map] {sub-tree-detection :sub-tree-detect out-var :out-var}]
            (let [sub-tree-loc (sub-tree-detection (zip/leftmost ctxt-loc))]
              ; FIXME this is because we do not handle this case properly: (if t (some-func ) nil).
              ; at least, we need another value here but I believe it is the same problem as handling a value selection like this:
              ; (if t val1 val2)
              ; the solution to both cases is a source code rewrite like this:
              ; (if t (id val1) (id val2)) and (if t (some-func ) (id nil))
              (if (not sub-tree-loc)
                [ctxt-loc c-map]
                (let [[new-subgraph new-ctxt-map]
                      (t-zip/walk-code (t-zip/tree-zipper (zip/node sub-tree-loc))
                                       #(and (seq? (zip/node %)) (contains? (meta (zip/node %)) :op-id))
                                       (fn [loc-fn]
                                         (let [op-id (:op-id (meta (zip/node loc-fn)))
                                               ctxt-ref {:type    (zip/node loc)
                                                         :op-id   ctxt-id
                                                         :out-var out-var}]
                                           [(zip/next (zip/edit loc-fn #(with-meta % (assoc (meta %) :ctxt [ctxt-ref]))))
                                            {op-id [ctxt-ref]}])))
                      tree (zip/replace sub-tree-loc new-subgraph)]
                  ;(println "current" c-map)
                  ;(println "new" (into {} new-ctxt-map))
                  [tree (conj c-map (into {} new-ctxt-map))]))
              ))
          [loc {}]
          (:out-vars ctxt-info))]
    [(zip/next (zip/leftmost new-loc))
     new-c-map]))

(defn combine-ctxt-path [ctxt new]
  ;(println "ctxt" ctxt (vector? ctxt))
  ;(println "new ctxt" new)
  (reduce-kv
    (fn combine-single-path [ctxt-map op-id ctxt-info]
      (assoc
        ctxt-map
        op-id
        (if (contains? ctxt-map op-id)
          (into [] (concat (get ctxt-map op-id) ctxt-info))
          ctxt-info)))
    ctxt
    new))

;;;
;;; Dataflow IR part
;;;

(defn needs-explicit-context-control-
  "
  A function needs explicit context control iff it defines no input that is located in the same (or a nested)
  context as the function itself and is not connected already via a data dependency to the context beginning.
  "
  [{graph :graph ctxt-map :ctxt-map} ctxt fun]
  (let [preds (ir/predecessors fun graph)
        in-ctxt-preds (filter
                        (fn in-ctxt-path? [pred]
                          (or
                            (= (:id pred) ctxt)
                            (not
                              (empty?
                                (filter
                                  (fn in-ctxt? [ctxt-ref] (= (:op-id ctxt-ref) ctxt))
                                  (get ctxt-map (:id pred))
                                  )
                                )
                              )
                            )
                          )
                        preds)
        ;_ (println fn "in context preds" in-ctxt-preds)
        ]
    (empty? in-ctxt-preds))
  )

(defn find-out-var [fn ctxt-fn ctxt-map find-ctxt-fn]
  ;(println "finding out var ..." fn ctxt-fn ctxt-map)
  (let [ctxt-ref (find-ctxt-fn (get ctxt-map (:id fn)))
        ;_ (println (:id fn) ctxt-map (get ctxt-map (:id fn)))
        ]
    ;(println (ir/get-return-vars ctxt-fn) (:out-var ctxt-ref))
    (nth (ir/get-return-vars ctxt-fn) (:out-var ctxt-ref))
    )
  )

(def find-ctxt last)

(defn find-control-ctxt
  [ctxt-path]
  (loop [path (reverse ctxt-path)]
    (if (empty? path)
      nil
      (if (some #{(:type (first path))} control-ctxts)
        (first path)
        (recur (rest path))))))

(defn find-ctxt-functions
  "Just checks for the last entry in the context path."
  [{graph :graph ctxt-map :ctxt-map :as df-ir} ctxt-id ctxt-path-lookup]
  (let [fns (filter
              (fn detect-ctxt-fn [[_ ctxt-path]]
                (= (:op-id (ctxt-path-lookup ctxt-path))
                   ctxt-id))
              ctxt-map)
        ;_ (println "functions in ctxt" fns (keys fns))
        ir-fns (filter #(some #{(:id %)} (keys fns)) graph)
        ;_ (println "(ir) functions in ctxt" ir-fns)
        dangling-fns (filter (partial
                               needs-explicit-context-control-
                               df-ir
                               ctxt-id)
                             ir-fns)
        ;_ (println "(ir) functions with explicit need for ctxt" dangling-fns)
        ]
    dangling-fns
    ))

(defn find-explicit-control-ctxt-functions
  "Control contexts are 'seq', 'if', 'smap'."
  [df-ir ctxt-id]
  (find-ctxt-functions df-ir ctxt-id find-control-ctxt))

(defn find-explicit-ctxt-functions
  "Just checks for the last entry in the context path."
  [df-ir ctxt-id]
  (find-ctxt-functions df-ir ctxt-id find-ctxt))

(defn contextify
  [ctxt-var ctxt-function]
  ;_ (println "found context-var" context-var)
  ;_ (println "updating function:" dangling-fn)
  (assoc
    ctxt-function
    :args
    (into []
          (cons
            (vary-meta ctxt-var assoc :in-idx -1)
            (:args ctxt-function))))
  )

(defn find-node [node graph]
  (let [current-node (filter #(and (= (:id %) (:id node))) graph)]
    (assert (= (count current-node) 1))
    (first current-node)))

(defn update-current-graph-node [g [old update-fn]]
  (let [current-old-node (find-node old g)]
    (ir/replace-node current-old-node
                     (update-fn current-old-node)
                     g))
  )

(defn conditionals-ctxt-transformation
  "
  The arcs for the merge are already correctly created because the 'if' expression in the AST gets the id of the 'merge'.
  Here, we create all arcs that are needed to set the functions on the branches into the right context.
  "
  [{graph :graph ctxt-map :ctxt-map :as df-ir}]
  ;(println "ctxtmap" ctxt-map)
  ;(println "graph" graph)
  (let [; first add the two return variables to the cond-stmts
        cond-updated-graph (reduce
                             (fn [new-graph cond-stmt]
                               ; check if the select uses the output of the switch. if so then use this var and don't redefine.
                               (let [true-branch-var (if (empty? (:return cond-stmt))
                                                       (gensym "true-branch")
                                                       (first (:return cond-stmt)))]
                                 (ir/replace-node
                                   cond-stmt
                                   (assoc cond-stmt :return [true-branch-var (gensym "false-branch")]) new-graph)))
                             graph
                             ; TODO turn this into a symbol because only the 'ifThenElse' is a string.
                             (filter #(= (:name %) "ifThenElse") graph))

        cond-stmts (filter #(do                             ;(println (type (:name %) ) (:name %))
                             (= (:name %) "ifThenElse")) cond-updated-graph)
        ;_ (println "found conditionals" cond-stmts)
        updated-graph (reduce
                        (fn [new-graph cond-stmt]
                          ; find all functions in its context
                          (let [dangling-fns (find-explicit-control-ctxt-functions df-ir (:id cond-stmt))]
                            (reduce
                              (fn [g dangling-fn]
                                (let [context-var (find-out-var dangling-fn cond-stmt ctxt-map find-control-ctxt)
                                      ; retrieve it again because it might be a cond stmt which has been updated previously
                                      up-to-date-fn (find-node dangling-fn g)
                                      updated-fn-graph (ir/replace-node up-to-date-fn
                                                                        (contextify context-var up-to-date-fn)
                                                                        g)
                                      ;_ (println "altered graph:")
                                      ;_ (pprint updated-fn-graph)
                                      ]
                                  updated-fn-graph))
                              new-graph
                              dangling-fns)
                            )
                          )
                        cond-updated-graph
                        cond-stmts)
        ]
    (assoc df-ir :graph updated-graph))
  )

(defn apply-alter-map [m graph]
  (map
    (fn [node] (if-let [alter-func (m node)] (alter-func node) node))
    graph))

(defn smap-transformation-
  "
  We use a 'seq' function to convert an emitted list value into a context (boolean) value.
  "
  [{graph :graph :as df-ir} ctxt-name]
  (let [smap-stmts (filter #(= (:name %) ctxt-name) graph)
        graph-updates (remove nil?
                              (map
                                (fn handling-smap-stmt [smap-stmt]
                                  (let [dangling-fns (find-explicit-control-ctxt-functions df-ir (:id smap-stmt))]
                                    (if-not (empty? dangling-fns)
                                      (let [ctxt-var (gensym "smap-ctxt")
                                            [smap-output-var smap-update] (if (empty? (df-ir/get-return-vars smap-stmt))
                                                                            (let [out-var (gensym "smap-out")]
                                                                              [out-var [smap-stmt (fn [old] (assoc old :return out-var))]])
                                                                            [(first (df-ir/get-return-vars smap-stmt)) nil])
                                            ctxt-seq-fn (ir/mk-func 'com.ohua.lang/seq [smap-output-var] ctxt-var)
                                            ctxt-fn-updates (into {} (map
                                                                       (fn attaching-ctxt [dangling-fn]
                                                                         [dangling-fn (partial contextify ctxt-var)])
                                                                       dangling-fns))
                                            ]
                                        [ctxt-seq-fn ctxt-fn-updates smap-update]
                                        ))))
                                smap-stmts))
        ;_ (do (println "seq fns added") (pprint seq-fns-added-graph))
        g-updates-map (into {} (map second graph-updates))
        smap-updates-map (into {} (map #(nth % 2) graph-updates))

        new-gr (->> (concat graph (map first graph-updates))
                    (apply-alter-map g-updates-map)
                    (apply-alter-map smap-updates-map))
        ;_ (do (println "updates applied") (pprint updates-applied-graph))
        ]
    (assoc df-ir :graph new-gr))
  )

(defn smap-ctxt-transformation
  [df-ir]
  (smap-transformation- df-ir 'com.ohua.lang/smap-fun))

(defn smap-io-ctxt-transformation
  [df-ir]
  (smap-transformation- df-ir 'com.ohua.lang/smap-io-fun))


(defn seq-ctxt-transformation
  "
  The part to transform is '(seq a b)' which means that 'seq' has two inputs. However, the output of 'seq' needs to be
  connected to the context port of 'b'.
  "
  [{graph :graph ctxt-map :ctxt-map :as df-ir}]

  (let [seq-stmts (filter #(do
                            ;(println (type (:name %) ) (:name %) (some #{(:name %)} '(com.ohua.lang/seq seq)))
                            ; FIXME see issue #226
                            (some #{(:name %)} '(com.ohua.lang/seq seq))) graph)
        ;_ (println "found:" seq-stmts)
        ; we have to perform the transformations from outside to inside because the output of the nested seq maybe directly
        ; input to another seq! (so we need to make sure the nested rewrite is performed on the updated graph surrounding it.)
        ; therefore, we simply order by length of ctxt-path. this is sufficient to respect the context order from outside to inside.
        sorted-seq-stmts (sort-by #(count (get ctxt-map (:id %))) seq-stmts)

        ;_ (println "sorted seq stmts" sorted-seq-stmts)
        final-graph (reduce
                      (fn handling-seq [graph seq-stmt]
                        (let [ctxt-var (gensym "seq-ctxt")
                              ; update the seq function
                              updated-seq-stmt (partial
                                                 (fn update-seq-stmt [ctxt-var seq-stmt]
                                                   (assoc seq-stmt
                                                     :args [(first (:args seq-stmt))]
                                                     :return ctxt-var))
                                                 ctxt-var)
                              ;_ (println "updated seq stmt" seq-stmt)

                              ; update the functions in the context of this seq stmt
                              dangling-fns (find-explicit-control-ctxt-functions df-ir (:id seq-stmt))
                              ;_ (println "dangling functions" dangling-fns)
                              updated-ctxt-functions (reduce
                                                       (fn attaching-ctxt [l dangling-fn]
                                                         (concat l [[dangling-fn (partial contextify ctxt-var)]]))
                                                       []
                                                       dangling-fns)
                              ;_ (println "updated ctxt functions" updated-ctxt-functions)

                              ; update the function that consumes the output of seq. it needs to consume
                              ; the second arg of 'seq'.
                              seq-output (first (ir/get-return-vars seq-stmt))
                              consumers (ir/get-consumers seq-output graph)
                              updated-consumers (map
                                                  (fn prepare-consumer-update [consumer]
                                                    [consumer
                                                     (fn updating-consumer [consumer]
                                                       (let [seq-output (first (ir/get-return-vars seq-stmt))
                                                             args (filter #(= % seq-output) (:args consumer))]
                                                         (assoc consumer
                                                           :args
                                                           (replace
                                                             (into {}
                                                                   (map
                                                                     (fn [arg]
                                                                       [arg (with-meta
                                                                              (second (:args seq-stmt))
                                                                              (meta arg))])
                                                                     args))
                                                             (:args consumer))))
                                                       )
                                                     ]
                                                    )
                                                  consumers)
                              ;_ (println "updated consumers" updated-consumers)

                              ; apply the updates
                              updated-graph (reduce
                                              update-current-graph-node graph
                                              (concat
                                                [[seq-stmt updated-seq-stmt]]
                                                updated-ctxt-functions
                                                updated-consumers))]
                          updated-graph)
                        )
                      graph
                      sorted-seq-stmts)
        ]
    (assoc df-ir :graph final-graph)
    )
  )

(defn algo-ctxt-transformation
  "
  Only required when there is no input to an algorithm.
  "
  [{graph :graph ctxt-map :ctxt-map :as df-ir}]
  (let [algo-ins (filter #(= (:name %) 'com.ohua.lang/algo-in) graph)
        ; we check the returned vars because there might be global input to algo-in!
        dangling-algo-ins (filter #(empty? (ir/get-return-vars %)) algo-ins)
        updated-graph (reduce
                        (fn [g algo-in-stmt]
                          (let [context-var (gensym "algo-in-ctxt-")
                                ; retrieve it again because it might be a stmt which has been updated previously
                                up-to-date-fn (find-node algo-in-stmt g)
                                updated-fn-graph (ir/replace-node up-to-date-fn
                                                                  (assoc up-to-date-fn
                                                                    :return context-var
                                                                    :name 'com.ohua.lang/algo-in-void)
                                                                  g)
                                dangling-ctxt-fns (find-explicit-ctxt-functions
                                                    df-ir
                                                    (:id algo-in-stmt))
                                changed-graph (reduce
                                                (fn attaching-ctxt [g dangling-fn]
                                                  (ir/replace-node
                                                    dangling-fn
                                                    (contextify context-var dangling-fn)
                                                    g))
                                                updated-fn-graph
                                                dangling-ctxt-fns)

                                ]
                            changed-graph)
                          )
                        graph
                        dangling-algo-ins)
        ]
    (assoc df-ir :graph updated-graph)
    )
  )

(defn apply-ctxt-transformations
  [df-ir]
  (-> df-ir
      algo-ctxt-transformation
      conditionals-ctxt-transformation
      seq-ctxt-transformation
      smap-ctxt-transformation
      smap-io-ctxt-transformation
      ))



(deftype Ctx [ctx-name
              begin-markers
              end-markers
              verify-context-wrap
              create-stack-value])


; This function is strictly not necessary as the constructor for Ctx does literally the same thing
; however as far as I am aware in clojure you cannot attach documentation to a constructor
; and thus this function carries the documentation instead
(def ctx
  "Defines a new context where:

  ctx-name:
      is a unique name for this context

  begin-markers:
      is A list of function names which mark the beginning of a context of this type
      type: #{Symbol}

  end-markers:
      is a list of function names which mark the end of a context of this type
      type: #{Symbol}

  verify-context-wrap:
      verifies a detected context has a valid wrapping (end marker matches created stack frame)
      type: (IRFunc, Stackframe) -> Boolean

  create-stack-value:
      creates a new stack frame entry for the context opened with `function` for subsequent functions connected via `out-var`
      type: ([IRFunc], IRFunc, Symbol) -> StackFrame"
  ->Ctx)


(defprotocol AssocCtx
  (get-ctx [this]
    "Retrieve a reference to the context this structure is associated with"))


(defrecord SmapStackEntry [size-op])
(defrecord IFStackEntry [if-op arg-id])
(defrecord LabeledFunction [function label])



; TODO handle smap-fun and smap-io-fun
(def smap-ctx
  (ctx
    "smap"
    #{'com.ohua.lang/smap-fun 'com.ohua.lang/smap-fun-io}
    #{'com.ohua.lang/collect}
    (fn [_ frame] (= SmapStackEntry (type frame)))
    (fn [_ smap-fun _]
      (->SmapStackEntry
        (case (.-name smap-fun)
          com.ohua.lang/smap-fun (first (.-args smap-fun))
          com.ohua.lang/smap-fun-io (second (.-return smap-fun))
          (throw (Exception. (str "Unexpected function type" (.-name smap-fun)))))))))


(def if-ctx
  (ctx
    "ite"
    #{'com.ohua.lang/ifThenElse}
    #{'com.ohua.lang/select}
    (fn [_ frame] (= IFStackEntry (type frame)))
    (fn [_ if-op arg-id] (->IFStackEntry if-op arg-id))))


(extend-protocol AssocCtx
  IFStackEntry
  (get-ctx [_] if-ctx)
  SmapStackEntry
  (get-ctx [_] smap-ctx))



(def context-stack [if-ctx smap-ctx])


(def context-trigger-map
  (persistent!
    (reduce
      (fn [map ctx]
        (reduce
          (fn [map marker] (assoc! map marker [ctx :end]))
          (reduce
            (fn [map marker] (assoc! map marker [ctx :begin]))
            map
            (.-begin_markers ctx))
          (.-end_markers ctx)))
      (transient {})
      context-stack)))


(defn fgr-helper0 [reduction-function state head nodes]
  (reduce
    (fn [[state fns] node]
      (let [[new-state labeled-fn]
            (reduction-function
              state
              head
              node)]
        [(if (nil? new-state) state new-state)
         (if (nil? labeled-fn) fns (conj fns labeled-fn))]))
    [state []]
    nodes))


(defn forward-graph-reduce [reduction-function
                            initial-state
                            initial-head-val
                            graph]
  (let [[initial-state0 initial-fns] (->> graph
                                          (filter (comp empty? :args))
                                          (fgr-helper0 reduction-function initial-state initial-head-val))]
    (loop [[head & queue] initial-fns
           state initial-state0]
      (let [{function :function
             label    :label} head
            successors (ir/successors function graph)
            [new-state fns]
            (fgr-helper0 reduction-function state head successors)
            new-queue (concat queue fns)]
        (if (empty? new-queue)
          new-state
          (recur
            new-queue
            new-state))))))


; works under the assumption that any function will only ever open one new context and no function will simultaneously open and close a context
(defn graph-reduction-function
  [graph
   ^PersistentArrayMap fn-map
   {function    :function
    label-stack :label
    :as         head}
   node]
  (let [^LabeledFunction saved (get fn-map node)]
    (cond
      (nil? head) (let [labeled-function (->LabeledFunction node [])]
                    [(assoc fn-map node labeled-function) labeled-function])
      (or (nil? saved) (<= (count (.-label saved)) (count label-stack)))
      (let [[ctx bracket-type] (get context-trigger-map (:name function))
            new-stack (case bracket-type
                        :begin (let [creator-fn (.-create_stack_value ctx)
                                     arg-id (first (set/intersection (set (.-return function)) (set (.-args node))))
                                     data (creator-fn graph node arg-id)]
                                 (conj label-stack data))
                        :end (pop label-stack)
                        nil label-stack)
            labeled-fn (->LabeledFunction node new-stack)]
        (if (and (= :end bracket-type) (not ((.-verify_context_wrap ctx) function (last label-stack))))
          nil
          [(assoc fn-map node labeled-fn) labeled-fn])))))


(defn label-graph [graph]
  (forward-graph-reduce
    (partial graph-reduction-function graph)
    {}
    nil
    graph))
