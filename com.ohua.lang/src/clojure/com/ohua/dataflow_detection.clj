;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.dataflow-detection
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [clojure.reflect :as reflect]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l])
  (:import [clojure.lang IPersistentVector ISeq Cons Symbol LazySeq]))

;;;
;;; Second pass over the code: find the data dependencies. The third pass will find all operators and 
;;; take the operator ids of all the arguments of an operator, no matter whether it is a function or a
;;; variable. Therefore, this pass must annotate all variables with the id of the operator that they
;;; were derived from.
;;;
;;; Used annotations:
;;; :skip - excludes this node from consideration as a source
;;; :is-op? - everything that is required to receive its own operator at execution
;;;


(defn find-target
  "This is supporting parts of the AST that are Clojure code instead of Ohua operators. It finds the target that will run
  the Clojure code and therefore must also receive the arguments."
  [loc]
  (if (or (contains? (meta (zip/node loc)) :op-id)
          (contains? (meta (zip/node loc)) :switch)
          )
    loc
    (find-target (zip/up loc))))

(defn find-in-idx [loc]
  (if (contains? (meta (zip/node loc)) :in-idx)
    ; if the node already knows its in-idx then use it (switch op case!)
    (:in-idx (meta (zip/node loc)))
    ; else use the index given in the program
    (dec (count
           (filter #(not (:skip (meta %)))
                   (zip/lefts loc)
                   )
           )))
  )

;;;
;;; We have found a node (leaf) that matches a specific variable that we try to understand who uses it as its input.
;;; Hence, we alter the parent node and add meta-data to it that describes its position in the input of this operator/function.
;;; Further it assigns the id of the source operator of the searched variable (together with its out-idx) to it.
;;;
(defn detect-dependency [source-id binding-vector? index loc]
  (if (or (and binding-vector? (odd? index) (not (zip/up loc))) ; assigned in the same vector where it was defined
          (and (zip/up loc) (-> loc zip/up zip/node vector?) (odd? (count (zip/lefts loc))))) ; assigned in nested binding vector
    ; propagate the variable information
    (do
      ;(println "propagating variable info for" (zip/node loc))
      [(zip/next (zip/edit loc #(with-meta %
                                           ; do an overwrite in order to support variable redefinitions
                                           (conj (if (meta %) (meta %) {})
                                                 {:op-id   (:op-id source-id)
                                                  :out-idx (:out-idx source-id)}))))
       nil]
      )
    (if (and (zip/up loc)
             (not (vector? (zip/node (zip/up loc)))))
      ; target found: add input information
      (let [op-meta (meta (zip/node (find-target (zip/up loc))))
            ;_ (println "op-meta:" op-meta)
            in-idx (find-in-idx loc)]
        [(zip/next (zip/edit loc #(with-meta %
                                             ; do an overwrite in order to support variable redefinitions
                                             (conj (if (meta %) (meta %) {})
                                                   {:op-id   (:op-id source-id)
                                                    :out-idx (:out-idx source-id)
                                                    :target  {:op-id (:op-id op-meta) :in-idx in-idx}}))))
         {:op-id (:op-id op-meta) :in-idx in-idx}]
        )
      [(zip/next loc) nil]                                  ; redefinition of the local
      )
    )
  )

(defn detect-dependency-matcher [var-binding loc]
  (and loc
       (not (zip/branch? loc))
       (= (zip/node loc) var-binding)
       ; don't visit nodes to be skipped
       (if (contains? (meta (zip/node loc)) :skip) (not (:skip (meta (zip/node loc)))) true)
       ))

;;;
;;; Here we walk all siblings to find nodes that use 'var-binding' as their input. Once we found one, we attach 
;;; the op-id of the source to the found parameter. Later on we can just walk the tree, check the inputs of the functions 
;;; and list the dependencies. 
;;;
(defn find-dependencies [loc var-binding source-id]
  ;(println "finding dependencies of" var-binding "in" (zip/rights loc) "starting at" (zip/node loc))
  ;(t-zip/print-tree loc)
  ; walk the children of this sequence one by one and then insert them back into the current tree
  ; (we need to edit each subgraph separately because we are in the middle of a 'walk')
  (let [new-siblings (map-indexed
                       (fn [index l] (t-zip/walk-code (t-zip/tree-zipper l)
                                                      (partial
                                                        detect-dependency-matcher
                                                        var-binding)
                                                      (partial detect-dependency
                                                               source-id
                                                               (vector? loc)
                                                               index)))
                       (zip/rights loc))]
    ;(println ">>>>> new siblings:" new-siblings)
    (assert (= (count (zip/rights loc)) (count new-siblings)))
    (let [new-tree (loop [sibling loc
                          [new-sibling result] (first new-siblings)
                          remaining (rest new-siblings)]
                     (if-not (zip/right sibling)
                       sibling
                       (let [new-loc (zip/replace (zip/right sibling) new-sibling)]
                         ;(println "replacing:" (zip/node new-loc) (meta (zip/node new-loc)))
                         (recur new-loc (first remaining) (rest remaining)))))]
      ; Finally we have to return to the position that we started from.
      (let [old-loc (t-zip/move new-tree (count new-siblings) zip/left)]
        ;(println ">>>>> altered siblings:" (zip/rights old-loc))
        [(filter #(not (empty? %)) (mapcat #(let [[_ r] %] r) new-siblings)) old-loc]
        ))
    )
  )

;;;
;;; This looks up the tree to find the succeeding op. 
;;;
(defmulti find-targets (fn [_ loc] (class (zip/node (zip/up loc)))))
(defmethod find-targets
  ; (No support for docstring in defmethod yet.)
  ; "Case: inside let binding such as (let [r (op arg)] ...).
  ; Here we have to make sure that we can handle the destructuring case to support multiple outgoing arcs such as (let [[r s] (op arg)] ...)."
  IPersistentVector [source-id loc]
  ;(println "handle let:" (zip/node loc) "binding:" (zip/node (zip/left loc)))
  (let [var-binding (zip/node (zip/left loc))]
    ; We have to take a look at two things:
    ; 1) The other bindings to the right of this binding.
    ; 2) The let body.
    ;(println "Checking var binding:" var-binding)
    (let [[result-binding loc-binding] (find-dependencies loc var-binding source-id)
          idx (count (zip/lefts loc))
          [result-body loc-body] (find-dependencies (zip/up loc-binding) var-binding source-id)]
      [(vec (concat result-binding result-body))
       (t-zip/move (zip/down loc-body) idx zip/right)])))
(defmethod find-targets
  ; (No support for docstring in defmethod yet.)
  ; "Case: nesting such as (op-2 (op-1 arg))"
  ISeq [source loc]
  ;(println "current:" (zip/node loc))
  ;(println "current (meta):" (meta (zip/node loc)))
  ;(println "parent sequence:" (zip/node (zip/up loc)))
  ;(println "target:" (meta (zip/node (zip/up loc))))
  ;(println (get (meta (zip/node (zip/up loc))) :op-id))
  ;(println (:op-id (meta (zip/node loc))))
  (let [target-op-id ;(get (meta (zip/node (zip/up loc))) :op-id)
        (:op-id (meta (zip/node (find-target (zip/up loc)))))]
    (if (and target-op-id
             (seq? (zip/node loc)) ; collect only direct deps (no var deps!)
             (not (= target-op-id (:op-id (meta (zip/node loc))))) ; propgation through lets: (let [] (let [] )). deleted connection below!
             )
      ; inputs to none-ops must be compounded (-> this is the support for conditions)
      (let [_ (if (not (:is-op? (meta (zip/node (zip/up loc)))))
                (assert (contains? (meta (zip/node loc)) :in-idx) (str "A stateful function that is input to Clojure code must have :in-idx pre-defined: "
                                                                       (zip/node loc) " "
                                                                       (zip/node (zip/up loc)) " "
                                                                       (meta (zip/node loc)))))
            in-idx (find-in-idx loc)]
        ;(println "target op:" target-op-id "in-idx:" in-idx)
        [[{:op-id target-op-id, :in-idx in-idx}]
         (zip/edit loc #(with-meta % (conj {:target {:op-id target-op-id :in-idx in-idx}} (meta %))))])
      [])
    )
  )

(defn branch-matcher [loc]
  ;(println "path:" (clojure.pprint/pprint (zip/path loc)))
  ;(println "matcher -> node" (zip/node loc) (meta (zip/node loc)))
  ; only match when it is a function!
  (and loc
       ; don't visit nodes to be skipped
       (if (contains? (meta (zip/node loc)) :skip) (not (:skip (meta (zip/node loc)))) true)
       ; make sure the sequence is an operator, not a function argument that we need to evaluate.
       (contains? (meta (zip/node loc)) :op-id)
       ))

(defn not-is-dest-vec
  "
   exclude the vector variables from destructuring:
   'Current node: vec__2581 {:op-id 100, :out-idx -1, :target {:op-id 100, :in-idx 0}}'
  "
  ([loc] (not-is-dest-vec (-> loc zip/node meta) (-> loc zip/node meta :target)))
  ([source target]
   (not (= (:op-id source) (:op-id target)))
    ))

(defn arc-editor [func-seq-loc]
  ; At this point we found a leaf. Now we have to walk up the path in order to understand our dependencies.
  ;(println "arc-editor > leaf" (zip/node func-seq-loc))
  ;(println "arc-discovery > operator:" (zip/node func-seq-loc) (meta (zip/node func-seq-loc)))
  (let [source (select-keys (meta (zip/node func-seq-loc)) [:op-id :out-idx])]
    ; Is this the root sequence? If so then just skip it.
    (if (zip/up func-seq-loc)
      (let [[target new-loc] (find-targets source func-seq-loc)]
        ;          (println "source:" source "target:" target)
        ;          (println ">> old:" (zip/node loc))
        ;          (println ">> function:" (zip/node func-seq))
        ;(if new-loc (println ">> new function:" (zip/node new-loc)))
        ;          (if new-loc (println ">> new:" (zip/node (zip/down new-loc))))
        ;(println "check" (and (not-empty target) (not (= (:op-id source) (:op-id (nth target 0))))))
        ;(println "target" target)
        ; exclude the destructuring case
        [(zip/next (if new-loc new-loc func-seq-loc))
         (if
           (and (not-empty target)
                (not-is-dest-vec source (nth target 0))
                ; only collect when this was not a local variable dependency
                (not (vector? (zip/node (zip/up func-seq-loc)))))
           {:source source :target target})]
        ))))

;;;
;;; Collect the dependencies coming from the local variables.
;;;

(defn arc-collection-matcher [loc] (and (symbol? (zip/node loc))
                                        (not (special-symbol? (zip/node loc)))
                                        (not (empty? (zip/lefts loc)))
                                        (meta (zip/node loc))
                                        (zip/up loc)
                                        (seq? (zip/node (zip/up loc))) ; it is an input to a function
                                        ;(if (contains? (-> loc zip/node meta) :op-id)
                                        (not-is-dest-vec loc)
                                        ;true)
                                        ))
(defn arc-collection-editor [loc]
  ;(println "Parent:" (zip/node (zip/up loc)) (meta (zip/node (zip/up loc))))
  ;(println "Current node:" (zip/node loc) (meta (zip/node loc)))
  ;(println "------------------------------")
  [(zip/next loc)
   {:source (select-keys (meta (zip/node loc)) [:op-id :out-idx])
    :target (:target (meta (zip/node loc)))}])
