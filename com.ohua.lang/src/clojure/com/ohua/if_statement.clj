;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.if-statement
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l]
            [com.ohua.link :as linker]))


(defn assign-in-idx [in-idx-generator loc]
  (let [in-idx (swap! in-idx-generator inc)]
    (l/printline "assign-in-idx:" (zip/node loc) in-idx)
    [
     (if (empty? (zip/lefts loc))
       ; we have to jump this thing because we do not want to traverse it further! (otherwise we will continue to
       ; increase the counter inside this thing that is anyways already stateful function composition!)
       ; stateful function call inside if-condition
       (t-zip/skip (zip/edit (zip/up loc) #(with-meta % (conj {:in-idx in-idx} (meta %)))))
       ; local variable used as input to function in if-condition
       (zip/next (zip/edit loc #(with-meta % (conj {:in-idx in-idx} (meta %))))))
     nil]))

;;;
;;; Handles the if special form: (if cond (then-branch) (else-branch?)).
;;; Here we check if the if-statement's output is used. If so then we need a merge. 
;;; That means we register a merge but also set the op-id of the if-statement to be the one of the merge.
;;; As a result, the algorithm for dependency detection will not need any intervention for the outputs.
;;;
(defn register-if-operator [id-generator loc func]
  ;  (println "registering if" (zip/node func))
  (let [op (zip/node func)
        id (swap! id-generator inc)
        switch-id (swap! id-generator inc)
        op-id (swap! id-generator inc)
        ; always register the condition to have the switch id because any input there needs to have the switch as its input.
        ; (only if it is not a real operator!)
        cond (zip/right func)
        ; walk the condition and mark every variable with an in-idx according to the traversal. 
        ; we use the same traversal that later on replaces these values with an 'nth' access such that both are in sync!
        [indexed-cond _] (t-zip/walk-code (t-zip/tree-zipper (zip/node cond))
                                          ; condition from below but at this moment nothing in the condition was worked so we have to ask more explicitly.
                                          #(and (zip/up %) (or (linker/is-defined? (zip/node %))
                                                               ; check if it is a variable
                                                               (t-zip/is-lvar? %)))
                                          (partial assign-in-idx (atom 0)))
        ; hook the changes into the current tree
        c (zip/replace cond indexed-cond)
        new-cond (zip/edit c
                           #(with-meta % (conj
                                           (if (and (seq? (zip/node cond)) (linker/is-defined? (-> cond zip/down zip/node)))
                                             {:skip false :is-op? true} ; the normal operator detection algorithm will take care of it
                                             {:skip   false :op-id switch-id :out-idx 0 ; input for the select comes from the 0th output of the switch
                                              :is-op? false :direct-access true})
                                           (meta %))))
        new-loc (zip/up new-cond)]
    (l/printline "Switch-id:" switch-id)
    (if
      (if
        ; FIXME only the t-zip/is-input? condition is the right one to decide. we leave the below condition in for now to make the 'for' statement work.
        ; however, this is not the right way to do it!
        (and (zip/up new-loc)
             (some #(= % (-> new-loc zip/up zip/down zip/node)) ['loop*]))
        false
        (t-zip/is-input? new-loc))
      ; merge case
      [(zip/next (zip/edit new-loc #(with-meta % (conj {:op-id id :out-idx -1 :switch switch-id :skip false :is-op? true :direct-access true} (meta %)))))
       [{:op-id id :type "select"} {:op-id switch-id :type "ifThenElse"}]]
      ; no merge needed!
      ; don't edit anything because then we do not register any wrong dependencies for the branches. we nevertheless return this operator such that it gets created properly.
      [(zip/next (zip/edit new-loc #(with-meta % (conj {:switch switch-id :is-op? false} (meta %))))) [{:op-id switch-id :type "ifThenElse"}]])))


(defn create-callable-condition [code]
  (l/printline "Creating callable condition:")
  (l/printline "code: ")
  (l/pprint code)
  (let [c `(reify com.ohua.lang.Condition
             (~(symbol "check") [~(symbol "this") ~(symbol "args")] ~code))]
    (l/printline "cond:" c)
    c))

;;;
;;; We hit an operator (input) in whatever form (implicit or explicit).
;;; 1) Take out the dependency information.
;;; 2) Replace this node with an argument name.
;;;
(defn condition-args-edit [idx loc]
  (l/printline "Arrived at" (zip/node loc) (meta (zip/node loc)))
  (let [condition (zip/node loc)
        i (swap! idx inc)]

    [(zip/next (zip/replace loc `(nth ~(symbol "args") ~i))) (update-in (meta condition) [:target :in-idx] (fn [_] i))]))

(defn condition-args-target-correct [switch-id arg-idx-generator loc]
  ;  (println "Arrived at" (zip/node loc))
  (let [condition (zip/node loc)
        arg-idx (swap! arg-idx-generator inc)]
    [(zip/next (zip/edit loc #(with-meta % (update-in (meta %) [:target :in-idx] (fn [_] arg-idx))))) nil]))

(defn handle-if-condition [switch-id cond discovered-deps]
  (l/printline "condition: " (zip/node cond))
  ; the condition of the switch always has an op-id, the one of the ifThenElse op!

  ; the final function needs to be compiled into a condition of the switch. however, the input to this function
  ; needs to flow into the first slots of the if-operator. once we compiled this code into a function/object, we
  ; will just push it as an argument to the OhuaFrontend.
  (do
    (l/printline "found function" switch-id)
    ; transform the condition replacing all calls to operators with args. return the new condition and the args including their dependencies.
    (let [[transformed-cond inputs] (t-zip/walk-code (t-zip/tree-zipper (zip/node cond))
                                                     #(and (contains? (meta (zip/node %)) :op-id) (zip/up %))
                                                     (partial condition-args-edit (atom -1)))
          ; correct the target reference of operators/functions that are input to this condition
          [corrected-cond _] (t-zip/walk-code (t-zip/tree-zipper (zip/node cond))
                                              #(and (contains? (meta (zip/node %)) :op-id) (zip/up %))
                                              (partial condition-args-target-correct switch-id (atom 0)))
          final-cond (zip/replace cond corrected-cond)]
      (l/printline "Done with condition transformation" inputs)
      (l/printline "New condition code:")
      (l/write transformed-cond :dispatch clojure.pprint/code-dispatch)
      (l/printline "")
      [[]
       ; turn this condition into a function and attach it as meta-data to the condition node
       (zip/edit final-cond #(with-meta % (conj
                                            {:condition (with-meta (create-callable-condition transformed-cond) {:java-type 'com.ohua.lang.Condition})}
                                            {:target {:op-id switch-id :in-idx 0}}
                                            (meta %))))])))


(defn if-matcher [loc] (= (zip/node loc) 'if))

(defn if-editor [discovered-deps loc]
  ; handle the condition first.
  ; two possible if conditions exist:
  ; 1) an operator -> implicit upstream input
  ; 2) a boolean function -> might contain input from upstream but need to compile boolean function
  (l/printline "Arrived at if" (zip/node (zip/up loc)))
  (l/printline "with the following registered dependencies:")
  (l/print-table discovered-deps)
  (l/printline)
  (let [cond (zip/right loc)
        switch-id (:switch (meta (zip/node (zip/up loc))))]
    ; handle the condition first
    (let [[updated-deps new-cond] (handle-if-condition switch-id cond discovered-deps)]
      ;        (println "changed condition" (meta (zip/node new-cond)))
      (l/printline "changed condition deps:")
      (l/print-table updated-deps)
      ; finally we continue with processing the if-branch -> this might mean that we do not support heavily nested conditions!
      [(zip/next (zip/right new-cond))
       updated-deps]
      )))

;;;
;;; Convert the condition into an argument of the if operator.
;;;
(defn if-arguments-handler [loc]
  ;(println "if args handler" (-> loc zip/down zip/right zip/node meta :condition))
  {:op-id (:switch (meta (zip/node loc))) :args [{:arg (:condition (-> loc zip/down zip/right zip/node meta)) :in-idx 0}]})



;;;
;;; I want the decision passed on to the first slot of the 'select' to *always*
;;; come from the switch (even when the condition is evaluated entirely by another op) because this makes sure that
;;; it is always of type Boolean.
;;; This an AST pass that wraps the boolean function around the condition! this solves all problems in the code above
;;; and even removes the case where input comes directly from an operator because this will never be the case anymore.
;;;

(defn transform
  ([code] (transform #{} nil code))
  ([&env &form code]
   (l/printline "################################################################################################")
   (l/printline "Condition transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               if-matcher
                                               (fn ast-if-editor [loc]
                                                 [(zip/replace (zip/next loc) `(clojure.core/boolean ~(-> loc zip/next zip/node)))
                                                  nil])
                                               )]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))
