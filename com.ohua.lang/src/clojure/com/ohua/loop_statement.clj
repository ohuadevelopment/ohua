;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.loop-statement
(:require [clojure.walk :as walk] 
          [clojure.zip :as zip]
          [com.ohua.tree-zipper :as t-zip]
          [com.ohua.logging :as l]))


(defn register-loop-operator [id-generator loc func]
  (l/printline "Registering loop")
  (let [id (swap! id-generator inc)]
    [(zip/next (zip/edit loc #(with-meta % (conj {:merge-id id :in-idx 0 :is-op? true :direct-access true} (meta %))))) [{:op-id id :type "nd-merge"}]]))

;;;
;;; Attach the id of the merge to the recur statement.
;;;
;FIXME this needs fixing. it needs to influence the surrounding if-statement to not create a merge operator!
;      therefore the loop pass needs to be performed before the if-pass.
(defn register-recur-operator [id-generator loc func]
  (l/printline "Registering recur")
  (let [l (t-zip/find-closure loc 'loop*)]
    [(zip/next (zip/edit loc #(with-meta % (conj {:op-id (:merge-id (meta (zip/node l))) :in-idx 1 :is-op? true :direct-access true} (meta %))))) nil]))


(defn loop-matcher [loc] (= (zip/node loc) 'loop*))

;;;
;;; For the time being we do not consider specific input or output schema slots.
;;;
(defn find-dependency [source target deps]
  (l/printline "finding dependency" source "->" target "in:")
  (l/print-table deps)
  (keep-indexed (fn [index item] (if (and 
                                       (= (:op-id (:source item)) source)
                                       (= (:op-id (:target item)) target)) 
                                   (list index item) nil)) deps))

;;;
;;; Inspect the arguments vector:
;;; - every argument needs to find its way to the very same port of the merge
;;; - afterwards args need to be dispatched as defined in the data flow schema
;;;
(defn loop-editor [discovered-deps loc]
  (l/printline "handling loop: " (meta (zip/node (zip/up loc))) (zip/node (zip/down (zip/next loc))))
  (loop [loop-args (zip/down (zip/next loc))
         result discovered-deps
         var-pos 0]
    (if-let [var loop-args]
      (if-let [source (-> var zip/right zip/node meta :op-id)]
        (let [_ (l/printline "handling loop var:" (zip/node var))
              targets (doall (t-zip/find-var-usage (-> loc zip/next zip/rights) (zip/node var)))]
          ; find the according dependencies via map
          (let [found-deps (reduce concat (map 
                                            #(let [target (:op-id (:target (meta (zip/node %))))]
                                               ; get the dependencies for one source to one target (might be multiple)
                                                (find-dependency source target result))
                                            targets))
                merge-meta (meta (zip/node (zip/up loc)))]
            (l/printline "found dependencies:" found-deps)
            ; Note that all locals enter the merge as a compound argument at position 1. As a result, we have to increase the counter 
            ; on the output position of the locals at the merge.
            ; FIXME This is tricky for locals that represent an implicit match. This does not work!
            (recur (zip/right (zip/right var)) 
                   (concat 
                     ; filter the old ones
                     (keep-indexed #(when-not ((set (map (fn [i] (first i)) found-deps)) %1) %2) result)
                     ; from (loop) external to merge
                     (map (fn [i] {:source (:source (second i)) :target {:op-id (:merge-id merge-meta) :in-idx 1}}) found-deps)
                     ; from merge to (loop) internal
                     (map (fn [i] {:source {:op-id (:merge-id merge-meta) :out-idx var-pos} :target (:target (second i))}) found-deps))
                   (inc var-pos))
            ))
        [(zip/next loc) result])
       [(zip/next loc) result])))
