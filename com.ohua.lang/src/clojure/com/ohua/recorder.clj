;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;

;;;
;;; This is an implementation for recording zipper operations and replaying them.
;;; This allows to edit a tree and easy return to where the edit started.
;;;

(ns com.ohua.recorder
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [clojure.algo.monads :refer :all]
            [com.ohua.logging :as l]))

(defn resolve-ns-now [sym]
  (let [f (first (distinct (remove nil? (map #(ns-resolve % sym) (all-ns)))))]
    (symbol (str (ns-name (:ns (meta f)))) (name sym))))

;;
;; This is a very easy implementation that can later turn into something more generic. 
;;

(defn print-s [loc] (fn [state] (println "debug->loc:" loc) (println "debug->node:" (zip/node loc)) (println "debug->meta:" (meta (zip/node loc))) [loc state]))

(defn up-s [loc]
  (fn [state]
    [(zip/up loc) (fn [l] 
                    (let [m (t-zip/move (zip/down l) (count (zip/lefts loc)) zip/right)
;                          _ (println "down and left" (zip/node m))
                          t (if state (state m) m)]
                      t))
     ]))

(defn right-s [loc]
  (fn [state]
;    (println "right" loc)
;    (println "right" (zip/node loc))
;    (println (zip/node (zip/right loc)))
    [(zip/right loc) (fn [l] 
                       (let [left (zip/left l)
;                             _ (println "left" (zip/node left))
                             t (if state (state left) left)]
                         t)
                       )]))

(defn left-s [loc]
  (fn [state]
    [(zip/left loc) (fn [l] 
                       (let [right (zip/right l)
;                             _ (println "left" (zip/node left))
                             t (if state (state right) right)]
                         t)
                       )]))

(defn down-s [loc]
  (fn [state]
;    (println "down" loc)
;    (println "down" (zip/node loc))
;    (println (zip/node (zip/down loc)))
    [(zip/down loc) (fn [l]  
                      (let [up (zip/up l)
;                            _ (println "up" (zip/node up))
                            t (if state (state up) up)]
                        t))]))

; TODO such functions could easily use a generic monadic version as they do not change the state. 
(defn edit-s [loc f]
  (fn [state]
;    (println "down" loc)
;    (println "down" (zip/node loc))
;    (println (zip/node (zip/down loc)))
    [(zip/edit loc f) (fn [l] (if state (state l) l))]))

;;
;; TODO a more advanced version of this monad would: 
;; - check whether the code includes any non-monadic operations and
;; - just provide a default monadic function that does not alter the state at all.
;;
(defn make-monadic [code]
  (let [code (walk/macroexpand-all code)
;        _ (println "code:" code)
        zipped-code (t-zip/tree-zipper code)
       [transformed-code _] (t-zip/walk-code zipped-code
                                             (fn [loc] (and (symbol? (zip/node loc)) (= (namespace (zip/node loc)) "zip")))
                                             (fn [loc]
                                               (cond 
                                                 ; TODO this could be way more generic
                                                 ; Note: symbols in this code need to be resolved
                                                 (= (resolve-ns-now (zip/node loc)) `clojure.zip/up) [(zip/next (zip/replace loc `up-s)) nil]
                                                 (= (resolve-ns-now (zip/node loc)) `clojure.zip/left) [(zip/next (zip/replace loc `left-s)) nil]
                                                 (= (resolve-ns-now (zip/node loc)) `clojure.zip/right) [(zip/next (zip/replace loc `right-s)) nil]
                                                 (= (resolve-ns-now (zip/node loc)) `clojure.zip/down) [(zip/next (zip/replace loc `down-s)) nil]
                                                 (= (resolve-ns-now (zip/node loc)) `clojure.zip/edit) [(zip/next (zip/replace loc `edit-s)) nil]
                                                 :default (throw (UnsupportedOperationException. (str "The following zip function does not have a monadic implementation:" (zip/node loc)))))) 
                                             concat)]
;    (println "transformed monadic code:")
;    (println transformed-code)
    transformed-code))

(defmacro rec
  "Records the zip operations performed and produces them as output to recording"
  [code]
;  (println `(domonad state-m ~(make-monadic code) ~(second (reverse code))))
  `(domonad state-m ~(make-monadic code) ~(second (reverse code))))

(defn play
  "Input is the state of the monad above to reverse the traversal."
  [state]
   ((second state) (first state)))