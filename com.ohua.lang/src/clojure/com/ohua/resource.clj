;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.resource
  (:import com.ohua.engine.resource.management.ResourceManager 
           com.ohua.lang.io.IOHandle))

(defn resource
  "Creates an IO resource that connections can be retrieved from."
  [id clz attr]
  (let [res-mngr (ResourceManager/getInstance)
        res-attr (doto (new java.util.HashMap) (.putAll (merge attr {"id" id})))
        res-args (into-array Object (list nil res-attr))
        res (clojure.lang.Reflector/invokeConstructor clz res-args)]
    (. res-mngr registerResource res)
    id))

(defn cnn
  "Creates a connection identifier that can be tied to an operator. 
   Only at runtime this identifier turns into a real connection that is tied to the operator."
  [res-id args]
  (new IOHandle res-id args))
