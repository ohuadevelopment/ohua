;
; ohua : walk.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.walk
  (:require [clojure.walk :as w])
  (:import (clojure.lang IObj)))


(defn with-meta-from [old new]
  (if (and (instance? IObj new) (instance? IObj old) (meta old))
    (vary-meta new #(merge (meta old) %))
    new))


(defn walk-preserving [inner outer form]
  (with-meta-from form (w/walk inner outer form)))

(defn prewalk-preserving [f form]
  (walk-preserving (partial prewalk-preserving f) identity (f form)))

(defn macroexpand [form] (with-meta-from form (clojure.core/macroexpand form)))


(defn macroexpand-all [form]
  (prewalk-preserving (fn [x] (if (seq? x) (macroexpand x) x)) form))
