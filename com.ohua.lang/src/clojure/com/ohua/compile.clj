;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.compile
  (:require [clojure.walk :as walk]
            [com.ohua.walk :as owalk]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.link :as ln]
            [com.ohua.operator-detection :as op-detect]
            [com.ohua.code-generation :as code-gen]
            [com.ohua.dataflow-detection :as df-detect]
            [com.ohua.arguments-handler :as args-handler]
            [com.ohua.if-statement :as if-stmt]
            [com.ohua.loop-statement :as loop-stmt]
            [com.ohua.context :as ctxt]
            [com.ohua.logging :as l]
            [com.ohua.util.visual :refer [render-ir-graph graph-to-str]]
            [com.ohua.optimization.redundant-computation-elimination :as redundant]
            [com.ohua.transform.local-variable-scoping :as l-scoping]
            [com.ohua.transform.higher-order-functions :as h-order]
            [com.ohua.transform.smap :as smap]
            [com.ohua.transform.destructuring :as dest]
            [com.ohua.transform.sfn-resolve :as sfn-resolve]
            [com.ohua.transform.non-sfn-resolve :as non-sfn-resolve]
            [com.ohua.transform.values :as values]
            [com.ohua.transform.algorithms :as algos]
            [com.ohua.transform.symbol-resolution :as symres]
            [clojure.pprint])
  (:use com.ohua.util.assert))
(require '[clojure.set :refer [difference]])
; compile-time debugging flag
(def produce-ir-graphs (atom false))
(defmacro enable-ir-graphs [] (reset! produce-ir-graphs true))

;;;
;;; Operator Compilation/Analysis
;;;


;;;
;;; Algorithm Compilation
;;;


;;;
;;; The algorithm runs various passes over the submitted code:
;;;   1) Assign all operators an id.
;;;   2) Detect dependencies and mark input parameters with the ids of the operator that they coming from.
;;;   3) Create the arcs by looking at the operators and their input parameters.
;;;

;;;
;;; Algorithm requirement:
;;; All further extensions work on the basic annotations of the code tree. Therefore, if constructs change the control flow, the :target tag
;;; of the according local or function needs to be updated accordingly (not only the mappings)!
;;;

(defmulti deep-convert (fn [arg] (type arg)))
(defmethod deep-convert java.util.List [arg] (cons 'list (map deep-convert arg)))
(defmethod deep-convert java.util.Map [arg] (into {} (for [[k v] arg] [k (deep-convert v)])))
(defmethod deep-convert java.util.Set [arg] (cons 'set (map deep-convert arg)))
(defmethod deep-convert :default [arg]
  (if (.isArray (type arg))
    ; recurse into the elements
    (cons 'into-array (cons (.getComponentType (type arg)) (conj '() (vec (map deep-convert arg)))))
    arg))

;TODO this needs to be integrated once more compilation support can be provided.
;TODO this needs to generate compile-time results that get added as code such that it becomes available at runtime to the OhuaRuntime!
(defn compile-flow-graph
  [compilation-code]
  (l/printline "Compiling flow graph ...")
  (let [compile-code (cons `doto compilation-code)]
    (let [compile-time-info (. (eval compile-code) getCompilationResults)
          converted (deep-convert compile-time-info)]
      (l/printline "compile time results (new): " converted)
      converted)))
(require '[clojure.pprint :refer [pprint]])
(require '[com.ohua.ir :as ir])

(def constructs #{'if 'recur})
(declare compile-and-run)

;;;
;;; The core compilation steps.
;;;
(defn ohua-compile
  ; some documentation
  [code option]
  ; 1st step: load macros for operators -> when we do it this way before then we even have code completetion for all operators!
  (l/printline "Executing ohua macro for the following clojure code:")
  (l/write code :dispatch clojure.pprint/code-dispatch)
  (l/printline "option:" option)
  (let [zipped-code (t-zip/tree-zipper code)
        [annotated-code extracted-ops]
        (t-zip/walk-code
          zipped-code
          op-detect/seq-matcher
          (partial op-detect/seq-editor (atom 99)) concat)

        _ (do
            (l/printline "extracted ops:")
            (l/print-table extracted-ops)
            ;(t-zip/print-tree (t-zip/tree-zipper annotated-code))
            )
        [ctxt-code ctxt-map]
        (t-zip/walk-code
          (t-zip/tree-zipper annotated-code)
          ctxt/ctxt-matcher
          ctxt/ctxt-editor
          ctxt/combine-ctxt-path
          {})

        ; 3rd step: find the dependencies -> two passes: 1. direct deps 2. lvar deps
        [arc-annotations discovered-arcs]
        (t-zip/walk-code
          (t-zip/tree-zipper ctxt-code)
          df-detect/branch-matcher
          df-detect/arc-editor)
        ;_ (t-zip/print-tree (t-zip/tree-zipper arc-annotations))
        flattened-arcs (flatten (map #(map (fn [target] {:source (:source %) :target target}) (:target %)) discovered-arcs))
        ; instead of collecting the arcs for local variables in the above step, we collect them in a separate pass.
        ; it will make the code easier to understand and more powerful (support for variable redefinitions).
        [_ lvar-arcs] (t-zip/walk-code
                        (t-zip/tree-zipper arc-annotations)
                        df-detect/arc-collection-matcher
                        df-detect/arc-collection-editor)
        detected-arcs (concat flattened-arcs lvar-arcs)
        _ (do
            (l/printline "found direct dependency arcs:")
            (l/print-table flattened-arcs)
            (l/printline "found local variable arcs:")
            (l/print-table lvar-arcs))

        ; resolve if statements
        [arc-annotations-two if-arcs]
        (t-zip/walk-code
          (t-zip/tree-zipper arc-annotations)
          if-stmt/if-matcher
          (partial if-stmt/if-editor detected-arcs)
          concat)

        discovered-arcs-two (concat detected-arcs if-arcs)

        _ (do
            (l/printline "found arcs (after if-handling):")
            (l/print-table discovered-arcs-two))

        ; resolve loop statements
        [arc-annotations-three discovered-arcs-three]
        (t-zip/walk-code
          (t-zip/tree-zipper arc-annotations-two)
          loop-stmt/loop-matcher
          (partial loop-stmt/loop-editor discovered-arcs-two)
          (fn [found editor-result] editor-result))

        _ (do
            (l/printline "found arcs (after loop-handling):")
            (l/print-table discovered-arcs-three))

        ; 4th step: register the arcs at the Ohua frontend
        all-discovered-arcs (if (empty? discovered-arcs-three)
                              (if (empty? discovered-arcs-two)
                                flattened-arcs
                                discovered-arcs-two)
                              discovered-arcs-three)

        _ (do
            (l/printline "Ops (before transformation)")
            (l/print-table extracted-ops)
            (l/printline "Arcs (before transformation)")
            (l/print-table all-discovered-arcs))

        _ (doall (map assert-valid-arc all-discovered-arcs))

        ; op-args :: [{:op-id :: Long, :args :: [{:arg :: a, :in-idx :: Long}]}]
        ; 5th step: handle env arguments
        [_ op-args]
        (t-zip/walk-code
          (t-zip/tree-zipper arc-annotations-two)
          args-handler/exclusive-op-matcher
          args-handler/arguments-editor)

        [ir-transformed-ops ir-transformed-arcs ir-env-args]
        (->
          (ir/gen-from-op-list extracted-ops all-discovered-arcs op-args ctxt-map)
          (ir/validate "before transformations")

          ((fn [{initial :graph :as df-ir}]
             (if @produce-ir-graphs
               (do
                 (println (graph-to-str initial))
                 (spit "test/initial-ir-graph.dot" (render-ir-graph initial))))
             df-ir))

          ; default IR transformations
          ctxt/apply-ctxt-transformations

          ((fn [transformed]
             (if-let [raw-trans (:df-transformations (:compile-with-config option))]
               (ir/apply-transformations transformed (eval raw-trans))
               transformed)))

          (ir/apply-transformations [redundant/eliminate])

          ((fn [{transformed :graph :as df-ir}]
             (if @produce-ir-graphs
               (do
                 (println (graph-to-str transformed))
                 (spit "test/transformed-ir-graph.dot" (render-ir-graph transformed))))
             df-ir))

          (ir/validate "after transformations")
          ir/op-list-from-ir)

        _ (do
            (l/printline "Ops (after transformation)")
            (l/print-table (sort-by :op-id ir-transformed-ops))
            (l/printline "Arcs (after transformation)")
            (l/print-table ir-transformed-arcs))

        prepared-ops (code-gen/prepare-ops ir-transformed-ops)
        prepared-arcs (code-gen/prepare-arcs ir-transformed-arcs)
        prepared-args (code-gen/prepare-arguments ir-env-args)

        _ (do
            (l/printline "Arc code generation done! Result:")
            (l/pprint prepared-arcs))

        _ (do
            (l/printline "Arguments:")
            (l/pprint prepared-args))]

    (condp #(contains? %2 %1) option
      :test-compile (list 'quote (code-gen/generate-compile-code prepared-ops prepared-arcs ir-env-args))
      :compile (cons 'doto (code-gen/generate-compile-code prepared-ops prepared-arcs ir-env-args))
      (compile-and-run option prepared-ops prepared-arcs prepared-args ir-env-args))))


(defn compile-and-run [option prepared-ops prepared-arcs prepared-args op-args]
  ; last step: insert code that actually executes the flow
  (let [prepared-code-stmt
        (concat [`(new com.ohua.lang.OhuaRuntime)]
                (vec (concat prepared-ops prepared-arcs prepared-args)))
        final-code
        (condp #(contains? %2 %1) option
          :test `'~(concat prepared-code-stmt `(.execute))
          :run-with-config (cons 'doto (concat prepared-code-stmt
                                               `((.execute ~(:run-with-config option)
                                                           ~(compile-flow-graph
                                                              (code-gen/generate-compile-code
                                                                prepared-ops
                                                                prepared-arcs
                                                                op-args))))))
          :run (cons 'doto
                     (concat prepared-code-stmt
                             `((.execute
                                 ~(compile-flow-graph
                                    (code-gen/generate-compile-code
                                      prepared-ops
                                      prepared-arcs
                                      op-args))))))
          (throw (Exception. (str "Expected option like :test, :compile etc. but was:" option))))]
    (l/printline "Final generated code:")
    (l/pprint final-code)
    (l/printline "")
    final-code))


(defn compile-code [code option]
  (let [code (walk/macroexpand-all code)]
    (l/printline "################################################################################################")
    ;    (println "Namespace variables: ")
    ;    (clojure.pprint/pprint (ns-map *ns*))
    (l/printline "Entered code:")
    (l/write code :dispatch clojure.pprint/code-dispatch)
    (l/printline)
    (ohua-compile code option)))

(defn exec-transformations
  [env form transformations code]
  (let [t-funs (map #(partial % env form) transformations)
        t-funs-ready (reverse (concat [(partial (first t-funs) code)] (rest t-funs)))]
    ((apply comp t-funs-ready))
    ))

(defn transformation
  ([code] (transformation code {}))
  ([code options] (transformation code options {} nil))
  ([code options env form]
   (let [code-transformations [algos/inline-global-algos
                               (partial symres/traverse options)
                               algos/transform-local-algos
                               non-sfn-resolve/transform
                               sfn-resolve/transform
                               if-stmt/transform
                               dest/transform
                               values/transform
                               l-scoping/transform
                               h-order/transform
                               smap/transform]              ; can later on be extended to pick up transformations automatically.]
         expanded (owalk/macroexpand-all code)]
     (exec-transformations env form code-transformations expanded))))

(defn- prepare-options [options]
  (assoc
    (cond
      (= (count options) 1) {(first options) true}
      (odd? (count options)) (throw (Exception. (str "Your options are odd (" (count options) "). Please make sure to submit a single option or key value pairs. Defined options: " options)))
      :default (apply hash-map options))
    :run true))

;;
;; Compilation works in two phases:
;; 1. Code transformations directly on the AST.
;; 2. Compilation: translation of Clojure code (AST) into a dataflow graph (dataflow IR).
;;
(defn compilation [code option env form]
  ; we need to make sure that we create the namespace for the built-in functions properly
  (if-not (ln/is-imported? 'com.ohua.lang)
    (ln/ohua-require-fn '[com.ohua.lang :refer :all]))
  (let [opts (prepare-options option)]
    (compile-code
      (transformation code opts env form)
      opts)))

(defn ohua- [code option env form]
  (if (= code :import)
    (do
      (binding [*out* *err*]
        (println "WARNING: The (ohua :import) macro is deprecated, please use ohua-require instead."))
      (apply ln/ohua-require-fn (map (fn [o] [o :refer :all]) (first option))))
    (compilation code option env form)))
