;
; ohua : algo.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.lang.algo
  (:import (clojure.lang IFn)
           (com.ohua.lang Algorithm$IAlgorithm)))


(defrecord Algo [ns
                 scope
                 name
                 code                                       ; (name (params meta body))
                 ] Algorithm$IAlgorithm IFn

  Algorithm$IAlgorithm
  (mkSymbol [this]
    (symbol
      ; it has to be that explicit because the linker overloads the shortcut with the value of :name
      (clojure.core/name
        (ns-name (:ns this)))
      (clojure.core/name (:name this)))
    )

  IFn
  (invoke [_ & _]
    (throw (Exception. "Algorithms can not be executed outside the scope of Ohua."))
    )
  )
