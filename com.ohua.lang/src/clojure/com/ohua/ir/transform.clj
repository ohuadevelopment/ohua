;
; ohua : transform.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2017. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.ir.transform
  (:require [com.ohua.ir :as ir]
            [monads.core :refer :all]
            [monads.util :refer :all]
            [monads.state :refer [exec-state]]
            [clojure.algo.generic.functor :refer [fmap]]))


(def get-graph (fmap :graph get-state))
(defn modify-graph
  ([f] (modify update :graph f))
  ([f x] (modify update :graph f x))
  ([f x y] (modify update :graph f x y))
  ([f x y z] (modify update :graph f x y z))
  ([f x y z & args] (modify #(apply update % :graph f x y z args))))


(defn- get-largest-id
  "Obtain the largest ID present in the given ir-graph"
  [ir-graph] (apply max (keep :id ir-graph)))


(defn mk-func-and-index
  "Create an IR function with given parameters and also index the arguments and
  returns"
  [id name args return]
  (ir/->IRFunc id name
               (into [] (ir/reindex-stuff :in-idx args))
               (if (coll? return)
                 (into [] (ir/reindex-stuff :out-idx return))
                 (vary-meta return assoc :out-idx -1))))


(defn- gen-many [ref i]
  (mdo
    s <- (fmap ref get-state)
    (modify assoc ref (drop i s))
    (return (take i s))))


(defn gen-names [i] (gen-many :name-gen i))
(def gen-name (fmap first (gen-names 1)))
(defn gen-ids [i] (gen-many :id-gen i))
(def gen-id (fmap first (gen-ids 1)))
(defn gen-func [name args return_] (fmap #(mk-func-and-index % name args return_) gen-id))
(defn gen-func-unindexed [name args return_] (fmap (fn [id] (ir/->IRFunc id name args return_)) gen-id))

(defn apply-stateful-transform [f graph other-state]
  (exec-state
    f
    (assoc
      other-state
      :graph graph
      :name-gen (map symbol (ir/name-gen-from-graph graph))
      :id-gen (iterate inc (inc (get-largest-id graph))))))

(defn state-get-producer [arg]
  (fmap (partial ir/get-producer arg) get-graph))

(defn state-get-consumers [arg]
  (fmap (partial ir/get-consumers arg) get-graph))

(defn state-find-func [fn-or-id]
  (fmap (comp first (partial filter #(= (ir/fn->id %) (ir/fn->id fn-or-id)))) get-graph))


(defn state-remove-node [& nodes-or-ids]
  (modify-graph (partial ir/drop-nodes-where (comp (set (map ir/fn->id nodes-or-ids)) :id))))


(defn state-replace-node [replacements]
  (modify-graph (partial ir/replace-nodes replacements)))

(defn state-update-graph [replacements]
  (modify-graph #(ir/update-graph replacements %)))
