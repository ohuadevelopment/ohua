;
; ohua : pmap.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2017. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.ir.transform.pmap
  (:require [com.ohua.ir :as ir]
            [monads.core :refer :all]
            [monads.util :refer :all]
            [monads.state :refer [exec-state]]
            [com.ohua.ir.transform :refer :all]
            [clojure.algo.generic.functor :refer [fmap]])
  (:use com.ohua.util.debug)
  (:import com.ohua.ir.IRFunc
           (com.ohua.util TypeError LazyChunkedIterableWrapper)))


(def pmap-width 4)


(def check-state-integrity (fmap #(assert (map? %)) get-state))


(defn- wrap-smap [batch-size-source smap-node collect-node]
  (mdo
    otn-node <- (state-get-producer (first (.-args smap-node)))
    let [old-size-out old-map-in] = (.-args otn-node)
    old-size-op <- (state-get-producer old-size-out)

    ; chunker
    chunk-out <- gen-name
    chunk-func <- (gen-func 'com.ohua.lang/__chunk [batch-size-source old-map-in] chunk-out)

    ; outer smap
    outer-smap-return <- gen-name

    inner-size-out <- gen-name
    inner-size <- (gen-func 'com.ohua.lang/size [outer-smap-return] inner-size-out)

    ; inner otn
    inner-otn-out <- gen-name
    inner-otn <- (gen-func 'com.ohua.lang/one-to-n [inner-size-out outer-smap-return] inner-otn-out)

    ; inner-smap
    inner-smap <- (gen-func 'com.ohua.lang/smap-fun [inner-otn-out] (.-return smap-node))
    check-state-integrity

    ; 1-N
    inner-size-1-N-return <- gen-name
    inner-size-1-N <- (gen-func 'com.ohua.lang/one-to-n [inner-size-out inner-size-out] inner-size-1-N-return)

    ; inner collect
    inner-collect-out <- gen-name
    inner-collect <- (gen-func 'com.ohua.lang/collect [inner-size-1-N-return (second (.-args collect-node))] inner-collect-out)

    ; 1-N for outer collect
    outer-size-1-N-return <- gen-name
    outer-size-1-N <- (gen-func 'com.ohua.lang/one-to-n [old-size-out old-size-out] outer-size-1-N-return)

    ; outer smap
    outer-collect-out <- gen-name

    ; flattener
    flattener <- (gen-func 'com.ohua.lang/__flatten [outer-collect-out] (.-return collect-node))

    let _ = (assert (= 'com.ohua.lang/collect (:name collect-node)) (:name collect-node))

    (state-update-graph
      {otn-node     [(assoc-in otn-node [:args 1] (vary-meta chunk-out assoc :in-idx 1))]
       old-size-op  [(assoc old-size-op :args [(vary-meta chunk-out assoc :in-idx 0)])]
       smap-node    [chunk-func
                     (assoc smap-node :return [outer-smap-return])
                     inner-size
                     inner-otn
                     inner-smap
                     inner-size-1-N
                     outer-size-1-N]
       collect-node [
                     inner-collect
                     (assoc collect-node
                       :args (ir/reindex-stuff :in-idx [outer-size-1-N-return inner-collect-out])
                       :return outer-collect-out)
                     flattener]
       })
    (return inner-size-out)
    ))


(defn- wrap-smap-io [batch-size-source {smap-return :return [iterable] :args :as smap-node} collect-node]
  (let [old-size-out (second smap-return)
        bss-value (nth batch-size-source 2)                 ; must be env arg here
        coll-source (nth iterable 2)                        ; must be env arg here
        lazy-coll-source `(fn [] ^{:java-type LazyChunkedIterableWrapper} (new LazyChunkedIterableWrapper ~bss-value ~coll-source))
        ]
    (mdo
      ; outer smap
      outer-smap-return <- gen-name

      inner-size-out <- gen-name
      inner-size <- (gen-func 'com.ohua.lang/size [outer-smap-return] inner-size-out)

      ; inner otn
      inner-otn-out <- gen-name
      inner-otn <- (gen-func 'com.ohua.lang/one-to-n [inner-size-out outer-smap-return] inner-otn-out)

      ; inner-smap
      inner-smap <- (gen-func 'com.ohua.lang/smap-fun [inner-otn-out] [(first smap-return)])
      check-state-integrity

      ; 1-N
      inner-size-1-N-return <- gen-name
      inner-size-1-N <- (gen-func 'com.ohua.lang/one-to-n [inner-size-out inner-size-out] inner-size-1-N-return)

      ; inner collect
      inner-collect-out <- gen-name
      inner-collect <- (gen-func 'com.ohua.lang/collect [inner-size-1-N-return (second (.-args collect-node))] inner-collect-out)

      ; 1-N for outer collect
      outer-size-1-N-return <- gen-name
      outer-size-1-N <- (gen-func 'com.ohua.lang/one-to-n [old-size-out old-size-out] outer-size-1-N-return)

      ; outer smap
      outer-collect-out <- gen-name

      ; flattener
      flattener <- (gen-func 'com.ohua.lang/__flatten [outer-collect-out] (.-return collect-node))

      let _ = (assert (= 'com.ohua.lang/collect (:name collect-node)) (:name collect-node))

      (state-update-graph
        {smap-node    [(assoc smap-node :return [outer-smap-return old-size-out]
                                        :args [(vary-meta lazy-coll-source assoc :in-idx 0)])
                     inner-size
                     inner-otn
                     inner-smap
                     inner-size-1-N
                     outer-size-1-N]
         collect-node [inner-collect
                     (assoc collect-node
                       :args (ir/reindex-stuff :in-idx [outer-size-1-N-return inner-collect-out])
                       :return outer-collect-out)
                     flattener]
       })
      (return inner-size-out)
      )))


(defn- transform-smap [batch-size-source target]
  (mdo
    {smap-name :name smap-args :args smap-returns :return :as smap-node} <- (state-find-func target)
    let is-smap-io = (case smap-name
                       com.ohua.lang/smap-fun false
                       com.ohua.lang/smap-io-fun true
                       (throw (IllegalArgumentException. (str "Unknown smap-type " smap-name))))
    collect-node <- (if is-smap-io
                      (mdo
                        [otn-node] <- (state-get-consumers (second smap-returns))
                        (fmap first (state-get-consumers (.-return otn-node))))
                      (mdo
                        otn-node <- (state-get-producer (first smap-args))
                        [collect-otn] <- (fmap (partial remove (partial = otn-node)) (state-get-consumers (first (.-args otn-node))))
                        (fmap first (state-get-consumers (.-return collect-otn)))))

    ((if is-smap-io wrap-smap-io wrap-smap) batch-size-source smap-node collect-node)))


(defn- transform-one [target size-source batch-size-source]
  (mdo
    {fn-name :name myargs :args ret :return :as mynode} <- (state-find-func target)


    ; packager
    packager-return <- gen-name
    packager <- (gen-func-unindexed 'com.ohua.lang/__package-args (.-args mynode) ^{:out-idx -1} packager-return)

    ; 1-N
    size-1-N-return <- gen-name
    size-1-N <- (gen-func 'com.ohua.lang/one-to-n [size-source size-source] size-1-N-return)

    ; collect 1
    collect-return <- gen-name
    collect <- (gen-func 'com.ohua.lang/collect [size-1-N-return packager-return] collect-return)

    ; the functions
    other-fns <- (sequence-m
                   (repeat
                     (dec pmap-width)
                     (if (= 1 (count myargs))
                       (mdo
                         fn-in <- gen-name
                         fn-out <- gen-name
                         func <- (gen-func fn-name [fn-in] fn-out)
                         (return [fn-in fn-out [func]]))
                       (mdo
                         pmap-out <- gen-name
                         fn-in <- (gen-names (count (.-args mynode)))
                         fn-out <- gen-name
                         id-func <- (gen-func 'com.ohua.lang/id [(vary-meta pmap-out assoc :in-idx 0)] fn-in)
                         func <- (gen-func fn-name fn-in fn-out)
                         (return [pmap-out fn-out [id-func func]]))
                       )))

    ; INFO
    ; I wish I didn't have to do this. All this code is just to reuse the old function node
    ; Theoretically it should be the same as removing the decrement from the repeat above.
    ; However then ohua complains that it doesn't find the node anymore.
    ; I'd prefer not to have this duplicated code here.
    new-fn-and-id <- (if (= 1 (count myargs))
                       (mdo
                         fn-out <- gen-name
                         fn-in <- gen-name
                         let new-func = (assoc mynode :return (vary-meta fn-out assoc :out-idx -1) :args [(vary-meta fn-in assoc :in-idx 0)])
                         (return [fn-in fn-out [new-func]]))
                       (mdo
                         pmap-out <- gen-name
                         fn-out <- gen-name
                         fn-in <- (gen-names (count (.-args mynode)))
                         id-func <- (gen-func 'com.ohua.lang/id [(vary-meta pmap-out assoc :in-idx 0)] fn-in)
                         let new-func = (assoc mynode :return fn-out :args (ir/reindex-stuff :in-idx fn-in))
                         (return [pmap-out fn-out [id-func new-func]])))
    let all-fns = (cons new-fn-and-id other-fns)
    ; pmap
    pmap-fn <- (gen-func 'com.ohua.lang/pmap [batch-size-source collect-return] (into [] (map first all-fns)))

    ; pcollect
    pcollect-return <- gen-name
    pcollect <- (gen-func 'com.ohua.lang/pcollect (into [batch-size-source] (map second all-fns)) pcollect-return)

    ; one-to-n
    otn-return <- gen-name
    otn <- (gen-func 'com.ohua.lang/one-to-n [size-source pcollect-return] otn-return)

    ; smap
    smap <- (let [gen-smap #(gen-func-unindexed 'com.ohua.lang/smap-fun [(vary-meta otn-return assoc :in-idx 0)] %)]
              (cond
                (symbol? ret) (fmap vector (gen-smap [(vary-meta ret assoc :out-idx 0)]))
                (vector? ret) (mdo
                                smap-out <- gen-name
                                smap-fn <- (gen-smap [smap-out])
                                id-fn <- (gen-func 'com.ohua.lang/id [(vary-meta smap-out assoc :in-idx 0)] ret)
                                (return [smap-fn id-fn]))
                :else (throw (TypeError. "vector or symbol" (str (type ret))))))

    let all = (concat
                (list packager size-1-N collect pmap-fn)
                (mapcat (fn [a] (nth a 2)) all-fns)
                (list pcollect otn)
                smap)

    ; graph update
    (state-update-graph {mynode all})
    check-state-integrity
    ))

; CAVEATS:
;   - Currently rewriting a function which has environment args as inputs does not work.
(defn transform [targets batch-size-source {graph :graph ctxt-map :ctxt-map}]
  (select-keys

    (apply-stateful-transform
      (let [associated-targets (map #(vector % (-> % ctxt-map first :op-id)) targets)
            tainted-smaps (set (map second associated-targets))]
        (mdo
          ; TODO handle case where the context is not present or not smap
          size-returns <- (map-m (partial transform-smap batch-size-source) tainted-smaps)
          (map-m
            (fn [[target size-source]]
              (mdo
                (transform-one target size-source batch-size-source)
                ))
            (map vector targets size-returns))))
      graph
      {:ctxt-map ctxt-map})
    [:graph :ctxt-map]))

