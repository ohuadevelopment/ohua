;;;
;;; Copyright (c) Sebastian Ertel 2013-14. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.arguments-handler
(:require [clojure.zip :as zip]
          [com.ohua.if-statement :as if-stmt]
          [com.ohua.logging :as l]
          [com.ohua.link :as linker]))

(defn exclusive-op-matcher
  [loc]
  (and loc (seq? (zip/node loc)) (linker/is-defined? (zip/node (zip/down loc)))))

(defn arguments-editor
  [loc]
  (l/printline "Found location: " (zip/node loc))
  (if (= (zip/node (zip/down loc)) 'if)
    [(zip/next loc) (if-stmt/if-arguments-handler loc)]
    (let [all-args (loop [args [] 
                          c-loc (zip/right (zip/down loc))]
                     (if (not c-loc)
                       args
                       (if (contains? (meta (zip/node c-loc)) :op-id)
                         (recur args (zip/right c-loc))
                         (recur (conj args {:arg (zip/node c-loc) :in-idx (int (- (count (zip/lefts c-loc)) 1))}) (zip/right c-loc))
                       )))]
      ;(println "node:" (zip/node loc) "args:" all-args)
      [(zip/next loc) (if (not (empty? all-args)) {:op-id (:op-id (meta (zip/node loc))) :args all-args})])))
