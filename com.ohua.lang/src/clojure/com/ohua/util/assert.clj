(ns com.ohua.util.assert)


(defn assert-type
  ([expected-type value] (assert-type expected-type value (str "Expected type <" expected-type "> got <" (if (nil? value) "nil-type" (type value)) ">!")))
  ([expected-type value message]
   (assert (= expected-type (type value)) message)
   value))
(defn assert-pred
  ([pred val] (assert (pred val)))
  ([pred val msg]
   (assert (pred val) msg)))
(defn assert-coll
  ([s] (assert-coll s (str "Expected sequence, got <" (type s) ">")))
  ([s message]
   (assert (coll? s) message)
   s))
(defn assert-coll-of-type
  ([expected-type coll] (doall (map (partial assert-type expected-type) coll)) coll)
  ([expected-type coll message]
   (doall (map #(assert-type expected-type % message) coll))
   coll))
(defn assert-count [l coll] (assert (= l (count coll)) (str "Expected collection of size '" l "' recieved '" (count coll) "'")) coll)
(defn assert-map [m] (assert-pred map? m (str "Expected mappable type, got <" (type m) ">")))
(defn assert-number
  ([n] (assert-number n (str "Expected a number, got <" (type n) ">")))
  ([n message] (assert-pred number? n message)))
(defn assert-valid-arc-source [{out-idx :out-idx op-id :op-id :as source}]
  (assert-number out-idx (str "Found invalid source " source))
  (assert-number op-id (str "Found invalid source " source))
  source)
(defn assert-valid-arc-target [{in-idx :in-idx op-id :op-id :as target}]
  (assert-number in-idx (str "Found invalid source " target))
  (assert-number op-id (str "Found invalid source " target))
  target)
(defn assert-valid-arc [{target :target source :source :as arc}]
  (assert-pred (comp not nil?) target (str "Found arc with invalid target: " arc))
  (assert-pred (comp not nil?) source (str "Found arc with invalid source: " arc))
  (assert-valid-arc-source source)
  (assert-valid-arc-target target)
  arc)
(defn assert-not-empty
  ([coll] (assert-not-empty coll (str "Collection empty")))
  ([coll message] (assert-pred (comp not empty?) coll message)))
