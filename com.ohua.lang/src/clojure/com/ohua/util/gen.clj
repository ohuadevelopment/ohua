;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.util.gen
  (:require [clojure.string :as string :refer [join capitalize]])
  (:import [com.ohua.lang defsfn]
           (com.ohua.lang.compile Linker)))

(defmacro def-sfn
  "Class generation for stateful functions.

  (aot-function name args body) generates a class in the current namespace
  (*ns* with all occurrences of '-' replaced by '_') named `name` capitalized.

  The class has one method `name` whos first argument (first element of `args`)
  is always the class instance ('this' in java).

  Unless your function uses 'this' it can be anything when calling the function
  from clojure but it will be the class instance when called from java.

  When the function is called from java the result will always be wrapped in an
  Object[].

  The namespace containing the functions needs to be AOT compiled in order to be
  used in an ohua algrithm."
  [name args & body]
  (let [m-name (symbol (Linker/convertFunctionName (clojure.core/name name)))]
  `(do
     (gen-class
       :name ~(string/replace (str *ns* "." (capitalize name)) "-" "_")
       :prefix "__mk_generated_"
       :methods
       [[~(with-meta m-name `{defsfn {}})
         [~@(map #(get (meta %) :tag Object) args)]
         ~(get (meta args) :tag Object)]])
     (defn ~(symbol (str "__mk_generated_" m-name)) [~(symbol "__mk_generated_this") ~@args]
       ~@body)
     (defn ~name ~args ~@body))))


; FIXME: this essentially does not work. it is unclear whether one can find this generated class from Java.
;        the classes are generated only when AOT is enabled but then the generated functions seem very weird!
; TODO: write a defsfn macro that annotates a function as 'stateful function'. our linker needs to be extended to detect those!
(defmacro def-sfn-dt
  "Class generation for stateful functions with `deftype`.
  "
  [name args & body]
  (let [interface-name (symbol (str "Intr" (capitalize name)))
        class-name (symbol (str "Impl" (capitalize name)))]
    (println "Generated type: " class-name)
    (println "Current ns:" (ns-name *ns*))
    `(do
       (defprotocol ~interface-name (~name ~args))
       (deftype ~class-name []
         ~interface-name
         (~(with-meta name `{defsfn {}}) ~args ~@body)))))
