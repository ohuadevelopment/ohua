(ns com.ohua.util.visual
  (:require [rhizome.dot :refer [graph->dot]]
            [com.ohua.ir :as ir]
            [clojure.set :as set]
            [clojure.java.io :as io]
            [clojure.string :refer [join]])
  (:import [com.ohua.ir IRFunc]))


(def ir-graphs-directory "ir-graphs")


(defn find [thing coll]
  (first (filter #(= thing %) coll)))


(defn render-ir-graph
  ([ir-graph] (render-ir-graph ir-graph (constantly false)))
  ([ir-graph highlight]
    (graph->dot ir-graph #(ir/get-consumers (.-return %) ir-graph)
                :node->descriptor
                (fn [n] {:label (str (.-name n) "-" (.-id n))
                         :shape "box"
                         :color (if (highlight n ir-graph)
                                  "orange"
                                  "black")})
                :edge->descriptor
                (fn [n1 n2]
                  (let [ret (ir/get-return-vars n1)
                        args (.-args n2)
                        e (first (set/intersection (set ret) (set args)))
                        out-idx (:out-idx (meta (find e ret)))
                        in-idx (:in-idx (meta (find e args)))]
                    {:label (str (if out-idx (str out-idx " -> ") "") e (if in-idx (str " -> " in-idx) ""))
                     :color (if (highlight n1 n2 ir-graph)
                              "red"
                              "black")}))
                ;:options {:splines "ortho"}
                )))


(defn render-to-file
  ([graph-type graph] (render-to-file graph-type graph (constantly false)))
  ([graph-type graph highlight]
   (io/make-parents ir-graphs-directory)
   (spit (str ir-graphs-directory "/" graph-type ".dot") (render-ir-graph graph highlight))))



(defn graph-to-str [fns]
  (str
    "(let ["
    (join "\n      "
          (map
            (fn [{name :name args :args return :return id :id}]
              (str
                (if (symbol? return) return (into [] return))
                " (" (join " " (cons (str name "<" id ">") args)) ")"))
            fns))
  "]\n  "
  (:return (last fns))
  ")"))
