(ns com.ohua.util.ir
  (:use com.ohua.util.assert))



(defn map-from-coll [l]
  (into {} l))


(defn const [x]
  (fn [_] x))

(def zip (partial map vector))


(defn map-second-with-key [f coll]
  (map
    (fn [[key val]]
      [key (f key val)])
    coll))
