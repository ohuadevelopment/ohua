;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.util.debug)

(defn trace
  ([result] (println result) result)
  ([msg result]
   (println msg result)
   result))

