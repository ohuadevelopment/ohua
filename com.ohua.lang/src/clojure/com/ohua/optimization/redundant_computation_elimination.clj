;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;

(ns com.ohua.optimization.redundant-computation-elimination
  (:require [com.ohua.ir :as df-ir]))

(defn find-output-usages [ir ir-entry]
  (reduce concat (map (partial df-ir/find-usages ir) (df-ir/get-return-vars ir-entry))))

; TODO fix collect optimisation
;(let [result-merge (long-array 10)]
;  (let [ohua-code (ohua (let [prod (produce)]
;                          (collect (cond (< prod 3) (add prod 100)
;                                         (< prod 5) (add prod 200)
;                                         (> prod 4) (subtract prod 3))
;                                   result-merge)) :test-compile)]
;
; For code like this, where the collect input comes from outside we cannot optimise away the `collect` because it may have side effects
; or at least I saw `collect` being used that way and I wonder if that is an intended usage that we should support

(defn eliminate-redundant- [ir fn-set]

  (doall
    (remove #(and
               (fn-set (:name %))
               (or (not (:return %))
                   (empty? (df-ir/get-return-vars %))
                   ;(and (vector? (:return %)) (empty? (:return %)))
                   (empty? (find-output-usages ir %)))
               )
            ir)))

(defn eliminate [{ir :graph :as df-ir}]
  (assoc df-ir :graph (loop [curr-ir ir]
                        (let [updated-ir (eliminate-redundant- curr-ir
                                                               #{'com.ohua.lang/collect
                                                                 'com.ohua.lang/one-to-n
                                                                  ; FIXME merge should be namespaced and a symbol!
                                                                  "merge"
                                                                  "select"
                                                                 'com.ohua.lang/merge
                                                                 'com.ohua.lang/select
                                                                 'com.ohua.lang/algo-out
                                                                 'com.ohua.lang/id
                                                                 'com.ohua.lang/seq})]
                          (if (= (count updated-ir) (count curr-ir))
                            updated-ir
                            (recur updated-ir))))))
