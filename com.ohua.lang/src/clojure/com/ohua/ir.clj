;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.ir
  (:require [clojure.set :as setlib]
            [com.ohua.util.ir :refer [map-from-coll]]
            [clojure.zip :as zip])
  (:use com.ohua.util.assert)
  (:import (clojure.lang PersistentVector PersistentList)))


(defrecord IR [graph ctxt-map])


(defrecord IRFunc [id name args return])


(defrecord IRCursor [history history-pos])


(defrecord IRGraphPosition [ir curr-nodes visited dependencies])


(defmulti fn->id type)
(defmethod fn->id IRFunc [f] (.-id f))
(defmethod fn->id Long [id] id)



(defn fn-name-in [set]
  (fn [f]
    (contains? set (.-name f))))


(defn fn-name-is [name func] (= name (.-name func)))


; Utility functions
(defn- mk-pred [args]
  (if (coll? args)
    (set args)
    (partial = args)))


(declare get-return-vars)
(defn get-producer [produced ir]
  (let [pred (if (coll? produced)
               (set produced)
               #{produced})]
    (first (filter (comp (partial setlib/subset? pred) set get-return-vars) ir))))


(defn get-consumers [consuming ir]
  (let [pred (mk-pred consuming)]
    (filter (comp (partial some pred) :args) ir)))


(defn predecessors [node graph]
  (let [pred (set (.-args node))]
    (filter
      #(some pred (get-return-vars %))
      graph)))


(defn successors [node graph]
  (let [pred (set (get-return-vars node))]
    (filter
      #(some pred (.-args %))
      graph)))


(defn replace-nodes [replacements graph]
  "
  Directory replaces each node in (keys replacements) with whatever the node is mapped to in replacements
  if you want to replace the node with multiple new nodes see update-graph
  "
  (replace
    (if (map? replacements) replacements (map-from-coll replacements))
    graph))


(defn replace-node
  "Replace all nodes which can be considered equal to the provided node by the new node."
  [old new graph]
  (replace-nodes {old new} graph))


(defn drop-node
  "Remove all nodes which can be considered equal to the provided node from the graph."
  [node graph]
  (remove (partial = node) graph))


(defn drop-nodes
  "Remove all node which can be considered equal to any of the nodes provided from the graph."
  [nodes graph]
  (remove (partial contains? (set nodes)) graph))


(defn change-nodes-where
  "Applies the alter function to any nodes matching 'pred' replaces each node by the new node returned from alter-func.
  (alter-func node) must return a single new node."
  [pred alter-func graph]
  (map (fn [node] (if (pred node) (alter-func node) node)) graph))


(defn update-nodes-where
  "Applies the alter function to any nodes matching 'pred' replaces each node by the new nodes returned from alter-func.
  (alter-func node) must return an iterable of nodes."
  [pred alter-func graph]
  (mapcat
    (fn [node]
      (if (pred node)
        (alter-func node)
        (list node)))
    graph))


(defn update-graph
  "Update the IR graph with a map of functions to sequences of functions"
  [replacement graph]
  (into [] (mapcat #(get replacement % [%]) graph)))


(defn drop-nodes-where
  "Remove all nodes from the graph which satisfy the predicate."
  [predicate graph]
  (update-nodes-where
    predicate
    (constantly nil)
    graph))


(defn reindex-stuff [target thing]
  (map-indexed (fn [index item] (vary-meta item assoc target index)) thing))


(defn mk-producer-map [graph] (into {} (mapcat #(map vector (get-return-vars %) (repeat %)) graph)))

(defn mk-func
  "Create a new IRFunc."
  ([name args return] (mk-func nil name args return))
  ([id name args return]
   (->IRFunc id
             name
             ; if none of the args specifies :in-idx then I will compute the indexes for you otherwise you better get your
             ; indexes right yourself.
             (if (not (reduce (fn [prev arg] (or prev (contains? (meta arg) :in-idx))) false args))
               (into [] (reindex-stuff :in-idx args))
               args)
             return)))

(defmulti get-return-vars (fn [ir-fn] (type (:return ir-fn))))
(defmethod get-return-vars clojure.lang.PersistentVector [ir-fn] (:return ir-fn))
(defmethod get-return-vars :default [ir-fn] [(:return ir-fn)])

(defn print-ir [input]
  (doseq [i input]
    (println (:return i) (:name i) (:args i))))

(defn mk-producers-consumers [ir]
  (reduce
    (fn [[producers consumers] function]
      (let [new-producers
            (reduce
              #(assoc %1 %2 function)
              producers
              (get-return-vars function))
            new-consumers
            (reduce
              (fn [consm arg]
                (if (contains? consm arg)
                  (update-in consm [arg] #(conj % function))
                  (assoc consm arg #{function})))
              consumers
              (:args function))]
        [new-producers new-consumers]))
    [{} {}]
    ir))


(defn current-position [{history :history pos :history-pos}]
  (nth history pos))


(defn first-position
  "docstring"
  [ir]
  (->IRGraphPosition ir (set (filter #(empty? (:args %)) ir)) #{} #{}))


(defn init-cursor [ir]
  (->IRCursor
    [(first-position ir)]
    0))


(defn next-satisfied [ir dependencies visited]
  (set
    (filter
      (fn [function]
        (and
          (not (contains? visited function))
          (every? dependencies (:args function))))
      ir)))


(defn next-nodes-with [{ir :ir curr-nodes :curr-nodes dependencies :dependencies visited :visited}]
  (let [new-dependencies
        (reduce
          (fn [deps node] (reduce conj deps (get-return-vars node)))
          dependencies
          curr-nodes)
        new-visited (setlib/union visited curr-nodes)
        new-nodes (next-satisfied ir new-dependencies new-visited)]
    (->IRGraphPosition ir new-nodes new-visited new-dependencies)))


(defn cursor-next [cursor]
  (let [{history     :history
         history-pos :history-pos} cursor
        new-history-pos (inc history-pos)]
    (if-not (= new-history-pos (count history))
      (update-in cursor [:history-pos] inc)
      (let [curr-pos (nth history history-pos)
            curr-nodes (:curr-nodes curr-pos)]
        (if (empty? curr-nodes)
          cursor
          (-> cursor
              (update-in [:history] #(conj % (next-nodes-with curr-pos)))
              (update-in [:history-pos] inc)))))))


(defn cursor-previous [cursor]
  (update-in cursor [:history-pos] #(if (= 0 %) 0 (dec %))))


(defn mk-name-gen [taken]
  (let [chars (map (comp str char) (range 97 123))
        N (cons "" (iterate inc 0))
        possible-names (for [dig N
                             chr chars]

                         (str chr dig))]
    (remove taken possible-names)))


(defn name-gen-from-graph [ir-graph]
  (->> (concat
         (mapcat :args ir-graph)
         (map :name ir-graph)
         (mapcat get-return-vars ir-graph))
       (map str)
       (set)
       (mk-name-gen)))


(defn name-finder
  "Given a set of taken names finds a new name for it which hasn't been taken yet."
  ([my-set] (name-finder my-set nil))
  ([my-set name]
   (let [chars (if (nil? name)
                 (map (comp str char) (range 97 123))
                 [name])
         N (cons "" (iterate inc 0))
         possible-names (for [dig N
                              chr chars]

                          (symbol (str chr dig)))]
     (first (remove my-set possible-names)))))


; [IRFunc] -> [IRFunc]
(defn ssa
  "Rename all bindings in the ir such that every binding is only created once."
  [source]
  (nth
    (reduce
      (fn [[identifiers replacements accumulator] function]
        (let [new-args
              (into []
                    (map
                      (fn [arg]
                        (if-let [i (identifiers arg)]
                          i
                          (throw (Exception. (str "Function " name " expects argument " arg " but was not previously created.")))))
                      (:args function)))
              [new-identifiers new-replacements new-return]
              (reduce
                (fn [[identifiers replacements returns] value]
                  (let [new-name (name-finder replacements value)]
                    [(assoc identifiers value new-name)
                     (conj replacements new-name)
                     (conj returns new-name)]))
                [identifiers replacements []]
                (get-return-vars function))
              new-func (->IRFunc (:id function) (:name function) new-args new-return)]
          [new-identifiers new-replacements (conj accumulator new-func)]))
      [{} (set (map :name source)) []]                      ; map of identifiers, used replacements, output ir
      source)                                               ; initial ir
    2))


; helper function
(defn- filter-with [containment-selector test-space-selector ir function]
  (set
    (let [my-set (set (containment-selector function))]
      (filter
        (fn [f]
          (some
            my-set
            (test-space-selector f)))
        ir))))


; [IRFunc] -> IRFunc -> #{IRFunc}
(def step-down (partial filter-with get-return-vars :args))

; [IRFunc] -> IRFunc -> #{IRFunc}
(def step-up (partial filter-with :args get-return-vars))


(defn validate-graph
  "Verify the ir graph with its current state and function ordering resembles a runnabel clojure program."
  [ir]
  (first
    (reduce
      (fn [[status identifiers] function]
        (let [new-status
              (and status (every? identifiers (:args function)))]
          [new-status (into identifiers (get-return-vars function))]))
      [true #{}]
      ir)))


; finds all immediate redundant output
(defn find-redundant-output [ir]
  (let [used-identifiers
        (reduce
          (fn [identifiers function]
            (into identifiers (:args function)))
          #{}
          ir)]
    (set
      (apply concat
             (map
               (fn [function]
                 (remove used-identifiers (get-return-vars function)))
               ir)))))


(defn find-redundant-functions [output-vals ir]
  (loop [used-identifiers (setlib/union output-vals (set (apply concat (map :args ir))))
         unused-functions #{}
         used-functions (set ir)]
    (let [new-unused-functions
          (set
            (filter
              (fn [function]
                (not (some used-identifiers (get-return-vars function))))
              used-functions))]
      (if (empty? new-unused-functions)
        [used-functions unused-functions]
        (let [new-unused-identifiers
              (set (apply concat (map :args new-unused-functions)))]
          (recur
            (setlib/difference used-identifiers new-unused-identifiers)
            (setlib/union unused-functions new-unused-functions)
            (setlib/difference used-functions new-unused-functions)))))))

(defn update-args [functions]
  (reduce (fn [new-map [id function]]
            (assoc new-map id
                           (update function :args
                                   (fn [args]
                                     (into [] (map #(vary-meta (second %) assoc :in-idx (first %)) (sort-by first (seq args))))))))
          {} functions))


(defn gen-from-op-list [ops arcs all-env-args ctxt-map]
  ; env-args :: [{:op-id :: Long, :args :: [{:arg :: a, :in-idx :: Long}]}]
  (let [name-map (reduce
                   (fn [my-map {identifier :op-id name :type}]
                     (assoc my-map identifier (->IRFunc identifier name {} [])))
                   {}
                   ops)
        name-gen (let [a (atom (map symbol (mk-name-gen (set (vals name-map)))))]
                   (fn []
                     (first (swap! a rest))))
        ; {:op-id x :out-idx 10} -> i (var name)
        data-source-addresses (reduce
                                (fn [mapping {data-address :source}]
                                  (if (contains? mapping data-address)
                                    mapping
                                    (assoc mapping data-address (name-gen))))
                                {}
                                arcs)

        ; arcs :: [{:source :: {:op-id, :out-idx} , :target :: {:op-id, :in-idx}]
        mapped-inputs (group-by (comp :op-id :target) arcs)
        mapped-outputs (group-by (comp :op-id :source) arcs)

        inputs-added (reduce
                       (fn [nmap [id inputs]]
                         (update-in nmap [id :args] #(reduce (fn [m {{idx :in-idx} :target source :source}]
                                                               (assoc m idx (data-source-addresses source))) % inputs)))
                       name-map
                       mapped-inputs)

        outputs-added (reduce
                        (fn [nmap [id o_]]
                          (let [[{idx_ :out-idx} :as outputs] (map :source o_)
                                out (map (fn [{idx :out-idx :as a}]
                                           (vary-meta (data-source-addresses a) assoc :out-idx idx))
                                         (sort-by :out-idx (distinct outputs)))]
                            (assoc-in nmap [id :return]
                                      (if (= -1 idx_)
                                        (if (= 1 (count out))
                                          (first out)
                                          (throw (RuntimeException. (str "Output idx -1 cannot be used with destructuring: " out))))
                                        (into [] out)))))
                        inputs-added
                        mapped-outputs)

        ; Insert env args
        ; env-args :: [{:op-id :: Long, :args :: [{:arg :: a, :in-idx :: Long}]}]
        functions (reduce
                    (fn [fn-map {op-id :op-id env-args :args}]
                      (update-in fn-map [op-id :args]
                                 #(reduce (fn [args {idx :in-idx arg :arg}] (assoc args idx (vary-meta (list 'fn [] arg) assoc :in-idx idx))) % env-args)))
                    outputs-added
                    all-env-args)

        _ (doall (map (partial assert-type IRFunc) (vals functions)))
        ; go over the ir-functions and finalize the args list
        final-ir-graph (update-args functions)
        _ (doall (map (partial assert-type IRFunc) (vals final-ir-graph)))
        ]
    ;[(vals final-ir-graph) data-source-addresses]
    (->IR (vals final-ir-graph) ctxt-map)))

(defn- convert-fn [ir-entry]
  {:op-id (:id ir-entry) :type (:name ir-entry)})

(defn find-usages [ir var]
  (mapcat
    (fn [ir-fn]
      (map
        (fn [in-var] {:op-id (:id ir-fn) :in-idx (:in-idx (meta in-var))})
        ; get the slots where var is used
        (filter
          (partial = var)
          (:args ir-fn))))
    (filter #(some (partial = var) (:args %)) ir)))

(defn- convert-variables
  "Converts all defined variables into dependencies for their usage."
  [ir {id :id ret :return :as ir-entry}]

  (mapcat
    (fn [var]
      (if-not (list? var)
        (map
          (fn [usage]
            {:source {:op-id id :out-idx (if-let [from-meta (:out-idx (meta var))]
                                           from-meta
                                           (if (vector? ret)
                                             (.indexOf ret var)
                                             -1))}
             :target usage})
          (find-usages ir var))))
    (get-return-vars ir-entry)))

(defn op-list-from-ir
  "Converts the IR into the raw format that is used by the compiler to produce code that creates the dataflow graph
   in the engine and executes it.
   Entries for functions/operators have the form: {:op-id 101 :type com.ohua.lang/smap-fun}
   Entries for variables have the form: {:source {:out-idx -1 :op-id 101}} {:target {:in-idx 0 :op-id 102}}"
  [{ir :graph}]
  (let [gen-op-id (atom (reduce max (keep :id ir)))
        identified-ir (map #(if (.-id %) % (assoc % :id (swap! gen-op-id inc))) ir)]
    [(map convert-fn identified-ir)
     (mapcat (partial convert-variables identified-ir) identified-ir)
     (mapcat (fn [{^PersistentVector args :args :as ir-fn}]
               (let [env-args (filter #(or (list? %) (seq? %)) args)]
                 (if-not (empty? env-args)
                   [{:op-id (.-id ir-fn)
                     :args  (map (fn [^PersistentList a]
                                   ; a = (fn [] arg)
                                   {:in-idx (if-let [from-meta (:in-idx (meta a))]
                                              from-meta
                                              (.indexOf args a))
                                    :arg    (nth a 2)       ; maybe in future don't remove the function wrapper
                                    }) env-args)}])))
             identified-ir)
     ]))

(defn apply-transformations [ir transformations]
  ((apply comp (reverse transformations)) ir))

(defn reorder-by-dependencies [ir]
  (let [new-ir
        (loop [position (first-position ir)
               ir []]
          (if (empty? (:curr-nodes position))
            [position (into [] ir)]
            (recur
              (next-nodes-with position)
              (concat ir (:curr-nodes position)))))]
    (if (every? (set ir) new-ir)
      new-ir
      (throw (Exception. "Invalid graph")))))

(defn print-fn-with-arg-idx [ir-fn]
  (print-str "Invalid argument meta data for the following IR function: " (print-str ir-fn) " arg indexes:"
             (print-str (into [] (map #(print-str (:in-idx (meta %))) (:args ir-fn))))))


(defn validate
  ([{ir :graph :as df-ir}] (validate ir "<default>"))
  ([{ir :graph :as df-ir} transformation-step]
   (assert (not (empty? ir)) (str "Empty IR detected at transformation step -> " transformation-step "!"))
   df-ir))
