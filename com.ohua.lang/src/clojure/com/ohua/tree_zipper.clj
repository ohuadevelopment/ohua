;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;

;;;
;;; This zipper for standard Clojure data structures was taken from:
;;; http://www.ibm.com/developerworks/library/j-treevisit/
;;;

(ns com.ohua.tree-zipper
  (:require [clojure.string :as string]
            [clojure.zip :as zip]
            [com.ohua.logging :as l])
  (:import [clojure.lang IPersistentVector IPersistentMap IPersistentList ISeq]))


;;;
;;; This is the heart of the algorithm: a zipper based code walk.
;;; Every pass must provide a matcher to select the item where it wishes to be dispatched to and
;;; an editor to modify or work on it.
;;;
(defn walk-code
  ([zipper matcher editor] (walk-code zipper matcher editor conj))
  ([zipper matcher editor combiner] (walk-code zipper matcher editor combiner []))
  ([zipper matcher editor combiner init]
   (loop [found init
         loc zipper]
	   (if (zip/end? loc)
	     [(zip/root loc) found]
       (if-let [matcher-result (matcher loc)]
           (let [[new-loc editor-result] (editor loc)]
             (if new-loc
               (if editor-result 
                 (recur (combiner found editor-result) new-loc)
                 (recur found new-loc))
               (recur found (zip/next loc))
             ))
           (recur found (zip/next loc))
       )))))

(defmulti tree-branch? class)
(defmethod tree-branch? :default [_] false)
(defmethod tree-branch? IPersistentVector [v] true)
(defmethod tree-branch? IPersistentMap [m] true)
(defmethod tree-branch? IPersistentList [l] true)
(defmethod tree-branch? ISeq [s] true)
(prefer-method tree-branch? IPersistentList ISeq)

(defmulti tree-children class)
(defmethod tree-children IPersistentVector [v] v)
(defmethod tree-children IPersistentMap [m] (seq m))
(defmethod tree-children IPersistentList [l] l)
(defmethod tree-children ISeq [s] s)
(prefer-method tree-children IPersistentList ISeq)

(defmulti tree-make-node (fn [node children] (class node)))
(defmethod tree-make-node IPersistentVector [v children] (with-meta (vec children) (meta v)))
(defmethod tree-make-node IPersistentMap [m children] (with-meta (apply hash-map (apply concat children)) (meta m)))
(defmethod tree-make-node IPersistentList [node children] (with-meta children (meta node)))
(defmethod tree-make-node ISeq [node children] (with-meta (apply list children) (meta node)))
(prefer-method tree-make-node IPersistentList ISeq)

(defn tree-zipper [node]
  (zip/zipper tree-branch? tree-children tree-make-node node))

(defn print-tree [zipper]
  (println "Walking tree ...")
  (loop [loc zipper]
    (if (zip/end? loc)
      (zip/root loc)
      (let [] 
        (println "node:" (zip/node loc) "meta:" (meta (zip/node loc)))
        (recur (zip/next loc))))))

(defn print-parents
  [zipper]
  (println "node:" (zip/node zipper))
  (println "parents:")
  (if (zip/up zipper)
    (loop [loc (zip/up zipper)]
      (if (zip/up loc)
        (do
          (println "parent:" (zip/node loc) "meta:" (meta (zip/node loc)))
          (recur (zip/up loc)))
        (println "root:" (zip/node loc) "meta:" (meta (zip/node loc)))))))

(defn move [loc n direction]
  (loop [idx n new-loc loc] (if (= idx 0) new-loc (recur (dec idx) (direction new-loc)))))

(defn root? [loc] (not (zip/up loc)))

(defn find-var-definition
  "The returned location is the closure that computes the value to be bound to this variable.
   The search is from the given location upwards."
  ([loc] (find-var-definition loc (zip/node loc)))
  ([loc var] (find-var-definition loc var (fn [l loc] (not l))))
  ([loc var exit-condition]
    (loop [l loc 
           path (list l)]
      (if (exit-condition l loc) 
        nil
        (if (and (zip/branch? l) (some #{(zip/node (zip/down l))} '(let let* clojure.core/let)))
          (let [binding-vector (zip/right (zip/down l))
                ; the point is to look only at the bindings that are to the left of where we came from!
                binding-context (if (= 1 (count (zip/lefts (first path))))
                                 (let [v (zip/lefts (second path))]
                                   ; we came up from a function so the list contains still our own local var definition. remove it.
                                   (if (even? (count v)) v (drop-last v))) 
                                  (zip/node binding-vector))
                var-binding-idxs (map-indexed #(if (sequential? %2)
                                                   (if (some #{var} (flatten %2)) %1 -1)
                                                   (if (= var %2) %1 -1))
                                                           (take-nth 2 binding-context))
                ; we take the last binding of the variable in this binding vector
                filtered-pos (if (empty? var-binding-idxs) -1 (apply max var-binding-idxs))]
            (if (< -1 filtered-pos)
              ; increment to fullfil the semantics of the find-var-definition function
              (move (zip/down binding-vector) (inc (* 2 filtered-pos)) zip/right)
              (recur (zip/up l) (conj path l))))
          (if (and (zip/branch? l) (some #{(zip/node (zip/down l))} '(fn fn* clojure.core/fn)))
            (let [var-vector (cond (= (zip/node (zip/down l)) 'fn) (zip/right (zip/down l))
                            (= (zip/node (zip/down l)) 'fn*) (zip/down (zip/right (zip/down l)))
                            :default (throw (Exception. "Unknown function expansion encountered.")))
                  var-binding-idxs (map-indexed #(if (= var %2) %1 -1) (zip/node var-vector))
                  filtered-pos (if (empty? var-binding-idxs) -1 (apply max var-binding-idxs))]
              (if (< -1 filtered-pos)
                (move (zip/down var-vector) filtered-pos zip/right)
                (recur (zip/up l) (conj path l))))
            (recur (zip/up l) (conj path l))
            )
          )))))

(defn find-closure [loc closure]
  (loop [l (zip/leftmost loc)]
    (if (or (not l) (= l (zip/root loc)))
      nil
      (if (= (-> l zip/leftmost zip/node) closure)
        (zip/up l)
        (recur (zip/up l))))))

(defn find-var-usage [locs var]
  (loop [l (tree-zipper locs)
         result []]
    (if (zip/end? l)
        result
        (if (= (zip/node l) var)
          (recur (zip/next l) (conj result l))
          (recur (zip/next l) result))
      )
    ))

(defn find-var-targets [locs var]
  (let [usages (doall (find-var-usage locs var))]
    (map
      #(loop [v (zip/up %)]
         (do
           (l/printline "node:" (zip/node v))
           (if (contains? (meta (zip/node v)) :op-id)
             (do 
               (l/printline "found" (zip/node v) (:op-id (meta (zip/node v))))
               v)
             (recur (zip/up v)))))
      usages)
    ))

(defn in-scope? [scope loc]
  (let [var-def (find-var-definition loc (zip/node loc) (fn [l loc] (not l)))]
    var-def
  ))

(defn is-input? [func]
  (loop [origin func
         l (zip/up func)]
;    (l/printline "current node:" (if l (zip/node l)))
    (cond
      ; done
      (not l) false
      ; it's an op
      (:is-op? (-> l zip/node meta)) true
      ; it's in a let binding whose result is being used
      (and (vector? (zip/node l)) (zip/up l) (= (zip/node (zip/down (zip/up l))) 'let*) (not= (zip/node (zip/left origin)) '_)) true
      ; recursion
      :else (recur l (zip/up l)))
  ))

;;;
;;; Found at: https://groups.google.com/forum/#!topic/clojure/FIJ5Pe-3PFM
;;;
(defn end
  "returns the location loc where (end? (next loc)) is true."
  [loc]
  (loop [loc loc]
    (let [loc (zip/rightmost loc)]
      (if (zip/branch? loc)
        (recur (zip/down loc))
        loc))))

(defn skip
  "returns the next location that is not a child of this one"
  [start-loc]
  (loop [loc start-loc]
    (cond
      ; can't skip, jump to end
      (nil? loc) (zip/next (end start-loc))
      ; at end
      (zip/end? loc) loc
      ; go to right/up
      true (or (zip/right loc)
               (recur (zip/up loc))))))

(defn- sibling-locs [loc direction]
  (loop [l (direction loc)
         rights []]
    (if (not l)
      rights
      (recur (direction l) (conj rights l)))))
(defn rights [loc] "Delivers the locations of the right siblings (not nodes!)." (sibling-locs loc zip/right))
(defn lefts [loc] "Delivers the locations of the left siblings (not nodes!)." (sibling-locs loc zip/left))

(defn global-var? [loc]
  (contains? (ns-map *ns*) (zip/node loc)))

(defn clj-context-local? [env loc]
  (contains? env (zip/node loc)))

(defn is-lvar?
  ([loc]
    (and
      (not (empty? (zip/lefts loc))) ; not a function name
      (not (zip/branch? loc)) ; not a function invocation
      (not (vector? (zip/node (zip/up loc)))) ; not a variable definition in a binding
      (symbol? (zip/node loc)) ; variables are always symbols
      ; it is totally ok to also be defined as a global var! --> overshadowing.
      ;(not (global-var? loc)) ; make sure it is not a global variable
      )
    )
  ([loc env]
    (and (is-lvar? loc)
         ; checks for local bindings
         (not (clj-context-local? env loc))))
  )
