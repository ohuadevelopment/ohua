;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform
  (:require [clojure.walk :as walk]
            [com.ohua.logging :as l]))

; FIXME does not work anymore! needs an explicit merge! the question is whether this should still be a macro or a real built-in symbol of our language.
;       this code should probably be transformed to a true transformation!

;;;
;;; 'branches' must be defined at compile-time.
;;; 'num' can be defined at runtime.
;;;
(defn balance-it [branches num code input]
  (if (< branches 2) code
    (concat
      `(let 
       ; destructuring vector (make sure "balance" is unqualified)
        [~(vec (map-indexed (fn [idx item] (symbol (str item "-" idx))) (repeat branches input))) (~'balance ~input ~branches ~num)])
       ; let body statements
       (map-indexed 
         (fn [idx item] (walk/postwalk-replace {input (symbol (str input "-" idx))} item)) 
         (repeat branches code)))))
;;;
;;; Due to the call to repeat, it is not possible to pass a variable for 'num' into the macro!
;;;
(defmacro || 
  ([num code input] (balance-it num num code input))
  ; this allows to provide the number of branches created in the flow graph at compile-time and the dispatching 
  ; (and therewith the degree of parallelism) at runtime.
  ([branches num code input] 
;    (balance-it branches num code input)
    (throw (java.lang.UnsupportedOperationException. "|| is currently not supported.")))
)
