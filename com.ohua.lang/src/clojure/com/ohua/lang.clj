;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.lang
  (:require [com.ohua.compile :refer [ohua-]]
            [com.ohua.transform.result :as result]
            [com.ohua.walk :as walk]
            [com.ohua.lang.algo :refer [->Algo]]
            [com.ohua.transform.symbol-resolution :as symres]
            [com.ohua.link :as link])
  (:import (java.util Collection)
           (com.ohua.transform.symbol_resolution UnresolvedSymbolError)))

;;;
;;; A namespace where all global functions from the Ohua language are defined in.
;;;

(defn smap
  "A stateful version of the higher-order map function. Functionality is implemented as a source code transformation that uses stateful functions. 
   Note that coll does not only need to be iterable but also provide a size which may be unbounded (-1)."
  [fn ^Collection coll]
  ; implemented as an AST traversal
  )

(defn smap-io
  "Works exactly the same as 'smap' but accepts an iterable only instead of a collection that knows its size."
  [fn ^Iterable coll]
  ; implemented as an AST traversal
  )

(defn seq
  "Enforces a dependency such that 'a' executes before 'b'."
  [a b]
  ; implemented as an IR transformation
  )

(defn algo*
  "This is the same as (fn* ([args] body)) but for Ohua algorithms."
  [& seqs]
  ; implemented as an AST traversal
  )

;;; Use cases:
;(smap
;  (algo [d]
;        (some-function d))
;  data)
; ((algo [d]
;        (some-function d)) some-d)
; --> should be transformed into the thing below

;(defalgo my-algo [d]
;         (some-function d))
;
;(smap my-algo data)
;(my-algo some-d)

;;; Advanced:
;(let [my-algo (algo [d]
;                    (some-function d))]
;      (my-algo some-d)
;   )
; --> is essentially the very same as above but the value is stored in a local variable.
;     if it was defined outside ohua then we can just check &env and find it there.
;     if it was defined inside ohua then it seems that we need a transformation pass that inlines
;     it whereever the var is used. -> in fact it is entirely unclear what it means to pass an algorithm as an argument to a stateful function!
;

(defn algo-literal [code]
  (let [collected-args (atom '())
        _ (clojure.walk/postwalk (fn [f]
                                      (if (and (symbol? f) (.startsWith (name f) "%"))
                                        (swap! collected-args #(distinct (conj %1 %2)) f))
                                      f) code)
        args (sort @collected-args)]
    ;(println "collected:" collected-args)
    ;(println "producing:" `(com.ohua.lang/algo [~@args] ~code))
    `(com.ohua.lang/algo [~@args] ~code)
    )
  )

(defn algo*
  "Final 'macro' that the transformation step is sensitive to."
  []
  )


; use the functionality of 'fn'
(defn
  algo-
  [env form & sigs]
  (let [[_ & clj-fn] (walk/macroexpand-all `(fn ~@sigs))
        ;_ (println "clj-fn:" clj-fn)
        [algo-name & bodies] (if (symbol? (first clj-fn))
                               clj-fn
                               (cons 'unnamed-algo clj-fn))
        generated-name (gensym "__generated__algo__")
        inner-code (with-meta `(com.ohua.lang/algo* ~algo-name ~@bodies) (meta form))
        ]
    ; output is the var itself which references the algo
    ; we need to use this little workaround here because 'def' is also a macro but we want to execute this code only inline (like a function)!
    (intern *ns* generated-name (->Algo *ns* env algo-name inner-code))
    generated-name))

(defmacro algo [& sigs] (apply algo- &env &form sigs))

; use the functionality of 'defn'
(defmacro
  defalgo
  [name & decls]
  (if-not (symbol? name) (throw (RuntimeException. "algo name must be symbol")))
  (intern *ns* name (var-get (resolve (apply algo- &env &form name decls))))
  )


(defmacro defsfn [name init-expr & body]
  `(defn ~(vary-meta name assoc :init-state `(quote ~init-expr)) ~@body))


(defmacro ohua
  ; some documentation
  ([code] (ohua- code '(:run) &env &form))
  ([code & option] (ohua- code option &env &form)))

(defmacro <-ohua
  ([code] (result/transform-ohua &env &form code '(:run)))
  ([code & option] (result/transform-ohua &env &form code option)))


(defmacro ohua-require [& args] (apply link/ohua-require-fn args))
(def ohua-require-fn link/ohua-require-fn)
