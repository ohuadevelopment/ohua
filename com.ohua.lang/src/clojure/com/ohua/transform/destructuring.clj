;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.destructuring
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [clojure.reflect :as reflect]
            [clojure.set :as set]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l]))

(defn destructure-matcher [loc]
  (some #{'let 'let* 'clojure.core/let} [(zip/node loc)]))

(defn destructing? [index binding-vector]
  (if (> (count binding-vector) (inc index))
    (let [var (nth binding-vector (dec index))
          next-form (nth binding-vector (inc (inc index)))]
      (and (seq? next-form) (= (first next-form) 'clojure.core/nth)
           (= (second next-form) var)))
    false)
  )

(defn destructure-editor
  ;; change (let [[args] x] body) to (let [[args] (destructure x)] body)
  [loc]
  (let [binding-vector (zip/node (zip/next loc))
        dest-sources-idx
        (keep-indexed
          (fn search-victims [index item] (if
                                            (and
                                              (odd? index)
                                              (symbol? item)
                                              (destructing? index binding-vector)
                                              )
                                            index))
          binding-vector)
        ]
    (loop [i 0
           l (-> loc zip/next zip/next)]                    ; move to the first element in the binding vector
      (if (>= i (count dest-sources-idx))
        [(zip/leftmost l) nil]
        (recur (inc i) (let [source (t-zip/move l (nth dest-sources-idx i) zip/right)]
                         (zip/leftmost (zip/replace source `(com.ohua.lang/destructure ~(zip/node source)))))))
      )
    )
  )

(defn transform
  ([code] (transform #{} nil code))
  ([&env &form code]
   (l/printline "################################################################################################")
   (l/printline "Destructuring transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               destructure-matcher
                                               destructure-editor
                                               concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))