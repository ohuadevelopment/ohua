;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.result
  (:require [com.ohua.logging :as l]))

; this transformation preceeds everything. it is an early transformation producing 
; code that is evaluated not by the Ohua compiler but by the Clojure interpreter.
(defn transform-ohua
  ([code option] (transform-ohua #{} nil code option))
  ([env form code option]
    (l/printline "################################################################################################")
    (l/printline "'Result' transformation:")
    (let [transformed-code `(let [result# (new java.util.concurrent.atomic.AtomicReference nil)] 
                              (com.ohua.lang/ohua (com.ohua.lang/capture ~code result#) ~@option)
                              (.get result#))]
      (l/printline "transformed code:")
      (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
      (l/printline)
      transformed-code)))


