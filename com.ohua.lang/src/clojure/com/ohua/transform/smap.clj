;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.smap
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [com.ohua.link :as ln]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l]
            [com.ohua.transform.local-variable-scoping :as lvar-scope]))

(defn resolve-smap [loc]
  (let [anon-fn (-> loc zip/down zip/next)
        data (-> loc zip/down zip/rightmost)
        ; note: (expandall (algo [args] body)) -> (algo* ([args] body)) OR
        ;       (expandall (algo algo-name [args] body)) -> (algo* algo-name ([args] body))
        args (-> anon-fn zip/down zip/rightmost zip/down)
        body (-> anon-fn zip/down zip/rightmost zip/down zip/rightmost)]
    (if (= 'fn* (-> anon-fn zip/down zip/node )) (println "WARNING: Please use algorithms instead of functions as input to smap!" ))
    {:anon-fn anon-fn :data data :args args :body body}))

(defn smap-matcher [loc]
  (and (seq? (zip/node loc))
       (symbol? (zip/node (zip/next loc)))
       (= (zip/node (zip/next loc)) 'com.ohua.lang/smap)))

(defn smap-editor [&env out-of-ctxt-lvars loc]
  ;  (println "smap-editor" (zip/node loc))

  (let [resolved-smap-fn (resolve-smap loc)]

    ; fill the smap-template
    (let [smap-template (fn [smap-data]
                          ; we have to create the gensymed var at this point in order to pass it to the function.
                          ; otherwise we would create different variables as those would be created in different syntax-quote blocks.
                          ; see also: http://stackoverflow.com/questions/2840119/controlling-symbol-generation-in-clojure-macros
                          (let [size `s#]
                            `(let [d# ~smap-data
                                   ~size (com.ohua.lang/size d#)]
                               (let [~(zip/node (:args resolved-smap-fn)) (com.ohua.lang/smap-fun (com.ohua.lang/one-to-n ~size d#))
                                     ~@(mapcat concat (map (fn [lvar] `(~lvar (com.ohua.lang/one-to-n ~size ~lvar))) out-of-ctxt-lvars))]
                                 (com.ohua.lang/collect (com.ohua.lang/one-to-n ~size ~size) ~(zip/node (:body resolved-smap-fn)))))))]

      ; insert into the tree and continue the traversal at the body
      [(-> (zip/replace
             loc
             (smap-template (zip/node (:data resolved-smap-fn)))
             ) t-zip/skip zip/next)
       nil])))

(defn combined-matcher [_] true)
(defn combined-editor [collect-edit &env loc]
  ;  (println "combined editor:" (zip/node loc))
  (if (smap-matcher loc)
    ; recursion
    (let [[transformed-code lvars] (t-zip/walk-code (t-zip/tree-zipper (zip/node loc))
                                                    smap-matcher
                                                    (partial collect-edit &env) concat)]
      ;      (println "returning from transform" transformed-code)
      ;      (println "new zipper" (zip/replace loc transformed-code))
      ;      (println "new zipper - next" (zip/next (t-zip/skip (zip/replace loc transformed-code))))
      [(t-zip/skip (zip/replace loc transformed-code)) (filter #(not (t-zip/find-var-definition loc %)) lvars)])
    ; otherwise collect the local variables
    (if (lvar-scope/lvar-collect-matcher loc &env)
      (let [[new-loc var] (lvar-scope/lvar-collect-editor loc)] [new-loc [var]])
      [nil nil])
    )
  )

(defn collect-editor [&env loc]
  ;  (println "collect-editor:" (zip/node loc))
  (let [resolved-smap-fn (resolve-smap loc)
        [walked-loc collected-lvars] (t-zip/walk-code (t-zip/tree-zipper (zip/node (:body resolved-smap-fn)))
                                                      combined-matcher
                                                      (partial combined-editor collect-editor &env)
                                                      concat)
        out-of-ctxt-lvars (vec (remove (fn [lvar] (some #{lvar} (zip/node (:args resolved-smap-fn)))) (distinct collected-lvars)))
        new-tree (zip/replace (:body resolved-smap-fn) walked-loc)]
    ;    (println "found lvars:" collected-lvars)
    ;    (println "smap-code" (zip/node loc))
    ;    (println "walked-code" walked-loc)
    ;    (println "new-tree" (zip/node new-tree))
    ;    (println "new-tree -> smap" (-> new-tree zip/up zip/up zip/up zip/node))
    [(let [smap-rewrite (smap-editor &env out-of-ctxt-lvars (-> new-tree zip/up zip/up zip/up))]
       ;       (println "after smap-rewrite:" (zip/node (first smap-rewrite)))
       (first smap-rewrite))
     out-of-ctxt-lvars]))

;;;
;;; Code for IO version of the smap function starts here
;;;
(defn smap-io-matcher [loc]
  (and (seq? (zip/node loc))
       (symbol? (zip/node (zip/next loc)))
       (= (zip/node (zip/next loc)) 'com.ohua.lang/smap-io)))

(defn smap-io-editor [&env loc]
  (let [smap-seq-loc (cond (= (count (zip/rights (zip/next loc))) 2) loc ;(zip/up (zip/insert-right (zip/rightmost (zip/next loc)) 1))
                           ;(= (count (zip/rights (zip/next loc))) 3) loc
                           :default (throw (IllegalArgumentException. "Wrong number of arguments passed to smap-io (2).")))]

    (let [resolved (resolve-smap smap-seq-loc)]
      ; TBD is this really required?
      (if (not (= (count (zip/node (:args resolved))) 1))
        (throw (IllegalArgumentException. "Function passed to smap-io can only have single argument.")))

      ; do the rewrite
      (let [code `(let [[~(zip/node (zip/down (:args resolved))) size#] (com.ohua.lang/smap-io-fun ~(zip/node (:data resolved)))]
                    (com.ohua.lang/collect (com.ohua.lang/one-to-n size# size#) ~(zip/node (:body resolved))))]
        [;(-> (zip/replace loc code) zip/down zip/rightmost zip/down zip/next)
         (-> (zip/replace smap-seq-loc code) t-zip/skip zip/next)
         nil]))
    ))

(defn transform
  ([code] (transform #{} nil code))
  ([&env &form code]
   (l/printline "################################################################################################")
   (l/printline "smap transformation:")
   (let [[transformed-smap-code _] (t-zip/walk-code (t-zip/tree-zipper code)
                                                    ; enable the 'enter' transformation in the smap-editor by using the collect-editor
                                                    smap-matcher
                                                    (partial collect-editor &env)
                                                    concat)]
     (l/printline "transformed code:")
     (l/write transformed-smap-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     (l/printline "################################################################################################")
     (l/printline "smap-io transformation:")
     (let [[transformed-smap-io-code _] (t-zip/walk-code (t-zip/tree-zipper transformed-smap-code)
                                                         smap-io-matcher
                                                         (partial smap-io-editor &env)
                                                         concat)]
       (l/printline "transformed code:")
       (l/write transformed-smap-io-code :dispatch clojure.pprint/code-dispatch)
       (l/printline)
       transformed-smap-io-code))))
