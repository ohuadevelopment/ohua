;
; ohua : symbol_resolution.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.transform.symbol-resolution
  (:require [com.ohua.logging :as l]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.link :as linker])
  (:import (clojure.lang Keyword Symbol PersistentVector IObj)))


(deftype EnvVar [^Object value])
(deftype OhuaVar [^Keyword type expr])
(deftype AlgoArg [^Integer index])

; Errors
(defprotocol ResolutionError
  (to-human ^String [this] "Create a human readable string representation of this error.")
  (get-line ^Integer [this] "Get the source code line where this error ocurred."))

(deftype UnresolvedSymbolError [^Symbol symbol ^Integer line]
  ResolutionError
  (to-human [this] (str "Symbol " (.-symbol this) " could not be resolved in this context."))
  (get-line [this] (.-line this)))
(deftype TypeError [^String location ^Class expected-type ^Class received-type ^Integer line]
  ResolutionError
  (to-human [this]
    (if-let [t (.-expected_type this)]
      (str "Expected type " t " in " (.-location this) ", not " (.-received_type this) ".")
      (str "Unexpected type " (.-received_type this) " in " (.-location this) ".")))
  (get-line [this] (.-line this)))
(deftype BindingVectorLengthError [^Integer line]
  ResolutionError
  (to-human [this] (str "Binding vector length must be positive and even."))
  (get-line [this] (.-line this)))
(defn let-binding-type-error [received-type line]
  (assert (not (symbol? received-type)))
  (->TypeError "Binding" Symbol received-type line))
(defn binding-vector-type-error [received-type line]
  (assert (not (vector? received-type)))
  (->TypeError "Binding vector" PersistentVector received-type line))
(defn algo-binding-type-error [received-type line]
  (assert (not (symbol? received-type)))
  (->TypeError "Algo binding" Symbol received-type line))
(defn algo-binding-vec-type-error [received-type line]
  (assert (not (vector? received-type)))
  (->TypeError "Algo binding vector" PersistentVector received-type line))


(defn skip-symres? [opts]
  (or (false? (:symres opts)) (:skip-symbol-resolution opts)))


(declare matcher)
(declare editor)


(defn find-line [loc]
  (loop [loc loc]
    (if (nil? loc)
      nil
      (if-let [line (if-let [m (meta (zip/node loc))] (:line m))]
        line
        (recur (zip/up loc))))))



(defn undefined-err [loc] (->UnresolvedSymbolError (zip/node loc) (find-line loc)))

(defn node-with-line [loc]
  (let [n (zip/node loc)]
    (if (instance? IObj n)
      (vary-meta (zip/node loc) assoc :line (find-line loc))
      n)))

(defn check-tree [code scope]
  (t-zip/walk-code (t-zip/tree-zipper code) matcher (editor scope) concat []))


(defn check-expr [expr scope]
  (let [zipped-code (t-zip/tree-zipper expr)
        [transformed errs] (t-zip/walk-code zipped-code matcher (editor scope) concat [])
        val (zip/node zipped-code)
        type (cond
               (list? val) (case (zip/node (zip/down zipped-code))
                             (algo* com.ohua.lang/algo*) :algo
                             :value
                             )
               (symbol? val) (if-let [res (scope val)]
                               (if (instance? OhuaVar res)
                                 (.-type res)
                                 (if (resolve val)
                                   :constant
                                   :value))
                               :value)
               (or (number? val) (string? val) (nil? val)) :constant
               ; TODO
               ; also: should we check the type on the transformed code or the original one?
               :else :value
               )]
    [transformed type errs]))


(defn check-binding-vec [binding-vec-loc initial-scope]
  "Note the subtile difference of this function, as it returns not only the errors, but also the variables bound."
  (let [binding-vec (node-with-line binding-vec-loc)]
    (cond
      (not (vector? binding-vec)) [[] {} [(binding-vector-type-error (type binding-vec) (find-line binding-vec-loc))]]
      (not (even? (count binding-vec))) [[] {} [(->BindingVectorLengthError (find-line binding-vec-loc))]]
      :else
      (loop [loc (zip/next (t-zip/tree-zipper binding-vec))
             scope initial-scope
             errs []]
        (if (zip/end? loc)
          [(zip/root loc) scope errs]
          (let [bnd (zip/node loc)
                expr (zip/right loc)
                [nexpr type expr-errs] (check-expr (node-with-line expr) scope)
                nloc (zip/replace expr nexpr)
                next-loc (t-zip/skip nloc)
                is-sym (symbol? bnd)
                nscope (if is-sym (assoc scope bnd (OhuaVar. type nexpr)) scope)
                nerrs (concat
                        (if is-sym
                          errs
                          (conj errs (let-binding-type-error (clojure.core/type bnd) (find-line loc))))
                        expr-errs)]
            (recur
              next-loc
              nscope
              nerrs)))))))

(defn check-decl-body [loc scope]
  (loop [loc loc
         errs []]
    (let [[new-code new-errs] (check-tree (node-with-line loc) scope)
          combined-errs (concat errs new-errs)
          replaced (zip/replace loc new-code)]
      (if (empty? (zip/rights loc))
        [(t-zip/skip replaced) combined-errs]
        (recur
          (zip/right replaced)
          combined-errs)))))

(defn- check-let
  "Perform resolution check on a `let` form."
  [loc scope]
  (let [binding-loc (zip/right (zip/down loc))
        [new-binding new-scope errs] (check-binding-vec binding-loc scope)
        replaced (zip/replace binding-loc new-binding)
        decl-loc (zip/right replaced)
        [nloc decl-errs] (if (nil? decl-loc) [replaced nil] (check-decl-body decl-loc new-scope))]
    [nloc (concat errs decl-errs)]))


(defn check-algo-vec [binding-vec-loc]
  (let [binding-vec (zip/node binding-vec-loc)]
    (cond
      (not (vector? binding-vec)) [{} [(algo-binding-vec-type-error (type binding-vec) (find-line binding-vec-loc))]]
      (empty? binding-vec) [{} nil]
      :else (let [sym-checker
                  (fn [bindings]
                    (loop [loc (zip/down (t-zip/tree-zipper bindings))
                           index 0
                           errs []
                           scope {}]
                      (if (nil? loc)
                        [scope errs]
                        (let [val (zip/node loc)
                              nerrs (if (symbol? val) errs (conj errs (algo-binding-type-error (type val) (find-line loc))))
                              nscope (if (symbol? val) (assoc scope val (AlgoArg. index)) scope)]
                          (recur
                            (zip/right loc)
                            (inc index)
                            nerrs
                            nscope)))))]
              (if (< (count binding-vec) 2)
                (sym-checker binding-vec)
                (let [[last ampercent? & rest] (rseq binding-vec)
                      to-check (if (= '& ampercent?)
                                 (reverse (conj last rest))
                                 binding-vec)]
                  (sym-checker to-check)))))))


(defn check-algo
  "Check an algo*. Only used when constructing the algo"
  [loc scope]
  (let [algo*-loc (zip/down loc)
        algo*-sym (zip/node algo*-loc)
        algo-name?-loc (zip/right algo*-loc)
        binding-vec-loc (zip/down (if (symbol? (zip/node algo-name?-loc)) (zip/right algo-name?-loc) algo-name?-loc)) ; maybe this can be cleaner somehow? I use this to skip the alog name (if there is one)
        [new-bindings binding-errs] (check-algo-vec binding-vec-loc)
        new-scope (merge scope new-bindings)
        [new-loc decl-errs] (check-decl-body (zip/right binding-vec-loc) new-scope)]
    ; Im not sure that body expressions in clojure might not be able to create bindings too
    ; If so we need to add these to the scope as well
    [new-loc (concat
               ; HACK add this back once we deprecate (fn []) as input to smap
               ; (if-not (or (= 'algo* algo*-sym) (= 'com.ohua.lang/algo* algo*-sym)) [(->TypeError "Algo definition" 'algo* algo*-sym (find-line algo*-loc))])
               binding-errs
               decl-errs)]))

(defn check-algo- [code scope]
  (let [[replaced errs] (check-algo (t-zip/tree-zipper code) scope)]
    [(zip/root replaced) errs]))

(defn match-algo-local-vars [loc scope]
  (let [n (zip/node loc)
        m (meta n)
        [new-loc errs] (if (every? scope (:ohua-missing-symbols m))
                         [(zip/replace loc (vary-meta n dissoc :ohua-missing-symbols :ohua-missing-sym-errs)) nil]
                         [loc (:ohua-missing-sym-errs m)])]
    [(t-zip/skip new-loc) errs]))


(def ^:private matcher (comp not zip/end?))

(defn ohua-wrap [val] (OhuaVar. :constant val))

(defn- editor [scope]
  (fn [loc]
    (let [val (zip/node loc)]
      (cond
        (zip/branch? loc) (let [down (zip/down loc)
                                fun (zip/node down)]
                            (case fun                       ; check for special, scope changing sybols

                              ; handle let
                              (let* clojure.core/let*) (check-let loc scope)

                              ; handle fn
                              (algo* com.ohua.lang/algo*) (let [new-scope (if-let [s (:algo-scope (meta val))]
                                                                            (merge (ns-map (:algo-ns (meta val))) s)
                                                                            scope)]
                                                            (check-algo loc new-scope))

                              ; HACK Remove this when we drop support for (fn []) as input to smap
                              fn* (if (= 'com.ohua.lang/smap (zip/node (zip/left loc)))
                                    (check-algo loc scope)
                                    [(t-zip/skip loc) nil])

                              quote [(t-zip/skip loc) nil]
                              ; handle other scope changing **symbols** here

                              [(zip/next loc) nil]
                              ))

        (symbol? val) (let [[l e] (if (contains? scope val)
                                    [loc nil]
                                    (if-let [resolved (linker/resolve val)]
                                      [(zip/replace loc resolved) ; replaces symbol with fully qualified symbol
                                       nil]
                                      (if-let [resolved (resolve val)]
                                        [loc
                                         ; This should be used later (new ast)
                                         ; (zip/replace loc (EnvVar. (ohua-wrap resolved)))
                                         nil]
                                        [loc [(undefined-err loc)]])))]
                        [(t-zip/skip l) e])
        :else
        [(zip/next loc)
         (cond
           (number? val) nil
           (string? val) nil
           (keyword? val) nil
           (instance? Boolean val) nil
           (var? val) nil
           (nil? val) nil
           true nil                                         ; TODO find out if there are actual invalid cases here
           :else [(->TypeError "AST node" nil (type val) (find-line loc))])]))))


(defn report-errors [errors]
  (if-not (empty? errors)
    (binding [*out* *err*]
      (println "Symbol resolution failed for ns: " (str *ns*))
      (doseq [^ResolutionError e errors]
        (println (to-human e) " at line " (get-line e))))))


(defn traverse
  ; TODO change 'env' to hold the type information we would expect
  ([code] (traverse nil nil nil code))
  ([options env _ code]
   (l/printline "################################################################################################")
   (l/printline "Symbol resolution:")
   (if (skip-symres? options)
     code
     (let [[transformed errors] (check-tree code env)
           report-level (get options :symres :abort)]
       (l/pprint transformed)
       (case report-level
         (:abort :warn true) (report-errors errors)
         :else nil)
       (if (and (#{:abort true} report-level) (not (empty? errors)))
         (throw (RuntimeException. "Symbol resolution failed")))
       transformed))))
