;
; ohua : sfn_resolve.clj
;
; Copyright (c) Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.transform.sfn-resolve
  (:require [com.ohua.logging :as l]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip])
  (:use com.ohua.link))

; TODO write a test case with a local binding which has the same name as a stateful function and make sure it is **not** wrapped

(defn- matcher [loc]
  (and (symbol? (zip/node loc)) (not-empty (zip/lefts loc)) (is-unresolved-sfn? (zip/node loc))))

(defn- editor [loc]
  (println "editing" (zip/node loc))
  (let [new-expr `(make-sfn '~(zip/node loc))]
    [(t-zip/skip (zip/replace loc new-expr)) nil]))


(defn transform
  ([code] (transform #{} nil code))
  ([env form code]
   (l/printline "################################################################################################")
   (l/printline "Stateful function resolve transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               matcher
                                               editor
                                               concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))
