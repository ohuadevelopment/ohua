;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.higher-order-functions
    (:require [clojure.walk :as walk] 
          [clojure.zip :as zip]
          [com.ohua.link :as ln]
          [com.ohua.tree-zipper :as t-zip]
          [com.ohua.logging :as l]))


;;;
;;; This namespace implements the transformation to functional programming, i.e. the invocation of functions passed around in local variables.
;;; Therefore, it turns (local-var arg-1 arg-2) into (apply local-var arg-1 arg-2).
;;;

(defn lvar-function-call-matcher [ops loc]
  (and 
    (and (zip/up loc) (not (vector? (zip/node (zip/up loc))))) ; make sure it is a function call
    (empty? (zip/lefts loc)) ; we are at the function to be called
    (symbol? (zip/node loc)) ; local variable names are symbols
    (not (contains? ops (zip/node loc))) ; it's not a defined function (shortcut to avoid the below checks. beware: Clojure functions!)
    (not (special-symbol? (zip/node loc))) ; it's not a special form
    (do
;      (println "checking:" (zip/node loc))
;      (println "found var definition:" (t-zip/find-var-definition loc))

      ; this is certainly not very efficient. if we find this too often then it might make sense to compute 
      ; that information once for all transformation and the whole compilation. for now though, this is not
      ; an issue.
      (t-zip/find-var-definition loc))))

(defn lvar-function-call-editor [loc]
  (l/printline "arrived at:" (zip/node loc) "in:" (zip/node (zip/up loc)))
  [(zip/next (zip/insert-left loc (symbol "apply"))) nil])

(defn partial-call-matcher [loc]
;  (l/printline "arrived at:" (zip/node loc) (zip/node (zip/next loc)) (type (zip/node (zip/next loc))))
  (and 
    (and (zip/up loc) (not (vector? (zip/node (zip/up loc))))) ; make sure it is a function call
    (empty? (zip/lefts loc)) ; we are at the function to be called
    (= (zip/node loc) 'partial) ; it's a partial function call
    ; the first argument is a function reference (not a local or computation)
    (symbol? (zip/node (zip/next loc)))
    (not (t-zip/find-var-definition (zip/next loc)))))

(defn partial-call-editor [loc]
  (l/printline "arrived at:" (zip/node loc) "first arg:" (zip/node (zip/next loc)) "type:" (type (zip/node (zip/next loc))) (type zip/replace))
  [(zip/next (zip/replace (zip/next loc) `(com.ohua.link/resolve-and-init ~(str (zip/node (zip/next loc)))))) nil])

(defn transform 
  ([code] (transform #{} nil code))
  ([&env &form code]
  (l/printline "################################################################################################")
  (l/printline "Functional programming transformation:")
  (l/printline "Phase 1: 'apply' rewrite")
  (let [[transformed-apply-code _] (t-zip/walk-code (t-zip/tree-zipper code)
                                                    (partial lvar-function-call-matcher (set (ln/list-links)))
                                                    lvar-function-call-editor concat)]
    (l/printline "transformed code:")
    (l/write transformed-apply-code :dispatch clojure.pprint/code-dispatch)
    (l/printline)
    (l/printline "Phase 2: 'partial' rewrite")
    (let [[transformed-code _] (t-zip/walk-code (t-zip/tree-zipper transformed-apply-code)
                                              partial-call-matcher 
                                              partial-call-editor)]
      (l/printline "transformed code:")
      (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
      (l/printline)
    transformed-code))))
