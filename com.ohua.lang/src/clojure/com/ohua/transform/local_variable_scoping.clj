;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.local-variable-scoping
  (:require [clojure.walk :as walk]
            [clojure.zip :as zip]
            [clojure.reflect :as reflect]
            [clojure.set :as set]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l]
            [com.ohua.transform.algorithms :as algorithms]))

;;;
;;; The algorithm works as follows:
;;; - walk the code and stop at every if-condition.
;;; - recurse on nested if-conditions (non-tail recursive algorithm -> recurse first!)
;;; - collect all variables in the two different branches
;;; - rewrite the branch by adding (let [[var1 var2] (scope var1 var2)] ... branch-code-here ...)
;;; - recursion propogates the variables to the outer scope
;;; - done.
;;;

(defn lvar-collect-matcher [loc &env]
  (and
    (t-zip/is-lvar? loc &env)
    (not (:ohua-algo (meta (zip/node loc))))
    (not (algorithms/algorithm? &env (zip/node loc)))
    (do
      ;(println "start:" loc "\nfound:" (t-zip/find-var-definition loc))
      (not (t-zip/find-var-definition loc)))))

(defn lvar-collect-editor [loc]
  (l/printline "gathering:" (zip/node loc) (zip/node (zip/up loc)))
  [(zip/next loc) (zip/node loc)])

(defn condition-matcher [loc] (and (= (zip/node (zip/leftmost loc)) 'if) (< 1 (count (zip/lefts loc)))))
(defn condition-editor [lvar-collection-matcher lvar-collection-editor lvar-collection-combiner loc]
  (let [if-cond (t-zip/tree-zipper (zip/node loc))
        _ (l/printline "Walking:" (zip/node if-cond))
        [walked-loc collected-lvars] (t-zip/walk-code if-cond lvar-collection-matcher lvar-collection-editor lvar-collection-combiner)
        lvars (vec (distinct collected-lvars))]
    (l/printline "collected local variables:" lvars (map type lvars))

    [; transformation
     (if (empty? lvars)
       (zip/next loc)
       ; hook in the let closure
       (let [scope-code `(let [~lvars ~(cons 'scope lvars)] ~walked-loc)
             ;           _ (println "Code to be replaced:" (-> loc zip/up zip/node))
             new-loc (zip/replace loc scope-code)]
         ;       (println "generated code:" scope-code)
         (t-zip/skip new-loc)))
     lvars]
    ))

;;
;; combines the functionality of the two handlers above.
;;
(defn combined-matcher [_] true)
(defn combined-editor [level &env loc]

  ;FIXME there is a bug here! if we are traversing a branch that carries a conditional expression then we also need
  ; to scope the vars used in the condition and not only the ones used on the branches!

  (if (condition-matcher loc)
    ; recursion.
    (let [[new-loc lvars] (condition-editor combined-matcher (partial combined-editor (inc level) &env) concat loc)]
      ; only propagate the variables that are not defined in this closure's/zipper's context 
      ; (use the old location that is not advanced yet)
      [new-loc (filter #(not (t-zip/find-var-definition loc %)) lvars)])
    ; collection driven by the current walk.
    (if (and (lvar-collect-matcher loc &env)
             ; Optimization: locals assigned to functions that are called straight after the condition do not need to be scoped.
             ; if this is the top-level 'if' statement and the top-level function inside of it 
             ; then don't collect. -> optimization
             (not (and
                    (= 1 level)
                    (empty? (filter sequential? (concat (zip/rights loc) (zip/lefts loc))))
                    ; we have to go check the type of the variables here. if there are in-scope variables input to this 
                    ; function then it is not a top-level function 
                    (empty? (filter (partial t-zip/in-scope? (zip/root loc)) (t-zip/rights (zip/leftmost loc))))
                    )))
      ; the condition-editor returns a vector, but the lvar-collect-editor returns a single symbol only.
      (let [[loc var] (lvar-collect-editor loc)] [loc [var]])
      [nil nil])))

(defn transform
  ([code] (transform #{} nil code))
  ([&env &form code]
   (l/printline "################################################################################################")
   (l/printline "Local variable scoping transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               ; include the top-level 'if' statement
                                               condition-matcher
                                               (partial condition-editor combined-matcher (partial combined-editor 1 &env) concat) concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))
