;
; ohua : non_sfn_resolve.clj
;
; Copyright (c) Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.transform.non-sfn-resolve
  (:require [com.ohua.logging :as l]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.link :as linker :refer [sfn?]]
            [com.ohua.lang.algo])
  (:import (com.ohua.lang.algo Algo)))


(defn algo? [a] (instance? Algo a))

(defn trace [a] (println a) a)
(defn- matcher [loc]
  (and (zip/branch? loc)
       (loop [loc loc]
         (let [left (zip/left loc)]
           (cond
             (nil? loc) true
             (nil? left) (recur (zip/up loc))
             (and (#{'if 'clojure.core/if} (zip/node left))
                  (empty? (zip/lefts left))) false
             :else (recur (zip/up loc)))))
       (let [function-loc (zip/down loc)
             function (zip/node function-loc)]
         (or (and (symbol? function)
                  (not (linker/is-builtin? function))
                  (not (sfn? function))
                  (let [resolved (resolve function)]
                    (and
                      (var? resolved)
                      (let [retrieved (var-get resolved)]
                        (and
                          (not (algo? retrieved))
                          (ifn? retrieved)
                          ; HACK This should be replaced with correct destructuring handling, aka once #236 is done
                          (not= clojure.core/nth retrieved))))))
             (and (zip/branch? function-loc)
                  (#{'fn 'fn*} (zip/node (zip/down function-loc))))))))


(defn- editor [loc]
  (let [nloc (zip/down loc)
        n (zip/node nloc)
        _ (l/printline "Editing function" n)
        ; I am not sure we need (meta n) or (meta (resolve n)) here
        meta- (meta (resolve n))
        has-state (contains? meta- :init-state)
        func-inserted (zip/insert-left nloc 'com.ohua.lang/__call-clojure-fn)
        init-inserted (if has-state
                        (-> func-inserted
                            (zip/insert-right (:init-state meta-))
                            zip/right)
                        func-inserted)
        next (t-zip/skip init-inserted)]
    [next nil]))


(defn transform
  ([code] (transform #{} nil code))
  ([env form code]
   (l/printline "################################################################################################")
   (l/printline "Non stateful function resolve transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               matcher
                                               editor
                                               concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))
