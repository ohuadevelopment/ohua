;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;

(ns com.ohua.transform.algorithms
  (:require [com.ohua.tree-zipper :as t-zip]
            [com.ohua.logging :as l]
            [clojure.zip :as zip]
            [com.ohua.transform.symbol-resolution :as symres]
            [com.ohua.lang.algo]
            [clojure.string :refer [starts-with?]])
  (:import (clojure.lang Compiler$VarExpr IObj)
           (com.ohua.lang.algo Algo)))


(defn trace [thing]
  (l/printline thing)
  thing)

(defn resolve-ns-or-&env
  [env sym]
  ; this does not resolve local bindings
  (let [v (resolve env sym)]
    (cond
      (class? v) nil
      (var? v) (var-get (trace v))
      ;(try ; do a try here because some expressions just may not evaluate at all!
      (nil? v) (if (and (contains? env sym)
                        (instance? Compiler$VarExpr (.init (get env sym))))
        ; Clojure did not yet provide a clean way to resolve local variable bindings
        (let [e
              (.eval                                        ; just dereferences the var
                (.init (get env sym)))
              ;_ (println sym ">>" e (instance? Algorithm$IAlgorithm e))
              ]
          e
          )
        )
      :else (throw (Exception. (str "Unexpected type of resolved var <" (type v) ">")))
      ;(catch Exception _ nil)
      ;)
      )
    )
  )


(defn algorithm? [env sym]
  (if-let [res (resolve-ns-or-&env env sym)]
    (instance? Algo res)))

(defn local-algo-editor [loc]
  (let [algo (zip/node loc)                                 ; (algo* name? ([params] body) ...)
        [algo-name [algo-params & body]] (if (symbol? (second algo)) [(second algo) (nth algo 2)] [(gensym "algo-") (second algo)]) ; ['algo-123 ([params] body)]
        template (fn [params]
                   `(let [~algo-params (com.ohua.lang/algo-in nil ~@params)]
                      (com.ohua.lang/algo-out ~@body)))]
    (if (empty? (zip/lefts loc))
      ; algo invocation: (my-algo arg1 arg2)
      [(zip/next
         (zip/replace
           (zip/up loc)
           (template (zip/rights loc))))
       nil]
      (if (#{'smap 'com.ohua.lang/smap 'smap-io 'com.ohua.lang/smap-io} (zip/node (zip/leftmost loc)))
        ; algo passed to higher order function: (smap my-algo some-arg)
        [(zip/next
           (zip/replace
             loc
             `(com.ohua.lang/algo* (~algo-params ~(template algo-params)))))
         nil]
        (if (not (= (zip/node (zip/leftmost loc)) 'com.ohua.lang/algo-in))
          (throw (new IllegalArgumentException (str "Algorithm passed as argument to function not allowed: "
                                                    algo-name
                                                    " "
                                                    (zip/node (zip/up loc)))))
          )
        )
      )
    )
  )

(defn inline-global-algos
  ([code] (inline-global-algos {} nil code))
  ([env form code]
   (l/printline "################################################################################################")
   (l/printline "Algorithms inlining:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               #(let [sym (zip/node %)]
                                                  (and
                                                    (symbol? sym)
                                                    (if-let [one-left (zip/left %)] ; filter names of algos that have already been spliced
                                                      (let [res
                                                            (not (and (= 'com.ohua.lang/algo* (zip/node one-left))
                                                                      (nil? (zip/left one-left) ; end of seq if this is nil
                                                                            )))]
                                                        res)
                                                      true)
                                                    (algorithm? env sym)))
                                               (fn [loc]
                                                 (let [sym (zip/node loc)
                                                       ^Algo algo (resolve-ns-or-&env env sym)
                                                       code (.-code algo)]
                                                   [(zip/next (zip/replace loc (if (starts-with? (str sym) "__generated__algo__")
                                                                                 code
                                                                                 (vary-meta
                                                                                   code
                                                                                   assoc
                                                                                   :algo-scope (.-scope algo)
                                                                                   :algo-ns (.-ns algo)))))
                                                    nil]))
                                               concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))

(defn transform-local-algos
  ([code] (transform-local-algos {} nil code))
  ([env form code]
   (l/printline "################################################################################################")
   (l/printline "Local algorithms transformation:")
   (let [zipped-code (t-zip/tree-zipper code)
         [transformed-code _] (t-zip/walk-code zipped-code
                                               #(and (zip/branch? %) (-> % zip/down zip/node #{'algo* 'com.ohua.lang/algo*}))
                                               local-algo-editor
                                               concat)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)))
