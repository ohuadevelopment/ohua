;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.values
  (:require [com.ohua.logging :as l]
            [com.ohua.tree-zipper :as t-zip]
            [clojure.zip :as zip]))


(defn values-matcher [&env loc]
  ; TODO (if (check 100) [100] #{:a 10}) -> these turn into Clojure functions and should also be wrapped into a value
  ; function to support execution of arbitrary Clojure code. This is a different work item though!
  (and
    (not (empty? (zip/lefts loc)))
    (not (sequential? (zip/node loc)))
    (not (special-symbol? (zip/node loc)))
    (zip/up loc)
    (seq? (zip/node (zip/up loc)))
    (or
      ;(special-symbol? (zip/node (zip/leftmost loc))) ; problem: would also do the following: (def (id fn-name) (fn [] body))
      (some #{(zip/node (zip/leftmost loc))} '(let* clojure.core/let let if))
      (some #{(zip/node (zip/leftmost loc))} '(seq))
      (if (= (zip/node (zip/leftmost loc)) 'com.ohua.lang/smap)
          (or (t-zip/clj-context-local? &env loc) (t-zip/global-var? loc))
          false))
    )
  )

(defn values-editor [loc]
  [(t-zip/skip (zip/replace loc `(com.ohua.lang/id ~(zip/node loc))))
   nil])

(defn transform
  ([code] (transform #{} nil code))
  ([&env &form code]
   (l/printline "################################################################################################")
   (l/printline "value support transformation:")
   (let [[transformed-code _] (t-zip/walk-code (t-zip/tree-zipper code)
                                               (partial values-matcher &env)
                                               values-editor)]
     (l/printline "transformed code:")
     (l/write transformed-code :dispatch clojure.pprint/code-dispatch)
     (l/printline)
     transformed-code)
    ))
