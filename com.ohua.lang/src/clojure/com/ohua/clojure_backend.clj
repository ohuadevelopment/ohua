;
; ohua : clojure_backend.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.clojure-backend
  (:require [clojure.string :as string]
            [com.ohua.link :as linker :refer [->ns-string]]
            [com.ohua.util.file :refer [load-from-classpath]])
  (:import (java.lang.reflect Method)
           (java.nio.file Path)
           (com.ohua.lang defsfn)
           (com.ohua.backend SFNBackend)
           (com.ohua.engine.flowgraph.elements.operator IOperatorFactory ClojureBackendOperatorFactory StatefulFunction DataflowFunction)
           (com.ohua.engine.exceptions OperatorLoadingException)))


(def ^:private sfn-cache (atom {}))


(defn to-java-name [name]
  (-> name
      (string/replace #"-(\w)" #(string/upper-case (second %1)))
      (string/replace "-" "_")))


(defn is-clojure-name? [name]
  (or
    (string/includes? name "-")
    (re-matches #"^\p{Lower}.*[^_]\p{Upper}" name)))


(defn to-clojure-name [name]
  (-> name
      (string/replace #"([^_])_" "$1-")
      (string/replace #"^(\p{Upper})" string/lower-case)    ; Don't add a '-' for the starting upper case letter
      (string/replace #"\p{Upper}" #(str "-" (string/lower-case %1))) ; Substitute camel case for '-' and the lower case letter
      ))


(defn clojure-java-bidirectional-name-converter [name]
  (if (is-clojure-name? name)
    (to-clojure-name name)
    (to-java-name name)))


(defn get-sfn-method [full-class-name cls]
  (let [sfn-methods (filter (fn [^Method m] (.isAnnotationPresent m defsfn)) (.getDeclaredMethods cls))]
    (case (count sfn-methods)
      ; class did not declare any stateful functions
      0 nil
      ; class declared a stateful funciton, add it to the package
      1 (first sfn-methods)
      ; class declared more than one stateful function, aborting
      (do
        (println "Class" full-class-name "declared more than one stateful function, aborting import.")
        nil))))


(defn- load-package [pkg-name]
  (let [java-pkg-name (string/replace pkg-name "-" "_")
        class-file-paths (load-from-classpath (string/replace java-pkg-name "." "/") "*.class")
        mapped-to-class
        (reduce
          (fn [pkg ^Path path]
            (let [class-name (string/replace (-> path
                                                 (.getFileName)
                                                 (.toString))
                                             #".class$"
                                             "")
                  full-class-name (str java-pkg-name "." class-name)]
              (if (pkg full-class-name)                     ; Skip previously handeled classes
                pkg
                (let [^Class cls (Class/forName full-class-name)]
                  (if-let [^Method method (get-sfn-method full-class-name cls)]
                    (assoc! pkg full-class-name method)
                    pkg)))))
          (transient {})
          class-file-paths)
        mapped-to-name
        (reduce
          (fn [pkg ^Method method]
            (let [method-name (.getName method)
                  clojure-function-name (to-clojure-name method-name)]
              (if-let [^Method previous (or (pkg method-name) (pkg clojure-function-name))]
                (throw (RuntimeException. (str "Stateful function " method-name " is defined twice in " pkg-name " (" (.getCanonicalName (.getDeclaringClass method)) "/" method-name " and " (.getCanonicalName (.getDeclaringClass previous)) "/" method-name ")."))))
              (assoc! pkg
                      method-name method
                      clojure-function-name method)))
          (transient {})
          (vals (persistent! mapped-to-class)))
        ]
    (persistent! mapped-to-name)
    ))

(defn get-or-load-ns [ns-name]
  (let [name-str (->ns-string ns-name)]
    (if-let [n (@sfn-cache name-str)]
      n
      (let [loaded (load-package name-str)]
        ((swap! sfn-cache assoc name-str loaded (clojure-java-bidirectional-name-converter name-str) loaded) name-str)))))

(defn- resolve-sfn [op-name]
  (let [sym (symbol op-name)
           ]
    (if-let [ns (@sfn-cache (.getNamespace sym))]
      (ns (.getName sym)))))

(defn register-function! [ns name handle]
  (swap! sfn-cache assoc-in [ns name] handle))

(defn clear-cache! []
  (reset! sfn-cache {}))

(defn load-core-operators []
  (get-or-load-ns 'com.ohua.lang))

(declare clojure-op-factory)

(def clojure-backend
  (reify
    SFNBackend
    (initialize [this])
    (listNamespace [this ns-name]
      (if-let [ns (get-or-load-ns ns-name)]
        (set (keys ns))))
    (exists [this ns-name fn-name]
      (if-let [ns (get-or-load-ns ns-name)]
        (contains? ns fn-name)
        false))
    (getOperatorFactory [this] clojure-op-factory)
    (asStatefulFunction [this ns-name name]
      (if-let [ns (get-or-load-ns ns-name)]
        (if-let [^Method method (ns name)]
          (-> method
              (.getDeclaringClass)
              (StatefulFunction/createStatefulFunctionObject)
              (StatefulFunction/resolve)))))))

(def clojure-op-factory
  (reify
    IOperatorFactory
    (exists [this thing]
      ; HACK since symbol resolution isn't implemented yet, we must query the linker here for now
      (if-let [sym (symbol thing)]
        (let [name (.getName sym)
              ns-name (.getNamespace sym)]
          (if (nil? ns-name)
            (throw (IllegalArgumentException. (str "Symbol must be fully qualified, found " sym))))
          (.exists clojure-backend ns-name name)
          ; NOTE below is what the code here *should* look like.
          ; We can activate it again once the s¥mbol resolution bit is done.
          ;
          ;(cond
          ;  (linker/is-builtin? sym) true
          ;  (or (nil? name) (nil? ns-name)) (throw (OperatorLoadingException. "Operator for existence check must be fully qualified."))
          ;  :else (.exists clojure-backend ns-name name))
          )
        (throw (OperatorLoadingException. (str "Operator " thing " is not resolvable.")))))
    (createUserOperatorCore [this graph op-name]
      (if-let [^Method sfn (resolve-sfn op-name)]
        (ClojureBackendOperatorFactory/createUserOperatorCore graph sfn op-name)
        (throw (OperatorLoadingException. (str "Operator not found " op-name)))))))
