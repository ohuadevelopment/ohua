;;;
;;; Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.code-generation
  (:require [com.ohua.logging :as l]
            [com.ohua.link :as linker])
  (:import [clojure.lang IPersistentVector ISeq Cons Symbol LazySeq]))

;;;
;;; Code generation for calls to the Ohua frontend happens here.
;;;

(defn prepare-op [op-details]
;  (println "op details:" op-details)
  `(.createOperator ~(str
                       (linker/resolve (symbol (:type op-details))) ; HACK Until sym-res this is necessary
                       ) ~(:op-id op-details)))

(defn prepare-ops [extracted-ops] (map prepare-op extracted-ops))

(defn prepare-arguments
  [args]
  ;(l/printline "Prepare arguments:")
  ;(l/pprint args)
  (map #(do `(.setArguments ~(:op-id %)
                            (into-array com.ohua.lang.Tuple
                                        (list
                                          ~@(map (fn [{arg :arg in-idx :in-idx}]
                                                   `(com.ohua.lang.Tuple. (int ~in-idx) ~arg)) (:args  %))))))  args))

(defn prepare-arcs [arcs]
     (map
       (fn [d] 
           `(.registerDependency ~(:op-id (:source d)) ~(:out-idx (:source d)) ~(:op-id (:target d)) ~(:in-idx (:target d)))) 
       arcs))

;TODO: find the correct way to pass (type arg)
(defmulti find-type (fn [arg] (type arg)))
(defmethod find-type Cons [arg] (type ((eval arg))))
(defmethod find-type LazySeq [arg] (type (eval arg)))
(defmethod find-type ISeq [arg] (type (eval arg)))
(defmethod find-type Symbol [arg] arg) ; it's a variable
(defmethod find-type :default [arg] (type arg))

(defn find-type-info [arg]
  (l/printline "find type info:" arg (meta arg))
  (if-let [type (:java-type (meta arg))]
    type
    (find-type arg)))

(defn prepare-arguments-for-compilation
  [args]
   (map #(do `(.setArguments ~(:op-id %)
                             (into-array com.ohua.lang.Tuple
                                         (list
                                           ~@(map (fn [{arg :arg in-idx :in-idx}]
                                                    `(com.ohua.lang.Tuple. (int ~in-idx) '~(find-type-info arg))) (:args  %))))))  args))


(defn generate-compile-code
  [ops arcs args]
  (l/printline "Generating compile code ...")
  (l/pprint args)
  ;(clojure.pprint/pprint args)
  (let [type-args (prepare-arguments-for-compilation args)]
    (l/printline "type args:")
    (l/pprint type-args)
    (let [compile-code (concat [`(new com.ohua.lang.compile.FlowGraphCompiler)] 
                                              (conj (vec (concat ops arcs type-args)) 
                                                    `(.compile true)))];TODO parameter compile
      (l/printline "Compilation code:")
      (l/pprint compile-code)
      compile-code)))
