;;;
;;; Copyright (c) Sebastian Ertel 2013-14. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.operator-detection
(:require [clojure.walk :as walk]
          [clojure.zip :as zip]
          [clojure.reflect :as reflect]
          [com.ohua.tree-zipper :as t-zip]
          [com.ohua.recorder :refer :all]
          [com.ohua.if-statement :as if-stmt]
          [com.ohua.loop-statement :as loop-stmt]
          [com.ohua.logging :as l]
          [com.ohua.link :as linker])
(:import  [clojure.lang IPersistentVector ISeq Cons Symbol LazySeq]))


(defrecord ASTMeta [in-idx out-idx op-id skip is-op? direct-access])

;;;
;;; The first part of the compilation algorithm walks the code and detects operators.
;;; Once it has found an operator, it assigns it an id.
;;;

(defn seq-matcher [loc]
;  (println "arrived:" (zip/node loc))
  (seq? (zip/node loc)))

;;;
;;; Destructuring is handled as follows:
;;; Assume (let* [vec__2358 (op-1) out-1 (clojure.core/nth vec__2358 0 nil) out-2 (clojure.core/nth vec__2358 1 nil)] (op-2 out-1) (op-3 out-2))
;;; Then we walk this vector one by one everytime we hit an nth, we look two items to the left to find the source operator.
;;; Once we have found it we will attach the op-id to the nth node. That way the op-id gets propagated through the
;;; destructuring list. In the end all nth sequences will have the op-id of the first (op-1) node.
;;;
(defn handle-destructuring [loc]
  ; In case of destructuring, we have to jump over the var to find the previous op.
;  (println "handle destructuring: " (zip/node loc))
  (let [
        ;FIXME this origin might just be a variable that was defined earlier. it should have the meta data attached though!
        origin (zip/left (zip/left loc))
        origin-meta (meta (zip/node origin))
        source-id (:op-id origin-meta)
        out-idx (:out-idx origin-meta)
        ; explicit schema matches start at 0
        ; just take the index directly from nth!
;        _ (println "origin" (zip/node origin) source-id out-idx)
        ; FIXME I do not understand the below condition!
        new-out-idx (if (or (= (zip/node (zip/down origin)) 'clojure.core/nth) (= (zip/node (zip/down origin)) 'nth))
                       ; (nth p 0 nil)
                      (-> loc zip/down zip/right zip/right zip/node)
                      0)]
    (zip/edit loc #(with-meta % (conj {:op-id source-id
                                       :out-idx new-out-idx
                                       :is-op? true
                                       :skip false
                                       :direct-access false}
                                      (meta %))))))

;;;
;;; direct access -> (let [packet (func-1)] (func-2 (nth packet 0)))
;;; We handle this case by moving up the tree, finding the let statement and the source.
;;; Then as in the above case, we add the op-id to the nth sequence.
;;;
(defn handle-direct-explicit-access [loc]
  ; In case of direct access, we have to walk up the tree and find the binding again such 
  ; that we can retrieve the according meta-data and annotate this node.
;  (println "direct access: " (zip/node loc))
  (let [origin (t-zip/find-var-definition (zip/up loc) (zip/node (zip/right (zip/down loc))))
;        t (println "found origin:" (zip/node origin))
        origin-meta (meta (zip/node origin))
        source-id (:op-id origin-meta)
        out-idx (:out-idx origin-meta)
        ; take from (nth p 9 nil)
        new-out-idx (zip/node (zip/right (zip/right (zip/down loc))))]
    (zip/edit loc #(with-meta % (conj {:op-id source-id
                                       :out-idx new-out-idx
                                       :is-op? true
                                       :skip false
                                       :direct-access false}
                                      (meta %)))))
)

(defn register-op
  [loc id-generator op]
;  (println "registering op" (zip/node func))
  (let [id (swap! id-generator inc)]
          ; by default everything is an implicit match (-1)
          [(zip/next (zip/edit loc #(with-meta % (conj {:op-id id
                                                        :out-idx -1
                                                        :is-op? true
                                                        :skip false
                                                        :direct-access true}
                                                       (meta %))))) [{:op-id id :type op}]])
  )


(defn dispatch-on-built-ins [id-generator loc]
  ;  (println "editor -> node:" (zip/node loc))
  (let [func (zip/down loc)
        n (zip/node func)]
    (case n
      if (if-stmt/register-if-operator id-generator loc func)
      loop* (loop-stmt/register-loop-operator id-generator loc func)
      recur (loop-stmt/register-recur-operator id-generator loc func)
      (clojure.core/nth nth) (if (vector? (zip/node (zip/up loc)))
                               [(zip/next (handle-destructuring loc)) nil]
                               [(zip/next (handle-direct-explicit-access loc)) nil])

      (cond

        (linker/is-defined? n) (register-op loc id-generator n)

        ; FIXME this can lead to pretty hard to detect errors when function names are just mis-spelled. we have to find a way to
        ;       understand if we can skip this function.
        :else (do (l/printline "Skipping function: " n) [(zip/next (zip/edit loc #(with-meta % (conj {:is-op? false :skip false} (meta %))))) nil])

        ; TODO This is the extension point where we have to start handling more special forms.
        )
      )))

; Note by macro expansion destructuring gets converted from this:
; (let [[out-1 out-2] (op-1)] (op-2 out-1) (op-3 out-2))
; into this:
; (let* [vec__2358 (op-1) out-1 (clojure.core/nth vec__2358 0 nil) out-2 (clojure.core/nth vec__2358 1 nil)] (op-2 out-1) (op-3 out-2))
; Hence, all succeeding nth-nodes inherit the op-id of its root.
(defn seq-editor [id-generator loc]
;  (println "current" (zip/node loc) (meta (zip/node loc)))
;  (println "path" (zip/path loc) (if (zip/path loc)(first (zip/path loc))))
;  (println "loc" loc)
  (let [result (dispatch-on-built-ins id-generator loc)]
    ; the macro expansion turns let and clojure.core/let into let*
    (if (and (not  (= 'let* (zip/node (zip/down loc)))) (= 'let* (zip/node (zip/leftmost loc))))
      (let [new-loc (zip/prev (first result))
            ; skip the function as a source
            skipped-new-loc (zip/edit new-loc #(with-meta % (conj (meta %) {:skip true})))]
        ; edit the let* itself as a source
        (if (not (zip/rights skipped-new-loc))
          [(let [meta-skipped (meta (zip/node skipped-new-loc))
                 updated-let ((rec [a (zip/up skipped-new-loc)
                                    b (zip/edit a
                                        #(with-meta % (conj (meta %) {:is-op? false
                                                                      :skip false
                                                                      :op-id (:op-id meta-skipped)
                                                                      :out-idx -1
                                                                      :direct-access true} )))])
                               nil)
                 outermost-let (loop [c updated-let]
                                 (do 
;                                   (println "current c" (zip/node c)) 
;                                   (if (zip/up (first c)) (println "parent c" (zip/node (zip/up (first c)))))                                  
                                   (if (and (zip/up (first c)) 
                                            (= 'let* (zip/node (zip/leftmost (first c)))) 
                                            (not (zip/rights (first c))))
                                     (let [updated-parent ((rec [
                                                             u (zip/edit (first c) #(with-meta % (conj {:skip true} (meta %)))) ; disable
;                                                             uv (print-s u)
                                                             v (zip/up u)
                                                             p (zip/edit v #(with-meta % (conj {:skip false} (meta (first u))))) ; propagate
                                                             ])
                                                            (second c))]
;                                       (println "updated parent:" (zip/node (first updated-parent)) (meta (zip/node (first updated-parent))))
                                       (recur updated-parent))
                                     c)))]
;             (println "play:" (zip/node (play outermost-let)))
             (zip/next (play outermost-let))
             ) 
           (second result)]
          [(zip/next skipped-new-loc) (second result)]))
      result)))

(defn detect-combiner [found editor-result]
  (let [[ops-editor-result id-generator] editor-result] [(conj found ops-editor-result) id-generator]))
