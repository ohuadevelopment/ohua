/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import clojure.lang.IRecord;
import clojure.lang.Symbol;
import com.ohua.engine.flowgraph.elements.operator.DataflowFunction;

import java.util.Arrays;
import java.util.stream.IntStream;


/**
 * Created by sertel on 6/8/16.
 */
public class Algorithm {

  public interface IAlgorithm extends IRecord {
    Symbol mkSymbol();
  }

  public static class AlgoInStrict {
    /**
     * The first argument is associated state that keeps the information
     * of the algorithm that this function is an entry to.
     */
    @defsfn(useJavaVarArgsSemantics = false)
    public Object[] algoInStrict(IAlgorithm algo, Object... args) {
      return args;
    }
  }

  public static class AlgoInVoid {
    /**
     * The first argument is associated state that keeps the information
     * of the algorithm that this function is an entry to.
     */
    @defsfn
    public boolean algoInVoid(IAlgorithm algo) {
      return true; // context signal
    }
  }

  public static class AlgoOut {
    @defsfn
    public Object algoOut(Object args) {
      return args;
    }
  }

  public static class AlgoInNonStrict {

    @defsfn
    @DataflowFunction
    public Continuations algoIn(@Deprecated IAlgorithm alg // TODO This is not used and does not work anymore if we don't register algorithms globally. If we want to pass this in here we need a different way of doing so (currently passing in nil)
            , NonStrict<?>... algoArgs) {
      Continuations c = Continuations.empty();
      IntStream.range(0, algoArgs.length)
              .forEach(i -> c
                      .at(algoArgs[i],
                              v -> {
                                Object[] ret = new Object[algoArgs.length];
                                Arrays.fill(ret, DataflowFunction.Control.DROP);
                                ret[i] = v;
                                return Continuations.finish(ret);
                              }));
      return c;
    }
  }


}
