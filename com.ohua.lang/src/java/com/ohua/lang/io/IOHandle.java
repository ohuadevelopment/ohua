/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.io;

public class IOHandle {
  private String _resourceID = null;
  private Object[] _arguments = new Object[0];
  
  public IOHandle(String resourceID){
    _resourceID = resourceID;
  }

  public IOHandle(String resourceID, Object[] arguments){
    this(resourceID);
    _arguments = arguments;
  }
  
  public String getResourceID(){
    return _resourceID;
  }
  
  public Object[] getArguments(){
    return _arguments;
  }
}
