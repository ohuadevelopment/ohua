/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.lang.OhuaFrontend;
import com.ohua.lang.Tuple;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.exceptions.CompilationException.CAUSE;

/**
 * Additionally, this flow graph knows the arcs and the order of the arcs that enter the
 * function represented by this flow graph.
 * 
 * @author sertel
 * 
 */
// FIXME this class has two facets: one for function declaration and the other for function
// invocation (runtime)
public class FunctionFlowGraph
{
  /**
   * These are the input slots (formals) of the function.<br>
   * (func-1 [formal-1 formal-2]<br>
   * (op-1 formal-1)<br>
   * (op-2 formal-2))<br>
   * or<br>
   * (func-1 [formal-1 formal-2]<br>
   * (op-1 formal-1 formal-2))<br>
   * 
   * So for a call like <br>
   * (registerDependency actual-1 -1 func-1 0)<br>
   * (registerDependency actual-2 -1 func-1 1)<br>
   * we would preserve the source part and recreate the target part to be<br>
   * (registerDependency actual-1 -1 op-1 0)<br>
   * (registerDependency actual-2 -1 op-2 0)<br>
   * or<br>
   * (registerDependency actual-2 -1 op-1 1)<br>
   * in the latter case.<br>
   * So this array stores exactly this information. The index is the index of the formal of this
   * function. Each entry contains a string (op-id) and an integer (formal slot index).
   */
  // private Object[] _formalsToInputs = null;
  /**
   * We map the above info straight to input port references because for functions it accounts
   * that formals can not be grouped together into a single arc.
   */
  private String[] _formalsToInputPorts = null;
  
  /**
   * The last op in this function. By definition of the clojure language, there is always only
   * one.
   */
  private String _outputFormal = null;
  
  /**
   * The output port connections to be adapted.
   */
  private String[] _outputActuals = null;
  
  /**
   * Basic meta data to reconstruct the flow graph.
   */
  private List<Object[]> _arguments = new ArrayList<>();
  private List<Object[]> _operators = new ArrayList<>();
  private List<int[]> _dependencies = new ArrayList<>();
  
  protected FunctionFlowGraph() {
    // default
  }
  
  public FunctionFlowGraph(String[] formalsToInputPorts, String outputFormal) {
    _formalsToInputPorts = formalsToInputPorts;
    _outputFormal = outputFormal;
  }
  
  public void setFormalsToInputPorts(String[] formalsToInputPorts) {
    _formalsToInputPorts = formalsToInputPorts;
  }
  
  /**
   * These are soft references (IDs) to the input ports of the form:
   * operator-name.input-port-name
   * 
   * @return
   */
  public String[] getFormalsToInputPorts() {
    return _formalsToInputPorts;
  }
  
  public void setOutputActuals(String[] outputActuals) {
    _outputActuals = outputActuals;
  }
  
  /**
   * These are references to output ports of the form: operator-name.output-port-name
   * @return
   */
  public String[] getOutputActuals() {
    return _outputActuals;
  }
  
  public void setOutputFormal(String outputFormal) {
    _outputFormal = outputFormal;
  }
  
  public String getOutputFormal() {
    return _outputFormal;
  }
  
  public void addArgument(Object... arg) {
    _arguments.add(arg);
  }
  
  public void addOperator(Object... op) {
    _operators.add(op);
  }
  
  public void addDependency(int... dep) {
    _dependencies.add(dep);
  }
  
  public List<int[]> findDependencies(String source) throws CompilationException {
    // get the id first
    int id = -1;
    for(Object[] op : _operators) {
      if(((String) op[0]).equals(source)) {
        id = (int) op[1];
        break;
      }
    }
    
    if(id == -1) {
      throw new CompilationException(CAUSE.NO_SUCH_FUNCTION, source);
    }
    
    List<int[]> deps = new ArrayList<>();
    for(int[] dependency : _dependencies) {
      if(dependency[0] == id) deps.add(dependency);
    }
    Collections.sort(deps, new Comparator<int[]>() {
      @Override public int compare(int[] o1, int[] o2) {
        return ((Integer) o1[1]).compareTo((Integer) o2[1]);
      }
    });
    return deps;
  }
  
  public void replicate(OhuaFrontend frontend, int defaultID, int contextID) {
    try {
      for(Object[] op : _operators) {
        int id = (int) op[1] == defaultID ? contextID : (int) op[1];
        frontend.createOperator((String) op[0], id);
      }
    }
    catch(OperatorLoadingException | CompilationException ole) {
      // we loaded all these components already, so there should be no loading problems
      Assertion.impossible(ole);
    }
    
    for(int[] dependency : _dependencies) {
      int sourceID = dependency[0] == defaultID ? contextID : dependency[0];
      int targetID = dependency[2] == defaultID ? contextID : dependency[2];
      frontend.registerDependency(sourceID, dependency[1], targetID, dependency[3], dependency[4]);
    }
    
    try {
      for(Object[] arg : _arguments) {
        int id = (int) arg[0] == defaultID ? contextID : (int) arg[0];
        frontend.setArguments(id, (Tuple<Integer, Object>[]) arg[1]);
      }
    }
    catch(CompilationException ce) {
      // everything get set once. no reason why it should not work now.
      Assertion.impossible(ce);
    }
  }
  
}
