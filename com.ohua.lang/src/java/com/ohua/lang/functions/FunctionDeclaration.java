/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.functions;

import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.lang.OhuaFrontend;
import com.ohua.lang.Tuple;
import com.ohua.lang.compile.FlowGraphCompiler;
import com.ohua.lang.compile.Linker;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.functions.FunctionalExecutionSemantics.Strict;

@Deprecated
public class FunctionDeclaration extends FlowGraphCompiler
{
  private FunctionFlowGraph _funcMeta = new FunctionFlowGraph();
  private String _functionName = null;
  
  public FunctionDeclaration(String functionName) {
    _functionName = functionName;
  }
  
  public FunctionDeclaration(String functionName, String outputFormal) {
    _functionName = functionName;
    _funcMeta.setOutputFormal(outputFormal);
    Linker.linkOperator(functionName, Strict.class);
  }
  
  public void createOperator(String type, int id) throws OperatorLoadingException, CompilationException {
    super.createOperator(type, id);
    
    // store as meta data information
    _funcMeta.addOperator(type, id);
  }
    
  public void registerDependency(int source, int sourcePos, int target, int targetPos, int isFeedback) {
    super.registerDependency(source, sourcePos, target, targetPos, isFeedback);
    
    // store as meta data information
    _funcMeta.addDependency(source, sourcePos, target, targetPos, isFeedback);
  }
  
  public void setArguments(int operator, Tuple<Integer, Object>[] arguments) throws CompilationException {
    super.setArguments(operator, arguments);
    
    // store as meta data information
    _funcMeta.addArgument(operator, arguments);
  }
  
  public void compile(boolean typeSensitive) throws CompilationException {
    super.compile(true);
    
    // prepare the function declaration here. investigate the input formals.
    _funcMeta.setFormalsToInputPorts(findInputFormals(_process.getGraph(), _functionName));
  }
  
  /*
   * TODO for the time being we assume that each argument is mapped to a different op. enable
   * this to work even when multiple args are input to the same operator by enforcing at
   * dependency resolution that args are never grouped and always get a separate arc.
   */
  protected static String[] findInputFormals(FlowGraph graph, String functionName) {
    OperatorCore functionOp = OhuaFrontend.findOperator(graph, functionName);    
    String[] inputFormals = new String[functionOp.getInputPorts().size()];
    for(int i=0;i<functionOp.getInputPorts().size();i++){
      InputPort inPort = functionOp.getInputPorts().get(i);
      inputFormals[i] = FlowGraph.constructPortReference(inPort);
    }
    return inputFormals;
  }
  
  protected static String findInputFormal(FlowGraph graph, String functionName){
    OperatorCore functionOp = OhuaFrontend.findOperator(graph, functionName);
    return functionOp.getOperatorName();
  }
  
  public FunctionInvocation createInvocation(int contextID) {
    FunctionInvocation invocation = new FunctionInvocation(_functionName);
    OperatorCore functionOp = OhuaFrontend.findOperator(_process.getGraph(), _functionName);
    String id = super.deconstructOperatorName(functionOp.getOperatorName())[1];
//    System.out.println("ID: " + id);
    _funcMeta.replicate(invocation, Integer.parseInt(id), contextID);
    return invocation;
  }
  
  public String getFunctionName() {
    return _functionName;
  }

  public String getOutputFormal() {
    return _funcMeta.getOutputFormal();
  }
}
