/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.functions;

import java.util.Arrays;

import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.lang.OhuaFrontend;
import com.ohua.lang.compile.Linker;

@Deprecated
public class FunctionInvocation extends OhuaFrontend
{
  private FunctionFlowGraph _metaData = new FunctionFlowGraph();
  private String _funcDecID = null;
  
  protected FunctionInvocation(String funcDecID) {
    _funcDecID = funcDecID;
  }
  
  public int getOutputFormal() {
    String outputFormal = null;
    if(_metaData.getOutputFormal() != null) {
      outputFormal = _metaData.getOutputFormal();
    }
    else {
      FunctionDeclaration dec = Linker.findFunction(null, _funcDecID);
      
      // FIXME This needs fixing because the name of the operator is not enough!
      String opName = dec.getOutputFormal();
      OperatorCore op = OhuaFrontend.findOperator(_process.getGraph(), opName);
      outputFormal = op.getOperatorName();
//      System.out.println("Detected output formal: " + outputFormal);
      _metaData.setOutputFormal(outputFormal);
    }
    String id = super.deconstructOperatorName(outputFormal)[1];
    return Integer.parseInt(id);
  }
  
  public int getInputFormal() {
    String opName = FunctionDeclaration.findInputFormal(_process.getGraph(), _funcDecID);
    String id = super.deconstructOperatorName(opName)[1];
    return Integer.parseInt(id);
  }
  
  public void prepare(){
    super.resolveDependencies();
  }

  public void analyze() {
    // prepare the output formal
    getOutputFormal();
    
    // prepare output actuals
    OperatorCore outputOp = _process.getGraph().getOperator(_metaData.getOutputFormal());
    String[] outputActuals = new String[outputOp.getOutputPorts().size()];
    for(int i = 0; i < outputOp.getOutputPorts().size(); i++) {
      OutputPort outPort = outputOp.getOutputPorts().get(i);
      outputActuals[i] = FlowGraph.constructPortReference(outPort);
    }
    
    analyze(outputActuals);
  }
  
  private void analyze(String[] outputActuals) {
    _metaData.setOutputActuals(outputActuals);
    
    // input actuals are prepared automatically because they connect to the function operator
    // output formal is prepared on request during dependency registration
    
    // prepare input formals
    String[] inputFormals = FunctionDeclaration.findInputFormals(_process.getGraph(), _funcDecID);
//    System.out.println("Found input formals: " + Arrays.deepToString(inputFormals));
    _metaData.setFormalsToInputPorts(inputFormals);    
  }

  public FunctionFlowGraph getCompileTimeMeta(){
    return _metaData;
  }
  
  public String getFunctionName(){
    return _funcDecID;
  }
}
