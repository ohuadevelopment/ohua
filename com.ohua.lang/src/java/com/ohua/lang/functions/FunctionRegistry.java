/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.functions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FunctionRegistry
{
  private static Map<String, FunctionDeclaration> _functions = new HashMap<>();
  
  public static void register(String namespace, String functionID, FunctionDeclaration func) {
    _functions.put(functionID, func);
  }
    
  public static Set<String> list() {
    return _functions.keySet();
  }
  
  public static void clear() {
    _functions.clear();
  }
  
  public static FunctionDeclaration find(String namespace, String id){
    return _functions.get(id);
  }
}
