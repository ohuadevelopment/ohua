/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.functions;

import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.lang.defsfn;

public abstract class FunctionalExecutionSemantics
{
  public static class Strict
  {
    @defsfn
    public Object[] enter(Object... funcArgs) {
      return funcArgs;
    }
  }
  
  public static class Lenient extends UserOperator
  {
    @Override public void runProcessRoutine() {
      // TODO Auto-generated method stub
      
    }
    
    @Override public void prepare() {
      // nothing
    }
    
    @Override public void cleanup() {
      // nothing
    }
    
    @Override public Object getState() {
      // TODO Auto-generated method stub
      return null;
    }
    
    @Override public void setState(Object checkpoint) {
      // TODO Auto-generated method stub
      
    }
  }
}
