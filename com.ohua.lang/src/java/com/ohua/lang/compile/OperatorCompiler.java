/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile;

import aua.analysis.AnalysisException;
import aua.analysis.escape.EscapeAnalysis;
import aua.analysis.escape.EscapeAnalysis.AnalysisBoundary;
import aua.analysis.escape.EscapeAnalysis.LeakDetectionException;
import aua.analysis.reference.ReturnArrayReferenceAnalysis;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Set;

public class OperatorCompiler {
  
  /**
   * This is a flag that can be used in the testing setup as such errors should be dealt with in
   * AUA.
   */
  public static boolean FAIL_ON_REFERENCE_ANALYSIS_FAILURE = false;

  
  private static StringBuffer constructIncompleteAnalysisMessage(StringBuffer s, Set<String> result) {
    s.append("Analysis result: INCOMPLETE.\n");
    s.append("The following methods were skipped in the analysis:\n");
    for(String skipped : result) {
      s.append(skipped);
      s.append("\n");
    }
    s.append("To avoid unneccessary runtime overhead, please add compiler knowledge about these functions and rerun the analysis.\n");
    return s;
  }
  
  public static void compile(Method m, int[] stateArgs, boolean strict) throws IOException,
                                                                        LeakDetectionException
  {
    Set<String> result = performEscapeAnalysis(m, stateArgs);
    if(!result.isEmpty()) {
      if(strict) {
        LeakDetectionException leak = new LeakDetectionException();
        leak._fieldLeaked = constructIncompleteAnalysisMessage(new StringBuffer(), result).toString();
        throw leak;
      } else {
        if(FlowGraphCompiler.FAIL_ON_SAFETY_ANALYSIS)
          System.out.println(constructIncompleteAnalysisMessage(new StringBuffer(), result));
      }
    }
    performReferenceAnalysis(m.getDeclaringClass().getName(), m);
  }

    private static Set<String> performEscapeAnalysis(Method m, int[] taintedArgs) throws
            IOException,
                                                                                LeakDetectionException
  {
    AnalysisBoundary b = new AnalysisBoundary() {
      @Override
      public boolean isAnalyzeMethod(String owner, String method) {
        return true; // analyze all
      }
    };
    EscapeAnalysis analysis = new EscapeAnalysis(b);
    try {
      return analysis.analyze(m, taintedArgs);
    }
    catch(Throwable t) {
      // FIXME we have to make a serious effort again into this direction. so I'm disabling any
      // exceptions for now.
      if(FAIL_ON_REFERENCE_ANALYSIS_FAILURE)
//      System.err.println("Analysis failed because: " + t.getLocalizedMessage());
        throw t;
      else
        return Collections.emptySet();
    }
  }


    /**
   * Currently we act more nicely during this analysis as it is not yet perfectly safe.
   * @param cls
   * @param method
   * @return
   * @throws LeakDetectionException
   */
  private static void performReferenceAnalysis(String cls, Method method) throws LeakDetectionException {
    ReturnArrayReferenceAnalysis analysis = new ReturnArrayReferenceAnalysis();
    // analysis.printInstructions(ReferenceAnalysisTestClasses.BasicReassignedFields.class.getName());
    try {
      // List<List<Reference>> references =
      analysis.analyze(cls, method.getName(), method.getReturnType(), method.getParameterTypes());
    }
    catch(LeakDetectionException e) {
      throw e;
    }
    catch(IOException e) {
      assert false;
    }
    catch(AnalysisException e) {
      // this is an unsupported analysis case. eat it for now.
    }
    catch(Throwable t) {
      // collect everything else because the analysis attempt has failed out of some reason.
      if(FAIL_ON_REFERENCE_ANALYSIS_FAILURE) {
        System.err.println("Hit unexpected exception in '" + cls + "/" + method + "'!");
        throw t;
      }
    }
  }


}
