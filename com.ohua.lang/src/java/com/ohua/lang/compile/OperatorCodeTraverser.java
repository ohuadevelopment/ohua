//package com.ohua.lang.compile;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.codehaus.janino.Java;
//import org.codehaus.janino.Java.FunctionDeclarator.FormalParameter;
//import org.codehaus.janino.Java.VariableDeclarator;
//import org.codehaus.janino.util.Traverser;
//
///**
// * The algorithm must track down the path of the input parameters for each function inside the
// * operator class and gather their data flow throw this function. The purpose of this function
// * is not to evaluate but to gather the data flow information needed to perform the analysis.
// * 
// * @author sertel
// * 
// */
//public class OperatorCodeTraverser extends Traverser
//{
//  private class FunctionFrame
//  {
////    Java.FunctionDeclarator _fn = null;
//    Map<String, String> _trackedLocalVars = new HashMap<String, String>();
//    Map<String, List<Object>> _parameterDF = new HashMap<String, List<Object>>();
//  }
//    
//  // the declared fields
//  private Map<String, VariableDeclarator> _fields = new HashMap<String, VariableDeclarator>();
//  
//  private FunctionFrame _currentFn = null;
//  private boolean _trackStmt = false;
//  private List<String> _usedTracked = null;
//  
//  @Override public void traverseFieldDeclaration(Java.FieldDeclaration fd) {
//    super.traverseFieldDeclaration(fd);
//    for(VariableDeclarator varDec : fd.variableDeclarators) {
//      _fields.put(varDec.name, varDec);
//    }
//  }
//  
//  @Override public void traverseFunctionDeclarator(Java.FunctionDeclarator fd) {
//    System.out.println("Entering function " + fd.name + " :");
//    _currentFn = new FunctionFrame();
////    _currentFn._fn = fd;
//    for(FormalParameter param : fd.formalParameters) {
//      List<Object> df = new ArrayList<Object>();
//      df.add(param.localVariable);
//      _currentFn._parameterDF.put(param.name, df);
//      _currentFn._trackedLocalVars.put(param.name, param.name);
//    }
//    super.traverseFunctionDeclarator(fd);
//    // TODO save this frame
//    _currentFn = null;
//  }
//  
//  /*
//   * Operations inside a function follow here:
//   */
//  
//  @Override public void traverseExpressionStatement(Java.ExpressionStatement es) {
//    // System.out.println("Hit expression: " + es);
//    // TODO What is referred to as an expression?!
//    super.traverseExpressionStatement(es);
//  }
//  
//  @Override public void traverseLocalVariableDeclarationStatement(Java.LocalVariableDeclarationStatement lvds) {
//    System.out.println("Hit local variable declaration: " + lvds);
//    super.traverseLocalVariableDeclarationStatement(lvds);
//    for(VariableDeclarator vd : lvds.variableDeclarators) {
//      System.out.println(vd.optionalInitializer);
//      super.traverseArrayInitializerOrRvalue(vd.optionalInitializer);
//    }
//  }
//  
//  @Override public void traverseAssignment(Java.Assignment a) {
//    System.out.println("Hit assignment: " + a);
//    _trackStmt = false;
//    _usedTracked = new ArrayList<String>();
//    super.traverseAssignment(a);
//    if(_trackStmt){
//      super.traverseLvalue(a.lhs);
//      // TODO
////      _currentFn._tracked.put(, a);
//    }
//  }
//  
//  @Override public void traverseUnaryOperation(Java.UnaryOperation uo) {
//    System.out.println("Hit unary operation: " + uo);
//    super.traverseUnaryOperation(uo);
//  }
//  
//  @Override public void traverseBinaryOperation(Java.BinaryOperation bo) {
//    System.out.println("Hit binary operation: " + bo);
//    super.traverseBinaryOperation(bo);
//  }
//  
//  @Override public void traverseCrement(Java.Crement c) {
//    super.traverseCrement(c);
//  }
//  
//  /*
//   * Here, the target of the method invocation can be accessed.
//   */
//  @Override public void traverseMethodInvocation(Java.MethodInvocation mi) {
//    System.out.println("Hit method invocation: " + mi);
//    super.traverseMethodInvocation(mi);
//  }
//  
//  /*
//   * This is bare invocation statement.
//   */
//  @Override public void traverseInvocation(Java.Invocation i) {
//    super.traverseInvocation(i);
//  }
//  
//  @Override public void traverseSuperclassMethodInvocation(Java.SuperclassMethodInvocation smi) {
//    super.traverseSuperclassMethodInvocation(smi);
//  }
//  
//  @Override public void traverseReturnStatement(Java.ReturnStatement rs) {
//    // TODO this is important in order to track the data flow
//    super.traverseReturnStatement(rs);
//  }
//  
//  /*
//   * Hooks for the evaluation of the statements start here:
//   */
//  
//  @Override public void traverseAmbiguousName(Java.AmbiguousName an) {
//    System.out.println("Hit ambiguous name: " + an);
//    if(_currentFn._trackedLocalVars.containsKey(an.identifiers[0])){ 
//      _trackStmt = true;
//      _usedTracked.add(an.identifiers[0]);
//    }
//    super.traverseAmbiguousName(an);
//  }
//    
//  @Override public void traverseFieldAccessExpression(Java.FieldAccessExpression fae) {
//    System.out.println("Hit field access expression: " + fae);
//    _trackStmt = true;
//    super.traverseFieldAccessExpression(fae);
//  }
//  
//  @Override public void traverseFieldAccess(Java.FieldAccess fa) {
//    System.out.println("Hit field access: " + fa);
//    super.traverseFieldAccess(fa);
//  }
//
//  @Override public void traverseLocalVariableAccess(Java.LocalVariableAccess lva) {
//    System.out.println("Hit local variable access: " + lva);
//    super.traverseLocalVariableAccess(lva);
//  }
//  
//  /*
//   * This is always being called last after the statement has been fully evaluated. We use it
//   * understand when the evaluation of a statement has finished. In case of an invocation for
//   * example it signals that we can store the parameters.
//   */
//  @Override public void traverseStatement(Java.Statement s) {
//    // System.out.println("Hit statement: " + s);
//    super.traverseStatement(s);
//  }
//  
//  /*
//   * This is a sequence of statements inside a loop or an if-statement.
//   */
//  @Override public void traverseBlock(Java.Block b) {
//    super.traverseBlock(b);
//  }
//  
//  // TODO later!
//  
//  // @Override public void traverseAnonymousClassDeclaration(Java.AnonymousClassDeclaration acd)
//  // {
//  // super.traverseAnonymousClassDeclaration(acd);
//  // }
//  //
//  // @Override public void traverseLocalClassDeclaration(Java.LocalClassDeclaration lcd) {
//  // super.traverseLocalClassDeclaration(lcd);
//  // }
//  //
//  // public void traverseLiteral(Java.Literal l) {
//  // this.traverseRvalue(l);
//  // }
//  //
//  // public void traverseIntegerLiteral(Java.IntegerLiteral il) {
//  // this.traverseLiteral(il);
//  // }
//  //
//  // public void traverseFloatingPointLiteral(Java.FloatingPointLiteral fpl) {
//  // this.traverseLiteral(fpl);
//  // }
//  //
//  // public void traverseBooleanLiteral(Java.BooleanLiteral bl) {
//  // this.traverseLiteral(bl);
//  // }
//  //
//  // public void traverseCharacterLiteral(Java.CharacterLiteral cl) {
//  // this.traverseLiteral(cl);
//  // }
//  //
//  // public void traverseStringLiteral(Java.StringLiteral sl) {
//  // this.traverseLiteral(sl);
//  // }
//  //
//  // public void traverseArrayType(Java.ArrayType at) {
//  // super.traverseArrayType(at);
//  // }
//  //
//  // public void traverseBasicType(Java.BasicType bt) {
//  // super.traverseBasicType(bt);
//  // }
//  //
//  // public void traverseReferenceType(Java.ReferenceType rt) {
//  // super.traverseReferenceType(rt);
//  // }
//  //
//  // public void traverseSimpleType(Java.SimpleType st) {
//  // super.traverseSimpleType(st);
//  // }
//  //
//  // public void traverseArrayAccessExpression(Java.ArrayAccessExpression aae) {
//  // super.traverseArrayAccessExpression(aae);
//  // }
//}
