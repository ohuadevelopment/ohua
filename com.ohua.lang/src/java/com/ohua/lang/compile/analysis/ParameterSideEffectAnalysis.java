/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile.analysis;

import aua.analysis.AbstractOperatorAnalysis;


/**
 * This analysis checks whether an operator performs side-effects to an object reference
 * (non-primitive type) of an actual parameter.
 * 
 * @author sertel
 * 
 */
public class ParameterSideEffectAnalysis extends AbstractOperatorAnalysis
{
  // TODO
}
