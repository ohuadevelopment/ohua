/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.lang.compile.analysis;

import aua.analysis.AbstractOperatorAnalysis;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.IFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.compile.analysis.qual.ReadOnly;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.exceptions.CompilationException.CAUSE;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

/**
 * Checks whether the type of the shared dependency is immutable.
 * 
 * @author sertel
 *
 */
public class ImmutabilityCheck implements SharedDependencyCheck {
  
  public void checkSharedDependency(CompileTimeView cv, Integer sourceSlot, List<Integer> targetSlots,
                                    OperatorCore sourceOp) throws CompilationException
  {
    // check the type of the slot for immutability. instead of using the return type, we
    // just take a look at the input schema of the downstream.
    int portIdx = targetSlots.get(1);
    InputPort targetPort = sourceOp.getOutputPorts().get(portIdx).getOutgoingArcs().get(0).getTargetPort();
    int inPortIdx = targetPort.getOwner().getInputPorts().indexOf(targetPort);
    int targetOpID = cv.extractIDFromRef(targetPort.getOwner().getOperatorName());
    IFunctionalOperator targetOp = SharedDependencyCheck.getTargetOperator(portIdx, sourceOp, cv);
    int[] inputSchema = (int[]) cv.getInputSchema(targetOpID).get(inPortIdx)[0];
      // just check the slots one at the time
      for (int slot : inputSchema)
          checkSlotImmutability(slot,
                  targetOp,
                  ((IFunctionalOperator) sourceOp.getOperatorAlgorithm()).getFunctionName());

  }
  
  private void checkSlotImmutability(int actualSlot, IFunctionalOperator op, String sourceOp)
                                                                                       throws CompilationException
  {
    // FIXME we are using an actual slot reference here but the below function requires a reference to a formal slot.
    int formalSlot = op.getFlowFormalsCount() - 1 < actualSlot ? op.getFlowFormalsCount() - 1 : actualSlot;
      if (formalSlot == -1) return;
    Class<?> t = op.getParameterType(formalSlot);
    Annotation[] annos = op.getParameterAnnotation(formalSlot);
    if(!AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(t.getName())
       && !isReadOnlyParamater(annos))
    {
      throw new CompilationException(CAUSE.SHARED_DEPENDENCY_DETECTED,
                                     sourceOp,
                                     new MutabilityException(sourceOp,
                                                             op.getFunctionName(),
                                                             op.getParameterName(actualSlot)));
    }
  }
  
  private boolean isReadOnlyParamater(Annotation[] annos) {
    for(Annotation anno : annos)
      if(anno.annotationType().isAssignableFrom(ReadOnly.class))
        return true;
    return false;
  }

    private int[] constructInputSchemaMapping(List<Object[]> inputSchema, int slotCount) {
    int[] input = new int[slotCount];
    Arrays.fill(input, -1);
    for(int inPortIdx = 0; inPortIdx < inputSchema.size(); inPortIdx++) {
      int[] portInput = (int[]) inputSchema.get(inPortIdx)[0];
      for(int portInputTargetSlot : portInput) {
        Assertion.invariant(portInputTargetSlot < slotCount);
        if(portInputTargetSlot > -1) // exclude conditional arcs
        input[portInputTargetSlot] = inPortIdx;
      }
    }
    return input;
  }

    public static class MutabilityException extends Exception {
        protected MutabilityException(String sourceOp, String targetOp, String inputSlotName) {
            super(sourceOp + " -> " + targetOp + "//" + inputSlotName);
        }
    }
  
}
