///*
// * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
// * 
// * This source code is licensed under the terms described in the associated LICENSE.TXT file.
// */
//package com.ohua.lang.compile.analysis;
//
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import org.codehaus.commons.compiler.CompileException;
//import org.codehaus.janino.Java;
//import org.codehaus.janino.Java.VariableDeclarator;
//import org.codehaus.janino.Parser;
//import org.codehaus.janino.Scanner;
//import org.codehaus.janino.util.Traverser;
//
//import com.ohua.lang.compile.OperatorCodeTraverser;
//import com.ohua.lang.exceptions.CompilationException;
//
///**
// * Since operators are written in Java, we have to detect when an operator implementation passes
// * the reference to a field into a packet.
// * 
// * @author sertel
// * 
// */
//public class ConcurrencyLeakAnalysis
//{
//  private enum TRAVERSER_COMMAND {
//    DETECT_ASSIGNMENT,
//    DETECT_ARGUMENTS,
//    NONE
//  }
//  
//  // FIXME all of this needs to be blockified to declare variables in a context!
//  @SuppressWarnings("unused")
//  private class DataFlowAnalysis extends Traverser
//  {
//    private Set<String> _boundaries = null;
//    
//    private Map<String, VariableDeclarator> _fields = new HashMap<String, VariableDeclarator>();
//    private Map<String, String> _aliases = new HashMap<String, String>();
//    
//    private TRAVERSER_COMMAND _command = TRAVERSER_COMMAND.NONE;
//    private List<String> _assignmentNames = new ArrayList<String>(2);
//    private List<String> _argumentNames = new ArrayList<String>();
//    
//    private List<CompilationException> _exceptions = new ArrayList<CompilationException>();
//    
//    private Map<Java.FunctionDeclarator, List<Java.MethodInvocation>> _connections =
//        new HashMap<Java.FunctionDeclarator, List<Java.MethodInvocation>>();
//    
//    private List<Java.FunctionDeclarator> _fnUsingFields = new ArrayList<Java.FunctionDeclarator>();
//    
//    private Java.FunctionDeclarator _currentFunction = null;
//    
//    public DataFlowAnalysis(List<String> dataBoundaries) {
//      _boundaries = new HashSet<String>(dataBoundaries);
//    }
//    
//    @Override public void traverseFunctionDeclarator(Java.FunctionDeclarator fd) {
//      System.out.println("Hit function declaration!");
//      _currentFunction = fd;
//      _connections.put(fd, new ArrayList<Java.MethodInvocation>());
//      super.traverseFunctionDeclarator(fd);
//    }
//    
//    @Override public void traverseMethodInvocation(Java.MethodInvocation mi) {
//      super.traverseMethodInvocation(mi);
//      _connections.get(_currentFunction).add(mi);
//    }
//    
//    // local fields represent potential concurrency leaks because they save pointers to
//    // objects that were already sent
//    @Override public void traverseFieldDeclaration(Java.FieldDeclaration fd) {
//      super.traverseFieldDeclaration(fd);
//      _fnUsingFields.add(_currentFunction);
//      System.out.println("Hit field declaration! " + fd);
//      for(VariableDeclarator varDec : fd.variableDeclarators) {
//        _fields.put(varDec.name, varDec);
//      }
//    }
//    
//    @Override public void traverseParameterAccess(Java.ParameterAccess pa) {
//      super.traverseParameterAccess(pa);
//      System.out.println("Param access: " + pa);
//    }
//
//    // // Too bad because this is only being hit if the field access happens via this.field. If
//    // you
//    // // just say "field" then we do not get here :(
//    // @Override public void traverseFieldAccessExpression(Java.FieldAccessExpression fae) {
//    // super.traverseFieldAccessExpression(fae);
//    // switch(_command)
//    // {
//    // case DETECT_ARGUMENTS:
//    // _argumentNames.add(fae.fieldName);
//    // break;
//    // case DETECT_ASSIGNMENT:
//    // _assignmentNames.add(fae.fieldName);
//    // break;
//    // case NONE:
//    // break;
//    // }
//    // }
//    //
//    // @Override public void traverseAmbiguousName(Java.AmbiguousName an) {
//    // super.traverseAmbiguousName(an);
//    // switch(_command)
//    // {
//    // case DETECT_ARGUMENTS:
//    // _argumentNames.add(an.identifiers[0]);
//    // break;
//    // case DETECT_ASSIGNMENT:
//    // _assignmentNames.add(an.identifiers[0]);
//    // break;
//    // case NONE:
//    // break;
//    // }
//    // }
//    //
//    // @Override public void traverseAssignment(Assignment a) {
//    // System.out.println("Hit assignment! " + a);
//    // TRAVERSER_COMMAND safe = _command;
//    // _command = TRAVERSER_COMMAND.DETECT_ASSIGNMENT;
//    // _assignmentNames.clear();
//    // super.traverseAssignment(a);
//    // _command = safe;
//    // if(_assignmentNames.size() == 2) {
//    // String dataSource = _assignmentNames.get(1);
//    // if(_fields.containsKey(dataSource)) {
//    // _aliases.put(_assignmentNames.get(0), dataSource);
//    // }
//    // }
//    // }
//    //
//    // // make sure that local fields do not get passed beyond the defined boundaries.
//    // @Override public void traverseInvocation(Java.Invocation i) {
//    // System.out.println("Hit method invocation! " + i);
//    // if(_boundaries.contains(i.methodName)) {
//    // TRAVERSER_COMMAND safe = _command;
//    // _command = TRAVERSER_COMMAND.DETECT_ARGUMENTS;
//    // _argumentNames.clear();
//    // super.traverseInvocation(i);
//    // _command = safe;
//    // for(String argumentName : _argumentNames) {
//    // if(_aliases.containsKey(argumentName)) {
//    // _exceptions.add(new CompilationException(CAUSE.SHARED_MEMORY_LEAK_DETECTED));
//    // }
//    // else if(_fields.containsKey(argumentName)) {
//    // _exceptions.add(new CompilationException(CAUSE.SHARED_MEMORY_LEAK_DETECTED));
//    // }
//    // }
//    // }
//    // else {
//    // super.traverseInvocation(i);
//    // }
//    // }
//  }
//  
//  /*
//   * A pass over the code must gather all information necessary in order to track down the data
//   * flow from source to sink. However, this analysis will only be limited to this single class
//   * and will not encompass other Java collection classes. Therefore, it might not be possible
//   * to track down the complete data flow. For that we potentially would need a data flow
//   * analysis of the common collections. Maybe we can define them statically. Further, it is
//   * important to also see that data gets overridden. For example a get() followed by a remove()
//   * should not result in an exception.
//   */
//  // I'm not so sure whether we can do this kind of analysis that easily. For basic types this
//  // seems to be do-able but for collections this seems to be really none-trivial.
//  /*
//   * Arguments are inputs. Function invocations are outputs. But what are return statements?
//   */
//  public List<CompilationException> analyze(String opSourceFile, List<String> boundaries) throws IOException,
//                                                                                            CompileException
//  {
//    OperatorCodeTraverser analysis = new OperatorCodeTraverser();
//    FileReader r = new FileReader(opSourceFile);
//    Java.CompilationUnit cu;
//    try {
//      cu = new Parser(new Scanner(opSourceFile, r)).parseCompilationUnit();
//      analysis.traverseCompilationUnit(cu);
//    }
//    finally {
//      r.close();
//    }
//    
////    System.out.println("Printing data flow:");
////    for(Map.Entry<Java.FunctionDeclarator, List<Java.MethodInvocation>> entry : analysis._connections.entrySet()) {
////      System.out.println("Function: " + entry.getKey());
////      StringBuffer buf = new StringBuffer();
////      buf.append("Flow: ");
////      for(Java.MethodInvocation mi : entry.getValue()){
////        buf.append(mi);
////        buf.append(" -> ");
////      }
////      System.out.println(buf);
////    }
//    
//    return Collections.emptyList();
//  }
//}
