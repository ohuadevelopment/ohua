/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile.analysis.qual;

/**
 * Tags a method that has side-effects to either a data structure or an external resources.
 * 
 * @author sertel
 *
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value=java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value=java.lang.annotation.ElementType.METHOD)
public @interface SideEffect {
  
}
