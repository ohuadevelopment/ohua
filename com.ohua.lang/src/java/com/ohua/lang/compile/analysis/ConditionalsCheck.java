/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile.analysis;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.IFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.Conditional;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.exceptions.CompilationException.CAUSE;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import com.ohua.engine.flowgraph.elements.operator.SwitchOperator;

/**
 * A check whether the targets of a shared dependency are located on a different branch of a
 * conditional.
 *
 * @author sertel
 */
public class ConditionalsCheck implements SharedDependencyCheck {

  @Override
  public void checkSharedDependency(CompileTimeView cv, Integer sourceSlot, List<Integer> targetSlots,
                                    OperatorCore sourceOp) throws CompilationException {
    /*
     * For the time being a dumb backward graph search is probably sufficient. However, it would
     * be smarter to iterate over the entire graph once and use a smarter numbering scheme that
     * allows to directly find the path from operator A to operator B. The scheme would be even
     * more powerful if it could tell already by the meta information it stores that operators A
     * and B are independent of each other. The OperatorCore has already the addProperty()
     * extension point to attach additional information.
     */

    List<List<OperatorCore>> scopes = new ArrayList<>();
    List<String> targetOps = new ArrayList<>();

    for (Integer targetSlot : targetSlots) {
      OperatorCore target = sourceOp.getOutputPorts().get(targetSlot).getOutgoingArcs().get(0).getTarget();
      // find the scopes
      List<OperatorCore> scopePath = findScope(target);
      scopes.add(scopePath);
      targetOps.add(target.getOperatorName());
    }

    // walk the scope paths top down and compare one against all the others
    for (int i = 0; i < scopes.size() - 1; i++) {
      List<OperatorCore> one = scopes.get(i);
      List<OperatorCore> two = scopes.get(i + 1);

      // if the scope paths are of different size then obviously they need to be scoped
      // differently
      if (one.size() == two.size()) {
        if (one.isEmpty()) {
          // there is no switch scope present so we have to fall back to diff the paths from
          // source to target
          List<List<OperatorCore>> slotPathsOne = findPaths(sourceOp, targetOps.get(i));
          List<List<OperatorCore>> slotPathsTwo = findPaths(sourceOp, targetOps.get(i + 1));
          if (!pathsDiff(cv, slotPathsOne, slotPathsTwo))
            throw new CompilationException(CAUSE.SHARED_DEPENDENCY_DETECTED,
                    sourceOp.getOperatorName());

        }
        // if they do not have the same immediate scope then they need to be scoped differently
        else if (one.get(one.size() - 1) == two.get(two.size() - 1)) {
          // here we check the paths from the (switch scope) to the targets to understand if
          // they are located on the same branch
          List<List<OperatorCore>> onePaths = findPaths(one.get(one.size() - 1), targetOps.get(i));
          List<List<OperatorCore>> twoPaths = findPaths(two.get(one.size() - 1), targetOps.get(i + 1));
          if (!pathsDiff(cv, onePaths, twoPaths)) throw new CompilationException(CAUSE.SHARED_DEPENDENCY_DETECTED,
                  sourceOp.getOperatorName());
        }
      }
    }
  }

  private boolean
  pathsDiff(CompileTimeView cv, List<List<OperatorCore>> target1, List<List<OperatorCore>> target2) {
    for (List<OperatorCore> pathToTarget1 : target1) {
      for (List<OperatorCore> pathToTarget2 : target2) {
        if (pathDiff(cv, pathToTarget1, pathToTarget2)) return true;// one is enough here I think
      }
    }
    return false;
  }

  private boolean isConditional(OperatorCore lastCommon){
    return lastCommon.getOperatorAlgorithm() instanceof IFunctionalOperator
            && ((IFunctionalOperator) lastCommon.getOperatorAlgorithm()).getFunctionType().isAssignableFrom(Conditional.class);
  }

  private boolean pathDiff(CompileTimeView cv, List<OperatorCore> pathToTarget1, List<OperatorCore> pathToTarget2) {
    for (int i = 0; i < Math.min(pathToTarget1.size(), pathToTarget2.size()); i++) {
      // object comparison is ok here
      if (pathToTarget1.get(i) == pathToTarget2.get(i)) {
        continue;
      } else {
        Assertion.invariant(i > 0); // the first op always matches because it is the source op.
        // the paths diverge here. so is the previous operator a switch?
        OperatorCore lastCommon = pathToTarget1.get(i - 1);
        if (isConditional(lastCommon)) {
          // we already understood that the current path items not match and we understood
          // that the reason is the switch.
          return true;
        } else {
          // the comparison here failed (no switch is the reason)
          return false;
        }
      }
    }

    // if everything matches then the operators are depending on each other and everything
    // is fine.
    return true;
  }

  private List<List<OperatorCore>> findPaths(OperatorCore sourceOp, String targetName) {
    // System.out.println("Finding paths from '" + sourceOp.getOperatorName() + "' to '" +
    // targetName + "'");
    List<OutputPort> outPorts = sourceOp.getOutputPorts();
    List<List<OperatorCore>> paths = new ArrayList<List<OperatorCore>>();
    for (int i = 0; i < outPorts.size(); i++) {
      OperatorCore neighbor = outPorts.get(i).getOutgoingArcs().get(0).getTarget();
      List<OperatorCore> path = new ArrayList<>();
      path.add(sourceOp);
      List<List<OperatorCore>> discoveredPaths = findPath(path, neighbor, targetName);
      if (discoveredPaths != null) paths.addAll(discoveredPaths);
    }
    // printPaths(paths);
    return paths;
  }

  private List<List<OperatorCore>> findPath(List<OperatorCore> path, OperatorCore current, String targetName) {
    path.add(current);
    if (current.getOperatorName().equals(targetName)) {
      return Collections.singletonList(path); // stopping condition: target found
    } else if (current.getNumGraphNodeOutputs() < 1) {
      return null; // stopping condition: end reached -> discard path
    } else { // recursion
      List<List<OperatorCore>> paths = new ArrayList<>();
      for (OperatorCore neighbor : current.getAllSucceedingGraphNodes(false)) {
        List<List<OperatorCore>> finalPaths = findPath(new ArrayList<OperatorCore>(path), neighbor, targetName);
        if (finalPaths != null) paths.addAll(finalPaths);
      }
      return paths;
    }
  }

  private List<OperatorCore> findScope(OperatorCore op) {
    List<InputPort> inPorts = op.getInputPorts();
    List<List<OperatorCore>> paths = new ArrayList<List<OperatorCore>>();
    for (int i = 0; i < inPorts.size(); i++) {
      OperatorCore neighbor = inPorts.get(i).getIncomingArc().getSource();
      List<OperatorCore> scopePaths = new ArrayList<>();
      List<List<OperatorCore>> discoveredPaths = findScope(scopePaths, neighbor);
      if (discoveredPaths != null) paths.addAll(discoveredPaths);
    }
    // printPaths(paths);
    // the longest path is the one we use to understand the scope of this operator
    List<OperatorCore> corePath =
            paths.stream().reduce(Collections.emptyList(), (one, two) -> one.size() >= two.size() ? one : two);
    // System.out.println("Selected scope path: " + Arrays.deepToString(corePath.toArray()));
    Collections.reverse(corePath);
    return corePath;
  }

  private List<List<OperatorCore>> findScope(List<OperatorCore> scopePath, OperatorCore current) {
    // TODO later this might need to include loops
    if (isConditional(current)) {
      scopePath.add(current); // this switch is the scope.
    }
    if (current.getNumGraphNodeInputs() < 1) {
      // stopping condition: end reached
      return scopePath.isEmpty() ? Collections.emptyList() : Collections.singletonList(scopePath);
    } else { // recursion
      List<List<OperatorCore>> scopePaths = new ArrayList<>();
      for (OperatorCore neighbor : current.getAllPreceedingGraphNodes(false)) {
        List<List<OperatorCore>> finalPaths = findScope(new ArrayList<OperatorCore>(scopePath), neighbor);
        if (!finalPaths.isEmpty()) scopePaths.addAll(finalPaths);
      }
      return scopePaths;
    }
  }

  @SuppressWarnings("unused")
  // for debugging purposes
  private void printPaths(List<List<OperatorCore>> paths) {
    for (List<OperatorCore> path : paths) {
      StringBuilder b = new StringBuilder();
      for (OperatorCore op : path) {
        b.append(op.getOperatorName());
        b.append("/");
      }
      System.out.println("Path: " + b.toString());
    }
  }

}
