/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile.analysis.qual;

/**
 * Marks a function that returns the same value for the same input and the same state of its
 * owner object (if it has any).
 * 
 * @author sertel
 * 
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value = java.lang.annotation.ElementType.METHOD)
public @interface Deterministic {
  
}
