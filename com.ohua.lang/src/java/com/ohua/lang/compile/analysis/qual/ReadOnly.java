/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.compile.analysis.qual;

/**
 * Use this annotation for parameters to indicate that they are accessed in a read-only fashion
 * inside the function.
 * 
 * @author sertel
 *
 */
@java.lang.annotation.Documented
@java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.RUNTIME)
@java.lang.annotation.Target(value = { java.lang.annotation.ElementType.PARAMETER})
public @interface ReadOnly {
  
}
