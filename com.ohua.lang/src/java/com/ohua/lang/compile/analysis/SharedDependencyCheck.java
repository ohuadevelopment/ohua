/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.lang.compile.analysis;

import java.util.List;

import com.ohua.engine.flowgraph.elements.operator.IFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.exceptions.CompilationException;

/**
 * The interface for checkers to interpret shared dependencies of results on the algorithm
 * level.
 * 
 * @author sertel
 * 
 */
public interface SharedDependencyCheck {

  static IFunctionalOperator getTargetOperator(int targetSlot, OperatorCore sourceOp, CompileTimeView cv){
    int portIdx = targetSlot;
    InputPort targetPort = sourceOp.getOutputPorts().get(portIdx).getOutgoingArcs().get(0).getTargetPort();
    IFunctionalOperator targetOp = (IFunctionalOperator) targetPort.getOwner().getOperatorAlgorithm();
    return targetOp;
  }

  void checkSharedDependency(CompileTimeView cv, Integer sourceSlot, List<Integer> targetSlots,
                                OperatorCore sourceOp) throws CompilationException;
  
}
