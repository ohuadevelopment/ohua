/*
 * Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.ohua.engine.OhuaProcessRunner;
import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.SimpleProcessListener;
import com.ohua.engine.UserRequest;
import com.ohua.engine.UserRequestType;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.FunctionalOperatorFactory;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.resource.management.ResourceConnection;
import com.ohua.engine.resource.management.ResourceManager;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.functions.FunctionInvocation;
import com.ohua.lang.io.IOHandle;
import com.ohua.lang.runtime.PreparedRuntimeConfiguration;
import com.ohua.lang.runtime.RuntimeStatistics;

public class OhuaRuntime extends OhuaProgram {
  private OhuaProcessRunner _runner = null;
  private SimpleProcessListener _listener = null;
  public static long EXEC_START = 0;
  
  public OhuaRuntime() {
    // TODO support multiple runtimes in a single clojure program
    OhuaRuntimeRegistry.get().register("single-runtime", this);
  }
  
  /**
   * Handle IO handles and transform them into real connections.
   */
  // TODO move this up such that it also applies to function invocations
  public void setArguments(int operator, Tuple<Integer, Object>[] arguments) throws CompilationException {
    for(int i = 0; i < arguments.length; i++) {
      if(arguments[i].second() instanceof IOHandle) {
        OperatorCore op = super.findOperator(super._process.getGraph(), operator);
        ResourceConnection cnn =
            ResourceManager.getInstance().openDirectConnection(op.getId(),
                                                               ((IOHandle) arguments[i].second()).getResourceID(),
                                                               ((IOHandle) arguments[i].second()).getArguments());
        arguments[i]._t = cnn;
      }
    }
    super.setArguments(operator, arguments);
  }
  
  /**
   * Execute the flow using the default runtime configuration.
   */
  public void execute() throws Throwable {
    RuntimeProcessConfiguration.LOGGING_ENABLED = false;
    execute(new RuntimeProcessConfiguration());
  }
  
  public void execute(Map<String, Object> compileTimeInfo) throws Throwable {
    RuntimeProcessConfiguration config = handleCompileTimeInfo(new RuntimeProcessConfiguration(), compileTimeInfo);
    execute(config);
  }
  
  /**
   * The final call to run the current flow graph.
   */
  public void execute(RuntimeProcessConfiguration config) throws Throwable {
    PreparedRuntimeConfiguration conf = handleCompileTimeInfo(config, Collections.emptyMap());
    executeNoWait(conf);
    awaitCompletion();
  }

  public void execute(PreparedRuntimeConfiguration config) throws Throwable {
    executeNoWait(config);
    awaitCompletion();
  }

  public void execute(RuntimeProcessConfiguration config, Map<String, Object> compileTimeInfo) throws Throwable {
    PreparedRuntimeConfiguration conf = handleCompileTimeInfo(config, compileTimeInfo);
    execute(conf);
  }
  
  private PreparedRuntimeConfiguration handleCompileTimeInfo(RuntimeProcessConfiguration config,
                                                            Map<String, Object> compileTimeInfo)
  {
    // System.out.println("Received compile time info: " + compileTimeInfo);
    PreparedRuntimeConfiguration conf =
        new PreparedRuntimeConfiguration(getCompileTimeView(), getRuntimeView(), compileTimeInfo);
    config.aquirePropertiesAccess(conf);
    return conf;
  }
    
  /**
   * Execute the flow without waiting for its completion (using the default runtime
   * configuration).
   */
  public void executeNoWait() throws Throwable {
    PreparedRuntimeConfiguration conf = handleCompileTimeInfo(new RuntimeProcessConfiguration(), Collections.emptyMap());
    executeNoWait(conf);
  }
  
  public void executeNoWait(RuntimeProcessConfiguration config) throws Throwable {
    PreparedRuntimeConfiguration conf = handleCompileTimeInfo(config, Collections.emptyMap());
    executeNoWait(conf);
  }

  public void executeNoWait(PreparedRuntimeConfiguration config) throws Throwable {
//    System.out.println(config);
    super.prepare();
    config.prepare(_process.getGraph());

    RuntimeProcessConfiguration.LOGGING_ENABLED = config.isLoggingEnabled();
    
    if(RuntimeProcessConfiguration.LOGGING_ENABLED) System.out.println("Executing flow graph ...");
    _runner = new OhuaProcessRunner(this, config);
    
    _listener = new SimpleProcessListener();
    _runner.register(_listener);
    Thread runnerThread = new Thread(_runner, "ohua-process");
    runnerThread.setDaemon(true);
    runnerThread.start();
    _runner.submitUserRequest(new UserRequest(UserRequestType.INITIALIZE));
    _listener.awaitProcessingCompleted();
    _listener.reset();
    EXEC_START = System.currentTimeMillis();
    _runner.submitUserRequest(new UserRequest(UserRequestType.START_COMPUTATION));
  }
  
  public void awaitCompletion() throws Throwable {
    _listener.awaitProcessingCompleted();
    if(RuntimeProcessConfiguration.LOGGING_ENABLED) System.out.println("Exec only: " + (System.currentTimeMillis() - EXEC_START));
    _listener.reset();
    _runner.submitUserRequest(new UserRequest(UserRequestType.SHUT_DOWN));
    _listener.awaitProcessingCompleted();
    RuntimeStatistics.GlobalStatsLogger.log(_process.getGraph());
    if(RuntimeProcessConfiguration.LOGGING_ENABLED) System.out.println("Done!");
  }
  
  public RuntimeView getRuntimeView() {
    final OhuaRuntime runtime = this;
    return new RuntimeView() {
      @Override
      public List<String> getAllOperators(String regex) {
        List<OperatorCore> ops = _process.getGraph().getOperators(regex);
        List<String> opIds = new ArrayList<String>(ops.size());
        for(OperatorCore op : ops)
          opIds.add(op.getOperatorName());
        return opIds;
      }
      
      @Override
      public AbstractFunctionalOperator createOperator(String type) throws OperatorLoadingException {
        return (AbstractFunctionalOperator) FunctionalOperatorFactory.getInstance().createUserOperatorInstance(type);
      }
      
      @Override
      public List<FunctionInvocation> getFunctionInvocations(String functionID) {
        return runtime.getInvocations(functionID);
      }
      
      @Override
      public OperatorCore findOperator(String opName) {
        return _process.getGraph().getOperator(opName);
      }

      @Override
      public OperatorCore findOperator(int id) {
        return runtime.findOperator(_process.getGraph(), id);
      }
    };
  }
  
  public void inject(LinkedList<IMetaDataPacket> requests) {
    UserRequest request = new UserRequest(UserRequestType.FLOW_INPUT, requests);
    _runner.submitUserRequest(request);
  }
}
