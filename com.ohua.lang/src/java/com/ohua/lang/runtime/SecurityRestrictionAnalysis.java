/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.RuntimeView;

import aua.analysis.AbstractOperatorAnalysis;
import clojure.lang.IDeref;
import transactified.compile.STMTransformation.TransformationImpossible;

public class SecurityRestrictionAnalysis implements RuntimeRestrictionAnalysis {

  private Map<String, List<int[]>> _sharedEnvVarInfo = null;
  
  protected SecurityRestrictionAnalysis(Map<String, List<int[]>> sharedEnvVarInfo){
    _sharedEnvVarInfo = sharedEnvVarInfo;
  }
  
  /**
   * check the type again to understand whether the argument brings its own protection or needs
   * to be protected by the Ohua runtime.
   * 
   * @return
   */  
  @Override
  public List<OperatorID[]> defineRestrictions(CompileTimeView cv, RuntimeView rv) {
      List<OperatorID[]> restrictions = new ArrayList<>();
      for(Map.Entry<String, List<int[]>> entry : _sharedEnvVarInfo.entrySet()) {
        // one retrieval is enough
        int[] opSlot = entry.getValue().get(0);
        Object sharedEnvVar = cv.getArguments(opSlot[0])[opSlot[1]];
        boolean trySTM = true;
        while(true) {
          /*
           * the compilation already checked the types and makes sure of the following: if the
           * function uses Clojure data structures as parameters then they must match with the
           * global variable. if this is an STM data structure than the parameter must be the type
           * of the data structure to be passed in.
           */
          if(trySTM && sharedEnvVar instanceof IDeref) {
            // STM protection is possible if we can convert this operator
            List<AbstractFunctionalOperator> ops =
                entry.getValue().stream().map(os -> (AbstractFunctionalOperator) rv.findOperator(os[0]).getOperatorAlgorithm()).collect(Collectors.toList());
            // perform op transformation here. if any of those ops can not be transformed then
            // we fall back to the section synchronization
            try {
              ProtectedFunctionExecution.loadAndCharge(ops);
              System.out.println("WARNING: STM restricted operators:");
              System.out.println(Arrays.deepToString(entry.getValue().stream().map((int[] os) -> rv.findOperator(os[0]).getOperatorName()).toArray(String[]::new)));
            }
            catch(TransformationImpossible ti) {
              System.out.println("WARNING: STM transformation for function '"
                                 + "' not possible. Falling back to section synchronization.");
              trySTM = false;
              continue; // fall back to section synchronization
            }
            catch(IOException i) {
              System.out.println("WARNING: STM transformation for function '"
                                 + "' has I/O problems. Falling back to section synchronization.\n I/O problem: "
                                 + i.getMessage());
              trySTM = false;
              continue; // fall back to section synchronization
              
            }
          } else if(sharedEnvVar.getClass().getName().startsWith("java.util.concurrent")
                    || AbstractOperatorAnalysis.IMMUTABLE_TYPES.contains(sharedEnvVar.getClass().getName()))
          {
            // it is ok to execute those operators in parallel as they are already protected
          } else {
            // collect all operator ids
            OperatorID[] opIDs = collectOpIds(rv, entry.getValue());
            restrictions.add(opIDs);
            System.out.println("WARNING: Restricting execution of the following operators:");
            System.out.println(Arrays.deepToString(entry.getValue().stream().map((int[] os) -> rv.findOperator(os[0]).getOperatorName()).toArray(String[]::new)));
          }
          break;
        }
      }
      return restrictions;
  }
  
  private OperatorID[] collectOpIds(RuntimeView rv, List<int[]> candidates) {
    return candidates.stream().map((int[] os) -> rv.findOperator(os[0]).getId()).toArray(OperatorID[]::new);
  }

}
