/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.runtime;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.RuntimeView;

@Deprecated // we will follow a different approach to realize fusion of functions.
public class OptimziationRestrictionAnalysis implements RuntimeRestrictionAnalysis {
  
  @Override
  public List<OperatorID[]> defineRestrictions(CompileTimeView cv, RuntimeView rv) {
    List<OperatorID[]> colocated = colocate(rv, "com.ohua.lang/destructure.*", this::colocateUpstream);
    colocated.addAll(colocate(rv, "com.ohua.lang/size.*", this::colocateUpstream));
    colocated.addAll(colocate(rv, "com.ohua.lang/one-to-n.*", this::colocateDownstream));

    // TODO merge!

    // TODO raise an exception when the developer tries to map one of these system ops!

    return colocated;
  }

  private List<OperatorID[]> colocate(RuntimeView rv, String opPattern, BiFunction<String, RuntimeView, OperatorID[]> f) {
    List<String> destOps = rv.getAllOperators(opPattern);
    return destOps.stream().map(s -> f.apply(s, rv)).collect(Collectors.toList());
  }
  
  private OperatorID[] colocateDownstream(String destOp, RuntimeView rv) {
    OperatorCore op = rv.findOperator(destOp);
    assert op.getAllSucceedingGraphNodes().size() == 1 : "Succeeding: " + op.getAllSucceedingGraphNodes().size();
    OperatorCore succeeding = op.getAllSucceedingGraphNodes().get(0);
    return new OperatorID[] { op.getId(),
                              succeeding.getId() };
  }

  private OperatorID[] colocateUpstream(String destOp, RuntimeView rv) {
    OperatorCore op = rv.findOperator(destOp);
    assert op.getAllPreceedingGraphNodes().size() == 1 : "Preceding: " + op.getAllPreceedingGraphNodes().size();
    OperatorCore preceding = op.getAllPreceedingGraphNodes().get(0);
    return new OperatorID[] { preceding.getId(), op.getId()};
  }

}
