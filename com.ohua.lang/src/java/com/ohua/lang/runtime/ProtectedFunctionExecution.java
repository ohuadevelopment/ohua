/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.runtime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.FunctionExecution;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.StatefulFunction;

import transactified.compile.STMTransformation.TransformationImpossible;
import transactified.runtime.STMClassLoading;
import transactified.runtime.STMFunctionExecution;

public class ProtectedFunctionExecution extends FunctionExecution {
  
  public static void loadAndCharge(List<AbstractFunctionalOperator> ops) throws TransformationImpossible, IOException {
    List<?> fos =
        STMClassLoading.load(ops.stream().map(AbstractFunctionalOperator::getFunctionType).collect(Collectors.toList()));
    Assertion.invariant(ops.size() == fos.size());
    for(int i = 0; i < fos.size(); i++) {
      AbstractFunctionalOperator op = ops.get(i);
      op.setFunctionObject(fos.get(i));
      op.setExecution(new ProtectedFunctionExecution());
    }
  }
  
  protected Object execute(String opName, final StatefulFunction algorithm, Object[] actuals) {
    final ProtectedFunctionExecution f = this;
    Object result = null;
    try {
      result = STMFunctionExecution.execute(new Callable<Object>() {
        @Override
        public Object call() throws Exception {
          return f.executeSuper(opName, algorithm, actuals);
        }
      });
    }
    catch(Exception e) {
      // the transaction failed out of whatever reason
      Assertion.impossible(e);
      return null;
    }
    
    return result;
  }
  
  protected Object executeSuper(String opName, StatefulFunction algorithm, Object[] actuals) {
    return super.execute(opName, algorithm, actuals);
  }
}
