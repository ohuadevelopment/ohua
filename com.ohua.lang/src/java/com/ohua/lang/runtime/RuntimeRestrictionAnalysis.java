/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.runtime;

import java.util.List;

import com.ohua.engine.flowgraph.elements.operator.OperatorID;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.RuntimeView;

public interface RuntimeRestrictionAnalysis {
  public List<OperatorID[]> defineRestrictions(CompileTimeView cv, RuntimeView rv);
}
