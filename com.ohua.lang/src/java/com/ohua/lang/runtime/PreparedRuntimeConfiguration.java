/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.runtime;

import com.ohua.engine.AbstractRuntime;
import com.ohua.engine.ConfigurationExtension;
import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.WorkBasedRuntime;
import com.ohua.engine.data.model.daapi.DataAccess;
import com.ohua.engine.data.model.daapi.DataFormat;
import com.ohua.engine.flowgraph.elements.ConcurrentArcQueue;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.*;
import com.ohua.engine.sections.AbstractSectionGraphBuilder;
import com.ohua.engine.sections.SectionScheduler;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.RestrictedSectionMapping;
import com.ohua.lang.RuntimeView;
import com.ohua.lang.compile.FlowGraphCompiler;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@SuppressWarnings("serial")
public class PreparedRuntimeConfiguration extends RuntimeProcessConfiguration implements ConfigurationExtension {

  private CompileTimeView _cv = null;
  private RuntimeView _rv = null;
  private Map<String, Object> _compileTimeInfo = null;

  /*
   * computed
   */
  private List<OperatorID[]> _sectionRestrictions = new ArrayList<>();

  public PreparedRuntimeConfiguration(CompileTimeView cv, RuntimeView rv, Map<String, Object> compileTimeInfo) {
    _cv = cv;
    _rv = rv;
    _compileTimeInfo = compileTimeInfo;
  }

  @SuppressWarnings("unchecked")
  public void prepare(FlowGraph graph) {
    prepareSectionRestrictions();
//    defineStrictCallSemantics();
  }

  public AbstractRuntime getRuntime() {
    return new WorkBasedRuntime(this);
  }

  // TODO implement via a scheduling algorithm
//  private void defineStrictCallSemantics() {
//    _properties.put("", (Function<NotificationBasedOperatorRuntime, Boolean>)
//            (NotificationBasedOperatorRuntime opRuntime) ->
//              (opRuntime.getOp().getOperatorAlgorithm() instanceof FunctionalOperator) ?
//                      // calls to stateful functions are strict
//                      opRuntime.getOp().getInputPorts().stream().
//                              allMatch(i ->
//                                      i.isMetaPort() || // meta ports must always be ready
//                                              // only if the port is active, it needs to have data!
//                                              i.hasSeenLastPacket() ||
//                                              // TODO could be expensive. buffer controllers in closure.
//                                              opRuntime.getOp().getDataLayer().getInputPortController(i.getPortName()).hasData())
//                      : true
//            );
//  }

  private void prepareSectionRestrictions() {
    if (_compileTimeInfo.containsKey(FlowGraphCompiler.SHARED_VARIABLES_INFO)) {
      List<OperatorID[]> securityRestrictions =
              new SecurityRestrictionAnalysis((Map<String, List<int[]>>) _compileTimeInfo.get(FlowGraphCompiler.SHARED_VARIABLES_INFO)).defineRestrictions(_cv,
                      _rv);
      _sectionRestrictions.addAll(securityRestrictions);
    }
  }

  public AbstractSectionGraphBuilder getSectionStrategy() {
    if (_properties.getProperty("section-strategy") == null) _properties.setProperty("section-strategy",
            "com.ohua.lang.runtime.RestrictionBasedSectionMapping");

    if (_sectionRestrictions.isEmpty()) return super.getSectionStrategy();
    else return new RestrictedSectionMapping(this, super.getSectionStrategy(), _sectionRestrictions);
  }

//  public AbstractProcessManager getProcessManager(DataFlowProcess process) {
//    return new LanguageDataAccess(process, this);
//  }

  public DataAccess getDataAccess(AbstractOperatorRuntime op, DataFormat dataFormat) {
    assert dataFormat == null;
    return new LanguageDataAccess(op, dataFormat);
  }

  public DataFormat getDataFormat() {
    return null;
  }

  public Class<? extends Deque> getInterSectionQueueImpl() {
    ConcurrentArcQueue.BatchedConcurrentLinkedDeque.BATCH_SIZE = getOrDefault("batching-concurrent-queue.batch-size", 10);
    assert ConcurrentArcQueue.BatchedConcurrentLinkedDeque.BATCH_SIZE < getInterSectionArcBoundary();
    return ConcurrentArcQueue.BatchedConcurrentLinkedDeque.class;
  }

  protected <T> T getOrDefault(String key, T defaultValue) {
    return (T) _properties.getOrDefault(key, defaultValue);
  }

//  public AbstractScheduler getScheduler() {
////    TaskScheduler scheduler = new TaskScheduler();
//    IRuntime scheduler = super.getScheduler();
////    scheduler.addActivationRequirement(new UpstreamActivationRequirement());
////    scheduler.addActivationRequirement(new NoUpStreamActivationWhenDataNotReady());
////    scheduler.addActivationRequirement(new AllInputsLoaded());
////    return scheduler;
////    DataflowScheduler scheduler = new DataflowScheduler(getOrDefault("df-quanta", 100));
//    return scheduler;
//  }

//  /**
//   * SCENARIO/GRAPH: Operator X has two downstream operators A and B, where A is fast and B is slow. At least 2 threads in the thread pool. Arc size is 100.</b>
//   * SCHEDULING: Operator X fills up the arcs in its first run and then A and B are activated and run in parallel. A finishes ahead of B where A has only processed 5 packets.
//   * Now A activates X (upstream activation). X starts to run and pushes only 10 more packets into each arc because then the boundary is reached for the arc X->B.
//   * Hence, also the arc between X->A also only carries 10 instead of 100 packets. => The result is heavy scheduling overhead.</b>
//   * ASSUMPTION: X is deterministic and backs out once one of its outgoing arc boundaries is reached.</b>
//   * RESTRICTION: X shall only become active iff the last downstream op finished,.i.e., all its outgoing arcs are empty.
//   */
//  // TODO the assumption holds for AbstractFunctionalOperator ops. not sure it holds for non-strict ops or merge!
//  private class UpstreamActivationRequirement implements SectionScheduler.ActivationRequirement {
//    public Boolean apply(OperatorCore op, SectionScheduler.ActivationType t) {
//      if (t == SectionScheduler.ActivationType.UPSTREAM) {
//        // to make this thing work with the batched arc, we will have to check whether the load in relation to the batch size.
//        // but this also means that if the arc is loaded with less data than the batch size, we will still allow ping pong scheduling situations.
////        boolean result = op.getOutputPorts().stream().filter(o -> !o.isMetaPort()).flatMap(o -> o.getOutgoingArcs().stream()).allMatch(Arc::isQueueEmpty);
//        boolean result = op.getOutputPorts().stream()
//                .filter(o -> !o.isMetaPort())
//                .flatMap(o -> o.getOutgoingArcs().stream())
//                .allMatch(a -> a.getLoadEstimate() <= ConcurrentArcQueue.BatchedConcurrentLinkedDeque.BATCH_SIZE);
//
////        if (!result) {
////          StringBuffer buf = new StringBuffer();
////          buf.append("Not scheduling operator: " + op.getOperatorName() + "\n");
////          buf.append("Input load:\n");
////          op.getGraphNodeInputConnections().stream().forEach(a -> buf.append("Arc size: " + a.getLoadEstimate() + "\n" + "empty?:" + a.isQueueEmpty() + "\n"));
////          buf.append("Output load:\n");
////          op.getGraphNodeOutputConnections().stream().forEach(a -> buf.append("Arc size: " + a.getLoadEstimate() + "\n" + "empty?:" + a.isQueueEmpty() + "\n"));
////          System.out.println(buf.toString());
////        }else{
////          StringBuffer buf = new StringBuffer();
////          buf.append("(Up) Scheduling operator: " + op.getOperatorName() + "\n");
////          buf.append("Input load:\n");
////          op.getGraphNodeInputConnections().stream().forEach(a -> buf.append("Arc size: " + a.getLoadEstimate() + "\n" + "empty?:" + a.isQueueEmpty() + "\n"));
////          buf.append("Output load:\n");
////          op.getGraphNodeOutputConnections().stream().forEach(a -> buf.append("Arc size: " + a.getLoadEstimate() + "\n" + "empty?:" + a.isQueueEmpty() + "\n"));
////          System.out.println(buf.toString());
////        }
//        return result;
//      } else {
////        StringBuffer buf = new StringBuffer();
////        buf.append("(Down) Scheduling operator: " + op.getOperatorName() + "\n");
////        op.getGraphNodeInputConnections().stream().forEach(a -> buf.append("Arc size: " + a.getLoadEstimate() + "\n" + "empty?:" + a.isQueueEmpty() + "\n"));
////        System.out.println(buf.toString());
//        return true;
//      }
//    }
//  }
//
//  // FIXME are these requirements general enough for the complete language implementation?
//  private class NoUpStreamActivationWhenDataNotReady implements SectionScheduler.ActivationRequirement {
//    public Boolean apply(OperatorCore op, SectionScheduler.ActivationType t) {
//      if (t == SectionScheduler.ActivationType.UPSTREAM) {
////        if(op.isInputComplete()){
////          // always activate when this is a source!
////          return true;
////        }else{
//        // FIXME there is no safety property here because what if the upstream op stopped producing data because the arc boundary was reached?
//        //       this would work only, if the operator would also activate itself again and does not rely on an upstream activation.
//        //       so this upstream activation is a safety property in our scheduling must never be tampered with. it is not clear to me yet
//        //       whether such situations should be detected by the scheduler before applying the requirements.
////          if(!op.isActive()){ // FIXME does not work because ops are already active before the section gets scheduled.
//        // note: op X -> op Y. boundary = 1. X pushes 1 packet and activates Y. Y deques the packet, processes it and activates X before X was deactivated.
//        //       in this case we drop the activation here and enter a deadlock. hence, there needs to be something inside the scheduler to catch that case.
//        //       checking this is currently not possible because first the data is sent to downstream and only then the flag can be set.
//        //       an easier solution is to never have operators that backed out because of full arcs wait for an upstream notification. it would probably
//        //       be better to let the scheduler decide: when an operator gets is done then the scheduler should just make a decision which operator to run next.
//        //       the question remains: how does this work for sections?
//
//        // TODO this is the very same restriction as below
//        if (op.getInputPorts()
//                .stream()
//                .filter(i -> !i.isMetaPort())
//                .filter(i -> i.getIncomingArc().getSourcePort().isActive()) // EOS send? -> always schedule!
//                .allMatch(i -> i.getIncomingArc().getLoadEstimate() >= ((AsynchronousArcImpl) i.getIncomingArc().getImpl()).getActivationMark())) {
//          return true;
//        } else {
//          // deactivate all upstream notifications.
//          return false;
//        }
////        }
//      } else {
//        return true;
//      }
//    }
//  }
//
//  private class AllInputsLoaded implements SectionScheduler.ActivationRequirement {
//    public Boolean apply(OperatorCore op, SectionScheduler.ActivationType t) {
//      if (t == SectionScheduler.ActivationType.DOWNSTREAM) {
//        if (op.getInputPorts()
//                .stream()
//                .filter(i -> !i.isMetaPort())
//                .filter(i -> i.getIncomingArc().getSourcePort().isActive()) // EOS send? -> always schedule!
//                .allMatch(i -> i.getIncomingArc().getLoadEstimate() >= ((AsynchronousArcImpl) i.getIncomingArc().getImpl()).getActivationMark())) {
//          return true;
//        } else {
//          return false;
//        }
//      } else {
//        return true;
//      }
//    }
//  }
}
