/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.utils.GraphVisualizer;
import com.ohua.lang.exceptions.CompilationException;
import com.ohua.lang.functions.FunctionInvocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class OhuaProgram extends OhuaFrontend
{
  // func-id -> function invocation
  private Map<Integer, FunctionInvocation> _functionInvocations = new HashMap<>();
  
  // TODO do the same for the output schema
  protected void registerInputSchema(int target, int[] explicitTargetMatching, int matchType) {
    if(_functionInvocations.containsKey(new Integer(target))) {
      FunctionInvocation inv = _functionInvocations.get(new Integer(target));
      inv.registerInputSchema(target, explicitTargetMatching, matchType);
    }
    else {
      super.registerInputSchema(target, explicitTargetMatching, matchType);
    }
  }
  
  // TODO handle namespaces!
  protected final List<FunctionInvocation> getInvocations(String functionName) {
    List<FunctionInvocation> invocations = new ArrayList<>();
    for(FunctionInvocation invocation : _functionInvocations.values()) {
      if(invocation.getFunctionName().equals(functionName)) {
        invocations.add(invocation);
      }
    }
    return invocations;
  }
  
  public void registerDependency(int source, int sourcePos, int target, int targetPos, int isFeedback) {
//    System.out.println("Register dependency: " + source + " " + target + " " + targetPos + " " + isFeedback);
    if(_functionInvocations.containsKey(new Integer(source))) {
      FunctionInvocation funcInv = _functionInvocations.get(new Integer(source));
      source = funcInv.getOutputFormal();
      if(RuntimeProcessConfiguration.LOGGING_ENABLED) {
        System.out.println("Corrected output: " + source);
      }
    }
    else if(_functionInvocations.containsKey(new Integer(target))) {
      FunctionInvocation funcInv = _functionInvocations.get(new Integer(target));
      target = funcInv.getInputFormal();
    }
    super.registerDependency(source, sourcePos, target, targetPos, isFeedback);
  }
  
  protected final void prepare() {
    for(FunctionInvocation inv : _functionInvocations.values()) {
      inv.resolveDependencies();
      _process.getGraph().addAll(inv._process.getGraph());
    }
    
    super.resolveDependencies();
    
    for(FunctionInvocation inv : _functionInvocations.values()) {
      inv.analyze();
    }
    
    // TODO we need an interface for transformation
    // FIXME these algorithms most likely want to be implemented on top of the new dataflow IR
    OneToNSupport.prepare(_process.getGraph());
    GraphVisualizer.printFlowGraph(_process.getGraph());
  }
  
  public void createOperator(String type, int id) throws OperatorLoadingException, CompilationException {
//    if(!Linker.isOperator(type)) {
//      handleFunctionCreation(type, id);
//    }
//    else {
//      super.createOperator(type, id);
//    }
      super.createOperator(type, id);
  }

//  private void handleFunctionCreation(String type, int id) throws CompilationException {
//    // TODO functions should actively participate in compilation similar to operators. this
//    // probably means there should be a Compilable interface for all components that wish to
//    // participate in compilation.
//    FunctionDeclaration func = Linker.findFunction(null, type);
//    FunctionInvocation funcInv = func.createInvocation(id);
//    _functionInvocations.put(new Integer(id), funcInv);
//  }
//
}
