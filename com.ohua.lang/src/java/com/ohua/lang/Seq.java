/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import com.ohua.lang.compile.analysis.qual.ReadOnly;

/**
 * Created by sertel on 6/1/16.
 */
public class Seq {
  @defsfn
  public boolean seq(@ReadOnly  Object... dependencies){
    return true;
  }
}
