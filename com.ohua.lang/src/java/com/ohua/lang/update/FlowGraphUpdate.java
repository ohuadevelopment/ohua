/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.update;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.FlowGraph;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.IFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.InputPort;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.engine.flowgraph.elements.operator.OutputPort;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.online.configuration.IConfigurationMarker;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;
import com.ohua.engine.online.configuration.ReplaceStateTransfer;
import com.ohua.lang.OhuaFrontend;
import com.ohua.lang.functions.FunctionFlowGraph;
import com.ohua.lang.functions.FunctionInvocation;

public class FlowGraphUpdate {
  private static class GraphIteration {
    enum FLOW {
               PIPE,
               SPLIT,
               END
    }
    
    List<OperatorCore> _graph = null;
    OperatorCore _currentEntry = null;
    List<OperatorCore> _visited = new ArrayList<>();
    
    GraphIteration(List<OperatorCore> graph) {
      _graph = graph;
    }
    
    /**
     * Returns the first operator that has an upstream connection to an operator outside of this
     * graph.
     * @return
     */
    FLOW findEntry() {
      for(OperatorCore op : _graph) {
        List<OperatorCore> upstream = getAllPreceedingGraphNodes(op);
        if(upstream.isEmpty() && !_visited.contains(op)) {
          _currentEntry = op;
          return FLOW.PIPE;
        } else {
          for(OperatorCore upOp : upstream) {
            if(!_graph.contains(upOp) && !_visited.contains(op)) {
              _currentEntry = op;
              return FLOW.PIPE;
            }
          }
        }
      }
      return FLOW.END;
    }
    
    /**
     * We can have inconnected input ports in the new graph.
     * 
     * @param op
     * @return
     */
    List<OperatorCore> getAllPreceedingGraphNodes(OperatorCore op) {
      List<OperatorCore> l = new ArrayList<>();
      for(InputPort inPort : op.getInputPorts()) {
        if(inPort.getIncomingArc() != null) {
          l.add(inPort.getIncomingArc().getSource());
        }
      }
      return l;
    }
    
    FLOW hasNext() {
      if(_currentEntry == null) return findEntry();
      
      List<OperatorCore> downstreamOps = _currentEntry.getAllSucceedingGraphNodes(false);
      if(downstreamOps.size() == 0) {
        _currentEntry = null;
        return FLOW.END;
      } else if(downstreamOps.size() == 1) {
        _currentEntry = downstreamOps.get(0);
        return FLOW.PIPE;
      } else if(downstreamOps.size() > 1) {
        // TODO what is to be done here? what control does a client of this class need at this
        // moment?
        return FLOW.SPLIT;
      } else {
        Assertion.impossible();
        return FLOW.END;
      }
    }
    
    OperatorCore next() {
      Assertion.invariant(_currentEntry != null);
      _visited.add(_currentEntry);
      return _currentEntry;
    }
  }
  
  private static class PreservationStateTransfer extends ReplaceStateTransfer {
    
    /**
     * Here it is the other way around: new is old and old is new!
     */
    @Override
    public void transfer(UserOperator oldOp, UserOperator newOp) {
      // TODO fix the pendingInput counter here!
      
      // this refreshes the references of the ports
      ((UserOperator) newOp).prepare();
      
      // TODO this guy also has to transfer the schema matching for the output ports of the
      // output operator!
      
//      System.out.println("Preservation transfer here!");
    }
    
  }
  
  public static LinkedList<IMetaDataPacket> update(FunctionInvocation oldGraphMeta,
                                                   FunctionInvocation newGraphMeta,
                                                   RuntimeProcessConfiguration config) throws Exception
  {
    // construct the input actuals for the new graph (the input ports of the function op)
    constructInputActuals(oldGraphMeta, newGraphMeta);
    
    return update(oldGraphMeta.getCompileTimeMeta(),
                  oldGraphMeta.load().getGraph(),
                  newGraphMeta.getCompileTimeMeta(),
                  newGraphMeta.load().getGraph(),
                  config);
  }
  
  public static LinkedList<IMetaDataPacket> update(FunctionFlowGraph oldGraphMeta, FlowGraph oldGraph,
                                                   FunctionFlowGraph newGraphMeta, FlowGraph newGraph,
                                                   RuntimeProcessConfiguration config)
  {
    // construct the output actual for the new graph
    constructOutputActual(oldGraphMeta, newGraphMeta, newGraph);
    
    // find the operators that are *not* to be updated
    Map<String, String> newToOld = mapOldToNew(oldGraph, newGraph);
    
    // construct the according packets and attach the preserve info
    return createReconnectionRequests(oldGraphMeta, oldGraph, newGraphMeta, newGraph, newToOld, config);
  }
  
  private static void constructInputActuals(FunctionInvocation oldGraphMeta,
                                            FunctionInvocation newGraphMeta) throws Exception
  {
    OperatorCore newFunctionOp =
        OhuaFrontend.findOperator(newGraphMeta.load().getGraph(), newGraphMeta.getFunctionName());
    String[] inputFormals = new String[oldGraphMeta.getCompileTimeMeta().getFormalsToInputPorts().length];
    for(int i = 0; i < oldGraphMeta.getCompileTimeMeta().getFormalsToInputPorts().length; i++) {
      String inputActual = oldGraphMeta.getCompileTimeMeta().getFormalsToInputPorts()[i];
      String portID = FlowGraph.parsePortReference(inputActual)[1];
      InputPort inPort = new InputPort(newFunctionOp);
      inPort.setPortName(portID);
      newFunctionOp.addInputPort(inPort);
      inputFormals[i] = FlowGraph.constructPortReference(inPort);
    }
    newGraphMeta.getCompileTimeMeta().setFormalsToInputPorts(inputFormals);
    
    // finally we have to inherit the explicit input schema match
    List<Object[]> explicitInput = oldGraphMeta.getCompileTimeView().getInputSchema(oldGraphMeta.getInputFormal());
    for(int i = 0; i < explicitInput.size(); i++)
      ((IFunctionalOperator) newFunctionOp.getOperatorAlgorithm()).setExplicitInputSchemaMatch(i,
                                                                                               (int[]) explicitInput.get(i)[0],
                                                                                               (int) explicitInput.get(i)[1]);
  }
  
  private static void constructOutputActual(FunctionFlowGraph oldGraphMeta, FunctionFlowGraph newGraphMeta,
                                            FlowGraph newGraph)
  {
    String[] newOutputActuals = new String[oldGraphMeta.getOutputActuals().length];
    for(int i = 0; i < oldGraphMeta.getOutputActuals().length; i++) {
      String output = oldGraphMeta.getOutputActuals()[i];
      String portName = FlowGraph.parsePortReference(output)[1];
      OperatorCore newOutput = newGraph.getOperator(newGraphMeta.getOutputFormal());
      OutputPort newOutPort = new OutputPort(newOutput);
      newOutPort.setPortName(portName);
      newOutput.addOutputPort(newOutPort);
      newOutputActuals[i] = FlowGraph.constructPortReference(newOutPort);
    }
    
    // register the new port reference at the meta data of the new graph
    newGraphMeta.setOutputActuals(newOutputActuals);
    
    // TODO and inherit the explicit output schema
    // int[] explicitOutput =
    // oldGraphMeta.getCompileTimeView().getOutputSchema(oldGraphMeta.getInputFormal());
    // ((AbstractFunctionalOperator)newOutputOp.getOperatorAlgorithm()).setExplicitoutputSchemaMatch(explicitInput);
  }
  
  private static LinkedList<IMetaDataPacket>
      createReconnectionRequests(FunctionFlowGraph oldGraphMeta, FlowGraph oldGraph,
                                 FunctionFlowGraph newGraphMeta, FlowGraph newGraph,
                                 Map<String, String> preserveInfo, RuntimeProcessConfiguration config)
  {
    LinkedList<IMetaDataPacket> packets = new LinkedList<IMetaDataPacket>();
    /*
     * reconnection for inputs
     */
    // the number of incoming arcs must be the same (for now)!
    // TODO handle args that are input to the same op (this would resemble only to one arc) ->
    // this would actually require fixing the upstream output schema matching and arc
    // connections
    String[][] inPortReconnect = new String[oldGraphMeta.getFormalsToInputPorts().length][2];
    Assertion.invariant(oldGraphMeta.getFormalsToInputPorts().length == newGraphMeta.getFormalsToInputPorts().length);
    for(int i = 0; i < oldGraphMeta.getFormalsToInputPorts().length; i++) {
      inPortReconnect[i][0] = oldGraphMeta.getFormalsToInputPorts()[i];
      inPortReconnect[i][1] = newGraphMeta.getFormalsToInputPorts()[i];
    }
//    System.out.println("Calculated input reconnections: " + Arrays.deepToString(inPortReconnect));
    
    /*
     * reconnection for output (there is always only one operator!)
     */
    String[][] outPortReconnect = new String[oldGraphMeta.getOutputActuals().length][2];
    Assertion.invariant(oldGraphMeta.getOutputActuals().length == newGraphMeta.getOutputActuals().length);
    for(int i = 0; i < oldGraphMeta.getOutputActuals().length; i++) {
      outPortReconnect[i][0] = oldGraphMeta.getOutputActuals()[i];
      outPortReconnect[i][1] = newGraphMeta.getOutputActuals()[i];
    }
//    System.out.println("Calculated output reconnections: " + Arrays.deepToString(outPortReconnect));
    
    // make sure that the output is in the preserve info
    if(!preserveInfo.containsKey(newGraphMeta.getOutputFormal())) preserveInfo.put(newGraphMeta.getOutputFormal(),
                                                                                   oldGraphMeta.getOutputFormal());
                                                                                   
    /*
     * finally construct the markers
     */
    IConfigurationMarker secondMarker =
        PacketFactory.createUpdateMarkerPacket(oldGraphMeta.getOutputFormal(),
                                               Category.REPLACE_OUTPUT,
                                               null,
                                               newGraph,
                                               outPortReconnect);
    IMetaDataPacket firstMarker = PacketFactory.createUpdateInitMarkerPacket(new Object[] { config,
                                                                                            newGraph,
                                                                                            inPortReconnect,
                                                                                            preserveInfo,
                                                                                            new PreservationStateTransfer() },
                                                                             secondMarker);
    packets.add(firstMarker);
    
    return packets;
  }
  
  /**
   * Assumptions on the flow graphs:
   * <ul>
   * <li>There exists exactly one single output operator.
   * </ul>
   * @param oldGraph
   * @param newGraph
   * @return
   */
  public static Map<String, String> mapOldToNew(FlowGraph oldGraph, FlowGraph newGraph) {
    Map<String, String> result = new HashMap<>();
    
    GraphIteration itOld = new GraphIteration(oldGraph.getContainedGraphNodes());
    
    // get the first op of the new graph
    GraphIteration itNew = new GraphIteration(newGraph.getContainedGraphNodes());
    itNew.findEntry();
    OperatorCore currentNew = itNew.next();
    boolean stop = false;
    while(true) {
      OperatorCore currentOld = null;
      switch(itOld.hasNext()) {
        case END:
//          System.out.println("END");
          switch(itOld.findEntry()) {
            case END:
              stop = true;
              break;
            default:
              currentOld = itOld.next();
          }
          break;
        case PIPE:
//          System.out.println("PIPE");
          currentOld = itOld.next();
          break;
        case SPLIT:
          // TODO what do we do here? do we have to make sure that the new graph takes the same
          // branch?
          break;
        default:
          break;
      }
      if(stop) break;
      
      OperatorCore found = search(currentOld, newGraph.getContainedGraphNodes(), currentNew);
      if(found == null) {
        // the operator seems to have been deleted from the graph
      } else {
        // we found the according op
        currentNew = found;
        result.put(currentNew.getOperatorName(), currentOld.getOperatorName());
      }
    }
//    System.out.println("Automatic mapping result: " + result);
    return result;
  }
  
  private static boolean operatorsAreEqual(OperatorCore a, OperatorCore b) {
//    System.out.println("Comparing operator " + a + " to operator " + b);
    boolean equal = a.getOperatorAlgorithm().getClass().equals(b.getOperatorAlgorithm().getClass());
    if(equal && AbstractFunctionalOperator.class.isAssignableFrom(a.getOperatorAlgorithm().getClass())) {
      equal = functionalOperatorsAreEqual((AbstractFunctionalOperator) a.getOperatorAlgorithm(),
                                          (AbstractFunctionalOperator) b.getOperatorAlgorithm());
    }
    return equal;
  }
  
  private static boolean functionalOperatorsAreEqual(AbstractFunctionalOperator a, AbstractFunctionalOperator b) {
    return a.getFunctionType().equals(b.getFunctionType());
  }
  
  protected static OperatorCore search(OperatorCore searched, List<OperatorCore> searchSpace, OperatorCore start) {
//    System.out.println("Searching operator " + searched + ", starting at operator " + start);
    if(operatorsAreEqual(searched, start)) {
      return start;
    } else {
      for(OperatorCore downstream : start.getAllSucceedingGraphNodes()) {
        if(searchSpace.contains(downstream)) {
          // depth first search
          OperatorCore found = search(searched, searchSpace, downstream);
          if(found != null) return found;
        }
      }
      return null;
    }
  }
  
}
