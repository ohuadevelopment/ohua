/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.update;

import java.util.List;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.IFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.UserOperator;
import com.ohua.engine.online.configuration.StateTransfer;
import com.ohua.lang.Tuple;

public class FunctionalOperatorStateTransfer implements StateTransfer<UserOperator, UserOperator> {
  private StateTransfer<Object, Object> _stateUpdate = null;
  
  private List<Object[]> _inputSchema = null;
  private List<int[]> _outputSchema = null;
  private Tuple<Integer, Object>[] _arguments = null;
  
  protected FunctionalOperatorStateTransfer(List<Object[]> inputSchema,
                                            List<int[]> outputSchema,
                                            Tuple<Integer, Object>[] arguments)
  {
    _inputSchema = inputSchema;
    _outputSchema = outputSchema;
    _arguments = arguments;
  }
  
  protected FunctionalOperatorStateTransfer(List<Object[]> inputSchema,
                                            List<int[]> outputSchema,
                                            Tuple<Integer, Object>[] arguments,
                                            StateTransfer<Object, Object> stateUpdate)
  {
    this(inputSchema, outputSchema, arguments);
    _stateUpdate = stateUpdate;
  }
  
  @Override
  public void transfer(UserOperator oldOp, UserOperator newOp) {
    Assertion.invariant(oldOp instanceof IFunctionalOperator);
    Assertion.invariant(newOp instanceof IFunctionalOperator);
    
    // set compile-time information
    if(_inputSchema != null) for(int i = 0; i < _inputSchema.size(); i++)
      ((IFunctionalOperator) newOp).setExplicitInputSchemaMatch(i,
                                                                (int[]) _inputSchema.get(i)[0],
                                                                (int) _inputSchema.get(i)[1]);
    if(_outputSchema != null) for(int i = 0; i < _outputSchema.size(); i++)
      ((IFunctionalOperator) newOp).setExplicitOutputSchemaMatch(i, _outputSchema.get(i));
    ((IFunctionalOperator) newOp).setArguments(_arguments);
    
    // transfer persistent state
    newOp.setState(oldOp.getState());
    
    // update state if necessary
    if(_stateUpdate != null) {
      ((AbstractFunctionalOperator) oldOp).transferState((AbstractFunctionalOperator) newOp, _stateUpdate);
    }
  }
  
}
