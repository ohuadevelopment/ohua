/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang.update;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.ohua.engine.RuntimeProcessConfiguration;
import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.extension.points.PacketFactory;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.packets.IMetaDataPacket;
import com.ohua.engine.online.configuration.IConfigurationMarker;
import com.ohua.engine.online.configuration.OnlineConfigurationMarker.Category;
import com.ohua.lang.CompileTimeView;
import com.ohua.lang.OhuaRuntime;
import com.ohua.lang.RuntimeView;
import com.ohua.lang.compile.Linker;
import com.ohua.lang.functions.FunctionDeclaration;
import com.ohua.lang.functions.FunctionInvocation;

/**
 * This class is the entry point for dynamic updates in Ohua driven from the Clojure-side.
 * 
 * @author sertel
 * 
 */
public abstract class OhuaUpdates
{
  
  public static void update(Map<String, String> updates, OhuaRuntime runtime) throws OperatorLoadingException {
    runtime.inject(update(updates, runtime.getCompileTimeView(), runtime.getRuntimeView()));
  }
  
  public static LinkedList<IMetaDataPacket> update(Map<String, String> updates, CompileTimeView compileTime,
                                                   RuntimeView runtime) throws OperatorLoadingException
  {
    Map<String, String> algUpdates = new HashMap<>();
    Map<String, String> opUpdates = new HashMap<>();
    for(Map.Entry<String, String> update : updates.entrySet()) {
      if(Linker.isOperator(update.getKey())) {
        opUpdates.put(update.getKey(), update.getValue());
      }
      else {
        algUpdates.put(update.getKey(), update.getValue());
      }
    }
    
    LinkedList<IMetaDataPacket> markers = new LinkedList<>();
    if(!algUpdates.isEmpty()) markers.addAll(updateAlgorithm(algUpdates, runtime));
    if(!opUpdates.isEmpty()) markers.addAll(updateOperators(opUpdates, compileTime, runtime));
    return markers;
  }
  
  /**
   * Updates the operators delivered to this function. All provided operators will be grouped
   * together as a mutual dependency update.
   * 
   * @return a list of packets to be injected into the flow graph
   * @throws OperatorLoadingException
   */
  private static
      LinkedList<IMetaDataPacket>
      updateOperators(Map<String, String> updates, CompileTimeView compileTime, RuntimeView runtime)
                                                                                                    throws OperatorLoadingException
  {
    // TODO support state transformers
    // TODO realize mutual dependencies explicitly. <- maybe this is not necessary anymore
    // because the engine should make sure that there is no packet injected into the flow in
    // between these packets. -> this requires that all meta packets are always retrieved before
    // any other packet is being handled! -> needs verification!
    LinkedList<IMetaDataPacket> updateRequests = new LinkedList<>();
    for(Map.Entry<String, String> update : updates.entrySet()) {
      String searchPattern = update.getKey() + ".*";
      for(String opRef : runtime.getAllOperators(searchPattern)) {
        int opID = compileTime.extractIDFromRef(opRef);
        AbstractFunctionalOperator newOp = runtime.createOperator(update.getValue());
        // System.out.println("New op: " + newOp.getClass());
        FunctionalOperatorStateTransfer stateTransfer =
            new FunctionalOperatorStateTransfer(compileTime.getInputSchema(opID),
                                                compileTime.getOutputSchema(opID),
                                                compileTime.getArguments(opID));
        IConfigurationMarker responseMarker =
            (IConfigurationMarker) PacketFactory.createConfigurationMarkerPacket(opRef,
                                                                                 "operator",
                                                                                 new Object[] { newOp,
                                                                                               stateTransfer },
                                                                                 Category.OPERATOR);
        updateRequests.add(responseMarker);
      }
    }
    return updateRequests;
  }
  
  /**
   * Creates the requests to perform an update of the currently executing algorithm.
   * 
   * @throws Exception
   */
  private static LinkedList<IMetaDataPacket> updateAlgorithm(Map<String, String> updates, RuntimeView runtime) {
    LinkedList<IMetaDataPacket> markers = new LinkedList<>();
    for(Map.Entry<String, String> entry : updates.entrySet()) {
      List<FunctionInvocation> oldFunctions = runtime.getFunctionInvocations(entry.getKey());
      FunctionDeclaration newFuncDecl = Linker.findFunction(null, entry.getValue());
      for(FunctionInvocation oldInv : oldFunctions) {
        FunctionInvocation newInv = newFuncDecl.createInvocation(oldInv.getInputFormal());
        newInv.prepare();
        // the output actuals are set in the update function below
        newInv.analyze();
        
        // TODO update the function invocation registry
        
        try {
          markers.addAll(FlowGraphUpdate.update(oldInv, newInv, new RuntimeProcessConfiguration()));
        }
        catch(Exception e) {
          // loading is done before, this is just a bare access
          Assertion.impossible();
        }
      }
    }
    
    return markers;
  }
}
