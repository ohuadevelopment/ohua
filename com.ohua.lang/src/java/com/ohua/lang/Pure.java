/*
 * ohua : Pure.java
 *
 * Copyright (c) Sebastian Ertel, Justus Adam 2017. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.lang;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by justusadam on 25/01/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Pure {
}
