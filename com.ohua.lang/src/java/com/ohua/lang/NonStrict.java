/*
 * Copyright (c) Sebastian Ertel and Justus Adam 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import com.ohua.engine.flowgraph.elements.operator.ContinuationRuntime;

import java.util.function.Supplier;

/**
 * Created by sertel on 9/12/16.
 */
public final class NonStrict<T> {

  private ContinuationRuntime.PendingValue<T> _pending = null;
  private boolean _frozen = false;

  public NonStrict(Supplier<T> retrieval, Supplier<Boolean> check) {
    _pending = ContinuationRuntime.createPendingValue(retrieval, check);
  }

  ContinuationRuntime.PendingValue<T> get(){
    return _pending;
  }

  void freeze() {
    if (_frozen) throw new DuplicateContinuationException();
    _frozen = true;
  }

  public static class DuplicateContinuationException extends RuntimeException {
    public DuplicateContinuationException() {
      super("Can't define more than one continutation for the same NonStrict.");
    }
  }


}
