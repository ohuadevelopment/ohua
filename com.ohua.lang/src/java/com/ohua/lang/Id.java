/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import aua.analysis.qual.Untainted;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

/**
 * Created by sertel on 6/2/16.
 */
public final class Id {
  @Pure
  @defsfn
  public Object id(@ReadOnly Object data) {
    @Untainted Object tmp = data;
    return tmp;
  }

}
