/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import clojure.lang.Indexed;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by sertel on 3/24/16.
 */
public class Tuple<S, T> implements Indexed {
  public S _s = null;
  public T _t = null;

  public Tuple(S s, T t) {
    _s = s;
    _t = t;
  }

  public S first() {
    return _s;
  }

  public T second() {
    return _t;
  }

  public boolean equals(Tuple<S, T> other) {
    return _s.equals(other._s) && _t.equals(other._t);
  }

  @Override
  public Object nth(int i) {
    if(i > 1)
      throw new IllegalArgumentException();
    return i == 0 ? _s : _t;
  }

  @Override
  public Object nth(int i, Object o) {
    Object a = nth(i);
    return a == null ? o : a;
  }

  @Override
  public int count() {
    return 2;
  }
}
