/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.lang;

import java.util.List;

import com.ohua.engine.exceptions.OperatorLoadingException;
import com.ohua.engine.flowgraph.elements.operator.AbstractFunctionalOperator;
import com.ohua.engine.flowgraph.elements.operator.OperatorCore;
import com.ohua.lang.functions.FunctionInvocation;

/**
 * This class is meant to provide a view on the currently executing graph.
 * 
 * @author sertel
 * 
 */
public interface RuntimeView
{
  /**
   * Finds all operators in the executing flow graph whose names match the provided regular
   * expression.
   * 
   * @param regex over the operator name
   * @return all operator names that match the regex
   */
  public List<String> getAllOperators(String regex);
    
  /**
   * Creates an operator of the defined type.
   * 
   * @param type
   * @return
   * @throws OperatorLoadingException 
   */
  public AbstractFunctionalOperator createOperator(String type) throws OperatorLoadingException;

  /**
   * Retrieves the list of function invocations in the currently executing flow graph.
   * 
   * @param functionID
   * @return
   */
  public List<FunctionInvocation> getFunctionInvocations(String functionID);

  /**
   * Retrieves the runtime instance of the operator.
   * 
   * @param id - compile-time id of the stateful function call site
   * @return
   */
  public OperatorCore findOperator(int id);

  /**
   * Retrieves the runtime instance of the operator.
   * 
   * @param opName - runtime name of the operator
   * @return
   */
  public OperatorCore findOperator(String opName);
}
