/*
 * ohua : Backend.java
 *
 * Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */

package com.ohua.backend;

import com.ohua.engine.flowgraph.elements.operator.IOperatorFactory;
import com.ohua.engine.flowgraph.elements.operator.StatefulFunction;

import java.util.Set;

/**
 * Created by justusadam on 01/12/2016.
 */
public interface SFNBackend {
    void initialize();

    Set<String> listNamespace(String namespace);

    boolean exists(String namespace, String functionName);

    IOperatorFactory getOperatorFactory();

    StatefulFunction asStatefulFunction(String namespace, String functionName);
}
