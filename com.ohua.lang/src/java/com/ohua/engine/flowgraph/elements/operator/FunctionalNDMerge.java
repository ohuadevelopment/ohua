/*
 * Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
 * 
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.flowgraph.elements.operator;

import java.lang.annotation.Annotation;
import java.util.Collections;

import com.ohua.engine.exceptions.Assertion;
import com.ohua.engine.operators.NonDeterministicMergeOperator;
import com.ohua.lang.Tuple;
import com.ohua.lang.exceptions.CompilationException;

public class FunctionalNDMerge extends NonDeterministicMergeOperator implements IFunctionalOperator {
  
  private AbstractSchemaMatcher _matcher = new FunctionalSchemaMatching.SchemaMatcher(this, this);
  
  protected void prepareOutputPorts() {
    _matcher.prepare();
  }
  
  protected boolean emitOutput(String inputPort) {
    Object results = super.getDataLayer().getInputPortController(inputPort).getData();
    return _matcher.matchOutputSchema(results);
  }

  public boolean isAlgoVarArgs(){
    return false;
  }
  
  @Override
  public int getFlowArgumentCount() {
    return 0;
  }
  
  public int getFlowFormalsCount() {
    return 0;
  }
  
  @Override
  public Class<?> getParameterType(int formalSchemaIndex) {
    return null;
  }
  
  @Override
  public Class<?> getReturnType() {
    return Object.class;
  }

  @Override
  public void compile(boolean typeSensitive) throws CompilationException {
    // we do not need any compilation here yet.
  }

  @Override
  public void runSafetyAnalysis(boolean strict) throws CompilationException {
    // nothing really
  }

  @Override
  public void setExplicitInputSchemaMatch(int[] explicitTargetMatching, int matchType) {
    // we discard these because there is no input schema matching in this operator.
  }
  
  public void setExplicitInputSchemaMatch(int portIdx, int[] explicitTargetMatching, int matchType) {
    // we discard these because there is no input schema matching in this operator.
  }
  
  public void setExplicitOutputSchemaMatch(int[] explicitSourceMatching) {
    SchemaSupport.setExplicitOutputSchemaMatch(this, _matcher, explicitSourceMatching);
  }
  
  public void setExplicitOutputSchemaMatch(int portIdx, int[] explicitSourceMatching) {
    SchemaSupport.setExplicitOutputSchemaMatch(this, _matcher, portIdx, explicitSourceMatching);
  }
  
  @Override
  public void setArguments(Tuple<Integer, Object>[] arguments) {
    // currently we do not support any arguments here. later on we might want to configure the
    // merge properties via this interface.
  }
  
  @Override
  public String getParameterName(int formalSchemaIndex) {
    Assertion.impossible("Should never be asked for this operator!");
    return null;
  }
  
  @Override
  public String getFunctionName() {
    Assertion.impossible("Should never be asked for this operator!");
    return null;
  }
  
  @Override
  public Annotation[] getParameterAnnotation(int formalSchemaIndex) {
    Assertion.impossible("Should never be asked for this operator!");
    return null;
  }
  
  @Override
  public Class<?>[] getFormalArguments() {
    return new Class<?>[0];
  }
  
  private Tuple<Integer, Object>[] _envArgs = new Tuple[0];
  
  @Override
  public Tuple<Integer, Object>[] getEnvironmentArguments() {
    return _envArgs;
  }
}
