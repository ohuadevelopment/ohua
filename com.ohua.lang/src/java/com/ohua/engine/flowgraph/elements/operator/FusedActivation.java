/*
 * Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
 *
 * This source code is licensed under the terms described in the associated LICENSE.TXT file.
 */
package com.ohua.engine.flowgraph.elements.operator;

@Deprecated
public abstract class FusedActivation {
  public static void activateDownstream(Arc executingOp, Arc target) {
    // FIXME
//    target.getImpl().activateDownstream(executingOp);
  }

  public static void activateDownstreamDefault(Arc executingOp, Arc target) {
    // FIXME
//    target.getImpl().activateDownstreamDefault(executingOp);
  }

  public static void activateUpstream(Arc executingOp, Arc upstream) {
    // FIXME
//    upstream.getImpl().activateUpstream(executingOp);
  }

  public static void activateUpstreamDefault(Arc executingOp, Arc upstream) {
    // FIXME
//    upstream.getImpl().activateUpstreamDefault(executingOp);
  }

  public static void finishOutputPort(OutputPort outPort){
    outPort.finish();
  }
}