;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(defproject jar-test "0.1.0-SNAPSHOT"
  :description "This project file can be used to run test cases against a jar."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  
  :plugins [[lein-pprint "1.1.1"]]
  
  :dependencies [ 
                 [org.clojure/clojure "1.6.0"]
                 
                 ; Compiler libraries
                 [org.ow2.asm/asm-all "5.0.3"]
                 [org.codehaus.janino/janino "2.7.6"]
                 [aua "0.1"]
                 [transactified "0.1"]
                 
                 ; Runtime/engine libraries
                 [org.codehaus.woodstox/woodstox-core-asl "4.4.1"]
                 [org.codehaus.castor/castor-xml "1.3.3"]
                 [org.zeromq/jzmq "3.1.0"]
                 
                 ;other
                 [clj-stacktrace "0.2.8"]
                 ]
  
;  :debug {:debug true
;          :injections [(prn (into {} (System/getProperties)))]}

   ; this allows to start an nREPL and do the compilation there via a (require 'my.test.namespace reload-all).
   ; it helps when errors happen in the macro and therefore compilation will not succeed!
   :repl-options {:init (do (require 'clj-stacktrace.repl))
                    :caught clj-stacktrace.repl/pst+}
  
  :source-paths ["test/clojure"]
;  :java-source-paths ["src" "src/java"]
  :java-source-paths ["test/java"]
;  :test-paths ["test" "test/clojure"]
  :resource-paths ["../target/ohua-0.2.4-SNAPSHOT.jar"]
  ;:aot [com.ohua.generator_test] ; necessary for classes created by (gen-class ...)
  
  :jvm-opts ["-Xmx512m"] ; there is no need for more than that during development
  
  ; this is important in order to produce enough information for our analysis to work properly.
  ; the -g option is important because it includes the local variable table into the byte code
  ; which we use to assign names to parameters.
  :javac-options ["-target" "1.8" "-source" "1.8" "-Xlint:-options" "-g"]
  )
