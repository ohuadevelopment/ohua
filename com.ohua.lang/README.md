# com.ohua.compile

A Clojure library designed to integrate Ohua into Clojure.

## Usage

To run tests just open up a REPL and do:
(use 'clojure.test)
(run-tests 'com.ohua.compile-test)

## License

See the LICENSE.txt file.