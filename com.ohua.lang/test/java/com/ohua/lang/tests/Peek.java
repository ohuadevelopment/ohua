package com.ohua.lang.tests;

import com.ohua.lang.defsfn;
import com.ohua.lang.compile.analysis.qual.ReadOnly;


public class Peek {
  
  @defsfn
  public Object peek(@ReadOnly Object i) {
    System.out.println("Peek:" + i);
    return i;
  }
  
}
