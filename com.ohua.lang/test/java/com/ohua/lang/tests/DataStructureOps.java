package com.ohua.lang.tests;

import java.util.Map;

import com.ohua.lang.defsfn;
import aua.analysis.qual.Untainted;

public abstract class DataStructureOps {
  
  public static class Access {
    @defsfn
    public Object[] get(String key, Map<String, Object> ds) {
      Object value = release(key, ds);
      return new Object[] { key,
                           value };
    }
    
    @Untainted private Object release(String key, Map<String, Object> ds) {
      return ds.get(key);
    }
  }
  
  public static class Update {
    @defsfn
    public Object[] replace(String key, String value, Map<String, Object> ds){
      return new Object[]{key, ds.put(key, value)};
    }
  }
  
}
