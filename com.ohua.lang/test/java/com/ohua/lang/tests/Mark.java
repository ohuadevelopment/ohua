package com.ohua.lang.tests;

import com.ohua.lang.defsfn;
import com.ohua.lang.compile.analysis.qual.ReadOnly;

/**
 * Created by sertel on 3/2/16.
 */
public class Mark {

    @defsfn
    public Object[] mark(@ReadOnly Object i, String id) {
        return new Object[] {i, id};
    }

}
