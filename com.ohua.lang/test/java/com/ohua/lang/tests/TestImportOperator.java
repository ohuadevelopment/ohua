package com.ohua.lang.tests;

import com.ohua.lang.defsfn;

/*
 * The test operator is used by test cases for the operator import in clojure.
 */
public class TestImportOperator {

	
	@defsfn
	public Object[] testImport() {
		return new Object[1];
	}
}
