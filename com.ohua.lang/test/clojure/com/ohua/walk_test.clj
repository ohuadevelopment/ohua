;
; ohua : walk_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.walk-test
  (:require [clojure.test :refer [deftest is]]
            [com.ohua.walk :as owalk]
            [clojure.walk :as walk]))


(deftest cond-equality
  (let [expr '(cond
                (nil? nil) nil
                (symbol? 'symbol) nil)]
    (is
      (=
        (walk/macroexpand-all expr)
        (owalk/macroexpand-all expr))))
  (let [expr '(fn [prod]
                (cond (< prod 3) (add prod 100)
                      (< prod 5) (add prod 200)
                      (> prod 4) (subtract prod 3)))]
    (is
      (=
        (walk/macroexpand-all expr)
        (owalk/macroexpand-all expr)))))
