(ns com.ohua.tree-zipper-test
  (:require
    [clojure.test :refer :all :as test]
    [com.ohua.tree-zipper :as t-zip]
    [clojure.zip :as zip]
    [clojure.walk :as walk]))

(deftest simple
  (let [zipper (-> (t-zip/tree-zipper '(let* [two (read input)] (send two))) zip/down zip/rightmost zip/down zip/rightmost)
        found (t-zip/find-var-definition zipper (symbol "two"))]
;    (println "found:" found)
    (test/is (= (zip/node found) '(read input)))
    ))

(deftest simple-destructuring
  (let [zipper (-> (t-zip/tree-zipper '(let* [[one two] (read input)] (send two))) zip/down zip/rightmost zip/down zip/rightmost)
        found (t-zip/find-var-definition zipper (symbol "two"))]
;    (println "found:" found)
    (test/is (= (zip/node found) '(read input)))
    ))

(deftest location-sensitivity-not-found
  "If the search starts already at a definition then the answer should be 'no'."
  (let [zipper (-> (t-zip/tree-zipper '(let* [[one two] (read one)] (send two)))
                 ; start at the local
                 zip/down zip/right zip/down zip/right)
        found (t-zip/find-var-definition zipper (symbol "one"))]
;    (println "found:" found)
    (test/is (not found))
    ))

(deftest location-sensitivity-found
  "If the search starts already at a definition then the answer should be 'no'."
  (let [zipper (-> (t-zip/tree-zipper '(let* [one (accept 80) [one two] (read one)] (send two)))
                 ; start at the local
                 zip/down zip/right zip/down zip/right zip/right zip/right)
        found (t-zip/find-var-definition zipper (symbol "one"))]
;    (println "start:" (zip/node zipper))
;    (println "found:" found)
    (test/is found)
    (test/is (= (zip/node found) '(accept 80)))
    ))

(deftest loc-sense-start-at-var-usage
  "Same as above but search starts inside the let body."
  (let [zipper (-> (t-zip/tree-zipper '(let* [vec__885 (add (read two)) r (clojure.core/nth vec__885 0 nil)] (write r)))
                 ; start at the variable usage of r
                 zip/down zip/rightmost zip/down zip/rightmost)
        found (t-zip/find-var-definition zipper (symbol "one"))]
;    (println "start:" (zip/node zipper))
;    (println "found:" found)
    (test/is (not found))
    ))

(deftest fn-var
  "Verify that local vars defined as function parameters are being detected."
  (let [zipper (-> (t-zip/tree-zipper '(fn [one] (some-function one)))
                 ; start at the variable usage of one
                 zip/down zip/rightmost zip/down zip/rightmost)
        found (t-zip/find-var-definition zipper (symbol "one"))]
;    (println "start:" (zip/node zipper))
;    (println "found:" found)
    (test/is found)
    ))

(deftest fn-expanded-var
  "Verify that local vars defined as function parameters are being detected."
  (let [zipper (-> (t-zip/tree-zipper (walk/macroexpand-all '(fn [one] (some-function one))))
              ; start at the variable usage of r
              zip/down zip/rightmost zip/down zip/rightmost)
        found (t-zip/find-var-definition zipper (symbol "one"))]
;    (println "start:" (zip/node zipper))
;    (println "found:" found)
    (test/is found)
    ))
