;
; ohua : backend_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.backend-test
  (:require [com.ohua.clojure-backend :refer :all]
            [clojure.test :refer [deftest is]]))


(deftest test-name-conversion
  (is
    (=
      "myFunction"
      (to-clojure-name "my-function")))
  (is
    (=
      "__my"
      (to-clojure-name "__my"))))
