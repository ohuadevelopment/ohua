(ns com.ohua.test-import-conflict
  (:require [com.ohua.lang :refer [ohua]]
            [clojure.test :refer [deftest is]]))

(ohua :import [com.ohua.lang])


(require '[com.ohua.test-import-conflict-inner :refer [import-found]])


(deftest test-import-conflict
  (is (not import-found)))
