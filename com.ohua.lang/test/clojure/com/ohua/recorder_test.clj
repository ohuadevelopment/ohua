(ns com.ohua.recorder-test
  (:require [clojure.test :refer :all :as test]
            [clojure.pprint :as pp]
            [clojure.algo.monads :refer :all]
            [clojure.walk :as walk]
;            [clojure.pprint :refer :only [pprint]]
;            [com.ohua.compile :refer :all]
            [com.ohua.logging :as l]
            [com.ohua.testing :refer :all :as ohua-test]
            [com.ohua.recorder :refer :all]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            ))

;(defmacro symbol?? [x]
;  (let [f (first (distinct (remove nil? (map #(ns-resolve % x) (all-ns)))))]
;      (println (ns-name (:ns (meta f)))))
;  (if (symbol? x)
;    (println "symbol" `zip/root x (= x `zip/root))
;    (println "not a symbol")))
;
;(deftest s
;  (symbol?? zip/root))

(deftest simple-recording
  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
        x (rec [a (zip/down loc)
;                ab (print-s a)
                b (zip/right a) 
                c (zip/down b) 
                d (zip/right c)])
        m (x nil)]
    (test/is (= (zip/node (first m)) '(some-first-function "start")))
  ))

(deftest simple-replay 
  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
        x (rec [a (zip/down loc)
;                ab (print-s a)
                b (zip/right a) 
                c (zip/down b) 
                d (zip/right c)])
        m (x nil)
        r (play m)]
    (test/is (= (zip/node r) (zip/node loc)))
    ))

(deftest upward-record-replay
  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
        s (-> loc zip/down zip/right zip/down zip/right)
        x (rec [a (zip/up s)])
        m (x nil)
        r (play m)]
    (test/is (= (zip/node r) (zip/node s)))
  ))

(deftest continuing-recording
  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
        m ((rec [a (zip/down loc)
                 b (zip/right a)]) nil)
        x ((rec [c (zip/down (first m)) 
                 d (zip/right c)]) (second m))
        r (play x)]
    (test/is (= (zip/node r) (zip/node loc)))
    ))

(deftest editing
  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
        v "outside-val"
        m ((rec [a (zip/down loc)
;                 ab (print-s a)
                 b (zip/edit a #(with-meta % (conj {} {:something "useless" :outside v})))]) nil)]
;    (println (meta (zip/node (first m))))
    (test/is (= (meta (zip/node (first m))) {:something "useless" :outside "outside-val"}))
;    (println (zip/node (play m)))
    (test/is (= (zip/node (play m)) (zip/node loc)))
    ))


; does not work in the monad. probably some other ingredient is needed here
;(deftest recording-with-nesting
;  (let [loc (t-zip/tree-zipper '(let [p (some-first-function "start")] (some-function p)))
;        x (rec [d (zip/right (zip/down (zip/right (zip/down loc))))])
;        m (x [])]
;    (test/is (= (zip/node (first m)) '(some-first-function "start")))
;    (test/is (= (map type [zip/up zip/left zip/up zip/left])
;                (map (fn [i] (type i)) (second m))))
;  ))