;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.util.gen-test
  (:require [clojure.test :refer :all :as test]
            [clojure.pprint :refer :all]
            [clojure.string :refer [join]]
            [com.ohua.logging :as l]
            [com.ohua.util.gen :refer :all]
            [com.ohua.testing :refer :all :as ohua-test]
            [clojure.walk :as walk]))

(deftest simple-transform
;  (l/enable-logging )
  (let [code '(com.ohua.util.gen/aot-function orderPosts [views]
                            (into [] (take 5 (sort-by last > views))))
;        t-code (walk/macroexpand-all code)
        t-code (macroexpand code)] 
    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(do
                                           (clojure.core/gen-class
                                             :name
                                             "user.Orderposts"
                                             :prefix
                                             "__mk_generated_"
                                             :methods
                                             [[orderPosts [java.lang.Object] java.lang.Object]])
                                           (clojure.core/defn __mk_generated_orderPosts [__mk_generated_this
                                                                                         views]
                                             (into [] (take 5 (sort-by last > views))))
                                           (clojure.core/defn orderPosts [views]
                                             (into [] (take 5 (sort-by last > views))))))
    ))

(deftest simple-transform-with-types
;  (l/enable-logging )
  (let [code '(com.ohua.util.gen/aot-function orderPosts ^LazySeq [^Iterable views]
                                              (take 5 (sort-by last > views)))
;        t-code (walk/macroexpand-all code)
        t-code (macroexpand code)] 
    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(do
                                           (clojure.core/gen-class
                                             :name
                                             "user.Orderposts"
                                             :prefix
                                             "__mk_generated_"
                                             :methods
                                             [[orderPosts [Iterable] LazySeq]])
                                           (clojure.core/defn __mk_generated_orderPosts [__mk_generated_this
                                                                                         views]
                                             (take 5 (sort-by last > views)))
                                           (clojure.core/defn orderPosts [views]
                                             (take 5 (sort-by last > views)))))
    ))

(deftest simple-deftype-transform
  ;  (l/enable-logging )
  (let [code '(com.ohua.util.gen/def-sfn orderPosts [views]
                                              (into [] (take 5 (sort-by last > views))))
        ;        t-code (walk/macroexpand-all code)
        t-code (macroexpand code)]
    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    ;(ohua-test/compare-deep-code t-code '(do
    ;                                       (clojure.core/gen-class
    ;                                         :name
    ;                                         "user.Orderposts"
    ;                                         :prefix
    ;                                         "__mk_generated_"
    ;                                         :methods
    ;                                         [[orderPosts [java.lang.Object] java.lang.Object]])
    ;                                       (clojure.core/defn __mk_generated_orderPosts [__mk_generated_this
    ;                                                                                     views]
    ;                                         (into [] (take 5 (sort-by last > views))))
    ;                                       (clojure.core/defn orderPosts [views]
    ;                                         (into [] (take 5 (sort-by last > views))))))
    ))
