;;;
;;; Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.loop-statement-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.testing :refer :all :as ohua-test]
            [com.ohua.lang :refer :all]))


(ohua :import [com.ohua.lang.tests])

(deftest implicit-recur
  []
  (let [ohua-code (ohua
                    (loop [loop-var (accept "8080")]
                      (if (< loop-var 100)
                        (write)
                        (recur (read)))) :test-compile)]
    ; 1 init stmt + 4 ops + 1 merge + 3 dependencies + 1 feedback dependency + 2 args + 1 condition + 1 compile stmt
    (test/is (= (count ohua-code) 14))
    (test/is (ohua-test/contains
               ohua-code
               '((.createOperator "nd-merge" 100)
                  (.registerDependency 106 -1 100 0)
                  (.registerDependency 101 -1 100 1)
                  (.registerDependency 100 0 103 1))))))

(deftest two-bindings
  []
  (let [ohua-code (ohua
                    (loop [loop-var-1 (accept "8080")
                           loop-var-2 (accept "80")]
                      (if (< loop-var-1 100)
                        (write loop-var-2)
                        (recur (read)))) :test-compile)]
    ; 1 init stmt + 6 ops (4 + if + loop) + 7 dependencies (5 + 1 feedback + 1 if) + 2 args + 1 condition + 1 compile stmt
    (test/is (= (count ohua-code) 18))
    (test/is (ohua-test/compare-code
               ohua-code
               '((new com.ohua.lang.compile.FlowGraphCompiler)
                  (.createOperator "nd-merge" 100)
                  (.createOperator "accept" 101)
                  (.createOperator "accept" 102)
                  (.createOperator "ifThenElse" 104)
                  (.createOperator "write" 106)
                  (.createOperator "read" 107)
                  (.registerDependency 100 0 104 1)
                  (.registerDependency 100 1 106 0)
                  (.registerDependency 101 -1 100 1)
                  (.registerDependency 102 -1 100 1)
                  (.registerDependency 104 0 106 -1)
                  (.registerDependency 104 1 107 -1)
                  (.registerDependency 107 -1 100 0)
                  (.setArguments 101 (clojure.core/into-array com.ohua.lang.Tuple
                                                              (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'java.lang.String))))
                  (.setArguments 102 (clojure.core/into-array com.ohua.lang.Tuple
                                                              (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'java.lang.String))))
                  (.setArguments 104 (clojure.core/into-array com.ohua.lang.Tuple
                                                              (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
                  (.compile true))
               ))
    (test/is (ohua-test/contains
               ohua-code
               '((.createOperator "nd-merge" 100)
                  (.registerDependency 107 -1 100 0)
                  (.registerDependency 104 0 106 -1)
                  (.registerDependency 104 1 107 -1)
                  (.registerDependency 101 -1 100 1)
                  (.registerDependency 100 0 104 1)
                  (.registerDependency 102 -1 100 1)
                  (.registerDependency 100 1 106 0))))
    )
  )
