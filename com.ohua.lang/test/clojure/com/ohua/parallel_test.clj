;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.parallel-test
  (:require [clojure.test :refer :all :as test]
            [clojure.walk :as walk]
            [com.ohua.transform :refer :all]
            [com.ohua.logging :refer :all]))


; FIXME

;(deftest balance-macro-expand-test []
;  (enable-compilation-logging )
;  (clojure.pprint/pprint (walk/macroexpand-all '(com.ohua.transform/|| 2 (-> accepted read parse))))
;  ; NOTE: we need a syntax-quote to resolve to the qualified path, only then macroexpand-all will detect || as a macro!
;  (let [code (walk/macroexpand-all `(let [accepted (accept "80")]
;                                      (|| 2 (-> accepted read parse) accepted)))]
;    (clojure.pprint/pprint (get (ns-map 'com.ohua.parallel-test) '||))
;    (pprint code)
;    
;    ; Result: (need to compare it the hard way due to the vec name randomness)
;                '(let*
;                   [com.ohua.parallel-test/accepted (com.ohua.parallel-test/accept "80")]
;                   (let*
;                    [vec__331 (balance com.ohua.parallel-test/accepted 2)
;                     com.ohua.parallel-test/accepted-0 (clojure.core/nth vec__331 0 nil)
;                     com.ohua.parallel-test/accepted-1 (clojure.core/nth vec__331 1 nil)]
;                    (com.ohua.parallel-test/parse (clojure.core/read com.ohua.parallel-test/accepted-0))
;                    (com.ohua.parallel-test/parse (clojure.core/read com.ohua.parallel-test/accepted-1))))))))
;    (test/is (= (first (second (next code))) 'let*))
;    (test/is (= (second (second (second (next code)))) '(balance com.ohua.parallel-test/accepted 2 2)))
;    (test/is (= (nth (second (second (next code))) 2) 'com.ohua.parallel-test/accepted-0))
;    (test/is (= (nth (second (second (next code))) 4) 'com.ohua.parallel-test/accepted-1))
;    (test/is (= (first (nth (second (second (next code))) 3)) 'clojure.core/nth))
;    (test/is (= (first (nth (second (second (next code))) 5)) 'clojure.core/nth))
;    (test/is (= (nth (second (next code)) 2) '(com.ohua.parallel-test/parse (clojure.core/read com.ohua.parallel-test/accepted-0))))
;    (test/is (= (nth (second (next code)) 3) '(com.ohua.parallel-test/parse (clojure.core/read com.ohua.parallel-test/accepted-1))))
;))                
