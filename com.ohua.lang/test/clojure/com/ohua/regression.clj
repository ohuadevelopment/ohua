;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.regression
  (:require [clojure.test :as test]
            [com.ohua.logging :as l]))

(defn run-regression []
  (l/disallow-logging )

  (require
    ['com.ohua.compile-test]
    ['com.ohua.explicit-schema-match-test]
    ['com.ohua.if-statement-test]
    ; removed until loops are back on the road map.
    ;['com.ohua.loop-statement-test]
    ['com.ohua.transform.smap-test]
    ['com.ohua.advanced-if-statement-test]
    ['com.ohua.tree-zipper-test]
    ['com.ohua.local-variable-override-test]
    ['com.ohua.local-scoping-test]
    ['com.ohua.higher-order-functions-test]
    ['com.ohua.namespace-test]
    ['com.ohua.transform.result-test]
    ['com.ohua.transform.destructuring-test]
    ['com.ohua.transform.algorithms-test]
    ['com.ohua.apply-test]
    ['com.ohua.ir-test]
    ['com.ohua.optimization.redundant-computation-elimination-test]
    ['com.ohua.context-test]
    ['com.ohua.null-values-test]
    ['com.ohua.ir.transform.pmap-test]
    ; FIXME currently only contains tests for pending issues
    ;            ['com.ohua.advanced-destructuring-test]
    ;            ['com.ohua.advanced-compilation-test]
    )

  (test/run-all-tests #"^com\.ohua.*-test"))
