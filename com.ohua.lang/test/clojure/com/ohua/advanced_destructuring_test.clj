;;;
;;; Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.advanced-destructuring-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [com.ohua.testing :refer :all :as ohua-test]
            [com.ohua.logging :as l]))

(ohua :import [com.ohua.lang.tests])

(deftest advanced-destructuring
  "The destructuring here is actually from a local."
  ; FIXME this is currently not supported yet. see issue #110
  []
  (let [ohua-code (ohua 
                   (let [one (accept "some-id")
                         [two three four] one] (read two))
                   :test-compile)]
  (test/is
    (ohua-test/compare-code
      ohua-code
      '((new com.ohua.lang.compile.FlowGraphCompiler)
         (.createOperator "accept" 100)
         (.createOperator "accept" 101)
         (.createOperator "ifThenElse" 103)
         (.createOperator "add" 105)
         (.createOperator "read" 106)
         (.createOperator "write" 107)
         (.registerDependency 100 -1 103 0)
         (.registerDependency 101 -1 106 0)
         (.registerDependency 106 -1 105 0)
         (.registerDependency 105 0 107 0)
         (.registerDependency 103 0 106 -1)
         (.setArguments
          100
          (clojure.core/into-array com.ohua.lang.Tuple
                                   (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'java.lang.String))))
         (.setArguments
          101
          (clojure.core/into-array com.ohua.lang.Tuple
                                   (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'java.lang.String))))
         (.setArguments
           103
           (clojure.core/into-array com.ohua.lang.Tuple
                                    (list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
         (.compile true))
      ))
  ))

