;
; ohua : linker_test_2.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.linker-test-2
  (:require [com.ohua.link :refer :all]
            [clojure.test :refer [deftest is]]
            [com.ohua.linker-test]))
(ohua-require [com.ohua.lang.tests :refer [write] :as tests])


(deftest linker-spill-test
  (is (not (is-imported? 'com.ohua.lang.tests)))
  (is (nil? (resolve 'com.ohua.lang.test/write))))

(deftest alias-spill-test
  (is (not (is-aliased? 'tests)))
  (is (nil? (resolve 'tests/accept)))
  (is (not (is-aliased? 'write)))
  (is (nil? (resolve 'write))))
