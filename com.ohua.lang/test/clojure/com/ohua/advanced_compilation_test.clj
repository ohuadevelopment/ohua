;;;
;;; Copyright (c) Sebastian Ertel 2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.advanced-compilation-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [com.ohua.logging :refer :all]))

(ohua :import [com.ohua.lang.tests])

(deftest environment-args-test
  "Tests the handling of environmental arguments during compilation."
  []

  ; preparation
  (def registry (into {} (map #(hash-map (clojure.string/join ["key-" %]) (clojure.string/join ["value-" %])) (range 10))))
  (def keys-input (into-array String (map #(clojure.string/join ["key-" %]) (range 10))))
  (def operations-input (into-array (range 10)))
  (def output (new java.util.ArrayList))
  
  ; execution
  (ohua (let [key (pipe keys-input)
              operation (pipe operations-input)]
          (let [[_ value] (if (< operation 5) 
                            (get key registry)
                            (replace key "default-value" registry))]
                   (l-collect value output))) :compile)
  )