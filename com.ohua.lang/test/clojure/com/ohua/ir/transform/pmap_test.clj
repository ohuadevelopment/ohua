;
; ohua : pmap_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2017. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.ir.transform.pmap-test
  (:require [clojure.test :as test :refer [deftest]]
            [com.ohua.lang :refer [<-ohua smap algo ohua-require]]
            [com.ohua.ir.transform.pmap]
            [clojure.pprint :refer [pprint]]))

(ohua-require [com.ohua.lang.tests :refer [add]])

(def source (int 4))


(deftest simple-execution-test
  (let [r (into [] (map int (range 100)))]
    (test/is
      (= (map #(+ % %) r)
         (<-ohua
           (smap
             (algo [i] (add i i))
             r)
           :compile-with-config {:df-transformations [(partial com.ohua.ir.transform.pmap/transform [109] '(fn [] source))]})))))


(deftest simple-smap-io-test
  (let [r (into [] (map int (range 100)))]
    (test/is
      (= (map #(+ % %) r)
         (<-ohua
           (smap-io
             (algo [i] (add i i))
             r)
           :compile-with-config {:df-transformations [(partial com.ohua.ir.transform.pmap/transform [106] '(fn [] source))]})))))
