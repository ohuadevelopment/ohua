(ns com.ohua.apply-test
  (:require [com.ohua.lang :refer [<-ohua ohua]]
            [clojure.test :refer [deftest is]]
            [com.ohua.logging :as l]))


(l/enable-compilation-logging)

(ohua :import [com.ohua.lang.tests])


(deftest simple-apply-test
  (is
    (=
      120
      (<-ohua (apply clojure.core/+ (id 20) 100))))
  (is
    (=
      120
      (<-ohua (id (apply com.ohua.lang.tests/add (int 20) (int 100))))))
  (is
    (=
      120
      (<-ohua (apply com.ohua.lang.tests/add (id (int 20)) (int 100))))))
