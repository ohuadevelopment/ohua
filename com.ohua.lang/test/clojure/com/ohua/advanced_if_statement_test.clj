;;;
;;; Copyright (c) Sebastian Ertel 2013-2014. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.advanced-if-statement-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [clojure.pprint :refer :all]
            [com.ohua.testing :refer :all :as ohua-test]
            [com.ohua.logging :as l]))

(ohua :import [com.ohua.lang.tests])

(deftest cond-select-compile-test
  "Tests the select operator in a nested if case."
  []
  (let [result-merge (long-array 10)]
    (let [ohua-code (ohua (let [prod (produce)]
                            (send (cond (< prod 3) (add prod 100)
                                        (< prod 5) (add prod 200)
                                        (> prod 4) (subtract prod 3))
                                  )) :test-compile)]
      ;(l/enable-logging )
      (l/write ohua-code :dispatch clojure.pprint/code-dispatch)
      (test/is (= (count ohua-code) 51))
      (test/is (ohua-test/contains
                 ohua-code
                 ; Note: tough to verify like this!
                 '((new com.ohua.lang.compile.FlowGraphCompiler)
                    (.createOperator "com.ohua.lang.tests/produce" 100)
                    (.createOperator "com.ohua.lang.tests/send" 101)
                    (.createOperator "com.ohua.lang/select" 102)
                    (.createOperator "com.ohua.lang/ifThenElse" 103)
                    (.createOperator "com.ohua.lang.tests/add" 105)
                    (.createOperator "com.ohua.lang/scope" 106)
                    (.createOperator "com.ohua.lang/select" 107)
                    (.createOperator "com.ohua.lang/ifThenElse" 108)
                    (.createOperator "com.ohua.lang/scope" 110)
                    (.createOperator "com.ohua.lang.tests/add" 111)
                    (.createOperator "com.ohua.lang/scope" 112)
                    (.createOperator "com.ohua.lang/select" 113)
                    (.createOperator "com.ohua.lang/ifThenElse" 114)
                    (.createOperator "com.ohua.lang/scope" 116)
                    (.createOperator "com.ohua.lang.tests/subtract" 117)
                    (.registerDependency 102 -1 101 0)
                    (.registerDependency 103 0 102 0)
                    (.registerDependency 105 -1 102 1)
                    (.registerDependency 107 -1 102 2)
                    (.registerDependency 108 0 107 0)
                    (.registerDependency 111 -1 107 1)
                    (.registerDependency 113 -1 107 2)
                    (.registerDependency 114 0 113 0)
                    (.registerDependency 117 -1 113 1)
                    (.registerDependency 100 -1 103 1)
                    (.registerDependency 100 -1 105 0)
                    (.registerDependency 100 -1 106 0)
                    (.registerDependency 106 0 108 1)
                    (.registerDependency 106 0 110 0)
                    (.registerDependency 110 0 111 0)
                    (.registerDependency 106 0 112 0)
                    (.registerDependency 112 0 114 1)
                    (.registerDependency 112 0 116 0)
                    (.registerDependency 116 0 117 0)
                    (.registerDependency 103 0 105 -1)
                    (.registerDependency 103 1 106 -1)
                    (.registerDependency 108 0 110 -1)
                    (.registerDependency 108 1 112 -1)
                    (.registerDependency 114 0 116 -1)
                    ;(.setArguments
                    ;  101
                    ;  (clojure.core/into-array com.ohua.lang.Tuple
                    ;                           (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 1) 'result-merge))))
                    (.setArguments
                      103
                      (clojure.core/into-array
                        com.ohua.lang.Tuple
                        (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
                    (.setArguments
                      105
                      (clojure.core/into-array com.ohua.lang.Tuple
                                               (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 1) 'java.lang.Long))))
                    (.setArguments
                      108
                      (clojure.core/into-array
                        com.ohua.lang.Tuple
                        (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
                    (.setArguments
                      111
                      (clojure.core/into-array com.ohua.lang.Tuple
                                               (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 1) 'java.lang.Long))))
                    (.setArguments
                      114
                      (clojure.core/into-array
                        com.ohua.lang.Tuple
                        (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
                    (.setArguments
                      117
                      (clojure.core/into-array com.ohua.lang.Tuple
                                               (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 1) 'java.lang.Long))))
                    (.compile true))))
      )))

(deftest cond-merge-run-test
  "Tests the merge operator in a nested if case."
  []
  (let [input (map int (range 10))
        result (<-ohua
                 (smap
                   (fn [prod]
                     (cond (< prod 3) (add prod 100)
                           (< prod 5) (add prod 200)
                           (> prod 4) (subtract prod 3)))
                 input))]
    (test/is
      (= (reduce + result) 730))))

(deftest nested-if-merge-run-test
  "Tests the merge operator in a nested if case."
  []
  (let [input (map int (range 10))
        ;  (set! (. com.ohua.engine.utils.GraphVisualizer PRINT_FLOW_GRAPH) "../test-output/test2-flow")
        result
        (<-ohua
          (smap
            (fn [prod]
              (if (< prod 3) (add prod 100)
                             (if (< prod 5) (add prod 200) (subtract prod 3))))
            input))]
    (test/is
      (= (reduce + result) 730))))

(deftest nested-if-on-if-branch
  "Tests a nested if on the if-branch. This makes sure that our if-traversal works properly."
  []
  (let [ohua-code (ohua
                    (let [input (accept "some-id")]
                      (if (= input "one")
                        (let [two (read input)]
                          (if (= two "two")
                            (send two)
                            (send (parse two))))
                        (parse input)))
                    :test-compile)]
    (test/is
      (ohua-test/contains
        ohua-code
        '((.setArguments
            102
            (clojure.core/into-array
              com.ohua.lang.Tuple
              (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
           (.setArguments
             106
             (clojure.core/into-array
               com.ohua.lang.Tuple
               (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition)))
             ))
        ))
    ))

(deftest locals-dependency
  "Shows that we can handle indirect connections via locals inside an if-statement."
  []
  ;  (l/enable-compilation-logging)
  (let [ohua-code (ohua
                    (let [input (accept "some-id")]
                      (if (= input "one")
                        (let [two (read input)]
                          (send two))))
                    :test-compile)]
    (ohua-test/contains
      ohua-code
      '((new com.ohua.lang.compile.FlowGraphCompiler)
         (.createOperator "com.ohua.lang.tests/accept" 100)
         (.createOperator "com.ohua.lang/ifThenElse" 102)
         (.createOperator "com.ohua.lang.tests/read" 104)
         (.createOperator "com.ohua.lang.tests/send" 105)
         (.registerDependency 100 -1 102 1)
         (.registerDependency 100 -1 104 0)
         (.registerDependency 104 -1 105 0)
         (.registerDependency 102 0 104 -1)
         (.setArguments
           100
           (clojure.core/into-array com.ohua.lang.Tuple
                                    (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'java.lang.String))))
         (.setArguments
           102
           (clojure.core/into-array
             com.ohua.lang.Tuple
             (clojure.core/list (com.ohua.lang.Tuple. (clojure.core/int 0) 'com.ohua.lang.Condition))))
         (.compile true))
      )
    ))

(deftest deep-nesting
  "Makes sure that redundant selects are omitted."
  []
  (l/enable-compilation-logging)
  (let [ohua-code (ohua
                    (let
                      [[type content] (read (accept "9080"))]
                      (cond
                        (= "bla" type) (parse content)
                        (= "blub" type) (let [[is-old new-content] (parse content)]
                                          (if (true? is-old)
                                            (send (parse new-content))
                                            (send (read new-content))))
                        ))
                    :test-compile true :symres false)]
    (test/is (= (count ohua-code) 41))
    (ohua-test/contains ohua-code '((.createOperator "com.ohua.lang/select" 102)) not)
    (ohua-test/contains ohua-code '((.createOperator "com.ohua.lang/select" 106)) not)
    (ohua-test/contains ohua-code '((.createOperator "com.ohua.lang/select" 110)) not)
    ))
