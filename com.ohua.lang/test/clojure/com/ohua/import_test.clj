(ns com.ohua.import-test
  (:require [com.ohua.lang :refer [ohua]]
            [com.ohua.link :refer :all :as ln]))

(use 'clojure.test)

(deftest test-import
  (is (nil? (ohua :import [com.ohua.lang.tests])) "Package should be found.")
  (is (contains? (into #{} (ln/list-links)) "testImport") "Test operator should be included.")
  (is (contains? (into #{} (ln/list-links)) "test-import") "Test operator should be included."))
