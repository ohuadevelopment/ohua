;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.optimization.redundant-computation-elimination-test
  (:require [clojure.walk :as walk]
            [clojure.test :as test :refer [deftest]]
            [com.ohua.logging :as l]
            [com.ohua.lang :refer [ohua]]
            [com.ohua.testing :as ohua-test]))

(ohua :import [com.ohua.lang.tests])

(deftest collect-merge-elimination
  ;(l/enable-compilation-logging )
  (let [with-transformation '(com.ohua.lang/ohua
                               (com.ohua.lang/smap
                                 (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                                 data)
                               :symres false)
        compiled-with-transformation (walk/macroexpand-all with-transformation)]
    ;(l/enable-logging)
    (l/printline "with transformation:")
    (l/write compiled-with-transformation :dispatch clojure.pprint/code-dispatch)
    (l/printline)

    (test/is
      (empty?
        (filter
          #(= (nth % 3) "com.ohua.lang/collect")
          (filter #(and (seq? %) (= (nth % 2) 'createOperator)) compiled-with-transformation ))
        ))

    (test/is
      (empty?
        (filter
          #(= (nth % 3) "select")
          (filter #(and (seq? %) (= (nth % 2) 'createOperator)) compiled-with-transformation ))
        ))

    (test/is
      (= 1
         (count
           (filter
             #(= (nth % 3) "com.ohua.lang/one-to-n")
             (filter #(and (seq? %) (= (nth % 2) 'createOperator)) compiled-with-transformation ))
           )))
    ))
