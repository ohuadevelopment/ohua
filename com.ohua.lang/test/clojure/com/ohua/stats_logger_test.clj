(ns com.ohua.stats-logger-test
  (:require
    [clojure.test :refer :all]
    [com.ohua.lang :refer [<-ohua ohua algo]]
    [clojure.java.io :as io])
  (:import (com.ohua.engine RuntimeProcessConfiguration)
           (java.util.function Consumer)
           (com.ohua.lang.runtime RuntimeStatistics$AccumulatedStats RuntimeStatistics$GlobalStatsLogger RuntimeStatistics$TracedStats)))

;;;
;;; I'm not adding this to the regression because there really is nothing to be tested for here.
;;;

(ohua :import [com.ohua.lang.tests])

(deftest log-to-std-out
  (set! RuntimeProcessConfiguration/STATS_COLLECTOR RuntimeStatistics$AccumulatedStats)
  (set! RuntimeStatistics$GlobalStatsLogger/LOG_STATS (reify Consumer
                                                        (accept [_ log-content]
                                                          (println log-content))))
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap
                   (algo [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    )
  )


(deftest log-to-file
  (io/delete-file "test-output/stats.json")
  (set! RuntimeProcessConfiguration/STATS_COLLECTOR RuntimeStatistics$AccumulatedStats)
  (set! RuntimeStatistics$GlobalStatsLogger/LOG_STATS (reify Consumer
                                                        (accept [_ log-content]
                                                          (spit "test-output/stats.json" log-content :append true))))
  (let [data (into () (map int (range 1000)))
        result (<-ohua
                 (smap
                   (algo [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    )
  )


(deftest trace-to-std-out
  (set! RuntimeProcessConfiguration/FRAMEWORK_STATS_COLLECTOR RuntimeStatistics$TracedStats)
  (set! RuntimeStatistics$GlobalStatsLogger/LOG_STATS (reify Consumer
                                                        (accept [_ log-content]
                                                          (println log-content))))
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap
                   (algo [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    )
  )

(deftest trace-to-file
  (io/delete-file "test-output/traced-stats.json")
  (set! RuntimeProcessConfiguration/FRAMEWORK_STATS_COLLECTOR RuntimeStatistics$TracedStats)
  (set! RuntimeStatistics$GlobalStatsLogger/LOG_STATS (reify Consumer
                                                        (accept [_ log-content]
                                                          (spit "test-output/traced-stats.json" log-content :append true))))
  (let [data (into () (map int (range 1000)))
        result (<-ohua
                 (smap
                   (algo [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    )
  )
