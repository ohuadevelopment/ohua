(ns com.ohua.test-scala-functions
  (:require [com.ohua.lang :refer [<-ohua ohua defalgo]]
            [clojure.test :refer [deftest is]]
            [com.ohua.logging :as l]))


(ohua :import [com.ohua.lang.tests])


(defalgo m [i] (scala-add i (int 1)))


(deftest test-scala-function
  (is 7
    (let [v1 (map int [1 2 3])
          v2 [3]]
      (<-ohua
        (scala-add
          (length (smap m v1))
          (scala-add
            (length v1)
            (length v2)))))))