;
; ohua : symbol_resolution_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.transform.symbol-resolution-test
  (:require [com.ohua.transform.symbol-resolution :refer [check traverse get-line check-tree to-human]]
            [com.ohua.logging :as l]
            [com.ohua.walk :refer [macroexpand-all]]
            [com.ohua.lang :refer [algo]]
            [clojure.test :refer [deftest is]])
  (:import (com.ohua.transform.symbol_resolution UnresolvedSymbolError BindingVectorLengthError TypeError)
           (clojure.lang Symbol)))

(require '[com.ohua.tree-zipper :as t-zip]
         '[clojure.zip :as zip])

(l/enable-compilation-logging)

(defn find-errors [code]
  (second (check-tree (macroexpand-all code) {})))

(deftest test-let-bnd
  (is
    (=
      (find-errors '(let []))
      []))
  (is
    (=
      (find-errors
        '(let [x 4] x))
      []))
  (let [[e :as errs] (find-errors '(let [x j] x))]
    (is (= (count errs) 1))
    (is (instance? UnresolvedSymbolError e))
    (is (= (.-symbol e) 'j))
    (is (not= nil (get-line e))))
  (let [[e :as errs] (find-errors '(let* [b 6 l] b))]
    (is (= (count errs) 2))
    (is (instance? BindingVectorLengthError e)))
  (let [[e :as errs] (find-errors '(let* ["str" 5] "str"))]
    (is (= (count errs) 1))
    (is (instance? TypeError e))
    (is (= Symbol (.-expected_type e)))
    (is (= String (.-received_type e)))))


(deftest test-nested-let-bnd
  (is
    (=
      (find-errors
        '(let [x 4] x))
      []))
  (let [[e :as errs] (find-errors '(let [] (let [x j] x)))]
    (is (= (count errs) 1))
    (is (instance? UnresolvedSymbolError e))
    (is (= (.-symbol e) 'j))
    (is (not= nil (get-line e))))
  (let [[vlength unres :as errs] (find-errors '(iterate (let* [b 6 l] b)))]
    (is (= (count errs) 2))
    (is (instance? BindingVectorLengthError vlength))
    (is (instance? UnresolvedSymbolError unres))
    (is (= 'b (.-symbol unres))))
  (let [[e :as errs] (find-errors '(let [x 5] (iterate 5) (let* ["str" 5] "str")))]
    (is (= (count errs) 1))
    (is (instance? TypeError e))
    (is (= Symbol (.-expected_type e)))
    (is (= String (.-received_type e)))))

