;
; ohua : non_sfn_resolve_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.transform.non-sfn-resolve-test
  (:require [clojure.test :as test :refer [deftest is]]
            [com.ohua.lang :refer :all]
            [com.ohua.logging :refer [enable-compilation-logging]]))
(enable-compilation-logging)


(def persist (atom 0))


(deftest find-clojure-func-test
  (is
    true
    (<-ohua
      (and (zero? 0)
           (= 7
              (+ 3 4))))))


(defn ^{:init-state 'persist} mysfn [this arg]
  (swap! this #(+ % arg))
  @this)


(deftest find-stateful-clojure-func-test
  (ohua (mysfn (mysfn 1)))
  (is 3
      @persist))


(def persist2 (atom true))


(defsfn anothersfn persist2 [this arg]
        (swap! this not)
        arg)


(deftest defsfn-test
  (ohua (anothersfn (anothersfn (anothersfn 0))))
  (is (false? @persist2))
  (ohua (anothersfn (anothersfn 0)))
  (is (true? @persist2)))
