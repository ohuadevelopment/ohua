;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;

(ns com.ohua.transform.algorithms-test
  (:require [clojure.test :refer :all :as test]
            [clojure.walk :as walk]
            [com.ohua.transform.algorithms :refer [transform-local-algos inline-global-algos]]
            [com.ohua.transform.smap]
            [com.ohua.logging :as l]
            [com.ohua.lang :refer [algo defalgo ohua <-ohua]]
            [com.ohua.testing :as ohua-test]
            [com.ohua.link :as link])
  (:import (com.ohua.lang.algo Algo)))

(ohua :import [com.ohua.lang.tests])

(def transform (comp transform-local-algos inline-global-algos))

; NOTE This is necessary because we have quoted ohua code here, see the documentation for more information :P
(defn import-fixture [f]
  (link/ohua-require-fn '[com.ohua.lang.tests :refer :all])
  (f))

(clojure.test/use-fixtures :once import-fixture)


(deftest expand-algo
  (let [code '(let [input (some-func)
                    algo-result ((com.ohua.lang/algo my-anon-algo [i] (parse i)) input)]
                (some-out-fn algo-result))
        t-code (walk/macroexpand-all code)]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [input (some-func)
                                            algo-result (^:skip-comparison user/my-anon-algo input)]
                                           (some-out-fn algo-result)))
    (test/is (instance? Algo (var-get (resolve (-> t-code second (nth 3) first)))))
    )
  )

(deftest expand-defalgo
  (let [code '(com.ohua.lang/defalgo my-algo [i] (parse i))
        t-code (walk/macroexpand-all code)]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (test/is t-code (var-get (resolve 'user/my-algo)))
    )
  )

(deftest transform-algo
  (let [code '(let [input (some-func)
                    algo-result ((com.ohua.lang/algo my-anon-algo [i] (parse i)) input)]
                (some-out-fn algo-result))
        t-code (transform {} nil (walk/macroexpand-all code))]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [input (some-func)
                                            algo-result (clojure.core/let [[i] (com.ohua.lang/algo-in nil input)]
                                                          (com.ohua.lang/algo-out (parse i)))]
                                           (some-out-fn algo-result)))
    )
  )

(deftest transform-defalgo
  (let [_ (walk/macroexpand-all '(com.ohua.lang/defalgo my-algo [i] (parse i)))
        code '(let [input (some-func)
                    algo-result (user/my-algo input)]
                (some-out-fn algo-result))
        t-code (transform {} nil (walk/macroexpand-all code))]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [input (some-func)
                                            algo-result (clojure.core/let [[i] (com.ohua.lang/algo-in nil input)]
                                                          (com.ohua.lang/algo-out (parse i)))]
                                           (some-out-fn algo-result)))
    )
  )

(defmacro transform-in-macro [code]
  ;(println "walking ...")
  ;(println code)
  ;(println "Env (macro)" &env)
  ;(println (get &env 'algo-var)
  ;         (.eval (.init (get &env 'algo-var))))
  ;(println (var-get (get &env 'algo-var)))
  (let [transformed (transform &env &form (walk/macroexpand-all code))]
    ;(println "transformed:" transformed)
    `'(~@transformed)
    ))

(deftest transform-algo-clj-var
  (let [f (fn test [])
        algo-var (com.ohua.lang/algo my-anon-algo-2 [i] (parse i))
        _ (println algo-var (type algo-var))                ;--> turns out that this is really the value instead of a var
        _ (println f (type f))

        ; register the functions to make the Clojure linker happy
        _ (defn some-func [])
        _ (defn some-out-fn [])
        t-code (transform-in-macro
                 (let [input (some-func)
                       algo-result (algo-var input)]
                   (some-out-fn algo-result)))]
    (l/enable-logging)
    (l/printline "pass completed")
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [input (some-func)
                                            algo-result (clojure.core/let [[i] (com.ohua.lang/algo-in nil input)]
                                                          (com.ohua.lang/algo-out (parse i)))]
                                           (some-out-fn algo-result)))
    )
  )


(deftest exec
  ;(l/enable-compilation-logging)
  (test/is (= 11
              (<-ohua
                (add
                  (int 10)
                  ((algo my-exec-test [i] (subtract (int 102) i))
                    (add (int 1) 100)))))))


(deftest smap-algo-integration
  (let [code '(let [input (some-func)
                    algo-result (com.ohua.lang/smap (com.ohua.lang/algo my-anon-algo [i] (send i)) input)]
                (some-out-fn algo-result))
        t-code (com.ohua.transform.smap/transform (transform (walk/macroexpand-all code)))]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [input (some-func)
                                            algo-result (clojure.core/let [^:skip-comparison d__549__auto__ input
                                                                           ^:skip-comparison s__548__auto__ (com.ohua.lang/size
                                                                                                                ^:skip-comparison d__549__auto__)]
                                                          (clojure.core/let [[i] (com.ohua.lang/smap-fun
                                                                                   (com.ohua.lang/one-to-n
                                                                                     ^:skip-comparison s__548__auto__
                                                                                     ^:skip-comparison d__549__auto__))]
                                                            (com.ohua.lang/collect
                                                              (com.ohua.lang/one-to-n
                                                                ^:skip-comparison s__548__auto__
                                                                ^:skip-comparison s__548__auto__)
                                                              (clojure.core/let [[i] (com.ohua.lang/algo-in
                                                                                       ^:skip-comparison user/my-anon-algo
                                                                                       i)]
                                                                (com.ohua.lang/algo-out (send i))))))]
                                           (some-out-fn algo-result))
                                 )
    )

  )

(deftest smap-algo-exec
  ;  (l/enable-compilation-logging )
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap
                   (algo [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))


(deftest smap-defalgo-exec
  ;  (l/enable-compilation-logging )
  (defalgo
    my-smap-algo
    [prod]
    (if (< prod 3)
      (com.ohua.lang.tests/add prod 100)
      (com.ohua.lang.tests/subtract prod 3)))

  (let [data (into () (map int (range 10)))
        result (<-ohua (smap com.ohua.transform.algorithms-test/my-smap-algo data))]
    ; validation
    (test/is (= (reduce + result) 324))
    )
  )

(deftest smap-algo-clj-var-exec
  ;  (l/enable-compilation-logging )
  (let [algo-var (algo
                   my-smap-algo
                   [prod]
                   (if (< prod 3)
                     (com.ohua.lang.tests/add prod 100)
                     (com.ohua.lang.tests/subtract prod 3)))
        data (into () (map int (range 10)))
        result (<-ohua (smap algo-var data))]
    ; validation
    (test/is (= (reduce + result) 324))
    )
  )

(deftest compile-independent-algo
  (let [code '(com.ohua.lang/ohua
                (let [input (accept)
                      algo-result ((com.ohua.lang/algo my-anon-algo [] (read)))]
                  (add algo-result input)))
        t-code (walk/macroexpand-all code)]
    ;(l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [^:skip-comparison G__2870 (new com.ohua.lang.OhuaRuntime)]
                                           (. ^:skip-comparison G__2870 createOperator "com.ohua.lang.tests/accept" 100)
                                           (. ^:skip-comparison G__2870 createOperator "com.ohua.lang/algo-in-void" 101)
                                           (. ^:skip-comparison G__2870 createOperator "com.ohua.lang/algo-out" 102)
                                           (. ^:skip-comparison G__2870 createOperator "com.ohua.lang.tests/read" 103)
                                           (. ^:skip-comparison G__2870 createOperator "com.ohua.lang.tests/add" 104)
                                           (. ^:skip-comparison G__2870 registerDependency 100 -1 104 1)
                                           (. ^:skip-comparison G__2870 registerDependency 101 -1 103 -1)
                                           (. ^:skip-comparison G__2870 registerDependency 102 -1 104 0)
                                           (. ^:skip-comparison G__2870 registerDependency 103 -1 102 0)
                                           (. ^:skip-comparison G__2870
                                              setArguments
                                              101
                                              (clojure.core/into-array
                                                com.ohua.lang.Tuple
                                                (clojure.core/list
                                                  (new
                                                    com.ohua.lang.Tuple
                                                    (clojure.core/int 0)
                                                    nil))))
                                           (. ^:skip-comparison G__2870 execute {"shared-env-vars" {}})
                                           ^:skip-comparison G__2870)
                                 )
    )
  )


(deftest literal
  (let [
        code '(let [input (some-func)
                    algo-result (#a(some-fun-in-algo %) input)]
                (some-out-fn algo-result))
        ;t-code (walk/macroexpand-all code)
        ]
    (l/enable-logging)
    (l/write code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code code '(let
                                         [input (some-func)
                                          algo-result ((com.ohua.lang/algo [%] (some-fun-in-algo %)) input)]
                                         (some-out-fn algo-result)))
    )
  )

(deftest literal-args-access
  (let [
        code '(let [input (some-func)
                    algo-result (#a(some-fun-in-algo %2 %3 %1) input)]
                (some-out-fn algo-result))
        ;t-code (walk/macroexpand-all code)
        ]
    (l/enable-logging)
    (l/write code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code code '(let
                                         [input (some-func)
                                          algo-result ((com.ohua.lang/algo [%1 %2 %3] (some-fun-in-algo %2 %3 %1)) input)]
                                         (some-out-fn algo-result)))
    )
  )


(deftest nested-inline-algos
  (test/is
    (=
      300
      (<-ohua
        ((algo my-adder [p]
               ((algo my-nested-adder [q] (add (int 100) q))
                 (add (int 100) p)))
          (id 100))))))

(deftest nested-global-algos
  (defalgo my-nested-adder [q]
           (add (int 100) q))
  (defalgo my-adder [p]
           (com.ohua.transform.algorithms-test/my-nested-adder (add (int 100) p)))
  (test/is
    (=
      300
      (<-ohua (my-adder (id 100))))))


(deftest values-to-algos
  (let [code '((com.ohua.lang/algo my-adder [p]
                                   (add (int 100) p))
                100)
        t-code (transform {} nil (walk/macroexpand-all code))]
    (l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(clojure.core/let [[p] (com.ohua.lang/algo-in nil 100)]
                                           (com.ohua.lang/algo-out (add (int 100) p))))
    )
  (test/is
    (= 200
       (<-ohua
         ((algo my-adder [p]
                (add (int 100) p))
           100)))))


(deftest array-input
  ;(let [code '((com.ohua.lang/algo my-id [p]
  ;                                 (com.ohua.lang/id p))
  ;              (com.ohua.lang/array 100))
  ;      t-code (transform {} nil (walk/macroexpand-all code))]
  ;  (l/enable-logging)
  ;  (l/write t-code :dispatch clojure.pprint/code-dispatch)
  ;  ;(ohua-test/compare-deep-code t-code '(clojure.core/let [[p] (com.ohua.lang/algo-in user/my-adder 100)]
  ;  ;                                       (com.ohua.lang/algo-out (add (int 100) p))))
  ;  )
  (test/is
    (java.util.Arrays/equals (into-array Object [100])
    (<-ohua
      ((com.ohua.lang/algo my-id [p]
                           (com.ohua.lang/id p))
        (com.ohua.lang/array 100))))))
