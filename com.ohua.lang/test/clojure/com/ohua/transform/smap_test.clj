;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.smap-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer [ohua <-ohua algo]]
            [clojure.pprint :refer :all]
            [clojure.string :refer [join]]
            [com.ohua.transform.smap :refer [transform]]
            [com.ohua.logging :as l]
            [com.ohua.testing :refer :all :as ohua-test]
            [clojure.walk :as walk])
  (:import (java.util Properties)
           (com.ohua.engine RuntimeProcessConfiguration)))

(ohua :import [com.ohua.lang.tests])

(deftest simple-smap-transformation
  ;  (l/enable-logging )
  (let [code '(let [l (produce)] (com.ohua.lang/smap (fn [input] (some-function input)) l))
        t-code (transform (walk/macroexpand-all code))]
    ;(l/enable-logging )
    ;(l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [l (produce)]
                                           (clojure.core/let [^:skip-comparison d l
                                                              ^:skip-comparison size (com.ohua.lang/size ^:skip-comparison d)]
                                             (clojure.core/let [[input] (com.ohua.lang/smap-fun
                                                                          (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison d))]
                                               (com.ohua.lang/collect
                                                 (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison size)
                                                 (some-function input))))))))

(deftest function-support
  "A test to make sure that all smap runtime functionality works properly."
  ;  (set! (. com.ohua.engine.utils.GraphVisualizer PRINT_FLOW_GRAPH) (str "test-output/bla-flow"))
  ;  (l/enable-compilation-logging )
  (let [data (into () (map int (range 10)))
        result (<-ohua (let [d (com.ohua.lang/id data)
                             s (com.ohua.lang/size d)]
                         (let [[prod] (com.ohua.lang/smap-fun (com.ohua.lang/one-to-n s d))]
                           (com.ohua.lang/collect
                             (com.ohua.lang/one-to-n s s)
                             (if (< prod 3)
                               (com.ohua.lang.tests/add prod 100)
                               (com.ohua.lang.tests/subtract prod 3))))))]

    ; validation
    (test/is (= (reduce + result) 324))
    ))

(deftest simple-map-exec
  ;  (l/enable-compilation-logging )
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (com.ohua.lang/smap
                   (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest out-of-scope-var-transformation
  ;  (l/enable-logging )
  (let [l '()
        code '(let [l (produce)
                    some-var (produce)] (com.ohua.lang/smap (fn [input] (some-function input some-var)) l))
        t-code (transform (walk/macroexpand-all code))]
        ;(l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [l (produce)
                                            some-var (produce)]
                                           (clojure.core/let [^:skip-comparison d__825__auto__ l
                                                              ^:skip-comparison size (com.ohua.lang/size ^:skip-comparison d__825__auto__)]
                                             (clojure.core/let [[input] (com.ohua.lang/smap-fun
                                                                          (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison d__825__auto__))
                                                                some-var (com.ohua.lang/one-to-n ^:skip-comparison size some-var)]
                                               (com.ohua.lang/collect
                                                 (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison size)
                                                 (some-function input some-var))))))))

(deftest out-of-scope-var-exec
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (let [add-var (id 100)
                       sub-var (id 3)]
                   (com.ohua.lang/smap
                     (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod add-var) (com.ohua.lang.tests/subtract prod sub-var)))
                     data)))
        ]
    ; validation
    (test/is
      (= (reduce + result) 324))
    )
  )

(deftest nested-smap-transformation
  ;  (l/enable-logging )
  (let [data '()
        code '(com.ohua.lang/smap
                (fn [d]
                  (com.ohua.lang/smap
                    (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                    d))
                data)
        t-code (transform {(symbol 'data) nil} nil (walk/macroexpand-all code))]
    ;(l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(clojure.core/let [^:skip-comparison d__825__auto__ data
                                                            ^:skip-comparison s__824__auto__ (com.ohua.lang/size ^:skip-comparison d__825__auto__)]
                                           (clojure.core/let [[d] (com.ohua.lang/smap-fun (com.ohua.lang/one-to-n
                                                                                                      ^:skip-comparison s__824__auto__
                                                                                                      ^:skip-comparison d__825__auto__))]
                                             (com.ohua.lang/collect
                                               (com.ohua.lang/one-to-n
                                                 ^:skip-comparison s__824__auto__
                                                 ^:skip-comparison s__824__auto__)
                                               ; FIXME the captures of the variables here are not unique! -> might be ok, due to context of a closure!
                                               (clojure.core/let [^:skip-comparison d__825__auto__ d
                                                                  ^:skip-comparison s__824__auto__ (com.ohua.lang/size
                                                                                                       ^:skip-comparison d__825__auto__)]
                                                 (clojure.core/let [[prod] (com.ohua.lang/smap-fun
                                                                             (com.ohua.lang/one-to-n
                                                                               ^:skip-comparison s__824__auto__
                                                                               ^:skip-comparison d__825__auto__))]
                                                   (com.ohua.lang/collect
                                                     (com.ohua.lang/one-to-n
                                                       ^:skip-comparison s__824__auto__
                                                       ^:skip-comparison s__824__auto__)
                                                     (if (< prod 3)
                                                       (com.ohua.lang.tests/add prod 100)
                                                       (com.ohua.lang.tests/subtract prod 3)))))))))
    ))


(deftest nested-smap-exec
  ;  (set! (. com.ohua.engine.utils.GraphVisualizer PRINT_FLOW_GRAPH) (str "test/nested-smap-exec-flow"))
  (let [data (into () [(map int (range 5)) (map int (range 5 10))])
        ;_ (println data)
        result (<-ohua
                 (com.ohua.lang/smap
                   (fn [d]
                     (com.ohua.lang/smap
                       (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                       d))
                   data)
                 :run-with-config (doto (new RuntimeProcessConfiguration)
                                    (.setProperties (doto (new Properties)
                                                      (.put "disable-fusion" true))))
                 )]
    (println "result" result)
    (println partial)
    ; validation
    (test/is (= (reduce + (flatten (map (partial into '()) result))) 324))))


(deftest smap-namespace-support
  ;  (l/enable-logging )
  (let [code '(let [l (produce)] (com.ohua.lang/smap (fn [input] (some-function input)) l))
        t-code (transform (walk/macroexpand-all code))]
    ;(l/enable-logging)
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let* [l (produce)]
                                           (clojure.core/let [^:skip-comparison d__1423__auto__ l
                                                              ^:skip-comparison size (com.ohua.lang/size ^:skip-comparison d__1423__auto__)]
                                             (clojure.core/let [[input] (com.ohua.lang/smap-fun
                                                                          (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison d__1423__auto__))]
                                               (com.ohua.lang/collect
                                                 (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison size)
                                                 (some-function input))))))))


(deftest singleton
  (let [data (into () [10])
        result (<-ohua (com.ohua.lang/smap (fn [d] (id d)) data))]
      (test/is 10 (first result))))


(deftest smap-nested-if
  (let [data (into () [10 20])
        result (<-ohua (com.ohua.lang/smap (algo [d] (if (= d 10) (id 10) (id 20))) data))]
    (test/is 10 (first result))
    (test/is 20 (second result))))

(deftest smap-nested-independent-if
  (let [data (into () [30 40])
        result (<-ohua (com.ohua.lang/smap (algo [d] (if (< 5 10) (id d) (id 20))) data))]
    (test/is 30 (first result))
    (test/is 40 (second result))))

(deftest smap-nested-if-scope
  "Here a var from outside the smap context is used on both branches of an if.
  As a result, there are two outgoing arcs (output ports) from the responsible one-to-n which breaks an assumption in the 1-To-N support."
  (let [data (into () [30 40])
        result (<-ohua
                 (let [out-of-ctxt-var (id 200)]
                   (com.ohua.lang/smap
                   (algo [d] (if (< d 10) (id out-of-ctxt-var) (id out-of-ctxt-var) ))
                   data)))]
    (test/is 200 (first result))
    (test/is 200 (second result))))
