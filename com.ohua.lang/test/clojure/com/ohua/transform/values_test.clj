;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.values-test
  (:require [clojure.test :refer [deftest]]
            [com.ohua.transform.values :refer [transform]]
            [clojure.walk :as walk]
            [com.ohua.logging :as l]
            [com.ohua.testing :as ohua-test]))

(deftest transformation
  (let [code '(let [x (some-fn 10)
                    y (if (check x) x 100)
                    z (smap (fn [a] (seq (test d) some-data)) data)]
                z)
        t-code (transform {'data true} nil (walk/macroexpand-all code))]
    ;(l/enable-logging)
    ;(l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [x (some-fn 10)
                                            y (if (check x)
                                                (com.ohua.lang/value x)
                                                (com.ohua.lang/value 100))
                                            z (smap
                                                (fn* ([a] (seq (test d) (com.ohua.lang/value some-data))))
                                                (com.ohua.lang/value data))]
                                           (com.ohua.lang/value z)))
    ))

(deftest transformation-negative
  (let [code '(let [x (some-fn 10)
                    z (smap (fn [a] (seq (test d) a)) x)]
                z)
        t-code (transform (walk/macroexpand-all code))]
    ;(l/enable-logging)
    ;(l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let*
                                           [x (some-fn 10)
                                            z (smap
                                                (fn* ([a] (seq (test d) (com.ohua.lang/value a))))
                                                x)]
                                           (com.ohua.lang/value z)))
    ))
