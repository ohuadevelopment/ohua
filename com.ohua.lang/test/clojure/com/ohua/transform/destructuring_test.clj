;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.destructuring-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [clojure.pprint :refer :all]
            [clojure.string :refer [join]]
            [com.ohua.transform.destructuring :refer [transform]]
            [com.ohua.logging :as l]
            [com.ohua.testing :refer :all :as ohua-test]
            [clojure.walk :as walk])
  (:import (com.ohua.engine RuntimeProcessConfiguration)
           (java.util Properties)))

(ohua :import [com.ohua.lang.tests])

(deftest destructure-binding-transformation
;  (l/enable-logging )
  (let [code '(let [[one two] three] (some-function one)) 
;        _ (l/write (walk/macroexpand-all code) :dispatch clojure.pprint/code-dispatch)
        t-code (transform {(symbol 'l) nil} nil (walk/macroexpand-all code))
        ] 
;    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let* [^:skip-comparison vec__1639 (com.ohua.lang/destructure three)
                                                one (clojure.core/nth ^:skip-comparison vec__1639 0 nil)
                                                two (clojure.core/nth ^:skip-comparison vec__1639 1 nil)]
                                           (some-function one)))))

(deftest no-destructure
  ;  (l/enable-logging )
  (let [code '(let [one two] (some-function one))
        ;        _ (l/write (walk/macroexpand-all code) :dispatch clojure.pprint/code-dispatch)
        t-code (transform {(symbol 'l) nil} nil (walk/macroexpand-all code))
        ]
        ;(l/enable-logging )
        (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(let* [one two]
                                           (some-function one)))))

(deftest destructure-binding-exec
;  (l/enable-compilation-logging )
  (let [data (into () (map #(into-array Object [(int %) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [item]
                     (let [[prod1 prod2] item]
                       (if (< prod1 3) (com.ohua.lang.tests/add prod1 100) (com.ohua.lang.tests/subtract prod2 3))))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest destructure-function-transformation
;  (l/enable-logging )
  (let [code '(smap-io (fn [[input1 input2]] (some-function input1)) l 20)
;        _ (l/write (walk/macroexpand-all code) :dispatch clojure.pprint/code-dispatch)
        t-code (transform {(symbol 'l) nil} nil (walk/macroexpand-all code))]
;    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(smap-io
                                           (fn*
                                             ([^:skip-comparison p__1639]
                                               (let*[^:skip-comparison vec__1640 (com.ohua.lang/destructure ^:skip-comparison p__1639)
                                                     input1 (clojure.core/nth ^:skip-comparison vec__1640 0 nil)
                                                     input2 (clojure.core/nth ^:skip-comparison vec__1640 1 nil)]
                                                 (some-function input1))))
                                           l
                                           20))))

(deftest destructure-function-exec
;  (l/enable-compilation-logging )
  (let [data (into () (map #(into-array Object [(int %) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [[prod1 prod2]] (if (< prod1 3) (com.ohua.lang.tests/add prod1 100) (com.ohua.lang.tests/subtract prod2 3)))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest destructure-dash-one
  (let [data (into () (map #(into-array Object [(int %) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [item]
                     (let [[_ prod2] item]
                       (if (< prod2 3) (com.ohua.lang.tests/add prod2 100) (com.ohua.lang.tests/subtract prod2 3))))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest destructure-dash-two
  (let [data (into () (map #(into-array Object [(int %) (int %) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [item]
                     (let [[_ _ prod3] item]
                       (if (< prod3 3) (com.ohua.lang.tests/add prod3 100) (com.ohua.lang.tests/subtract prod3 3))))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest destructure-dash-middle
  (let [data (into () (map #(into-array Object [(int %) (int %) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [item]
                     (let [[_ prod2 _] item]
                       (if (< prod2 3) (com.ohua.lang.tests/add prod2 100) (com.ohua.lang.tests/subtract prod2 3))))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest destructure-dash-multiple
  (let [data (into () (map #(into-array Object [(int %) (into-array Object [(int %) (int %)]) (int %)]) (range 10)))
        result (<-ohua
                 (smap
                   (fn [item]
                     (let [[_ prod2 _] item]
                       (let [[_ prod] prod2]
                         (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))))
                   data)
                 :run-with-config (doto (new RuntimeProcessConfiguration)
                                    (.setProperties (doto (new Properties)
                                                      (.put "disable-fusion" true))))
                 )]
    ; validation
    (test/is (= (reduce + result) 324))))