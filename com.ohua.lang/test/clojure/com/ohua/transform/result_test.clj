;;;
;;; Copyright (c) Sebastian Ertel 2015. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.result-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer [ohua <-ohua]]
            [clojure.pprint :refer :all]
            [com.ohua.transform.result :refer [transform-ohua] :rename {transform-ohua transform}]
            [com.ohua.logging :as l]
            [com.ohua.testing :refer :all :as ohua-test]
            [com.ohua.walk :as walk]
            [clojure.zip :as zip]
            [com.ohua.tree-zipper :as t-zip]
            [com.ohua.link :as link]))

(ohua :import [com.ohua.lang.tests])

(defn import-fixture [f]
  (link/ohua-require-fn '[com.ohua.lang.tests :refer :all])
  (f))

(clojure.test/use-fixtures :once import-fixture)

(l/enable-compilation-logging)

(deftest simple-transformation
  ;  (l/enable-logging )
  (let [code '(let [input nil] (accept input))
        options '(:compile)
        t-code (transform (walk/macroexpand-all code) options)]
    ;    (l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(clojure.core/let [^:skip-comparison result (new java.util.concurrent.atomic.AtomicReference nil)]
                                           (com.ohua.lang/ohua (com.ohua.lang/capture (let* [input nil] (accept input)) ^:skip-comparison result) :compile)
                                           (.get ^:skip-comparison result)))))

(deftest integration
  ;  (l/enable-compilation-logging )
  ; NOTE: we have to fully qualify the macro call to make the expansion work!
  (let [ohua-code (walk/macroexpand '(com.ohua.lang/<-ohua (write "some-arg") :compile true :symres false))
;        *print-meta* true
        ]
    (l/printline "code:")
    (l/write ohua-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code ohua-code '(let* [^:skip-comparison result
                                                   (new java.util.concurrent.atomic.AtomicReference nil)]
                                              (com.ohua.lang/ohua
                                                (com.ohua.lang/capture
                                                  (write "some-arg")
                                                  ^:skip-comparison result)
                                                :compile true :symres false)
                                              (.get ^:skip-comparison result)))
;    (l/printline (-> zipped zip/down zip/right zip/right zip/down zip/next zip/node))

    (let [; see above note on macro expansion on why we do that
;          updated (zip/root (zip/replace (-> zipped zip/down zip/right zip/right zip/down) 'com.ohua.compile/ohua))
          ; NOTE this is the reason we have to skip symres here, because macroexpanding does not evaluate the 'let' and the 'result#' symbol will be unresolved
          full-expansion (walk/macroexpand-all ohua-code)]
;      (l/write full-expansion :dispatch clojure.pprint/code-dispatch)
      (ohua-test/compare-deep-code full-expansion 
                                   '(let*
                                      [^:skip-comparison result
                                       (new java.util.concurrent.atomic.AtomicReference nil)]
                                      (let*
                                        [^:skip-comparison G__1002 (new com.ohua.lang.compile.FlowGraphCompiler)]
                                        (. ^:skip-comparison G__1002 createOperator "com.ohua.lang/capture" 100)
                                        (. ^:skip-comparison G__1002 createOperator "com.ohua.lang.tests/write" 101)
                                        (. ^:skip-comparison G__1002 registerDependency 101 -1 100 0)
                                        (. ^:skip-comparison G__1002
                                          setArguments
                                          100
                                          (clojure.core/into-array com.ohua.lang.Tuple
                                                                   (clojure.core/list (new com.ohua.lang.Tuple (clojure.core/int 1) '^:skip-comparison result))))
                                        (. ^:skip-comparison G__1002
                                          setArguments
                                          101
                                          (clojure.core/into-array com.ohua.lang.Tuple
                                                                   (clojure.core/list (new com.ohua.lang.Tuple (clojure.core/int 0) '"class java.lang.String")))) ;small problem with testing framework
                                        (. ^:skip-comparison G__1002 compile true)
                                        ^:skip-comparison G__1002)
                                      (. ^:skip-comparison result get))
                                   ))))

(deftest run-me
  "Makes sure that capture receives all the results."
  (test/is (= 111 (<-ohua (peek (produce))))))
