;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.transform.smap-io-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [clojure.pprint :refer :all]
            [clojure.string :refer [join]]
            [com.ohua.transform.smap :refer [transform]]
            [com.ohua.logging :as l]
            [com.ohua.testing :refer :all :as ohua-test]
            [clojure.walk :as walk]))

(ohua :import [com.ohua.lang.tests])

(deftest simple-transformation
;  (l/enable-logging )
  (let [code '(smap-io (fn [input] (some-function input)) l 20)
        t-code (transform {(symbol 'l) nil} nil (walk/macroexpand-all code))] 
    ;(l/enable-logging )
    (l/write t-code :dispatch clojure.pprint/code-dispatch)
    (ohua-test/compare-deep-code t-code '(clojure.core/let [[input ^:skip-comparison size] (com.ohua.lang/smap-io-fun l)]
                                           (com.ohua.lang/collect
                                             (com.ohua.lang/one-to-n ^:skip-comparison size ^:skip-comparison size)
                                             (some-function input))))
    ))

(deftest simple-execution-no-chunk
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap-io
                   (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest simple-execution-fine
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap-io
                   (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                   data
                   1))]
    ; validation
    (test/is (= (reduce + result) 324))))

(deftest simple-execution-chunked
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap-io
                   (fn [prod-list]
                     (smap
                       (fn [prod]
                         (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                       prod-list))
                   data
                   5))]

    ; validation
    (test/is(= 2 (count result)))
    (test/is (= 5 (count (first result))))
    (test/is (= 5 (count (second result))))
    (test/is (= (reduce + (reduce concat result)) 324))))

(deftest simple-execution-uneven-chunked
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap-io
                   (fn [prod-list]
                     (smap
                       (fn [prod]
                         (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                       prod-list))
                   data
                   3))]

    ; validation
    (test/is (= 4 (count result)))
    (test/is (= (reduce + (reduce concat result)) 324))))

(deftest simple-single-test
  (let [data (into () (map int (range 10)))
        result (<-ohua
                 (smap-io
                   (fn [prod-list]
                     (smap
                       (fn [prod]
                         (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                       prod-list))
                   data
                   1))]

    ; validation
;    (test/is (= 4 (count result)))
    (test/is (= (reduce + (reduce concat result)) 324))))
