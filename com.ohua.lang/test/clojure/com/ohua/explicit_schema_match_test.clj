;;;
;;; Copyright (c) Sebastian Ertel 2013. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.explicit-schema-match-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [com.ohua.logging :as l]
            [com.ohua.testing :as ohua-test]))

(ohua :import [com.ohua.lang.tests])

(deftest simple-match
 "testing the case where output has to be disptached to multiple downstream ops (and skip over one)"
 []
 (test/is
   (= (count (ohua 
               (let [[one two _] (accept "input")]
                 (read one)
                 (write two)) :test-compile))
      ; init stmt + 3 ops + 2 dependencies + 1 arg + compile stmt
   8))
 )

(deftest switch-order-match
 "values are used in reverse order"
 []
 (test/is
   (= (count (ohua 
               (let [[one two _] (accept "input")]
                 (read two)
                 (write one)) :test-compile))
      ; init stmt + 3 ops + 2 dependencies + 1 arg + compile stmt
   8))
 )

(deftest multiple-to-one
 "multiple values arcs are passed to the same operator -> should result into a single arc only"
 []
 (test/is
   (= (count (ohua 
               (let [[one two three] (accept "input")]
                 (read two)
                 (write one three)) :test-compile))
      ; init stmt + 3 ops + 3 dependencies + 1 arg + compile stmt
   9))
 )

(deftest mixed
 "a function that takes multiple partial outputs from one op and an implicit from another"
 []
 (test/is
   (= (count (ohua 
               (let [[one two three] (accept "input")]
                 (write one (read "something") three)) :test-compile))
      ; init stmt + 3 ops + 3 dependencies + 2 arg + compile stmt
   10))
 )

(deftest mixed-twisted
 "like the above but switches again the partial outputs"
 []
 (test/is
   (= (count (ohua 
               (let [[one two three] (accept "input")]
                 (write three (read "something") one)) :test-compile))
      ; init stmt + 3 ops + 3 dependencies + 2 arg + compile stmt
   10))
 )

(deftest direct-access
 "accessing an output argument list via indexing."
 []
 (test/is
   (= (count (ohua 
               (let [packet (accept "input")]
                 (read (nth packet 0))
                 (write (nth packet 1))) :test-compile))
      ; init stmt + 3 ops + 2 dependencies + 1 arg + compile stmt
   8))
 )
