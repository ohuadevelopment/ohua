(ns com.ohua.null-values-test
  (:require [clojure.test :refer :all :as test]
            [com.ohua.lang :refer :all]
            [com.ohua.logging :as l]))

(ohua :import [com.ohua.lang.tests])

(deftest null-value-propagation
  "Making sure we do not drop null values. (In which case this test would deadlock.)"
  ;(l/enable-compilation-logging )
  (test/is
    (=
      0
      (com.ohua.lang/<-ohua
        (let [one (id (int 100))
              c (id nil)]
          (if c
            (add one 100)
            (subtract one 100))))
      )
    )
  )

