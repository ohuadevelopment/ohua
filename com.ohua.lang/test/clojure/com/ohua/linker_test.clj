;
; ohua : linker_test.clj
;
; Copyright (c) Sebastian Ertel, Justus Adam 2016. All Rights Reserved.
;
; This source code is licensed under the terms described in the associated LICENSE.TXT file.
;

(ns com.ohua.linker-test
  (:require [com.ohua.link :refer :all]
            [clojure.test :refer [deftest is use-fixtures]]))


(defn fixture [f]
  (ohua-require-fn '[com.ohua.lang.tests :refer [write] :as tests])
  (f))

(use-fixtures :once fixture)


(deftest import-test
  (is (is-imported? 'com.ohua.lang.tests))
  (is (not (nil? (resolve 'com.ohua.lang.tests/write))))
  (is (not (nil? (resolve 'com.ohua.lang.tests/parse)))))

(deftest refer-test
  (is (is-aliased? 'write))
  (is (not (nil? (resolve 'write))))
  (is (=
        (resolve 'write)
        (resolve 'com.ohua.lang.tests/write))))

(deftest as-test
  (is (is-aliased? 'tests))
  (is (not (nil? (resolve 'tests/accept))))
  (is (=
        (resolve 'tests/accept)
        (resolve 'com.ohua.lang.tests/accept)))
  (is (not (nil? (resolve 'tests/write)))))
