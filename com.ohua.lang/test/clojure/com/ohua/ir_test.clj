;;;
;;; Copyright (c) Sebastian Ertel 2016. All Rights Reserved.
;;;
;;;This source code is licensed under the terms described in the associated LICENSE.TXT file.
;;;
(ns com.ohua.ir-test
  (:require [com.ohua.ir :as ir :refer [->IRFunc mk-func ->IR]]
            [clojure.test :as test :refer [deftest]]
            [com.ohua.logging :as l]
            [clojure.walk :as walk]
            [com.ohua.lang :refer [ohua]]
            [com.ohua.testing :as ohua-test]))


(def source-irs
  [[[(mk-func "f" [] ['y])
     (mk-func "g" [] ['z])
     (mk-func "h" ['y] ['p])
     (mk-func "i" ['z] ['y])
     (mk-func "j" ['y] ['z])]
    [(mk-func "f" [] ['y])
     (mk-func "g" [] ['z])
     (mk-func "h" ['y] ['p])
     (mk-func "i" ['z] ['y0])
     (mk-func "j" ['y0] ['z0])]]
   [[(mk-func "f" [] ['y 'p 'q 'r])
     (mk-func "m" [] ['gg 'p0 'r1])
     (mk-func "g" ['p 'r] ['z])
     (mk-func "h" ['y 'r] ['p])
     (mk-func "i" ['z 'q] ['y 'r])
     (mk-func "j" ['y 'y 'r] ['q 'z 'r])
     (mk-func "k" ['gg 'p0 'q 'r] ['gg 'p 'q])]
    [(mk-func "f" [] ['y 'p 'q 'r])
     (mk-func "m" [] ['gg 'p0 'r1])
     (mk-func "g" ['p 'r] ['z])
     (mk-func "h" ['y 'r] ['p1])
     (mk-func "i" ['z 'q] ['y0 'r0])
     (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
     (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]]])


(deftest ssa-test
  (doall
    (for [[source expected] source-irs]
      (test/is
        (=
          (ir/ssa source)
          expected)))))


(deftest step-up-test
  (test/is
    (=
      (ir/step-up
        [(mk-func "f" [] ['y])
         (mk-func "g" [] ['z])
         (mk-func "h" ['y] ['p])
         (mk-func "i" ['z] ['y0])
         (mk-func "j" ['y0] ['z0])]
        (mk-func "i" ['z] ['y]))
      #{(mk-func "g" [] ['z])}))
  (test/is
    (=
      (ir/step-up
        [(mk-func "f" [] ['y])
         (mk-func "g" [] ['z])
         (mk-func "h" ['y] ['p])
         (mk-func "i" ['z] ['y0])
         (mk-func "j" ['y0] ['z0])]
        (mk-func "f" [] ['y]))
      #{}))
  (test/is
    (=
      (ir/step-up
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]
        (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1]))

      #{(mk-func "m" [] ['gg 'p0 'r1])
        (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])})))


(deftest step-down-test
  (test/is
    (=
      (ir/step-down
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]
        (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1]))
      #{}))
  (test/is
    (=
      (ir/step-down
        [(mk-func "f" [] ['y])
         (mk-func "g" [] ['z])
         (mk-func "h" ['y] ['p])
         (mk-func "i" ['z] ['y0])
         (mk-func "j" ['y0] ['z0])]
        (mk-func "f" [] ['y]))
      #{(mk-func "h" ['y] ['p])}))
  (test/is
    (=
      (ir/step-down
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]
        (mk-func "i" ['z 'q] ['y0 'r0]))

      #{(mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])}))
  (test/is
    (=
      (ir/step-down
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]
        (mk-func "f" [] ['y 'p 'q 'r]))
      #{(mk-func "g" ['p 'r] ['z])
        (mk-func "h" ['y 'r] ['p1])
        (mk-func "i" ['z 'q] ['y0 'r0])})))


(deftest validation-test
  (test/is
    (ir/validate-graph
      [(mk-func "f" [] ['y 'p 'q 'r])
       (mk-func "m" [] ['gg 'p0 'r1])
       (mk-func "g" ['p 'r] ['z])
       (mk-func "h" ['y 'r] ['p1])
       (mk-func "i" ['z 'q] ['y0 'r0])
       (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
       (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]))
  (test/is
    (ir/validate-graph
      [(mk-func "f" [] ['y])
       (mk-func "g" [] ['z])
       (mk-func "h" ['y] ['p])
       (mk-func "i" ['z] ['y0])
       (mk-func "j" ['y0] ['z0])]))
  (test/is
    (not
      (ir/validate-graph
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'qp] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])])))
  (test/is
    (not
      (ir/validate-graph
        [(mk-func "f" ['p] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'qp] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])]))))


(deftest find-redundant-functions-test
  (test/is
    (=
      (ir/find-redundant-functions
        #{'gg0 'p2 'q1 'p1}
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])])

      [#{(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['z 'q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])}
       #{}]))
  (test/is
    (=

      (ir/find-redundant-functions
        #{'gg0 'p2 'q1}
        [(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])
         (mk-func "i" ['q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])])
      [#{(mk-func "f" [] ['y 'p 'q 'r])
         (mk-func "m" [] ['gg 'p0 'r1])
         (mk-func "i" ['q] ['y0 'r0])
         (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
         (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])}
       #{(mk-func "g" ['p 'r] ['z])
         (mk-func "h" ['y 'r] ['p1])}])
    (test/is
      (=
        (ir/find-redundant-functions
          #{'gg0 'p2 'q1}
          [(mk-func "f" [] ['y 'p 'q 'r])
           (mk-func "m" [] ['gg 'p0 'r1])
           (mk-func "g" ['p 'r] ['z])
           (mk-func "h" ['y 'z 'r] ['p1])
           (mk-func "i" ['q] ['y0 'r0])
           (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
           (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])])

        [#{(mk-func "f" [] ['y 'p 'q 'r])
           (mk-func "m" [] ['gg 'p0 'r1])
           (mk-func "i" ['q] ['y0 'r0])
           (mk-func "j" ['y0 'y0 'r0] ['q0 'z0 'r2])
           (mk-func "k" ['gg 'p0 'q0 'r2] ['gg0 'p2 'q1])}
         #{(mk-func "g" ['p 'r] ['z])
           (mk-func "h" ['y 'z 'r] ['p1])}]))))


(deftest prod-cons-test
  (let [f1 (mk-func "f" [] ['y])
        f2 (mk-func "g" [] ['z])
        f3 (mk-func "h" ['y] ['p])
        f4 (mk-func "i" ['z 'y] ['y0])
        f5 (mk-func "j" ['y0] ['z0])
        my-ir [f1 f2 f3 f4 f5]]
    (test/is
      (=
        (ir/mk-producers-consumers my-ir)
        [{'y  f1
          'z  f2
          'p  f3
          'y0 f4
          'z0 f5}
         {'y  #{f3 f4}
          'z  #{f4}
          'y0 #{f5}}]))))


(use 'clojure.pprint)

(deftest cursor-test
  (let [f1 (mk-func "f" [] ['y])
        f2 (mk-func "g" [] ['z])
        f3 (mk-func "h" ['y] ['p])
        f4 (mk-func "i" ['z 'y] ['y0])
        f5 (mk-func "j" ['y0] ['z0])
        my-ir [f1 f2 f3 f4 f5]
        cursor (ir/init-cursor my-ir)
        step1 (ir/cursor-next cursor)
        step2 (ir/cursor-next step1)
        step3 (ir/cursor-next step2)]
    (test/is
      (=
        cursor
        (ir/->IRCursor
          [(ir/->IRGraphPosition my-ir #{f1 f2} #{} #{})]
          0)))
    (test/is
      (=
        step1
        (ir/->IRCursor
          [(ir/->IRGraphPosition my-ir #{f1 f2} #{} #{})
           (ir/->IRGraphPosition my-ir #{f3 f4} #{f1 f2} #{'y 'z})]
          1)))
    (test/is
      (=
        step2
        (ir/->IRCursor
          [(ir/->IRGraphPosition my-ir #{f1 f2} #{} #{})
           (ir/->IRGraphPosition my-ir #{f3 f4} #{f1 f2} #{'y 'z})
           (ir/->IRGraphPosition my-ir #{f5} #{f1 f2 f3 f4} #{'y 'z 'p 'y0})]
          2)))
    (test/is
      (=
        (ir/current-position step1)
        (ir/current-position (ir/cursor-previous step2))))
    (test/is
      (=
        (ir/current-position cursor)
        (ir/current-position (ir/cursor-previous step1))))
    (test/is
      (= #{} (-> step3 ir/current-position :curr-nodes)))
    (test/is (ir/cursor-next step3) step3)))


(deftest basic-conversion-test
  (let [raw-test-ir (list (mk-func "f" [] ['y])
                          (mk-func "g" [] ['z])
                          (mk-func "h" [(with-meta 'y {:in-idx 0})] ['p])
                          (mk-func "i" [(with-meta 'z {:in-idx 0})] ['a])
                          (mk-func "j" [(with-meta 'a {:in-idx 0})] []))
        test-ir (map-indexed #(assoc %2 :id %1) raw-test-ir)
        backend-format (ir/op-list-from-ir (->IR test-ir {}))]
    ;(println backend-format)
    ;(println "deps:")
    ;(println (second backend-format))
    ;(println "functions:")
    ;(print-table (first backend-format))
    ;(println "dependencies:")
    ;(print-table (second backend-format))
    (test/is (= (first backend-format)
                '({:op-id 0, :type "f"} {:op-id 1, :type "g"} {:op-id 2, :type "h"} {:op-id 3, :type "i"} {:op-id 4, :type "j"})))
    (test/is (= (second backend-format)
                '({:source {:op-id 0, :out-idx 0}, :target {:op-id 2, :in-idx 0}}
                   {:source {:op-id 1, :out-idx 0}, :target {:op-id 3, :in-idx 0}}
                   {:source {:op-id 3, :out-idx 0}, :target {:op-id 4, :in-idx 0}}))))
  )

(deftest op-id-generation-test
  (let [test-ir (list (mk-func 1 "f" [] ['y])
                      (mk-func "g" [] ['z])
                      (mk-func 3 "h" [(with-meta 'y {:in-idx 0})] ['p])
                      (mk-func "i" [(with-meta 'z {:in-idx 0})] ['a])
                      (mk-func 5 "j" [(with-meta 'a {:in-idx 0})] []))
        backend-format (ir/op-list-from-ir (->IR test-ir {}))]
    ;(println backend-format)
    ;(println "functions:")
    ;(print-table (first backend-format))
    ;(println "dependencies:")
    ;(print-table (second backend-format))
    (test/is (= (first backend-format)
                '({:op-id 1, :type "f"} {:op-id 6, :type "g"} {:op-id 3, :type "h"} {:op-id 7, :type "i"} {:op-id 5, :type "j"})))
    (test/is (= (second backend-format)
                '({:source {:op-id 1, :out-idx 0}, :target {:op-id 3, :in-idx 0}}
                   {:source {:op-id 6, :out-idx 0}, :target {:op-id 7, :in-idx 0}}
                   {:source {:op-id 7, :out-idx 0}, :target {:op-id 5, :in-idx 0}}))))
  )


(ohua :import [com.ohua.lang.tests])

(deftest ir-integration-test
  ;(l/enable-compilation-logging )
  (let [without-transformation '(com.ohua.lang/ohua
                                  (com.ohua.lang/smap
                                    (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                                    data)
                                  :symres false)


        with-transformation `(com.ohua.lang/ohua
                               (com.ohua.lang/smap
                                 (fn [prod] (if (< prod 3) (com.ohua.lang.tests/add prod 100) (com.ohua.lang.tests/subtract prod 3)))
                                 data)
                               ; implements a very simple transformation
                               :compile-with-config {:df-transformations [~(fn [{ir :graph :as df-ir}]
                                                                            ;(println "passed ir:" ir)
                                                                            (assoc df-ir
                                                                              :graph
                                                                              (into [] (map #(do
                                                                                              ;(println "function" % (type (:name %)))
                                                                                              (if (= (:name %) 'com.ohua.lang.tests/add)
                                                                                                (assoc % :name 'com.ohua.lang.tests/mult)
                                                                                                %))
                                                                                            ir))
                                                                              ))]}
                               :symres false)
        compiled-without-transformation (walk/macroexpand-all without-transformation)
        compiled-with-transformation (walk/macroexpand-all with-transformation)]
    ;(l/enable-logging)
    (l/printline "without transformation:")
    (l/write compiled-without-transformation :dispatch clojure.pprint/code-dispatch)
    (l/printline)
    (l/printline "with transformation:")
    (l/write compiled-with-transformation :dispatch clojure.pprint/code-dispatch)

    (ohua-test/compare-deep-code
      (map #(if (= (nth % 3) "com.ohua.lang.tests/add") (apply list (assoc (into [] %) 3 "com.ohua.lang.tests/mult")) %)
           (sort-by last (filter #(and (seq? %) (= (nth % 2) 'createOperator)) (nth compiled-without-transformation 2))))
      (map
        (fn [op-def] (apply list (assoc (into [] op-def) 1 (with-meta (second op-def) {:skip-comparison true}))))
        (sort-by last (filter #(and (seq? %) (= (nth % 2) 'createOperator)) (nth compiled-with-transformation 2)))))

    (ohua-test/compare-deep-code
      (sort-by (juxt #(nth % 3) #(nth % 4) #(nth % 5) #(nth % 6)) (filter #(and (seq? %) (= (nth % 2) 'registerDependency)) (nth compiled-without-transformation 2)))
      (map
        (fn [op-def] (apply list (assoc (into [] op-def) 1 (with-meta (second op-def) {:skip-comparison true}))))
        (sort-by (juxt #(nth % 3) #(nth % 4) #(nth % 5) #(nth % 6)) (filter #(and (seq? %) (= (nth % 2) 'registerDependency)) (nth compiled-with-transformation 2)))))
    ))
