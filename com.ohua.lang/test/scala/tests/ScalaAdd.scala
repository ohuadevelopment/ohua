package com.ohua.lang.tests


import com.ohua.lang.defsfn


class ScalaAdd {
    var c = 0

    @defsfn
    def scalaAdd(x :Int, y:Int) = {
        c = c + 1
        println(s"Add was called $c times")
        x + y
    }

}