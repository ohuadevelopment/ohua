package com.ohua.lang.tests

import scala._
import com.ohua.lang.defsfn
import java.util.Collection

class Length {
    @defsfn
    def length(coll:Collection[Any]) = coll.size()
}