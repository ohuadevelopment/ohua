# Ohua

Ohua is an implementation of the stateful functional programming model along with a dataflow execution engine as a runtime system. Ohua mixes object-oriented (imperative) and functional programming to provide implicit parallelism.

## Stateful functions

Define functionality as a Java class that may or may not hold state.

```
#!java
class StatefulFunction {
  private Map<String, Object> _state = new HashMap<>();
  @Function Object[] functionY(String arg1, List<?> arg2){
    // implementation
  }
}
```

## Functional programming

#### Implement your algorithm in Clojure using your stateful functions. 

```
#!clojure
(ohua (let [r (function-y (function-x ) (new java.util ArrayList))] (function-z r))
```
Ohua converts the Clojure algorithm into a dataflow graph and executes it in parallel.

#### Embed your algorithm into a Clojure program.

```
#!clojure
(def func[l] 
  ; use a list from the surrounding clojure program
  (ohua (let [r (function-y (function-x ) l)] (function-z r)))
```

## Safety.

Ohua uses static analysis to avoid racy executions. Ohua raises a compile-time exception for situations that result in shared memory. The developer can resolve these errors by refining the code or enhancing with information that helps to Ohua compiler to better understand the code purpose.

#### Release state explicitly.
```
#!java
class StatefulFunction {
  private Object _state;
  @Function Object[] function(boolean release){
    if(release){
      Object s = _state;
      _state = null; // explicit state release
      return s;
    }else{
      return new Object[0];
    }
  }
}
```

#### Enhance type information.
```
#!java
class StatefulFunction {
  @Function Object[] function(@ReadOnly List<?> arg2){
    // implementation
  }
}
```

## Getting started
Just register the following dependency in your leiningen project:
```
#!clojure
:dependencies [[ohua/ohua "0.3"]]
```

The provided Leiningen template has all the dependencies needed:
[leiningen_project_template.clj](https://bitbucket.org/sertel/ohua/raw/master/leiningen_project_template.clj)

## Projects
For examples you can either have a look at the test suite or check out one of the following projects:

* [Ohua-based web server](https://bitbucket.org/sertel/ohua-server)
* [Ohua-based prototype for an online trading platform](https://bitbucket.org/sertel/mini-trading)

## For contributors

#### Setup
Ohua uses ZeroMQ for IPC (com.ohua.distributed). Therefore, it is necessary to install JZMQ along with ZMQ of course!

If running on a Linux system you will need to add the following to your .bashrc:
export LD_LIBRARY_PATH=<path/to/zmq/lib/folder>

#### Regression
Use Leiningen:

```
lein junit
lein run -m com.ohua.regression/run-regression
```

Please also verify that your jar works properly by doing:

```
lein jar
cd com.ohua.lang
lein run -m com.ohua.regression/run-regression
```
This will run the test cases of the language folder against the jar and verifies that the new jar is packaged properly and resource loading works.

If you want to run individual leiningen test then do
```
lein test :only test.ns/fn
```
    
#### Build
Use Leiningen:

```
lein jar
```